
// add timestamps in front of log messages
require('console-stamp')(console, 'yyyymmddhhmmss.l');

let express = require('express');
let bodyParser = require('body-parser');
let fileUpLoad = require('express-fileupload');
let http = require('http');
let path = require('path');
let app = express();
let utils = require('./src/utils');
let config = utils.getConfig().conf ;
//---------------------------------------------------------------------------------------
let mssql = require('mssql');
require('mssql').connect(config.mssqlconnection,(err)=> {
        console.log("Starting MS SqlServer %s for %s - %s",err||"OK",config.mssqlconnection);
});
 
app.set('port', process.env.PORT || 4200);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

//------------------------------------------------------
let hbm = require('./src/routes/HeartbeatMonitor');
app.get('/',hbm.dashboard);   
app.get('/HBM/Chart/:rhio',hbm.qechart);
app.get('/HBM/Chart/:rhio/:date',hbm.qechart);
app.get('/HBM/Graph',hbm.chart);
app.get('/HBM/Charts',hbm.charts);

app.get('/HBM/Graph1',hbm.chart1);
app.get('/HBM/list',hbm.qechartlist);
app.get('/HBM/list/:rhio',hbm.qechartlist);
app.get('/HBM/list/:rhio/:date',hbm.qechartlist);
app.get('/HBM/Report',hbm.HBMlist); 
app.get('/HBM/XCAReport',hbm.list); 

//--------------------------------------------------------
http.createServer(app).listen(app.get('port'), function(){
     console.log('Patient Portal HBM application server listening on port ' + app.get('port'));
});



