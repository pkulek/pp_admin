
    const http = require('http')  
    let config = require('./src/utils').getConfig().config;
    let port = config.port;
    let hostname = config.hostname;
    //----------------------------------------------------------------------------
    //----------------------------------------------------------------------------
    // mysql for doc scheduler
    //console.log(config)
    var mysql = require('mysql');
    var conn = mysql.createConnection(config.mysqlconfig,'single');
    conn.connect((err)=>{
        console.log("Starting mySQL SqlServer %s for %s ",err||"OK",JSON.stringify(config.mysqlconfig));            
    });
    // global microsoft mssql in sprl and heartbeat use only one instance
    let mssql = require('mssql');
    mssql.connect(config.mssqlconnection,(err)=> {
            console.log("Starting MS SqlServer %s for %s ",err||"OK",config.mssqlconnection);
    });

    let server = http.createServer().listen(port, hostname, () => {
        console.log(`Server running at http://${hostname}:${port}\nPID=%s`,process.pid);
    });
    
    // give time for mysql to connect
    setTimeout(function() {
        let ppdoc = require('./src/ppDocRefresh');
        console.log("Doc Refresh Proccess Started");
        ppdoc.DocProcess();
        var intervalId = setInterval(() => { 
            ppdoc.DocProcess();
        },60*1000 );               
    }, 1200 );
    setTimeout(function() {
        let ppReg = require('./src/ppPatientReg');
        console.log("Patient Reg Proccess Started");
        ppReg.PatientRegProcess();
        var intervalId = setInterval(() => { 
            ppReg.PatientRegProcess();
        },60*1000 );               
    }, 1300 );

    // EXIT cleanup if needed
    process.stdin.resume();//so the program will not close instantly
    function exitHandler(options, err) {
        if (options.cleanup) {
            console.log('clean up');
            
        }
        if (err) console.log(err.stack);
        if (options.exit) process.exit();
    }
    //do something when app is closing
    process.on('beforeExit', exitHandler.bind(null, {exit:true}));
    process.on('exit', exitHandler.bind(null,{cleanup:true}));

    //catches ctrl+c event
    process.on('SIGINT', exitHandler.bind(null, {exit:true}));
    //process.on('SIGKILL', exitHandler.bind(null, {exit:true}));

    //catches uncaught exceptions
    process.on('uncaughtException', exitHandler.bind(null, {exit:true}));

