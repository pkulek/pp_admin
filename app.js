
// add timestamps in front of log messages
require('console-stamp')(console, 'yyyymmddHHmmss.l');

let DEVENV = 'STAGE' ;
let conf = require('./src/utils').getConfig().conf;
//------------------------------------------------------------------------------------
let dict = require('./src/dic/dict');
dict.get("JSCRIPT","./src/utils","REQUIRE","",(err,data) =>{
    let utils = require('./src/utils');
    if (err || utils.isEmpty(data)) {
        //utils = require('./src/utils');
    } else {
        //utils = require('eval')(data,true);
    }
    //let test = 'undefined' ;
    //console.log("GET UTILS %s",utils.isEmpty(test)) 
    console.log("\nUSING DevEnv= %s \nversion %s\n",conf['devEnv'], require('./src/utils').getConfig().config['version'])
    
    let express = require('express');
    let bodyParser = require('body-parser');
    let fileUpLoad = require('express-fileupload');
    let http = require('http');
    let path = require('path');

    let app = express();
    //----------------------------------------------------------------------------
    // mysql for ppreg
    let connection  = require('express-myconnection'); 
    let mysql = require('mysql');
    //let conn = mysql.createConnection(conf.mysqlconfig,'single');
    //conn.connect((err)=>{
    //    console.log("Starting mySQL SqlServer %s for %s ",err||"OK",JSON.stringify(conf.mysqlconfig));            
    //});
    //------------
    //app.use(
    //    connection(mysql,conf.mysqlconfig,'single')
    //);
    //---------------------------------------------------------------------------------------
    // global microsoft mssql in sprl and heartbeat use only one instance
    //let mssql = require('mssql');
    //mssql.connect(conf.mssqlconnection,(err)=> {
    //        console.log("Starting MS SqlServer %s for %s ",err||"OK",conf.mssqlconnection);
    //});
    //---------------------------------------------------------------------------------------
    app.use( bodyParser.json() );       // to support JSON-encoded bodies
    app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
        extended: true  
    })); 
    // test create functions for ejs
    app.locals.getMsg = ["Login","Logout","three"];
    app.locals.MSG = {"WEB_001":"Login","WEB_002":"Logout"};
    // all environments
    app.set('port', process.env.PORT || conf.port);
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');
    console.log("app.get('views')=",app.get('views'))
    app.use(fileUpLoad());
    app.use(express.static(path.join(__dirname, 'public')));
    //--------------------------------
    let routes = require('./src/routes');
    app.get('/', routes.index); 
    //---------------------------------------------------
    app.get('/app/config', utils.config);    
    app.get('/translate',utils.MStranslate)
    app.get('/translate/:text/:from/:to',utils.MStranslate)
    //---------------------------------------------------
    app.get('/HLP/:id/:parent/:ext', dict.getHTM);
    app.get('/HTM/:id/:parent/:ext', dict.getHTM);
    app.get('/JPEG/:id/:parent/:ext', dict.getJPG);
    app.get('/PDF/:id/:parent/:ext', dict.getPDF);
    app.get('/Msg/:id/:lang/:msgdef',dict.GetWebMsg );
    app.get('/dict/exportsqlite', dict.mysql2sqlite);
    app.get('/dict/list', dict.list);
    app.get('/dict/backup', dict.backup);
    app.get('/dict/export', dict.dicexport);
    app.get('/dict/import', dict.dicimport);
    app.get('/dict/get',dict.dict);
    app.get('/dict/get/:type',dict.dict);
    app.get('/dict/get/:type/:id',dict.dict);
    app.get('/dict/get/:type/:id/:parent',dict.dict);
    app.get('/dict/ping',dict.isDict);
    app.get('/dict/delete',dict.dicDelete)
    app.get('/dict/langlist',dict.getLangList)
    app.get('/dict/langlist',dict.getLangList)
    
    //----------------------------------------------------------

    app.get('/uithemes',utils.uithemes) ;
    
    let issues = require('./src/routes/issuesmgm');
    app.get("/issues/html",issues.index)
    app.get("/issues/list",issues.list)
    app.get("/issues/tasks",issues.tasks)
    app.post("/issues/update",issues.update)

    app.get("/tasks/list",issues.list)
    app.post("/tasks/update",issues.taskupdate);

    app.get("/issues/select",issues.select)
    app.get("/tasks/select",issues.select)
    
    let ppdoc = require('./src/ppDocRefresh');
    app.get('/ppdoc/test',ppdoc.test);
    app.get('/ppdoc/test1',ppdoc.test1);
    app.get('/ppdoc/list',ppdoc.list)
    app.get('/ppdoc/html',ppdoc.html)
    app.get('/ppdoc/tab',ppdoc.tab)
    app.post('/ppdoc/save_edit',ppdoc.save_edit)
    app.post('/ppdoc/insert',ppdoc.insert)

    /*
    let ppPat = require('./src/ppPatientReg');
    app.get('/ppreg/test3',ppPat.test3 );
    app.get('/ppreg/test3/:mpiid',ppPat.test3 );
    app.get('/ppreg/test1',ppPat.test1 );
    app.get('/ppreg/test',ppPat.testUsers );
    app.get('/ppreg/test/:type',ppPat.testUsers );
    app.get('/ppreg/proc',ppPat.testproc );
    */
    //-----------------------------------------------------
    /*
    let hbm = require('./src/routes/HeartbeatMonitor');
    app.get('/HBM/Charts',hbm.charts);
    app.get('/HBM/Graph',hbm.chart);
    app.get('/HBM/Graph1',hbm.chart1);
    app.get('/HBM/Chart/:rhio',hbm.qechart);
    app.get('/HBM/Chart/:rhio/:date',hbm.qechart);
    app.get('/HBM/list',hbm.qechartlist);
    app.get('/HBM/list/:rhio',hbm.qechartlist);
    app.get('/HBM/list/:rhio/:date',hbm.qechartlist);
    app.get('/HBM/dashboard',hbm.HBMhtml); 
    app.get('/HBM/Report',hbm.HBMlist); 
    app.get('/HBM/XCAReport',hbm.list); 
    app.get('/HBM/Report/html',hbm.htmllist);
    app.get('/SPRL/mpi/html/:mpiid',hbm.htmlsprllist); 
    */  


    let sprl = require('./src/routes/sprl');
    app.get('/SPRL/testmpi',sprl.testmpi); 
    app.get('/SPRL/testmpi/:mpiid',sprl.testmpi); 
    app.get('/SPRL/soap',sprl.soapedit); 
    app.get('/SPRL/listsoap',sprl.listsoap); 
    app.get('/SPRL/mpi/html/:mpiid',sprl.htmlsprllist); 

    //----------------------------------------------------------------------
    let sprlconf = require('./src/SPRL_Config.js') ;
    app.get('/sprlconf/testxml',sprlconf.testxml) ;
    app.get('/sprlconf/config/json',sprlconf.qexml) ;
    app.get('/sprlconf/config/html',sprlconf.configTHTML) ;
    app.get('/sprlconf/files',sprlconf.filelist) ;
    app.get('/sprlconf/admin',sprlconf.admin) ;
    app.get('/sprlconf/cert',sprlconf.cert) ;
    app.get('/sprlconf/certs/json',sprlconf.filelist) ;
    app.get('/sprlconf/certs/html',sprlconf.certsHtml) ;
    app.get('/sprlconf/readcert',sprlconf.readcert) ;
    app.get("/sprlconf/config/download",sprlconf.download);
    app.get("/sprlconf/certs/download",sprlconf.downloadcert);
    app.get("/sprlconf/certs/download/:filename",sprlconf.downloadcert);
    //---------------------------------------------------------------------------------
    let adminusers = require('./src/routes/adminusers');
    app.get('/adminusers/list',adminusers.list )
    app.get('/adminusershtml/:tokenid',adminusers.htmllist )
    app.post('/adminuser/update',adminusers.update )
    app.get('/adminuser/pwdreset',adminusers.pwdreset )
    app.get('/adminuser/disable',adminusers.disable )
    app.get('/adminusersave',adminusers.save )
    app.get('/adminuser/:userid',adminusers.user )
    app.get('/adminuser/idlist',adminusers.useridlist);
    //----------------------------------------------------------
    let users = require('./src/routes/users'); 
    app.get('/lookup/clients',users.ppclients)
    app.get('/users/clients',users.ppclients)
    app.get('/users/clients/html',users.htmlclients)
    app.post('/users/verifyuser',users.VerifyUser);
    app.post('/users/save',users.save);
    app.get('/user/:user_id',users.list); 
    app.get('/users',users.list); 
    app.get('/users/json',users.list); 
    app.get('/users/html/:tokenid', users.htmllist);
    app.get('/users/edit',users.save_edit);
    app.get('/users/delete',users.del_user);
    app.get('/users/gethash',users.gethash);
    app.post('/users/register/:id',users.register_user);
    app.post('/users/save_edit',users.save_edit);
    app.post('/users/sendemail',users.sendemail);
    app.get('/userlist/:id',users.users); 
    //-----------------------------------------------------------
    let login = require('./src/routes/login');
    app.get('/captcha',login.captcha);
    app.get('/captcha.png',login.captchapng);
    app.post('/login',login.login);
    app.post('/login/register/user',login.register);
    app.post('/login',login.loginpost);
    app.get('/login',login.login);
    app.get('/login/auth/:tokenid/:roles',login.authenticate);
    app.get('/login/verifyuser',login.loginVerifyUser);
    app.post('/login/verifyuser',login.loginVerifyUser);
    app.get('/login/verifytoken',login.VerifyToken);
    app.get('/login/logout',login.logout);
    app.post('/login/logout',login.logout);
    app.get('/login/resetpwd',login.resetpwd);
    app.get('/login/html',login.htmllogin);
    app.get('/login/secquestions',login.securityQuestions);
    app.get('/login/secanswers',login.securityAnswers);
    app.get('/login/forgotPasswordEmail',login.forgotPasswordEmail);
    app.get('/login/password/change',login.changepwd);
    //-----------------------------------------------------------
    let PPusers = require('./src/routes/PPusers'); 
    app.get('/PPusers',PPusers.list); 
    app.get('/PPusers/html',PPusers.htmllist); 
    //-----------------------------------------------------------
    let documents = require('./src/routes/documents');
    app.get('/documents/:id',documents.list); 
    app.get('/documents/html',documents.htmllist); 
    app.get('/HSdocumentstest',documents.test); 
    app.get('/HSdocuments',documents.HSlist); 
    app.get('/HSdocuments/:tokenid/:id',documents.HSlist); 
    app.get('/HSdocuments/tokenid:/:id/:type',documents.HSlist);
    app.get('/HSdocumentshtml/:tokenid',documents.HShtmllist); 
    app.get('/HSdoc/:tokenid/:id',documents.HSDoc); 
    app.get('/HSdoc/:tokenid/:id/:type',documents.HSDoc); 
    app.get('/ODdoc/:tokenid/:id',documents.OnDemandDoc); 
    app.get('/ODdoc/:tokenid/:id/:type',documents.OnDemandDoc); 
    app.get('/RegisterSPRLDocument/:id',documents.RegisterSPRLDoc);
    app.get('/SPRLDocument/:tokenid/:id',documents.SPRLDoc);
    //-----------------------------------------------------------
    /*
    let logging = require('./src/routes/logging');
    app.get('/logging',logging.list); 
    app.get('/logginghtml',logging.htmllist); 
    app.get('/logginghtml/:tokenid',logging.html_list); 
    app.post('/log',logging.log)
    */    
    /*
    let reports = require('./src/routes/reports');
    app.get('/reports',reports.list); 
    app.get('/reports/html', reports.htmllist);
    */

    http.createServer(app).listen(app.get('port'), function(){
        console.log('\nPatient Portal Admin server listening on port %s \nEnv = %s ', app.get('port'), conf['devEnv']);
    });

    // EXIT cleanup if needed
    process.stdin.resume();//so the program will not close instantly
    function exitHandler(options, err) {
        if (options.cleanup) {
            console.log('clean up');
            //conn.close()
        }
        if (err) console.log(err.stack);
        if (options.exit) process.exit();
    }
    //do something when app is closing
    process.on('beforeExit', exitHandler.bind(null, {exit:true}));
    process.on('exit', exitHandler.bind(null,{cleanup:true}));

    //catches ctrl+c event
    process.on('SIGINT', exitHandler.bind(null, {exit:true}));
    //process.on('SIGKILL', exitHandler.bind(null, {exit:true}));

    //catches uncaught exceptions
    process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
    
});


