
// add timestamps in front of log messages
require('console-stamp')(console, 'yyyymmddhhmmss.l');
let DEVENV = 'DEV' ;
//----------------------------------------------------------------------------------
let config =  eval('require("./dictconfig.json")') ;
//------------------------------------------------------------------------------------

let express = require('express');
let bodyParser = require('body-parser');
let fileUpLoad = require('express-fileupload');
let http = require('http');
let path = require('path');
let captchapng = require('captchapng');

let app = express();
let dict = require('./src/dic/dict');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(fileUpLoad());
app.use(express.static(path.join(__dirname, 'public')));
http.createServer(app).listen((config.port), function(){
    console.log('\nPatient Portal Admin server listening on port %s \nEnv = %s ', config.port, config.env);
});
// EXIT cleanup if needed
process.stdin.resume();//so the program will not close instantly
function exitHandler(options, err) {
    if (options.cleanup) {
        console.log('clean up');
        //dict.conn.close()
    }
    if (err) console.log(err.stack);
    if (options.exit) process.exit();
}
//do something when app is closing
process.on('beforeExit', exitHandler.bind(null, {exit:true}));
process.on('exit', exitHandler.bind(null,{cleanup:true}));
//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));
//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
//------------------------------------------------------------------------------------
let startup = ()=> {    
    dict.setup('sqlite');
    dict.get("JSCRIPT","./src/utils","REQUIRE","",(err,data) =>{
        console.log("REQUIRE %j",err||data);
        if (err) {
            //var utils = require('./src/utils');
        } else {
            //let _eval = require(eval)
            //let utils = _eval(data,true) ;
           // var utils = require('eval')(data,true);
        }

        let utils = require('./src/utils');
        // dictionary
        app.get('/dict/list', dict.list);
        app.get('/dict/backup', dict.backup);
        app.get('/dict/export', dict.dicexport);
        app.get('/dict/import', dict.dicimport);
        app.get('/dict/get',dict.dict);
        app.get('/dict/getLANG',dict.getLANG);
        app.post('/dict/saveLANG',dict.saveLANG);
        app.get('/dict/get/:type',dict.dict);
        app.get('/dict/get/:type/:id',dict.dict);
        app.get('/dict/get/:type/:id/:parent',dict.dict);
        app.get('/dict/ping',dict.isDict);
        app.get('/dict/delete',dict.dicDelete);
        app.get('/Msg/:id/:lang/:msgdef',dict.GetWebMsg );
        app.get('/translate',utils.MStranslate)
        app.get('/translate/:text/:from/:to',utils.MStranslate)
        //-------------------------------------------------------------------
        let dicmgmt = require('./src/dic/dict_manager');
        app.get('/dict/manager', dicmgmt.manager);
        
        //console.log("GET UTILS isEmpty()->%s",utils.isEmpty(""))
        //console.log("GET UTILS isEmpty()->%s",utils.isEmpty("fff"))
        /*
        setTimeout(function () {
            dict.savefile("JSCRIPT","USERS","ROUTES",'',"src/routes/users.js") ;
            dict.savefile("JSCRIPT","SPRL","ROUTES",'',"src/routes/sprl.js") ;
            dict.savefile("JSCRIPT","SKINS","ROUTES",'',"src/routes/skins.js") ;
            dict.savefile("JSCRIPT","REPORTS","ROUTES",'',"src/routes/reports.js") ;
            dict.savefile("JSCRIPT","PPUSERS","ROUTES",'',"src/routes/ppusers.js") ;
            dict.savefile("JSCRIPT","MAIL","ROUTES",'',"src/routes/main.js") ;
            dict.savefile("JSCRIPT","LOGIN","ROUTES",'',"src/routes/login.js") ;
            dict.savefile("JSCRIPT","LOGGING","ROUTES",'',"src/routes/logging.js") ;
            dict.savefile("JSCRIPT","INDEX","ROUTES",'',"src/routes/index.js") ;
            dict.savefile("JSCRIPT","HS","ROUTES",'',"src/routes/hs.js") ;
            dict.savefile("JSCRIPT","HEARTBEATMONITOR","ROUTES",'',"src/routes/heartbeatmonitor.js") ;
            dict.savefile("JSCRIPT","GROUPS","ROUTES",'',"src/routes/groups.js") ;
            dict.savefile("JSCRIPT","DOCUMENTS","ROUTES",'',"src/routes/documents.js") ;
            dict.savefile("JSCRIPT","ADMIN","ROUTES",'',"src/routes/admin.js") ;      
            dict.savefile("JSCRIPT","ADMINUSERS","ROUTES",'',"src/routes/adminusers.js") ;
        }, 300);
        */
        /*
        setTimeout(function () {
            dict.savefile("JSCRIPT","DICT","DICT",'',"src/dic/dict.js") ;
            dict.savefile("JSCRIPT","DIC_SQLITE","DICT",'',"src/dic/dic_sqlite.js") ;
            dict.savefile("JSCRIPT","DIC_REDIS","DICT",'',"src/dic/dic_redis.js") ;
            dict.savefile("JSCRIPT","DIC_MYSQL","DICT",'',"src/dic/dic_mysql.js") ;
            dict.savefile("JSCRIPT","DIC_PG","DICT",'',"src/dic/dic_pg.js") ;

        }, 300);
        */
        /*
        setTimeout(function () {
            dict.savefile("SQL","PERFORMANCE_TABLES","MYSQL",'',"mysql_performance_tables.sql") ;
            dict.savefile("SQL","FILL_HELP_TABLES","MYSQL",'',"fill_help_tables.sql") ;
            dict.savefile("SQL","INSTALL_SPIDER","MYSQL",'',"install_spider.sql") ;

        }, 300);
        setTimeout(function () {
            dict.savefile("ZIP","PPADMIN_PROD","PPADMIN",'',"ppadmin_prod.zip") ;
        
        }, 300);
        dict.savefile('JPEG','HELP-LOGIN1','HELP','en-US','c:\\WORK\\Patient Portal\\nodecrud-master\\Healthshare PP API_files\\image006.jpg','',(err,data) => {
            //console.log(err||data);
        }) ;
        dict.savefile('PDF','HELP-SPRL-WORKFLOW','HELP','en-US','c:\\WORK\\Patient Portal\\SPRL workflow.pdf','',(err,data) =>{
            //console.log(err||data);
        });    
        dict.savefile('HTM','HELP-LOGIN1','HELP','en-US','c:\\WORK\\Patient Portal\\nodecrud-master\\Healthshare PP API.htm',(err,data) => {
        });
        dict.savefile('HTM','HELP-LOGIN2','HELP','en-US','c:\\WORK\\Patient Portal\\nodecrud-master\\Healthshare PP API.htm','',(err,data) =>{
        });
        // save file to dic cargo or notes
        dict.savefile('JAVASCRIPT','dicedit','DICT-editors','en-US','c:\\WORK\\Patient Portal\\nodecrud-master\\src\\dic\\editors\\dicedit.js','',(err,data) => {
        }); 
        //*/
        //---------------------------------------------------------------------------------

        let dicedit = require('./src/dic/editors/dicedit');
        app.get('/dicedit/:type',dicedit.editor );
        /*
        app.get('/dicedit/json',dicedit.json );
        app.get('/dicedit/sql',dicedit.sql );
        app.get('/dicedit/xml',dicedit.xml );
        app.get('/dicedit/email',dicedit.email );
        app.get('/dicedit/javascript',dicedit.javascript );
        app.get('/dicedit/ejs',dicedit.ejs );
        app.post('/dicedit/post',dicedit.post  );
        app.post('/dicedit/upload',dicedit.upload  );
        */

        let adminusers = require('./src/routes/adminusers');
        app.get('/adminusers/list',adminusers.list )
        app.get('/adminusershtml/:tokenid',adminusers.htmllist )
        app.post('/adminuser/update',adminusers.update )
        app.get('/adminuser/pwdreset',adminusers.pwdreset )
        app.get('/adminuser/disable',adminusers.disable )
        app.get('/adminusersave',adminusers.save )
        app.get('/adminuser/:userid',adminusers.user )


        let login = require('./src/routes/login');
        app.get('/captcha',login.captcha);
        app.get('/captcha.png',login.captchapng);


        app.get('/login',login.htmllogin);
        app.post('/login/register/user',login.register);
        app.get('/login/auth/:tokenid/:roles',login.authenticate);
        app.get('/login/verifyuser',login.loginVerifyUser);
        app.post('/login/verifyuser',login.loginVerifyUser);
        app.get('/login/verifytoken',login.VerifyToken);
        app.get('/login/logout',login.logout);
        app.post('/login/logout',login.logout);

        app.get('/login/resetpwd',login.resetpwd);
        app.get('/login/password/change',login.changepwd);
        
    });
}
setTimeout(function () { // this gives time for the dictionary to connect
    startup();
},1250);





        

