
// add timestamps in front of log messages
require('console-stamp')(console, 'yyyymmddHHmmss.l');

let DEVENV = 'STAGE' ;
let conf = require('./src/utils').getConfig().conf;
//------------------------------------------------------------------------------------
let dict = require('./src/dic/dict');
dict.get("JSCRIPT","./src/utils","REQUIRE","",(err,data) =>{
    let utils = require('./src/utils');
    if (err || utils.isEmpty(data)) {
        //utils = require('./src/utils');
    } else {
        //utils = require('eval')(data,true);
    }
    //let test = 'undefined' ;
    //console.log("GET UTILS %s",utils.isEmpty(test)) 
    console.log("\nUSING DevEnv= %s \nversion %s\n",conf['devEnv'], require('./src/utils').getConfig().config['version'])
    
    let express = require('express');
    let bodyParser = require('body-parser');
    let fileUpLoad = require('express-fileupload');
    let http = require('http');
    let path = require('path');

    let app = express();
    //---------------------------------------------------------------------------------------
    app.use( bodyParser.json() );       // to support JSON-encoded bodies
    app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
        extended: true  
    })); 
    // test create functions for ejs
    app.locals.getMsg = ["Login","Logout","three"];
    app.locals.MSG = {"WEB_001":"Login","WEB_002":"Logout"};
    // all environments
    app.set('port', process.env.PORT || 4700);
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');
    console.log("app.get('views')=",app.get('views'))
    app.use(fileUpLoad());
    app.use(express.static(path.join(__dirname, 'public')));
    //--------------------------------
    //---------------------------------------------------
    app.get('/app/config', utils.config);    
    app.get('/translate',utils.MStranslate)
    app.get('/translate/:text/:from/:to',utils.MStranslate)
    //---------------------------------------------------
    app.get('/uithemes',utils.uithemes) ;
    
    let issues = require('./src/routes/issuesmgm');
    app.get('/', issues.index); 
    app.get("/issues/html",issues.html)
    app.get("/issues/list",issues.list)
    app.get("/issues/tasks",issues.tasks)
    app.post("/issues/update",issues.update)

    app.get("/tasks/list",issues.list)
    app.post("/tasks/update",issues.taskupdate);

    app.get("/issues/select",issues.select)
    app.get("/tasks/select",issues.select)
    
    //---------------------------------------------------------------------------------
    let adminusers = require('./src/routes/adminusers');
    app.get('/adminusers/list',adminusers.list )
    app.get('/adminusershtml/:tokenid',adminusers.htmllist )
    app.post('/adminuser/update',adminusers.update )
    app.get('/adminuser/pwdreset',adminusers.pwdreset )
    app.get('/adminuser/disable',adminusers.disable )
    app.get('/adminusersave',adminusers.save )
    app.get('/adminuser/:userid',adminusers.user )
    app.get('/adminuser/idlist',adminusers.useridlist);
    //----------------------------------------------------------
    let users = require('./src/routes/users'); 
    app.get('/lookup/clients',users.ppclients)
    app.get('/users/clients',users.ppclients)
    app.get('/users/clients/html',users.htmlclients)
    app.post('/users/verifyuser',users.VerifyUser);
    app.post('/users/save',users.save);
    app.get('/user/:user_id',users.list); 
    app.get('/users',users.list); 
    app.get('/users/json',users.list); 
    app.get('/users/html/:tokenid', users.htmllist);
    app.get('/users/edit',users.save_edit);
    app.get('/users/delete',users.del_user);
    app.get('/users/gethash',users.gethash);
    app.post('/users/register/:id',users.register_user);
    app.post('/users/save_edit',users.save_edit);
    app.post('/users/sendemail',users.sendemail);
    app.get('/userlist/:id',users.users); 
    //-----------------------------------------------------------
    let login = require('./src/routes/login');
    app.get('/captcha',login.captcha);
    app.get('/captcha.png',login.captchapng);
    app.post('/login',login.login);
    app.post('/login/register/user',login.register);
    app.post('/login',login.loginpost);
    app.get('/login',login.login);
    app.get('/login/auth/:tokenid/:roles',login.authenticate);
    app.get('/login/verifyuser',login.loginVerifyUser);
    app.post('/login/verifyuser',login.loginVerifyUser);
    app.get('/login/verifytoken',login.VerifyToken);
    app.get('/login/logout',login.logout);
    app.post('/login/logout',login.logout);
    app.get('/login/resetpwd',login.resetpwd);
    app.get('/login/html',login.htmllogin);
    app.get('/login/secquestions',login.securityQuestions);
    app.get('/login/secanswers',login.securityAnswers);
    app.get('/login/forgotPasswordEmail',login.forgotPasswordEmail);
    app.get('/login/password/change',login.changepwd);
    //-----------------------------------------------------------

    http.createServer(app).listen(app.get('port'), function(){
        console.log('\nPatient Portal Admin server listening on port %s \nEnv = %s ', app.get('port'), conf['devEnv']);
    });

    // EXIT cleanup if needed
    process.stdin.resume();//so the program will not close instantly
    function exitHandler(options, err) {
        if (options.cleanup) {
            console.log('clean up');
            //conn.close()
        }
        if (err) console.log(err.stack);
        if (options.exit) process.exit();
    }
    //do something when app is closing
    process.on('beforeExit', exitHandler.bind(null, {exit:true}));
    process.on('exit', exitHandler.bind(null,{cleanup:true}));

    //catches ctrl+c event
    process.on('SIGINT', exitHandler.bind(null, {exit:true}));
    //process.on('SIGKILL', exitHandler.bind(null, {exit:true}));

    //catches uncaught exceptions
    process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
    
});


