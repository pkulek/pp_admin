// add timestamps in front of log messages
require('console-stamp')(console, 'yyyymmddHHmmss.l');

const DEBUG = true;

let config =  require('./utils').getConfig().config;
let SENDMAIL = config.SendMail ;

let email   = require("emailjs");
let dict = require('./dic/dict');
let server  = email.server.connect({
   user:    "", 
   password:"", 
   host:    "192.168.170.230", 
   port:25, 
   ssl:false
});
/*
// send the message and get a callback with an error or details of the message that was sent
server.send({
   text:    "i hope this works", 
   from:    "you <username@your-email.com>", 
   to:      "someone <someone@your-email.com>, another <another@your-email.com>",
   cc:      "else <else@your-email.com>",
   subject: "testing emailjs"
}, function(err, message) { console.log(err || message); });
*/

module.exports = server;

//---------------------------------------------------------------------------------------
module.exports.sendEmail = (obj,mailtype,oMail) => {
    let cMail = '';
    if (isEmpty(mailtype)) {
       mailtype = "PatientPortal" ;
    };
    if (typeof oMail == 'object') {
        cMail = JSON.stringify(oMail);        
    }
    if (typeof oMail == 'string') {
        cMail = oMail;        
    }
    //let cMail = JSON.stringify(oMail);
    if( cMail != '') {
        if (DEBUG) { 
            //console.log("email.send %s\n Template = %s",mailtype,cMail) ;
            //console.log("email.send object = %j",obj) ;
        }
        dict.get("JSON",mailtype,"MAIL",'en-US',(err,maildata)=> {
            if (! isEmpty(maildata)) {
                if (typeof maildata == 'object') {
                    cMail = JSON.stringify(maildata);
                } else {
                    cMail = maildata ;
                }
            } else {                    
                dict.set("JSON",mailtype,"MAIL",'en-US',cMail,(err,data)=>{});
            }
            let sprintf = require("sprintf-js").sprintf;
            try {
                cMail = sprintf(cMail,obj);
            } catch(ex) {
                console.error("sprintf error=%s",ex);
            }
            oMail = JSON.parse(cMail);
            oMail.from = "nyppsupport@nyehealth.org";
            if (DEBUG) {        
                  console.log("mail template \nmsg type =%s\n mail=%j\n obj=%j\n\n",mailtype,oMail,obj);
            }
            if (SENDMAIL) {
                server.send(oMail, (err, message) => {
                    if (DEBUG) { 
                        console.log("\n%s \nemail response result =%j\n Error=%s\n\n",mailtype,message,err);
                    }
                });
            } else {
                console.log("SENDMAIL turned off");
            }
        });           
    }
    return 0;
}
