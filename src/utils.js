

!function(){
    let _config = {};
    String.prototype.replaceAll = function(str1, str2, ignore) {
        return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
    } 
    // http://api.microsofttranslator.com/v2/Http.svc/Translate?appId=599FA87C780A9743C56B85FA00AF65BA6ED641D3&text=testin from bing&from=en-US&to=es-ES
    const MicrosoftTranslatorTranslateUri = 'http://api.microsofttranslator.com/v2/Http.svc/Translate?appId=%s&text=%s&from=%s&to=%s';
    const MicrosoftTranslatorDetectUri    = 'http://api.microsofttranslator.com/v2/Http.svc/Detect?appId=%s&text=%s';
    const MicrosoftTranslatorGetLngUri    = 'http://api.microsofttranslator.com/v2/Http.svc/GetLanguagesForTranslate?appId=%s';
    const MicrosoftTranslatorGetSpkUri    = 'http://api.microsofttranslator.com/v2/Http.svc/GetLanguagesForSpeak?appId=%s';
    const MicrosoftTranslatorSpeakUri     = 'http://api.microsofttranslator.com/v2/Http.svc/Speak?appId=%s&text=%s&language=%s';
    const BingAppId                       = '599FA87C780A9743C56B85FA00AF65BA6ED641D3';//'73C8F474CA4D1202AD60747126813B731199ECEA';
    //-----------------------------------------------------------------------------------
    let mstanslate = exports.mstranslate = (text,from,to,callback)=>{
        let http = require('http');
        let url = `http://api.microsofttranslator.com/v2/Http.svc/Translate?appId=${BingAppId}&text=${text}&from=${from}&to=${to}` ;
        console.log(url)
        http.get(url, (resp) => {
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });
            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                //console.log("Result data = %s",data);
                if ( data.substring(0,7) == '<string' ) {
                    let tag = data.match(/<string [^>]+>([^<]+)<\/string>/)[1];
                    //let tag = Tags(data,'<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">','</string>')[0].name ;
                    callback("",tag)
                } else {
                    callback(data,"")
                }
            });        
        }).on("error", (err) => {
            console.log("Error: " + err.message);
            //res.send(err)
            callback(err,"")
        });
    }
    //---------------------------------------------------------------------------------
    exports.MStranslate = (req,res)=> {
        let http = require('http');
        let origtext = req.query.text;      
        let from = req.query.from;
        let to = req.query.to;
        if (isEmpty(origtext) ){
            origtext = req.params.text; 
        }
        if (isEmpty(from) ){
            origtext = req.params.from; 
        }
        if (isEmpty(to) ){
            origtext = req.params.to; 
        }
        console.log("param = %j",req.params)
        console.log("query = %j",req.query)
        let url = `http://api.microsofttranslator.com/v2/Http.svc/Translate?appId=${BingAppId}&text=${origtext}&from=${from}&to=${to}` ;
        console.log(url)
        http.get(url, (resp) => {
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });
            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                console.log("Result data = %s",data);
                if ( data.substring(0,7) == '<string' ) {
                    let tag = data.match(/<string [^>]+>([^<]+)<\/string>/)[1];
                    //let tag = Tags(data,'<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">','</string>')[0].name ;
                    res.send(tag)
                } else {
                    res.send("")
                }
            });        
        }).on("error", (err) => {
            console.log("Error: " + err.message);
            res.send(err)
        });
    } 
    //---------------------------------------------------------------------------------
    let UIThemeList = exports.UIThemeList = (srcpath,type) =>{
        let fs = require('fs') ;
        let path = require('path')
        if (isEmpty(srcpath)) {
            srcpath = __dirname+"/../public/themes/" ;
        }
        let flist = fs.readdirSync(srcpath).filter(file => fs.lstatSync(path.join(srcpath, file)).isDirectory())
        if (type === 1 ) {
            return (flist)
        } else if (type === 4 ) {
            let slist = ""
            flist.forEach((o,i)=>{
                slist += `${o}:${o};`
            })    
            let newStr = slist.substring(0, slist.length-1);   
            return (newStr)
        } else {     
            let sellist =   `<select name ="jqtheme_select" id="jqtheme_select" class="jqtheme_selector">`
            flist.forEach((o,i)=>{
                sellist += `<option value="${o}">${o}</option>\n`
            })
            sellist +=  '</select>'
            return(sellist)
        }
    }
    //-----------------------------------------------------------------------------------
    exports.uithemes = (req,res)=>{   
        let path = req.query.path;      
        let type = req.query.type;
        if (isEmpty(path)) {
            path = __dirname+"/../public/themes/" ;
        }
        if (isEmpty(type)){
            type = 1;
        }
        res.send(UIThemeList(path,type))
        //console.log(utils.UIThemeList(__dirname+"/../public/themes/",type))
    }
    //-----------------------------------------------------------------------------------
    exports.ejsTags = function(str,sub1,sub2){
        if (isEmpty(sub1)){
            sub1 = "<%-"
        }
        if (isEmpty(sub2)){
            sub2 = "%>"
        }
        let a = [];
        if (typeof str == 'string') {
            let aData = str.split(/(?:\r\n|\r|\n)/g);  //get lines
            aData.forEach((value,i)=>{
                //console.log(value,i)
                let x = value.indexOf(sub1,0);
                let y = value.indexOf(sub2,0);
                if (x !== -1 ){
                    let tag = value.substr(x,y-x);
                    let etag = value.substr(y,sub2.length)
                    let token = tag+etag;
                    if (token.indexOf("include") !== -1){            
                        let name = tag.replace(sub1+" include","").trim() ;
                        a.push({token,name});            
                    }
                }        
            });
        }
        return a;
    };
    //match(/<a [^>]+>([^<]+)<\/a>/)[1];
    //-----------------------------------------------------------------------------------
    let Tags = exports.Tags = function(str,sub1,sub2){
        if (isEmpty(sub1)){
            sub1 = "<{"
        }
        if (isEmpty(sub2)){
            sub2 = "}>"
        }
        let a = [];
        if (typeof str == 'string') {
            let aData = str.split(/(?:\r\n|\r|\n)/g);  //get lines
            aData.forEach((value,i)=>{
                //console.log(value,i)
                let x = value.indexOf(sub1,0);
                let y = value.indexOf(sub2,0);
                if (x !== -1 ){
                    let tag = value.substr(x,y-x);
                    let etag = value.substr(y,sub2.length)
                    let token = tag+etag;
                    let name = token.replace(sub1,"").trim() ;
                    name = name.replace(sub2,"").trim() ;
                    a.push({token,name});            
                }        
            });
        }
        return a;
    };
    //<{WEB_010,login,en-US}>
    //----------------------------------------------------------------------------------
    let replaceLang = exports.replaceLang = (template,callback) =>{    
        let dict = require('./dic/dict');
        let out = []
        let a =  Tags(template,"<{","}>");
        let count = 0;
        let len = a.length;
        a.forEach((o)=>{
            console.log("Lang obj = %j",o);
            let aList = o.token.split(",");
            let aName = o.name.split(",");
            //console.log(aList)
            console.log(aName)
            dict.get("LANG,",aName[0],"LOOKUP",aName[2],(err,data)=>{
                count += 1;
                if (isEmpty(data)) {
                    dict.save("LANG",aName[0],"LOOKUP",aName[2],aName[1]) ;
                    data = aName[1]
                }
                console.log("Err=%s,data=%s toekn=%s",err,data,o.token)
                template = template.replace(o.token,data)
                console.log(count);
                if (count == len) {
                    console.log("callback %s",template)
                    callback(template)        
                }
            }) ;
            // relace tag with lang        
        })
    }     
    //-----------------------------------------------------------------------------------
    exports.scriptTags = function(str,sub1,sub2){
        if (isEmpty(sub1)){
            sub1 = "<script "
        }
        if (isEmpty(sub2)){
            sub2 = "</script>"
        }
        let a = [];
        if (typeof str == 'string') {
            let aData = str.split(/(?:\r\n|\r|\n)/g);  //get lines
            aData.forEach((value,i)=>{
                //console.log(value,i)
                let x = value.indexOf(sub1,0);
                let y = value.indexOf(sub2,0);
                if (x !== -1 ){
                    let tag = value.substr(x,y-x);
                    let etag = value.substr(y,sub2.length)
                    let token = tag+etag;
                    if( token.includes('src=')  &&  token.includes(".js") ){     
                        let x =  tag.split('"');
                        x.forEach((src,i)=>{
                            if (src.includes(".js")){
                                a.push({name:src.trim(),token:token})
                            }
                        });       
                    }
                }        
            });
        }
        return a;
    };
    //-----------------------------------------------------------------------------
    exports.stylesheetTags = function(str,sub1,sub2){
        if (isEmpty(sub1)){
            sub1 = "<link"
        }
        if (isEmpty(sub2)){
            sub2 = ">"
        }
        let a = [];
        if (typeof str == 'string') {
            let aData = str.split(/(?:\r\n|\r|\n)/g);  //get lines
            aData.forEach((value,i)=>{
                //console.log(value,i)
                let x = value.indexOf(sub1,0);
                let y = value.indexOf(sub2,0);
                if (x !== -1 ){
                    let tag = value.substr(x,y-x);
                    let etag = value.substr(y,sub2.length)
                    let token = tag+etag;
                    if( token.includes('rel="stylesheet"')  &&  token.includes("href=") ){     
                        let x =  tag.split('"');
                        x.forEach((src,i)=>{
                            if (src.includes(".css")){
                                a.push({name:src.trim(),token:token})
                            }
                        });       
                    }
                }        
            });
        }
        return a;
    };
    //-----------------------------------------------------------------------------------
    exports.token = token = {
        get:function (str,sub1,sub2) {
            return this.token(str,sub1,sub2);
        },
        token:function(str,sub1,sub2){
            let a = [];
            let x = str.indexOf(sub1,0);
            let y = str.indexOf(sub2,0);
            while (x !== -1 && y !== -1) {
                let tag = str.substr(x,y-x);
                let etag = str.substr(y,sub2.length)
                let o = {x,y,tag,etag,token:tag+etag``};
                //console.log("\ntag = %s\n",tag);
                if( tag.includes('src=')  &&  tag.includes(".js") ){
                    let x =  tag.split('"');
                    x.forEach((src,i)=>{
                        if (src.includes(".js")){
                            o.src=src;
                        }
                    });
                //str =  str.replace(s,'<script>this is a test');
                    a.push(o)
                }
                x = str.indexOf(sub1,x+1);
                y = str.indexOf(sub2,y+1);            
            }
            return a;
        }
    };
    //-----------------------------------------------------------------------------------
    let require_ = exports.require_ = (mod,callback) =>{
        let id = mod || "UTILS" ;
        get("JSCRIPT",mod,"REQUIRE","",(err,data) =>{
            try{
                let _eval = require('eval');
                let utils = _eval(data,true) ;
                if (callback) {
                    callback(err,utils);
                }
            } catch(e){
                console.trace(e);
            }
        });
    }
    //----------------------------------------------------------------------------------
    var isEmpty = exports.isEmpty = function(str) { // check if string is null or empty usually missing parameter
        if ( str == undefined ) {
            return true;
        } else if ( str == null ) {
            return true;
        } else if (typeof str == 'string' ){
                return typeof str == 'string' && !str.trim() || str == 'undefined' || str == 'null' || str === '[]'|| str === '{}';
        } else if (typeof str == 'array') {
            return str.length == 0;
        } else if ( typeof str == 'object' ) {
            return Object.keys(str).length == 0;
        } else {
            return str == null || str == undefined;    
        } 
    }
    //----------------------------------------------------------------------------------- 
    module.exports.config = () => {
        return(_config);
    }
    //----------------------------------------------------------------------------------
    let getConfig = module.exports.getConfig = (devEnv,file,callback) => {
        //console.log("\narg0=%s\narg1=%s\narg2=%s\narg3=%s\nconfigfile=%s\n",process.argv[0],process.argv[1],process.argv[2],process.argv[3],file)
        if(devEnv && devEnv == true) {
            _config = {} ; //reset
            devEnv = 'PROD' ;
        }   
        if (! devEnv) { devEnv = 'PROD' ; }
        if (! isEmpty(process.argv[2])) { // 1st arg on command line  
            let set = new Set(["DEV","STAGE","PROD"]);
            let env = process.argv[2].toUpperCase();    
            if ( set.has(env) ) {
                devEnv = env;        
            }
        }
        if ( isEmpty(file) ){
            let aname = ""+ process.argv[1].split('\\').slice(-1) +"" ;
            file = aname.replace('.js','.json') ;
        } else {
            if(typeof file == "function") {
                callback = path;
                file = "../config.json";           
            }
        }
        if (! isEmpty(process.argv[3])) { // 2nd arg on command line
            file = "../"+process.argv[3] ;
        }
        if( isEmpty(_config)) {
            try {
                console.log("calling config file %s",file);
                _config = eval(`require('${file}')`) ;
                _config.configfile = file;
                _config.devEnv = devEnv;
            } catch(e) {
                let _file = "../config.json";
                try{
                    _config = eval(`require('${_file}')`) ;
                    console.log("ERROR in reading app Config %s,\n Falling back to Calling default config file %s",file,_file)
                    _config.configfile = _file;
                    _config.devEnv = devEnv;
                } catch(ex) {
                    _config = {
                        "version":"1.0",
                        "port":4300 ,
                        "usedict":true,    
                        "dict":{
                            "dba":"sqlite",
                            "redis":{"host":"http://192.168.160.141","auth":"manahealth","port":"6379","options":{"no_ready_check":"true"}},
                            "sqlite":{
                                "schema":"dict.export",
                                "table":"dict"
                            },
                            "mysqlconfig":{
                                "host":"192.168.160.151",
                                "user":"root",
                                "password":"",
                                "port":"3306",
                                "typeCast":true,
                                "database":"dict"
                            }                 
                        },
                        "dict_dba":"sqlite",
                        "name": "application-name",
                        "env":"DEV",                               
                        "SendMail":false,                                           
                        "theme":"blitzer",
                        "templatePath":"c:/WORK/Patient Portal/ppadmin/views",
                        "publicPath":"c:/WORK/Patient Portal/ppadmin",
                        "config":{
                            "port":4300 ,
                            "devEnv":"STAGE",                                          
                            "RefreshFlowpReg":2,
                            "RefreshFlowDoc":2,
                            "mssqlconnection":"mssql://stella_user:heartbeat@192.168.130.109/SW_Reporting",
                            "DEBUG":true,                                                                                                        
                            "HS_auth": { "user": "HS_Services", "password": "$Nyec123" },
                            "HSECR_APIurl":"http://192.168.160.173:57772/csp/healthshare/ppedge/api/NYeC.PP.Api" ,
                            "HSHUB_APIurl":"http://192.168.160.174:57772/csp/healthshare/hsregistry/api/NYeC.PP.Api",
                            "HSACCESS_APIurl":"http://192.168.160.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api",
                            "redis":{"host":"192.168.160.141","auth":"manahealth","port":"6379","options":{"no_ready_check":"true"}},
                            "SPRL_url":"http://192.168.130.23:9448",
                            "SPRLPIXManager":"http://192.168.130.23:9448/PIXManager",
                            "mysqlconfig":{
                                "host":"192.168.160.151",
                                "user":"root",
                                "password":"",
                                "port":"3306",
                                "typeCast":true,
                                "database":"dict"
                            }                 
                        },
                        "configDEV":{
                                "port":4300 ,
                                "devEnv":"DEV",
                                "RefreshFlowpReg":10,
                                "RefreshFlowDoc":10,
                                "mssqlconnection":"mssql://stella_user:heartbeat@192.168.130.109/SW_Reporting",        
                                "DEBUG":true,                                                                                                        
                                "HS_auth": { "user": "HS_Services", "password": "$Nyec123" },
                                "HSREPOSITORY_APIurl":"http://192.168.160.173:57772/csp/healthshare/hsrepository/api/NYeC.PP.Api" ,
                                "HSECR_APIurl":"http://192.168.160.173:57772/csp/healthshare/ppedge/api/NYeC.PP.Api" ,
                                "HSHUB_APIurl":"http://192.168.160.174:57772/csp/healthshare/hsregistry/api/NYeC.PP.Api" ,
                                "HSACCESS_APIurl":"http://192.168.160.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api",
                                "SPRL_url":"192.168.130.23:9448",
                                "SPRLPIXManager":"http://192.168.130.23:9448/PIXManager",
                                "redis":{"host":"http://192.168.160.141","auth":"manahealth","port":"6379","options":{"no_ready_check":"true"}},
                                "mysqlconfig":{
                                    "host":"localhost",
                                    "user":"root",
                                    "password":"EndorKroner#1",
                                    "port":"3306",
                                    "typeCast":true,
                                    "database":"dict"
                                }
                        },
                        "configSTAGE":{                                       
                            "port":4300 ,
                            "devEnv":"STAGE",                                          
                            "RefreshFlowpReg":2,
                            "RefreshFlowDoc":2,
                            "mssqlconnection":"mssql://stella_user:heartbeat@192.168.130.109/SW_Reporting",
                            "DEBUG":true,                                                                                                        
                            "HS_auth": { "user": "HS_Services", "password": "$Nyec123" },
                            "HSREPOSITORY_APIurl":"http://192.168.160.173:57772/csp/healthshare/hsrepository/api/NYeC.PP.Api" ,
                            "HSECR_APIurl":"http://192.168.160.173:57772/csp/healthshare/ppedge/api/NYeC.PP.Api" ,
                            "HSHUB_APIurl":"http://192.168.160.174:57772/csp/healthshare/hsregistry/api/NYeC.PP.Api",
                            "HSACCESS_APIurl":"http://192.168.160.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api",
                            "redis":{"host":"192.168.160.141","auth":"manahealth","port":"6379","options":{"no_ready_check":"true"}},
                            "SPRL_url":"http://192.168.130.23:9448",
                            "SPRLPIXManager":"http://192.168.130.23:9448/PIXManager",
                            "mysqlconfig":{
                                "host":"192.168.160.151",
                                "user":"root",
                                "password":"",
                                "port":"3306",
                                "typeCast":true,
                                "database":"dict"
                            }                                         
                        }, 
                        "configPROD":{
                            "port":4300 ,
                            "devEnv":"PROD",
                            "RefreshFlowpReg":10,
                            "RefreshFlowDoc":10,
                            "mssqlconnection":"mssql://stella_user:heartbeat@192.168.180.45:3978/TEMP_SW_Reporting",
                            "DEBUG":false,    
                            "HS_auth": { "user": "HS_Services", "password": "HS_Services" },
                            "HSREPOSITORY_APIurl":"http://192.168.180.173:57772/csp/healthshare/hsrepository/api/NYeC.PP.Api" ,
                            "HSECR_APIurl":"http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.Api",
                            "HSHUB_APIurl":"http://192.168.180.174:57772/csp/healthshare/pphub/api/NYeC.PP.Api" ,
                            "HSACCESS_APIurl":"http://192.168.180.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api",
                            "HSECR_CIDApi":"http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.Service.CID.ClinicalAuthentication/api",
                            "redis":{"host":"192.168.180.141","auth":"manahealth","port":"6379","options":{"no_ready_check":"true"}},
                            "SPRL_url":"http://192.168.180.83:9445",
                            "SPRLPIXManager":"http://192.168.180.83:9445/PIXManager",
                            "mysqlconfig":{
                                "host":"192.168.180.151",
                                "user":"root",
                                "password":"",
                                "port":"3306",
                                "typeCast":true,
                                "database":"dict"
                            }
                        } 
                    }
                    console.log("ERROR Missing default config file %s\nFalling back to Calling embedded config \n%j",_file,_config)
                }
            }     
        }
        if( callback) {
            callback({ "config":_config,"conf":_config['config'+devEnv] });
        }
        return { "config":_config,"conf":_config['config'+devEnv] };
    }
    //----------------------------------------------------------------------------------
    let bit_test = exports.bit_test = (num, bit)=>{
        return ((num>>bit) % 2 != 0)
    }
    let bit_set = exports.bit_set = (num, bit)=>{
        return num | 1<<bit;
    }
    let bit_clear = exports.bit_clear = (num, bit)=>{
        return num & ~(1<<bit);
    }
    let bit_toggle = exports.bit_toggle = (num, bit) =>{
        return bit_test(num, bit) ? bit_clear(num, bit) : bit_set(num, bit);
    }
    //-----------------------------------------------------------------------------------
    let genToken = exports.genToken = function(len) {    
        let crypto = require('crypto');
        len = len || 20;
        if (crypto.randomBytes) {
            return crypto.randomBytes(Math.ceil(len / 2)).toString('hex').substring(0, len);
        } else {
            for (var i = 0, salt = ''; i < len; i++) {
            salt += saltChars.charAt(Math.floor(Math.random() * saltCharsCount));
            }
            return salt;
        }
    }
}()

