
const TEST_TEMPLATE = false;
let dict = require('../dict');
let utils = require('../../utils');
// add timestamps in front of log messages
require('console-stamp')(console, 'yyyymmddhhmmss.l');
const DEBUG = false;
let isEmpty = utils.isEmpty;
//-------------------------------------------------------------------------
let getParams = (req)=> {
    let query = req.query ;
    let params = req.params;
    let type = params.type;
    let id = params.id;
    let parent = params.parent;
    let ext = params.ext;  
    let lang = params.lang;
    let file = params.file;
    if (isEmpty(file)) {  
        file=""; 
        if (! isEmpty(query.file)) {
            file = query.file;
        }
    }
    if (isEmpty(lang)) {  
        lang = req.headers["accept-language"] ;
        if (! isEmpty(query.lang)) {
            lang = query.lang;
        }        
    }
    if (isEmpty( type )){ 
        type=""; 
        if (! isEmpty(query.type)) {
            type = query.type;
        }
    }
    if ( isEmpty( id )){
        id="" ; 
        if (! isEmpty(query.id)) {
            id = query.id;
        }
    }    
    if ( isEmpty( parent )){ 
        parent="" ; 
        if (! isEmpty(query.parent)) {
            parent = query.parent;
        }
    }    
    if ( isEmpty( ext )){ 
        ext="" ; 
        if (! isEmpty(query.ext)) {
            ext = query.ext;
        }
    }     
    let pars = {type,id,parent,ext,lang,file}; // new es6 way
    return(pars); 
}
//----------------------------------------------------
exports.upload = (req, res) => {
     if (! req.files)
        return res.status(400).send('No files were uploaded.');
  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file 
    let tempFile = req.files.tempFile;
    if(DEBUG) {
        console.log("",tempFile);
    }
  // Use the mv() method to place the file somewhere on your server 
  tempFile.mv('/tmp/tempfile.tmp', function(err) {
    if (err)
      return res.status(500).send(err);
 
    res.send('File uploaded!');
  });
};
//----------------------------------------------------
exports.list = function(req,res){
   var type = req.params.type;
   getlist(type,(err,data)=>{
       res.send(data);
   });
}
//----------------------------------------------------
let getlist = exports.getlist = function(type,callback){
    if (utils.isEmpty(type)) {
        type = 'JSON';
    }
    var id = '';
    var parent = '';
    var ext = '';
    dict.get(type,id,parent,ext,(err,rows)=>{
        callback(err,rows) ;
    });
};

//----------------------------------------------------
exports.post = function(req, res) {
    let id = req.query.id ;
    let type = req.query.type;   
    let parent = req.query.parent; 
    let ext = req.query.ext;
    let data = req.body;
    if (DEBUG) {
        console.log("%s",data.cargo);
    }
    dict.set(data.type,data.id,data.parent,data.ext,data.cargo,(err,result)=>{
        if (DEBUG){
            console.log("Result:%s %s",result,err)
        }
    });
    res.send("200 OK");
};
//---------------------------------------------------
exports.test = function(req, res){
    // get ejs from dict and render
    dict.get("EJS","dicedit_test","TEMPLATE","en-US",(err,data)=>{
        if( isEmpty(data)){ // file 
            res.render('dicedit_test',{page_title:"JSON Source2 Editor "});           
        } else {  //database
            let ejs = require("ejs"); 
            let html = ejs.render(data,{page_title:"JSON Source4 Editor "}) ;   
            res.send(html) ;   
        }
    });                       
};
//---------------------------------------------------
exports.editor = function(req, res){
    let params = getParams(req);
    let type = params.type ;
    if (isEmpty(type)){type = "json"; };
    let editor = `dicedit_${type}`;
    getTemplate(type,(err,templ)=>{
        if(! isEmpty(templ)){
            let ejs = require('ejs');
            res.send(ejs.render(templ,{page_title:`${type.toUpperCase()} Source Editor `,"filename":`${editor}.ejs`}));
        } else {
            console.log("USING re.render");
            res.render(editor,{page_title:"XML Source Editor ","filename":`${editor}.ejs`});   
        }     
    })
}
//---------------------------------------------------
let getTemplate = (type,callback)=>{     
    let diceditor = `dicedit_${type}`;
    dict.get("EJS",diceditor,"DICTIONARY","",(err,template)=>{ //get ejs template from dictionary
        if (!isEmpty(template)){  // fallback to file based 
            try {
                let fs = require('fs');
                let filePath = __dirname + `/${diceditor}.ejs`; // the ejs file is in the direcrory where the js file being run
                if ( fs.existsSync(filePath) ) {
                    template = fs.readFileSync(filePath, 'utf8');
                    dict.set("EJS",diceditor,"DICTIONARY","",template);
                }                
            } catch(e){
                console.error(e);
            }
        }
        // TESTING replace script tags with embedded script 
        if (TEST_TEMPLATE) {
            let a = utils.token.get(template,'<script ','</script>');
            console.log(a)
            a.forEach((o,i)=>{
                let xname =  o.src.split("/").slice(-1).pop();
                let sname = xname.split('.').slice();
                sname.pop();
                sname = sname.join('.');
                //console.log("src=%s s =%s,x=%s", o.src,sname,xname) ;
                dict.get("JS",sname,"DICTIONARY","",(err,data)=> {
                    if (isEmpty(data)){  // fallback to file based 
                        try {
                            let fs = require('fs');
                            let filePath = "public/"+o.src;
                            if ( fs.existsSync(filePath) ) {
                                data = fs.readFileSync(filePath,'utf8');
                                dict.set("JS",sname,"DICTIONARY","",data);
                            }
                        } catch(ex) { 
                            console.log(ex);
                        }
                    }
                    // replace script tag with code
                    //template =  template.replace(o.tag,`<script type="text/javascript" charset="utf-8" >\n${data}\n`);
                    if(i == a.length-1) {
                        callback(err,template);
                        return 0;
                    }
                });      
            });
        } else {
            callback(err,template);
        }
    });            
};
/*
//---------------------------------------------------
exports.json = function(req, res){
    let type = "xml";
    let editor = `dicedit_${type}`;
    dict.get("jSCRIPT",editor,"DICTIONARY","",(err,data)=>{
        let template = "";
        if (isEmpty(data)){
            let fs = require('fs');
            let filePath = __dirname + `/${editor}.ejs`;
            template = fs.readFileSync(filePath, 'utf8');
            dict.set("jSCRIPT",editor,"DICTIONARY","",template);
        } else {
            template = data;
        }
        res.end(require('ejs').render(template,{page_title:"Source Editor ","filename":`${editor}.ejs`}));       
    });               
};
//---------------------------------------------------
exports.sql = function(req, res){
       res.render('dicedit_sql',{page_title:"SQL Source Editor "});                        
};
//---------------------------------------------------
exports.html = function(req, res){
       res.render('dicedit_html',{page_title:"HTML Source Editor "});                        
};
//---------------------------------------------------
exports.xml = function(req, res){
       res.render('dicedit_xml',{page_title:"XML Source Editor "});                        
};
//---------------------------------------------------
exports.javascript = function(req, res){
       res.render('dicedit_js',{page_title:"Javascript Source Editor "});                        
};
//---------------------------------------------------
exports.email = function(req, res){
       res.render('dicedit_email',{page_title:"email Editor "});                        
};
//---------------------------------------------------
exports.ejs = function(req, res){
       res.render('dicedit_ejs',{page_title:"EJS Source Editor "});                        
};
*/
//----------------------------------------------------
exports.save = function(req, res){
   let tokenid = req.params.tokenid ;
   let type = req.params.type;
   let input = JSON.parse(JSON.stringify(req.body));
   if (DEBUG) {
        console.log(JSON.stringify(input));
   }
   if (isEmpty( type )){       
        type="JSON";
   }
};


