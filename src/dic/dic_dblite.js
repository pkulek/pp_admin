
require('console-stamp')(console, 'yyyymmddHHmmss.l');
let moment = require('moment'); // for date time
const DICT_TABLE = 'dict';
const DICT_DB = 'dict.dic'
const DEBUG = true ;
let connected = false;
let fs = require('fs') ;
var db = '';
//let sqlite = require('sqlite3').verbose();
//let db = new sqlite.Database('dict.db');

// sqllite test    


/*
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('dict.db');
// create dict if not there 
var qry = "CREATE TABLE dict ( \
                type CHAR(10) ,\
                id CHAR(20) ,\
                parent CHAR(20) ,\
                ext CHAR(10) ,\
                cargo BLOB,\
                date datetime ,\
                notes TEXT,\
                date_time CHAR(22) ,\
                flags INT ); \
                CREATE INDEX type on dict (type,id,parent,ext);\
                CREATE INDEX parent on dict (type,parent,id,ext);\
                " 
if (! fs.existsSync('dict.db')) {
          db.run(qry); 
};                 
db.serialize(function() {

  //db.run("CREATE TABLE lorem (info TEXT)");
  var stmt = db.prepare("INSERT INTO dict (type,id,parent) VALUES ('TEST1', 'TESTING1',  'PARENT1'),('TEST2', 'TESTING2',  'PARENT2'),('TEST3', 'TESTING3',  'PARENT3'),('TEST4', 'TESTING4',  'PARENT4') ");
  for (var i = 0; i < 10; i++) {
      stmt.run();//["Ipsum " + i,"fsdaf","fsadsasafd","","","","","",""]);
  }
  stmt.finalize();
  db.each("SELECT rowid ,type, id, parent FROM dict", function(err, row) {
      console.log("%s,%s,%s,%s", row.rowid , row.type, row.id,row.parent);
   });

  var stmt = db.prepare("DELETE from dict where type= 'TEST1' ");
  stmt.run();//["Ipsum " + i,"fsdaf","fsadsasafd","","","","","",""]);
  stmt.finalize();
  db.each("SELECT rowid ,type, id, parent FROM dict", function(err, row) {
      console.log("%s,%s,%s,%s", row.rowid , row.type, row.id,row.parent);
   });
        db.all(`SELECT count(*) qty,rowid ,type, id, parent FROM dict `, function(err, rows) {
            if (err) throw err;
            console.log(rows);
            rows.forEach( (element)=> {
                console.log("ID=%s qty=%s ",element.id,element.qty)                
            }, this);
        }); 
});
db.close();
*/

//----------------------------------------------------------------------------------
let isEmpty = (str)=> { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//----------------------------------------------------------------------------------
let buildWhere = (type,id,parent,ext)=>{
    if (typeof type == 'object'){
        let o = type;
        id = o.id;
        parent = o.parent;
        ext = o.ext ;
        type = o.type;
    }
    let where = "";
    if ( ! isEmpty(type) ) {
        if( typeof type == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` type = '${type}'`;
        }
    }
    if ( ! isEmpty(id)  ) {
        if( typeof id == 'string' ){
            where += ((where == "") ? "where " : " and " ) +`id = '${id}'`;
        }
    }
    if (! isEmpty(parent)) {
        if( typeof parent == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` parent = '${parent}'`
        }
    }
    if (! isEmpty(ext)) {
        if( typeof ext == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` ext = '${ext}'`;
        }
    } 
    return where;   
}
//----------------------------------------------------------------------------------
exports.connected = ()=> {
    return connected;
}

//---------------------------------------------------------------------------------
let query = exports.query = function(qry,callback){
    //let db = open(DICT_DB,DICT_TABLE);
     db.serialize(function() {
         console.log(qry)
        db.all(qry, function(err, rows) {
            console.log("%s,%j",err,rows)
            if (callback) {
                callback(err,rows) ;
            }
        });         
     });        
}
//----------------------------------------------------------------------------------
let insert = exports.insert = function(type,id,parent,ext,cargo,notes,callback){
    let d = new Date(); 
    let date = moment(d).format('YYYYMMDD');   
    let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
    try {
        if(DEBUG) {
            console.log("sqlite.insert qry=%s") ;
        }
        let qry = `INSERT INTO ${DICT_TABLE} (type,id,parent,ext,cargo,notes,date,date_time) VALUES (?,?,?,?,?,?,?,?) `;
        let stmt = db.prepare(qry,type,id,parent,ext,cargo,notes,date,date_time);
        stmt.run();
        stmt.finalize();             
    } catch(ex){
        console.trace("dic_sqlite.mysql_save Error: %s",ex);
        callback(ex,[])
    }                  
    return true;
}
//----------------------------------------------------------------------------------
let save = exports.save = function(type,id,parent,ext,cargo,notes,callback){
    let d = new Date(); 
    let date = moment(d).format('YYYYMMDD');   
    let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
    try {
        let where = buildWhere(type,id,parent,ext);
        let qry =  `select count(*) as qty from ${DICT_TABLE} ${where} `
        if (DEBUG) {
            console.log("sqlite.save qry=%s",qry) ;
        }
        db.serialize(function() {
            db.all(qry, function(err, rows) {
                if(DEBUG) {
                    console.log("sqlite.rows =%j",rows) ;
                }
                if (rows && rows.length > 0 && rows[0].qty > 0) { // update    
                    let qry = `UPDATE ${DICT_TABLE} set cargo = ? ${where} ` ;
                    if (DEBUG) {
                        console.log("dic_sqlite.update qry=%s",qry) ;
                    }
                    db.run(qry, cargo, function(err) {
                        if (callback) {
                            callback(err,cargo);
                        }
                        if( DEBUG) {
                            console.log("dic_sqlite.update qry=%s  error=%s",qry,err)
                        }                             
                    });
                    
                } else {
                    insert(type,id,parent,ext,cargo,notes);
                }
            });         
        });        
    } catch(ex){
        console.trace("dict.sqllite_save Error: %s",ex);
        callback(ex,[])
    }                  
    return true;   
};
//----------------------------------------------------------------------------------
let get = exports.get = (type,id,parent,ext,callback)=> {
    let result= 0;
    if (isEmpty(callback)){
        if (isEmpty(ext) && typeof parent === 'function'){
            callback = parent;
            parent = "";
        }
        if (typeof  ext === 'function' ){        
            callback = ext;
            ext = "";
        }
    }
    try {
        let where = buildWhere(type,id,parent,ext) ;
        let qry = `select  type, id as dicid ,parent,ext, cargo from ${DICT_TABLE} ${where} ` ;
        db.serialize(function() {
            db.query(qry, function(err, rows) {
                rows.forEach(function(element, index) { // convert blob to text from buffer
                    rows[index].cargo = element.cargo.toString()  
                    //rows[index].cargo = JSON.stringify(element.cargo)  
                });                
                if(DEBUG) {
                    fs.writeFile("/tmp/sqlite_testget.sqliteget", JSON.stringify(rows), function(err) {
                        if(err) { console.log(err); }
                    });   
                    if(DEBUG) {
                        console.log("sqlite.get qry=%s",qry);
                        console.log("sqlite.get length = %s rows=%j err=%s",rows.length,rows,err);
                    }
                }
                if (rows && rows.length > 0 ) {
                    callback(err,rows); 
                } else {
                    callback(err,[]);
                }
            }); 
        });
    } catch(ex){
        console.trace("catch:"+ex);
    }
    return result;
};
//----------------------------------------------------------------------------------
// backup rows in database for editing tools
let backup = exports.backup = (type,id,parent,ext,callback) => {
    let d = new Date(); 
    let date = moment(d).format('YYYYMMDD');   
    let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
    let where = buildWhere(type,id,parent,ext) ;
    if ( isEmpty(where) ) { // for backup all
        where = " where type not like '~%'"; 
    }
    let qry = `select type,id,parent,ext,cargo,notes,flags from ${DICT_TABLE} ${where} ` ;
    query(qry,(err,rows)=>{
         if (rows && rows.length > 0 ) { 
            rows.forEach(function(element, index) {
                try {
                    let qry = `INSERT INTO ${DICT_TABLE} (type,id,parent,ext,cargo,notes,date,date_time) VALUES (?,?,?,?,?,?,?,?) `;
                    let stmt = db.prepare(qry,element.id, '~'+element.type,element.parent, element.ext, element.cargo, element.notes, date_time, date);
                    stmt.run();
                    stmt.finalize();             
                } catch(ex){
                    console.trace("dic_sqlite.mysql_backup Error: %s",ex);
                }                               
            });             
         }
         if( callback){
             callback(err,rows)
         }
    })
};
//----------------------------------------------------------------------------------
let remove = exports.remove = (type,id,parent,ext,callback)=> {   
    let where = buildWhere(type,id,parent,ext) ;
    var qry = `delete from ${DICT_TABLE} ${where} ` ;
    query(qry,function(err, rows, fields){
        if (callback) {
            callback(err,rows) ;
        }
    });
}
//----------------------------------------------------------------------------------
let close = exports.close = ()=>{
    db.close();
}
//----------------------------------------------------------------------------------
let open = exports.create = (schema,table)=>{
    let sqlite3 = require('dblite').withSQLite('3.8.6+');;
    let fs = require('fs');
    if ( isEmpty( schema )){
        schema = 'dict.dicl' ;
    }  
    if ( isEmpty( table )){
        table = 'dict' ;
    }        
    if ( fs.existsSync(schema)) {
        try {
            db = new sqlite3.Database(schema);
            connected = true ;
        } catch(ex){
            connected = false ;
        }
    } else {
        db = dblite(schema);
        // check if table exists
        let qry = "CREATE TABLE dict ( \
                type CHAR(10) ,\
                id CHAR(20) ,\
                parent CHAR(20) ,\
                ext CHAR(10) ,\
                cargo BLOB,\
                date datetime ,\
                notes TEXT,\
                date_time CHAR(22) ,\
                flags INT ); \
                CREATE INDEX type on dict (type,id,parent,ext);"
        try{                
            connected = true ;
            db.query(qry);    
        } catch(ex){
            connected = false ;
            console.trace(ex);
        }
    }
    console.log("starting dblite %j",db);
    //db.on('error',(err)=>{
    //    console.log("sqlite Error: %s",err);
    //    connected = false ;
    //});
    return db ;
}     

open(DICT_DB,DICT_TABLE);
  





