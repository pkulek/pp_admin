
//var arr = [249, 13, 105, 170];
//var sql = "INSERT INTO b SET `data` = CHAR( " + arr + " )";
//client.query(sql);

//new Date().getTime()

/*
Of note: mysql-node automatically converts Blob objects into javascript Buffer objects.
the simplest way to just read it as a string in node : myObject.myBlobAttr.toString('utf-8')
For your convenience, this driver will cast mysql types into native JavaScript types by default. The following mappings exist:
TINYBLOB MEDIUMBLOB LONGBLOB BLOB BINARY VARBINARY BIT (last byte will be filled with 0 bits as necessary)
*/
/* usage for mysql
conn.connect();
conn.query('SELECT 1 + 1 AS solution', function(err, rows, fields) {
  if (err) throw err;

  console.log('The solution is: ', rows[0].solution);
});
// store image from file
fs.readFile("/path/to/some/jpg", function (err, data) { // <-- data is a buffer
    conn.query("UPDATE test_table SET photo = ? WHERE id = 2", [ data ], function (err) {
        // err = null
        // photo is a LONGBLOB, id = 1 has the same photo but inserted using Sequel Pro
        // both getting
    });
});

conn.end();
*/
/*
 The structure for dict table
CREATE DATABASE dict;	
use dict;
CREATE TABLE dict (                     
  type varchar(10) DEFAULT NULL,                    
  id varchar(20) DEFAULT NULL,
  parent varchar(20) DEFAULT NULL,
  ext varchar(10) DEFAULT NULL,                                           
  cargo blob,
  date datetime DEFAULT NULL,
  notes text,                                                       
  datetime datetime DEFAULT NULL,
  flags int(10) unsigned DEFAULT NULL,
  KEY type (type,id,parent),
  KEY parent (type,parent,id)                   
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 */

//force mysql to use an index
//SELECT a, b, c FROM my_table FORCE INDEX(index_name) WHERE a = {value};
 
var DICT_TABLE = 'dict.dict';
//var LOG_TABLE = 'log.log';
var fs = require('fs');
var moment = require('moment'); // for date time
var mysql = require('mysql2');
var tokenExpire = 5;//in minutes
var tokenExpireFormat = 'YYYYMMDDhhmm';
var login = require('../routes/login');
var conn = mysql.createConnection({
  host     : '192.168.160.151',
  user     : 'root',
  password : '',
  port : 3306,
  typeCast :true,
  database : 'dict'
},'single');
conn.connect();

//----------------------------------------------------------------------------------
function isEmpty(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//----------------------------------------------------------------------------------
function getParams(req){
    var type = req.query.type;
    var id = req.query.id;
    var parent = req.query.parent;
    var ext = req.query.ext;  
    var notes = req.query.notes;  
    var cargo = req.query.cargo;  
    var lang = req.headers["accept-language"] ;
    //console.log(JSON.strinigy(req.headers));
    if (isEmpty( type )){       
        type="";
    }
    if ( isEmpty( id )){
        id="" ;
    }    
    if ( isEmpty( parent )){ parent="" ; }    
    if ( isEmpty( ext )){ ext="" ; }     
    if ( isEmpty( cargo )){ cargo="" ; }       
    if ( isEmpty( notes )){ notes="" ; }       
    return {type:type,id:id,parent:parent,ext:ext,cargo:cargo,notes:notes,lang:lang};// emulate multiple return values
}
//----------------------------------------------------------------------------------
var get = exports.get = function(type,id,parent,ext,callback){
    result= 0;
    var where = "";
    if ( ! isEmpty(type) ) {
        where += ((where == "") ? "where " : " and " ) +"type = '"+ type +"'";
    }
    if ( ! isEmpty(id) ) {
        where += ((where == "") ? "where " : " and " ) +"id = '"+ id +"'";
    }
    if ( ! isEmpty(parent)) {
        where += ((where == "") ? "where " : " and " ) +" parent = '" + parent +"'"
    }
    if (! isEmpty(ext)) {
        where += ((where == "") ? "where " : " and " ) +" ext = '"+ ext +"'";
    }    
    // add force index for speed and incremental search
    //where = " FORCE INNDEX(type)  "+ where
    var qry = "select type,id,parent,ext,CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" " + where ; 
    conn.query(qry,function(err, rows, fields){    
        if (err)  console.log("Error getting result 01 : %s ",err );        
        callback(rows,err);
    });
    return result;
};
//----------------------------------------------------------------------------------
exports.set = function(type,id,parent,ext,cargo,notes,callback) {
    if (isEmpty(cargo) && isEmpty(notes)) {
        get(type,id,parent,ext,(data,err)=> {
            callback(data,err);        
        });
    } else {
        _dicSave(type,id,parent,ext,cargo,notes,function(result,err){
            if (! isEmpty(callback)) {
                callback(result.err);
            }
        });        
    }
    return true;
};
//----------------------------------------------------------------------------------
exports.save = function(type,id,parent,ext,cargo,notes,callback){
    _dicSave(type,id,parent,ext,cargo,notes,function(result,err){
        if (! isEmpty(callback)) {
            callback(result,err);
        }
    });
};
//----------------------------------------------------------------------------------
function _dict(req,res,type,id,parent,ext){
    result= 0;
    var where = "";
    if ( type != "" ) {
        where += ((where == "") ? "where " : " and " ) +"type = '"+ type +"'";
    }
    if ( id != "" ) {
        where += ((where == "") ? "where " : " and " ) +"id = '"+ id +"'";
    }
    if (parent != "") {
        where += ((where == "") ? "where " : " and " ) +" parent = '" + parent +"'"
    }
    if (ext != "") {
        where += ((where == "") ? "where " : " and " ) +" ext = '"+ ext +"'";
    }    
    // add force index for speed and incremental search
    //where = " FORCE INNDEX(type)  "+ where
    var qry = "select  type,id,parent,CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" " + where ; // converts to string
    if (type =='jpg') { // need binary data setup for 
        var stringify = require('csv-stringify');
        var stream = require('stream');
        var str = stringify();
        qry = "select  cargo from "+DICT_TABLE+" " + where ; // converts to string
        var query = conn.query(qry).stream().pipe(str).pipe(res);    
    } else {
        conn.query(qry,function(err, rows, fields){    
            if (err)  console.log("Error getting result 01 : %s ",err );        
            res.send(rows);
        });
    }
    return result;
};
//----------------------------------------------------------------------------------
exports.dict = function(req,res){  // generic dictionary GET
    var pars = getParams(req);
    //console.log("DEBUG,dict,dict.js"+JSON.stringify(pars));
    return _dict(req,res,pars.type,pars.id,pars.parent,pars.ext);
};
//----------------------------------------------------------------------------------
exports.saveFile = function(type,id,parent,ext,cargofile,notesfile, callback){
    var fn =  '';
    var setcargo = false;
    var cargo = '' ;
    var notes = '' ;
    if (isEmpty(cargofile) ){
        if ( ! isEmpty(notesfile) ){
            fn = notesfile;
        }
    } else {
        setcargo = true;
        fn = cargofile;
    }
    if (! isEmpty(fn)) {
        fs.readFile( fn, function (err, data) {           
            if (err) {
                throw err; 
            } else {
                if ( setcargo ) {
                    cargo = data;
                } else {
                    notes = data;
                };
                // store image from file                
                _dicSave(type,id,parent,ext,'','',function(result,err){
                    conn.query("UPDATE "+DICT_TABLE+" SET cargo = ? WHERE id = '"+id+"' and type='"+type+"' and parent = '"+parent+"'", [ data ], function (err) {
                    });
                    if (! isEmpty(callback)) {
                        callback(result,err);
                    };
                });
                
            };
        });
              
    };
};
//----------------------------------------------------------------------------------
exports.getSQN = function(id,callback) {
    var type = "SQN";
    var parent = "00000000001";
    var result = 1;
    if ( isEmpty(id) ) {
        id = "TASKMANGER"
    }
    var incr = 13;    
    var qry = "select count(*) as qty,parent from "+DICT_TABLE+" where type= '"+type+"' and id = '"+id+"'" ;
    conn.query(qry,function(err, rows, fields){       
        if (err)  console.log("Error getting Swquence Number : %s ",err );
        if ( rows && rows.length > 0 ){
            var qty= rows[0].qty;
            if ( qty > 0 ) {
                //increment counter
                result = parseInt(rows[0].parent,10);
                result += incr;
                qry = "UPDATE "+DICT_TABLE+" SET parent='"+result+"' where type='"+type+"' and id='"+id+"'" ;
            } else { 
                result = 1;
                qry = "INSERT INTO "+DICT_TABLE+" SET id='"+id+"', type='"+type+"',parent='"+result+"'" ;
            }    
            conn.query(qry,[], function (err, rows) {
                //console.log(err||rows);   
            });
        }
        callback( result   ,err);               
    });    
}
//----------------------------------------------------------------------------------
exports.GETSET = function(type,id,parent,def,callback){  
    if (! isEmpty(id) ) {
        var ext = "";
        if ( isEmpty(type) ) {
            typet = "XML";
        }
        if ( isEmpty(parent) ) {
            parent = "SOAPENV";
        }
        if ( isEmpty( def )){ def="" } ;
        var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"'" ; 
        conn.query(qry,function(err, rows, fields){
            if (err) {
                callback(def,"Error:Query not found \n "+err)         
                console.log("Error getting data : %s ",err );
            }
            if (rows) {
                if (rows && rows.length > 0 ){
                    var qty= rows[0].qty
                    if ( qty > 0 ) {
                        qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent='"+parent+"'" ;
                        var query = conn.query(qry,[], function (err, rows) {
                                if (err) {
                                    callback(def,JSON.stringify({error:err}));
                                } else {
                                    callback( rows[0].cargo ,"");
                                }
                        });
                    } else {
                        if (! isEmpty(def) ) {
                            _dicSave( type,id,parent,ext, def,'',function(result,err){
                                callback(result,err);
                            });
                        } else {
                                callback("","No Data to save");
                        }
                    };
                }
            }
        });                  
    } else {
        callback("","No ID defined")
    }  
};
//----------------------------------------------------------------------------------
var _dicSave = function(type,id,parent,ext,cargo,notes,callback){
    var d = new Date(); 
    var date = moment(d).format('YYYYMMDD');   
    var date_time = new moment(d).format('YYYYMMDD-hh:mm:ss') + '.' + d.getMilliseconds();
    if ( isEmpty( parent )){ parent='PPORTAL' ; } ;
    if ( isEmpty( ext )){ ext='' ; } ;
    if ( (! isEmpty(id)) && (!isEmpty(type))) { 
        if (ext == "" ) {
            var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"'" ;
        } else {
            var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"' and ext ='"+ext+"'" ;
        }    
        conn.query(qry,function(err, rows, fields){
            if (err)  console.log("Error getting result 02 : %s ",err );
            var qty= 0;
            if (rows && rows.length > 0 ) {
                qty= rows[0].qty
            }
            if ( qty > 0 ) {
                qry = "UPDATE "+DICT_TABLE+" SET cargo='"+cargo+"' ,notes='"+notes+"',date_time='"+date_time+"',ext='"+ext+"', date='"+date+"' where type='"+type+"' and id='"+id+"' and parent ='"+parent+"'" ;
            } else {
                qry = "INSERT INTO "+DICT_TABLE+" SET id='"+id+"', type='"+type+"',parent='"+parent+"', cargo='"+cargo+"', date_time='"+date_time+"', date='"+date+"', ext='"+ext+"'" ;
            };
            var query = conn.query(qry,[], function (err, rows) {
                if (! isEmpty(callback)) {
                    if (isEmpty(err)) {
                        err = qty;    
                    };
                    callback(rows,err);
                }
                console.log(err);   
            });
        
        });   
    }              
    return true;   
};
//----------------------------------------------------------------------------------
exports.dictSave = function(req,res){
    var pars = getParams(req);   
    return save(req,res,pars.type,pars.id,pars.parent,pars.ext,pars.cargo);
};
//----------------------------------------------------------------------------------
// does an isert or update 
/*
function save(type,id,parent,ext,cargo,notes,callback) {   
    var d = new Date(); 
    var date = moment(d).format('YYYYMMDD');   
    var date_time = new moment(d).format('YYYYMMDD-hh:mm:ss') + '.' + d.getMilliseconds();
    if ( (! isEmpty(id)) && (!isEmpty(type)) && (!isEmpty(parent))  ) { 
        if (ext == "" ) {
            var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id = '"+id+"' and parent= '"+parent+"'" ;
        } else {
            var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id = '"+id+"' and parent= '"+parent+"' and ext ='"+ext+"'" ;
        }    
        conn.query(qry,function(err, rows, fields){
            var qty= rows[0].qty ;
            var qry = "INSERT INTO "+DICT_TABLE+" SET ?" ;                    
            if ( qty > 0 ) {
                qry = "UPDATE "+DICT_TABLE+" SET ? where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"' and ext ='"+ext+"'" ;
            } else {
                qry = "INSERT INTO "+DICT_TABLE+" SET ?" ;                    
            }
            var values = {
                type:type,
                id: id,
                parent:parent,
                ext:ext,
                date_time:date_time,
                date:date
            };
            if (! isEmpty(cargo) ) {
                values.cargo = cargo;
            };
            if ( ! isEmpty(notes)) {
                values.notes = notes;
            };
            var query = conn.query(qry,values,function(err, rows){
                if (callback) {
                    callback(err,rows);
                }
            });
        });
    };  
    return true;
}
*/
//----------------------------------------------------------------------------------
_dicGet = function(type,id,parent,ext,callback){
    var date = moment().format('YYYYMMDD');
    if ( isEmpty( ext )){ ext=''  } ;
    //console.log('Get Type='+type+" cargo="+cargo);
    if (ext == "") {
        var qry = "select CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"'" ;
    } else {
        var qry = "select CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"' and ext='"+ext+"'" ;
    }
    console.log("_dicGet qry="+qry);
    conn.query(qry,function(err, rows, fields){
            if (err) {
                console.log("Error getting result 04 : %s ",err );
                callback("",err);
            } 
            if (rows && rows.length > 0 ) {
                cargo = rows[0].cargo
                //console.log(cargo);
                
                callback(rows[0].cargo);
            } else {
                callback("",err);                
            }
    });
    return true;     
};
//----------------------------------------------------------------------------------
exports.getCONFIG = function(id,parent,callback){  
    var type = "CONFIG";
    var qry = "select  type,id as idconf,parent,notes,CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"'" ;
    if (! isEmpty(id)) { 
        qry += " and id='"+id+"' " ;
    } 
    if (! isEmpty(id)) { 
        qry += " and parent='"+parent+"' " ;
    } 
    //var qry = "select  id,parent,notes,CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+"" ; 
    //console.log("getCONFIG.qry="+qry);
    conn.query(qry,function(err, rows, fields){       
        if (err)  console.log("Error getting result 05 : %s ",err );
        if (callback) {
            callback(rows,err);
        } else {
            return rows;
        }              
    });    
}
//-----------------------------------------------------------------------------------
exports.setCONFIG = function(id,parent,cargo,notes){
    var d = new Date(); 
    var date = moment(d).format('YYYYMMDD');   
    var date_time = new moment(d).format('YYYYMMDD-hh:mm:ss') + '.' + d.getMilliseconds();
    var type = 'CONFIG';
    var ext = '';
    var datetime = moment().format();
    if ( isEmpty(notes) ) {
        notes = '';
    }
    //console.log("setCONFIG.cargo="+cargo);
    //return save(req,res,type,id,parent,ext,cargo);
    var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id='"+id+"'" ;
    conn.query(qry,function(err, rows, fields){       
        if (err)  console.log("Error getting result 06 : %s ",err );
            var qty= rows[0].qty
            if ( qty > 0 ) {
                qry = "UPDATE "+DICT_TABLE+" SET cargo='"+cargo+"',notes='"+notes+"', date='"+date+"' where type='"+type+"' and id='"+id+"'" ;
            } else {
                qry = "INSERT INTO "+DICT_TABLE+" SET id='"+id+"', type='"+type+"',parent='"+parent+"',notes='"+notes+"', cargo='"+cargo+"', date='"+date+"', ext='"+ext+"'" ;
            };
            conn.query(qry,function(err, rows, fields){
                if (err)  console.log("Error getting result 07 : %s ",err );
                console.log('Saved Group :'+JSON.stringify(rows));  
            });
    });    
}
//----------------------------------------------------------------------------------
exports.getGRP = function(id,callback){  
    var type = "GRP";
    var qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"'" ; 
    conn.query(qry,function(err, rows, fields){       
        if (err) {
            callback(rows,require('../routes/login').getRoles() ,err);
            console.log("Error dict.getGRP : %s ",err );
        } else {
            if (rows && rows.length > 0 ){
                callback(rows[0].cargo.split(','),require('../routes/login').getRoles(),err);       
            } else {
                callback('','',id+" Not Found");
            }      
        }
    });    
}
//----------------------------------------------------------------------------------
exports.getUSERS = function(callback){  
    var type = "USER";
    var parent = "CREDENTIALS";
    var qry = "select  id as iduser, parent, notes, CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and parent = '"+parent+"'" ; 
    conn.query(qry,function(err, rows, fields){       
        if (err)  console.log("Error getting result 05 : %s ",err );
        callback(rows,err);               
    });    
}
//----------------------------------------------------------------------------------
exports.ISUSER = function(userid,callback){  
    var type = "USER";
    var parent = "CREDENTIALS";
    var qry = "select count(*) as qty from "+DICT_TABLE+" where type= '"+type+"' and parent = '"+parent+"' and id = '"+userid+"'" ;
    var result = false;
    conn.query(qry,function(err, rows, fields){       
        if (err)  console.log("Error getting result 06 : %s ",err );
        if ( rows && rows.length > 0 ){
            var qty= rows[0].qty;
            if ( qty > 0 ) {
                result = true;
            } else { 
                result = false ;
            }    
        }
        callback( result   ,err);               
    });    
}
//----------------------------------------------------------------------------------
exports.getHTM = function(req,res){  
    var id = req.params.id;
    var parent = req.params.parent;
    var ext = req.params.ext;
    var type = "HTM";
    var qry = "select  cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent = '"+parent+"' and ext = '"+ext+"'" ;
    conn.query(qry,function(err, rows, fields){       
        if (err)  console.log("Error getting result 05 : %s ",err );
        if ( rows && rows.length > 0 ) {
            res.setHeader('content-type', 'text/html');
            res.status(200).send(rows[0].cargo);               
        } else {
            res.status(400).send(id+parent+ext+" Help not found");               
        }
    });    
}
//----------------------------------------------------------------------------------
exports.getPDF = function(req,res){  
    var id = req.params.id;
    var parent = req.params.parent;
    var ext = req.params.ext;
    var type = "PDF";
    var qry = "select  cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent = '"+parent+"' and ext = '"+ext+"'" ;
    conn.query(qry,function(err, rows, fields){       
        if (err)  console.log("Error getting result 05 : %s ",err );
        // set header
        if ( rows && rows.length > 0 ) {
            res.setHeader('content-type', 'application/pdf');
            res.status(200).send(rows[0].cargo);               
        } else {
            res.status(400).send(id+parent+ext+" PDF not found");               
        }
    });    
}
//----------------------------------------------------------------------------------
exports.getJPG = function(req,res){  
    var id = req.params.id;
    var parent = req.params.parent;
    var ext = req.params.ext;
    var type = "JPEG";
    var qry = "select  cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent = '"+parent+"' and ext = '"+ext+"'" ;
    conn.query(qry,function(err, rows, fields){       
        if (err)  console.log("Error getting result 05 : %s ",err );
        // set header
        if ( rows && rows.length > 0 ) {
            res.setHeader('content-type', 'application/jpeg');
            res.status(200).send(rows[0].cargo);               
        } else {
            res.status(400).send(id+parent+ext+" JPEG not found");               
        }
    });    
}
//----------------------------------------------------------------------------------
exports.saveGRP = function(id,cargo,parent,callback){
    var type = 'GRP';
    var ext = '';
    var datetime = moment().format();
    var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"'" ;
    //return save(req,res,type,id,parent,ext,cargo);
    conn.query(qry,function(err, rows, fields){       
        if (err)  console.log("Error getting result 06 : %s ",err );
        if (rows && rows.length > 0 ){
            var qty= rows[0].qty
            //console.log("1 "+qty);    
            if ( qty > 0 ) {
                qry = "UPDATE "+DICT_TABLE+" SET cargo='"+cargo+"', date='"+datetime+"' where type='GRP' and id='"+id+"'" ;
            } else {
                qry = "INSERT INTO "+DICT_TABLE+" SET id='"+id+"', type='"+type+"',parent='"+parent+"', cargo='"+cargo+"', date='"+datetime+"', ext='"+ext+"'" ;
            };
            //console.log("2"+qry);
            conn.query(qry,function(err, rows, fields){
                if (err) {
                    callback(rows,err);
                     console.log("Error getting result 07 : %s ",err );
                } else {
                    if (rows && rows.legth > 0) {
                        callback(rows,err);
                    } else {
                        callback('',id+" Not Found");
                    }
                };
            });
            //console.log("3"+qry);
        }
    });    
}
//----------------------------------------------------------------------------------
exports.saveTOKN = function(id,parent,cargo){
    var type = 'TOKN';
    var ext = '';
    var datetime = moment().format('YYYYMMDD');
    if ( isEmpty( parent )){ parent='PPTOKEN' ; } ;
    //console.log('Save Token='+cargo);
    var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"'" ;
        conn.query(qry,function(err, rows, fields){
            if (err)  console.log("Error getting result 08 : %s ",err );
            var qty= rows[0].qty
            if ( qty > 0 ) {
                qry = "UPDATE "+DICT_TABLE+" SET cargo='"+cargo+"', date='"+datetime+"' where type='"+type+"' and id='"+id+"' and parent ='"+parent+"'" ;
            } else {
                qry = "INSERT INTO "+DICT_TABLE+" SET id='"+id+"', type='"+type+"',parent='"+parent+"', cargo='"+cargo+"', date='"+datetime+"', ext='"+ext+"'" ;
            };
            //console.log(qry);
            conn.query(qry,function(err, rows, fields){
                if (err)  console.log("Error getting result 09 : %s ",err );
                //console.log('Saved Token :'+JSON.stringify(rows));  
            });
    });                    
};
//----------------------------------------------------------------------------------
exports.saveUSER = function(userid,cargo,insert,callback){ // cargo = token
    var type = 'USER';
    var ext = '';
    var parent = "CREDENTIALS";
    var datetime = moment().format('YYYYMMDD');
    var id = userid;
    if ( isEmpty( insert )){ insert=false ; } ;
    console.log('Save USER='+cargo+' INS='+insert);
    var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"'" ;
    conn.query(qry,function(err, rows, fields){
            if (err)  console.log("Error getting result 10 : %s ",err );
            var qty = rows[0].qty
            if ( qty > 0 ) {
                qry = "UPDATE "+DICT_TABLE+" SET cargo='"+cargo+"', date='"+datetime+"' where type='"+type+"' and id='"+id+"' and parent ='"+parent+"'" ;
            } else {
                if ( insert) {
                    qry = "INSERT INTO "+DICT_TABLE+" SET id='"+id+"', type='"+type+"',parent='"+parent+"', cargo='"+cargo+"', date='"+datetime+"', ext='"+ext+"'" ;
                }
            };
            console.log(qry);
            conn.query(qry,function(err, rows, fields){
               if (err) {
                    console.log("Error getting result 11 : %s ",err );
               }
               if (callback){
                   callback(rows,err);
               }
            });
    });                    
    return 'OK';
};
//----------------------------------------------------------------------------------
exports.savePWDHistory = function(userid,cargo,callback){ // cargo = token
    var type = 'PWD';
    var ext = '';
    var parent = "PWDHISTORY";
    var datetime = moment().format('YYYYMMDD');
    var id = userid;
    console.log('Save PWD HISTORY='+cargo);
    var qry = "INSERT INTO "+DICT_TABLE+" SET id='"+id+"', type='"+type+"',parent='"+parent+"', cargo='"+cargo+"', date='"+datetime+"', ext='"+ext+"'" ;
    conn.query(qry,function(err, rows, fields){
               if (err) {
                    console.log("Error saving pwd : %s ",err );
               }
               if (callback){
                   callback(rows,err);
               }
    });                    
    return 'OK';
};
//----------------------------------------------------------------------------------
exports.getPWDHistory = function(userid,callback){ // cargo = token
    var type = 'PWD';
    var ext = '';
    var parent = "PWDHISTORY";
    var datetime = moment().format('YYYYMMDD');
    var id = userid;
    console.log('get PWD HISTORY='+userid);
    var qry = "select CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent = '"+parent+"'" ;
    conn.query(qry,function(err, rows, fields){       
        if (err)  console.log("Error getting pwd history : %s ",err );
        callback(rows,err);               
    }); 
    return 'OK';
};
//----------------------------------------------------------------------------------
exports.getURL = function(id,parent,defurl,callback){  
    var type = "URL";
    var ext = '';
    if ( isEmpty(parent) ) {
        parent = "RESTURL";
    }
    if ( isEmpty( defurl )){ defurl="http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.API" } ;
    var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"'" ; 
    //console.log("GetURl.qry="+qry);
    conn.query(qry,function(err, rows, fields){
        if (err) {
            callback(defurl,"{Error:'not found'}")         
            console.log("Error getting result 12 : %s ",err );
        }
        if (rows) {
            if (rows && rows.length > 0 ){
                var qty= rows[0].qty
                //console.log(qty+":"+defurl);
                if ( qty > 0 ) {
                    qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent='"+parent+"'" ;
                    var query = conn.query(qry,[], function (err, rows) {
                            if (err) {
                                callback(defurl,JSON.stringify({error:err}));
                            } else {
                                //console.log("DICTgetuser="+rows[0].cargo);
                                callback( rows[0].cargo ,"");
                            }
                    });
                } else {
                    //console.log("Returning:"+defurl);
                    // save to dict
                    _dicSave(type,id,parent,ext,defurl,'added new url',function(result,err){
                        callback(result,"Error: not found" );
                    });
                };
            }
        }
    });                    
};
//----------------------------------------------------------------------------------
exports.getSQL = function(id,parent,defsql,callback){  
    var type = "SQL";
    var ext = '';
    if ( isEmpty(parent) ) {
        parent = "PP_SQL";
    }
    if ( isEmpty( defsql )){ defsql="select * from portal.users" } ;
    var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"'" ; 
    //console.log("GetSQL.qry="+qry);
    conn.query(qry,function(err, rows, fields){
        if (err) {
            callback(defurl,"{Error:'sql not found'}")         
            console.log("Error getting sql : %s ",err );
        }
        if (rows) {
            if (rows && rows.length > 0 ){
                var qty= rows[0].qty
                //console.log(qty+":"+defsql);
                if ( qty > 0 ) {
                    qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent='"+parent+"'" ;
                    var query = conn.query(qry,[], function (err, rows) {
                            if (err) {
                                callback(defsql,JSON.stringify({error:err}));
                            } else {
                                //console.log("DICTgetuser="+rows[0].cargo);
                                callback( rows[0].cargo ,"");
                            }
                    });
                } else {
                    //console.log("Returning:"+defsql);
                    // save to dict
                    _dicSave( type,id,parent,ext, defsql,'',function(result,err){
                        callback(result,err);
                    });
                };
            }
        }
    });                    
};
//----------------------------------------------------------------------------------
exports.getSOAP = function(id,parent,defenv,callback){  
    var type = "XML";
    var ext = '';
    if ( isEmpty(parent) ) {
        parent = "SOAPENV";
    }
    if ( isEmpty( defenv )){ defenv="" } ;
    var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"'" ; 
    conn.query(qry,function(err, rows, fields){
        if (err) {
            callback(defenv,"{Error:'soap envelope not found'}")         
            console.log("Error getting soap envelope : %s ",err );
        }
        if (rows) {
            if (rows && rows.length > 0 ){
                var qty= rows[0].qty
                if ( qty > 0 ) {
                    qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent='"+parent+"'" ;
                    var query = conn.query(qry,[], function (err, rows) {
                            if (err) {
                                callback(defenv,JSON.stringify({error:err}));
                            } else {
                                callback( rows[0].cargo ,"");
                            }
                    });
                } else {
                    if (! isEmpty(defenv) ) {
                        _dicSave( type,id,parent,ext, defenv,'',function(result,err){
                            callback(result,err);
                        });
                    } else {
                            callback("","No Envelope to save");
                    }
                };
            }
        }
    });                    
};
//---------------------------------------------------
exports.GetWebMsg = function(req, res){
    var id = req.params.id;
    var lang = req.params.lang;
    var ext = req.params.ext;
    var msgdef = req.params.msgdef;
    if (isEmpty(lang)) {
        var lang = req.headers["accept-language"] ;
    }
    getMesg(id,'',lang,msgdef,function(data,err){
        res.send( data);
    });      
};
//----------------------------------------------------
exports.getMSG = function(id,parent,def,lang,callback){
    getMesg(id,parent,lang,def,function(data,err){
        if (! isEmpty(callback)){
            callback(data);
        } else {
            return data;
        }
    });
};
//----------------------------------------------------------------------------------
getMesg = function(id,parent,lang,def,callback){  
    var type = "MESG";
    if ( isEmpty( parent )){ parent="WEB_LOOKUP" } ;
    if ( isEmpty( id )){ id="WEB_001" } ;
    if ( isEmpty( lang )){ lang="en-US" } ;
    if ( isEmpty( def )){ def="No message defined" } ;
    if ( ! isEmpty( lang )){  ext = lang; };
    var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"' and ext='"+lang+"'" ; 
    //console.log(qry);
    conn.query(qry,function(err, rows, fields){
        if (err) {
            callback(def,"Error:'not found'") ;        
            //console.log("Error getting result 13  : %s ",err );
        }
        if (rows) {
            if (rows && rows.length > 0 ){
                var qty= rows[0].qty
                //console.log(qty+":"+def);
                if ( qty > 0 ) {
                    qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent='"+parent+"' and ext ='"+lang+"'" ;
                    var query = conn.query(qry,[], function (err, rows) {
                            if (err) {
                                callback(def,JSON.stringify({error:err}));
                            } else {
                                //console.log("DICTgetuser="+rows[0].cargo);
                                callback( rows[0].cargo ,"");
                            }
                    });
                } else {
                    //console.log("Returning:"+def);
                    // save to dict
                    _dicSave(type,id,parent,ext,def);
                    if (callback) {
                        callback(def,"Error: not found" );
                    } else {
                        return def;
                    }
                };
            };
        };
    });                    
};
//----------------------------------------------------------------------------------
exports.getUSER = function(id,callback){  
    var type = "USER";
    var ext = "";
    var parent = "CREDENTIALS";
    var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"'" ; 
    console.log("DICT.getUSER="+qry);
    conn.query(qry,function(err, rows, fields){
        if (err) {
            callback("",'user not found');
            console.log("Error getting result 14  : %s ",err );
        }
        if (rows) {
            if (rows && rows.length > 0 ){
                var qty= rows[0].qty;
                if ( qty > 0 ) {
                    var qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent='"+parent+"'" ;
                    conn.query(qry,function (err, rows) {
                       if (rows && rows.length > 0 ) {
                            callback(rows[0].cargo,"");
                       } else {
                            var token = login.newToken();
                            token.userid = id; 
                            callback(JSON.stringify(token),"user not found");
                       }
                    });
                } else {
                    var token = login.newToken();
                    //var token = login.newToken();
                    token.userid = id; 
                    callback(JSON.stringify(token),"user not found");
                }
            }
        }
    });                    
};
//---------------------------------------------------------------------------------
_getToken = function(id,callback){
    var parent='PPTOKEN' ; 
    var qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= 'TOKN' and id ='"+id+"' and parent ='"+parent+"'" ; 
    conn.query(qry,function(err, rows, fields){
        if(err) {
            console.log("ERROR,getToken,dict.js"+err )
        };
        if ( rows && rows.length > 0 ) {
            //console.log("Token +_getToken: %s",JSON.stringify(rows[0].cargo));
            callback( JSON.parse(rows[0].cargo) ,"");
        } else {
            callback({error:err},{'dict._getToken.error':'No token found'});
        }           
    });  
}
//----------------------------------------------------------------------------------
exports.getTOKN = function(id,callback){  
    var parent='PPTOKEN' ; 
    var qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= 'TOKN' and id ='"+id+"' and parent ='"+parent+"'" ; 
    conn.query(qry,function(err, rows, fields){
        if(err) {console.log("getTOKN :Error Selecting : %s ",err )};
        if ( rows && rows.length > 0 ) {
            //console.log("Token: getTOKN%s",JSON.stringify(rows[0].cargo));
            callback( rows[0].cargo ,"");
        } else {
            callback("{dict.getTOKN.error:'No token found'}","No token found");
        }           
    });
}
//----------------------------------------------------------------------------------
exports.removeExpiredTOKN = function(parent){
    if ( isEmpty( parent )){ parent='PPTOKEN' ; } ;
    var pType = 'TOKN';
    var qry = "select CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type = '"+pType+"' and parent ='"+parent+"'" ;
    conn.query(qry,function(err, rows, fields){
        if(err) {
            console.log("removeExpiredTOKN:Error Selecting : %s ",err )
        };
        var now = moment().format(tokenExpireFormat);
        if ((!isEmpty(rows)) && (rows.length > 0)) {
            for (var i = 0, len = rows.length; i < len; i++) {
                try {
                    value = JSON.parse(rows[i].cargo);
                    var expire = value.expire;
                    var id  = value.tokenid;
                    var userid = value.userid;
                    //console.log("id="+id+" now="+now+" expire="+expire);
                    if (now > expire )   {         
                        //_log("LOG","EXPIREDTOKEN","LOGGING",rows[i].cargo)
                        var qry = "delete from "+DICT_TABLE+" where type = '"+pType+"' and id ='"+id+"'" ;
                        //console.log('deleting expired:'+qry);
                        conn.query(qry,function(err, rows, fields){
                            //console.log("Deleted "+rows)
                        });                        
                    }    
                } catch (err)  {
                    //_log("ERROR","removeExpiredTOKN","dict.js",err);
                    console.log("removeExpiredTOKN Error: %s",err)
                }                
            };                
        }
    });
    return true;
}
//----------------------------------------------------------------------------------
exports.getMsg = function(req,res){
    var pars = getParams(req);
    pars.type = 'MSG'; 
    return dict(req,res,pars.type,pars.id,pars.parent,pars.ext);
}
//----------------------------------------------------------------------------------
exports.getJson = function(req,res){
    var pars = getParams(req);
    pars.type = 'JSON'; 
    return dict(req,res,pars.type,pars.id,pars.parent,pars.ext);
}

//----------------------------------------------------------------------------------
exports.dicDelete = function(req,res){
    var pars = getParams(req);   
    return del(req,res,pars.type,pars.id,pars.parent,pars.ext);
}
//----------------------------------------------------------------------------------
function del(req,res,type,id,parent,ext) {   
        var qry = "delete from "+DICT_TABLE+" WHERE type=? and id =? and parent = ? and ext = ? " ;
        conn.query(qry,function(err, rows, fields){
            if (err) {
                 res.status(400).send('Error'+err)   
            } else {
                 res.status(200).send('OK'+JSON.stringify(rows) ); 
            }
        });
}

//----------------------------------------------------------------------------------
exports.isDict = function (req,res) {
    var schema = req.query.schema;
    var table = req.query.table;
    var result = 0;
    if (isEmpty( table )){       
        table='dict';
    }
    if ( isEmpty( schema )){
        schema='dict' ;
    }       
    var qry = "SELECT COUNT(*) AS isTable FROM information_schema.tables WHERE TABLE_SCHEMA = '"+schema+"' AND TABLE_NAME = '"+table+"'" ;
    conn.query(qry,function(err, rows, fields){           
        if(err) {           
            result=0;    
            res.status(400).send('Error')                       
        } else {
            result = rows[0].isTable ;
            res.status(200).send(result > 0 ? "Pong " : "No Dictionary found " )  
        }
    });        
    return result;
}    
//----------------------------------------------------------------------------------
exports.dicCreate = function(req,res){  
    var schema = req.param('schema');
    var table = req.param('table');
    return create(req,res,schema,table);
}            
//----------------------------------------------------------------------------------  
create = function (req , res,schema,table){
    var schematable = '';
    if ( isEmpty( schema )){
        schematable += 'dict' ;
    }  else {
        schematable = schema;
    }  
    if ( isEmpty( table )){
        schematable += '.dict' ;
    }  else {
        schematable += '.'+table;
    }    
    var qry = "CREATE TABLE "+schematable+" ( \
                type varchar(10) ,\
                id varchar(20) ,\
                parent varchar(20) ,\
                ext varchar(10) ,\
                cargo blob,\
                date datetime ,\
                notes text,\
                date_time varchar(22) ,\
                flags int(10) unsigned ,\
                KEY data (date_time), \
                KEY type (type,id,parent),\
                KEY parent (type,parent,id)) \
                ENGINE=InnoDB DEFAULT CHARSET=utf8";       
    conn.query(qry,function(err, rows, fields){           
        // Case there is an error during the creation
        if(err) {
           console.log("ERROR,dict.create,DICT:"+err);
            res.status(400).send('Dictionary Create for '+schematable+' ---- '+err)           
        } else {
            console.log("Dictionary  Created");
            res.status(200).send(schematable+ ' Dcitionary created ')  
        }
    });
}
//----------------------------------------------------------------------------------
// upload file by POST
/* sample form for upload
    <div id="uploaddialog">
        <form ref='uploadForm' 
          id='uploadForm' 
          action='/upload' 
          method='post' 
          encType="multipart/form-data">
            <input type="file" name="sampleFile" />
            <input type='submit' value='Upload!' />
        </form>		
    </div>
*/
exports.upload = function(req, res) {
	if (! req.files) {
		res.send('No files to upload.');
		return;
	}
	var sampleFile = req.files.sampleFile;
    var ext =  sampleFile.name.split('.').pop();
    console.log(req.files);
    console.log(sampleFile.name);
    // save to dictionbary
    var qry = "INSERT INTO "+DICT_TABLE+" SET ?",
            values = {
                type:ext.toUpperCase(),
                id: sampleFile.name,
                parent:sampleFile.mimetype,
                ext:ext.toUpperCase(),
                cargo: sampleFile.data
            };
    conn.query(qry, values,function (err, data) {
        if(err) {           
            console.log('upload error');
            res.status(400).send('Error')                       
        } else {
            console.log('Uploaded OK ') 
            res.status(200).send('Uploaded')  
        }
    });
    return  ;
};

