

!function() {
        require('console-stamp')(console, 'yyyymmddHHmmss.l');
        const DICT_TABLE = 'dict';
        let utils = require('../utils') ;
        let moment = require('moment'); // for date time
        let config = utils.getConfig().config;
        let DEBUG =  config['DEBUG'];
        DEBUG = false ;

        let db = config.dict['dba']
        let dba = require('./dic_'+db);
        console.log("using Dict DB= %s\n%j",db,config)
        let dicconfig = exports.config = (devEnv,file,callback) =>{
            //console.log('\nold config \n%j',config)
            config = utils.getConfig(devEnv,file).config;
            //console.log('\nnew config \n%j',config)
            db = config.dict.dba ; 
            dba =  require('./dic_'+db);
            return this;
        }
        exports.getconfig = () =>{
            return config ;
        }
        if (DEBUG) {
            console.log("DICT CONFIG= %j",config)
        }
        let setup = exports.setup = (usedb)=>{
            db = usedb;
            dba = require('./dic_'+db);
        }
        const isEmpty =  utils.isEmpty;
        //-------------------------------------------------------------------------
        let getParams = (req)=> {
            let query = req.query ;
            let params = req.params;
            if (DEBUG) {
                console.log("GetParams incomming query=%j params=%j",query,params)
            }
            var type = params.type;
            var id = params.id;
            var parent = params.parent;
            var ext = params.ext;  
            var notes = params.notes;  
            var cargo = params.cargo;  
            var lang = params.lang;
            var file = params.file;
            if (isEmpty(file)) {  
                file=""; 
                if (! isEmpty(query.file)) {
                    file = query.file;
                }
            }
            if (isEmpty(lang)) {  
                lang = req.headers["accept-language"] ;
                if (! isEmpty(query.lang)) {
                    lang = query.lang;
                }        
            }
            if (isEmpty( type )){ 
                type=""; 
                if (! isEmpty(query.type)) {
                    type = query.type;
                }
            }
            if ( isEmpty( id )){
                id="" ; 
                if (! isEmpty(query.id)) {
                    id = query.id;
                }
            }    
            if ( isEmpty( parent )){ 
                parent="" ; 
                if (! isEmpty(query.parent)) {
                    parent = query.parent;
                }
            }    
            if ( isEmpty( ext )){ 
                ext="" ; 
                if (! isEmpty(query.ext)) {
                    ext = query.ext;
                }
            }     
            if ( isEmpty( cargo )){ 
                cargo="" ; 
                if (! isEmpty(query.cargo)) {
                    cargo = query.cargo;
                }
            }       
            if ( isEmpty( notes )){ 
                notes="" ; 
                if (! isEmpty(query.notes)) {
                    notes = query.notes;
                }
            }       
            //let params = {type:type,id:id,parent:parent,ext:ext,cargo:cargo,notes:notes,lang:lang};// emulate multiple return values
            let pars = {type,id,parent,ext,cargo,notes,lang,file,query,params}; // new es6 way
            if (DEBUG) {
                console.log("getParams.params = %j",pars);
            }
            return(pars); 
        }
        //----------------------------------------------------------------------------------
        let buildWhere = (type,id,parent,ext)=>{
            if (typeof type == 'object'){
                let o = type;
                id = o.id;
                parent = o.parent;
                ext = o.ext ;
                type = o.type;
            }
            let where = "";
            if ( ! isEmpty(type) ) {
                if( typeof type == 'string' ){
                    where += ((where == "") ? "where " : " and " ) +` type = '${type}'`;
                }
            }
            if ( ! isEmpty(id)  ) {
                if( typeof id == 'string' ){
                    where += ((where == "") ? "where " : " and " ) +`id = '${id}'`;
                }
            }
            if (! isEmpty(parent)) {
                if( typeof parent == 'string' ){
                    where += ((where == "") ? "where " : " and " ) +` parent = '${parent}'`
                }
            }
            if (! isEmpty(ext)) {
                if( typeof ext == 'string' ){
                    where += ((where == "") ? "where " : " and " ) +` ext = '${ext}'`;
                }
            } 
            return where;   
        }
        //-----------------------------------------------------------------------------------
        // export data from mysql into sqlite
        exports.mysql2sqlite = (req,res) => {
            let params = getParams(req);    
            let sqlitedic = params.sqlitedic ;
            let mysqldb =  require('./dic_mysql');
            if (isEmpty(sqlitedic) ){
                sqlitedic = '/tmp/dict.export';
            }
            let qry = `select * from dict where type not like '~%'`;
            if (! isEmpty(params.type)) {        
                qry = `select * from dict where type ='${params.type}'`;
            }
            mysqldb.query(qry,function(err, rows, fields){
                let sqlite = require('./dic_sqlite') ;
                sqlite.create(sqlitedic);
                rows.forEach((part, index) => { 
                    if (DEBUG) {
                        console.dir(part.cargo);
                    }
                    res.write("ID = "+part.type + "~"+part.id+"")
                    sqlite.save(part.type,part.id,part.parent,part.ext,part.cargo,part.notes) ;
                    if (index == (rows.length-1) ){
                        res.end("OK") ;     
                        setTimeout(function() { // give time for cache to clear
                            sqlite.close();  
                        }, 1350 );            
                    } 
                }); 
            });
        } 
        //-----------------------------------------------------------------------------------
        // export data from  sqlite into mysql
        exports.sqlite2mysql = (req,res) => {
            let params = getParams(req);    
            let qry = `select * from dict where type ='${params.type}'`;
            let mysqldb =  require('./dic_mysql');
            let sqlite = require('./dic_sqlite') ;
            sqlite.query(qry,function(err, rows){
                rows.forEach((part, index) => { 
                    if (DEBUG) {
                        console.dir(part.cargo);
                    }
                    mysqldb.save(part.type,part.id,part.parent,part.ext,part.cargo,part.notes) ;
                    if (index == (rows.length-1) ){
                        setTimeout(function() { // give time for cache to clear
                            sqlite.close();  
                        }, 1350 );            
                    } 
                }); 
                res.send("OK") ;     
            });
        } 
        //----------------------------------------------------------------------------------------
        exports.backup = (req,res)=> {
            let params = getParams(req);      
            let d = new Date(); 
            let date = moment(d).format('YYYYMMDD');   
            let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
            let where = buildWhere(params) ;
            if ( isEmpty(where) ) { // for backup all
                where = " where type not like '~%'"; 
            }
            let qry = `select type,id,parent,ext,cargo,notes,flags from ${DICT_TABLE} ${where} ` ;
            dba.query(qry, function (err, rows) {
                if (rows && rows.length > 0 ) {
                    rows.forEach(function(element, index) {
                        dba.insert('~'+element.type,element.id ,element.parent, element.ext, element.cargo,element.notes) ;
                    });
                    if (callback) {
                        callback(err,rows)
                    }                     
                }     
            });
        }
        /*
        //-----------------------------------------------------------------------------------
        exports.backup = (req,res)=> {
            let params = getParams(req);    
            //mysql_backup(params.type,params.id,params.parent,params.ext);
            dba.backup(params.type,params.id,params.parent,params.ext);
            res.send("backing up dict"+JSON.stringify(params)+" query ="+JSON.stringify(req.query));
            return 0;
        };
        */
        //--------------------------------------------------------------------------------
        exports.dicexport = (req,res)=> {
            let params = getParams(req);    
            let dir = params.dir;
            if (isEmpty(dir) ){
                dir = '/tmp/dict/export/';
            };
            //let qry = `select * from dict `;
            dba.query(qry,function(err, rows, fields){
                let fs = require('fs');
                try { fs.mkdirSync(dir); } catch(e) { }
                rows.forEach((element, index) => { 
                    let file = dir+`/${element.id}~${element.parent}~${element.ext}.${element.type}` ;
                    fs.writeFileSync(file,element.cargo);
                }); 
            });
            res.send("OK") ;     
        };
        //-----------------------------------------------------------------------------------
        exports.dicimport = (req,res)=> {
            let params = getParams(req);    
            let dir = params.dir;
            let pType = params.type;
            if (isEmpty(dir) ){
                dir = '/tmp/dict/export/';
            };
            if (isEmpty(pType)) {
                pType = '*';
            }
            try { 
                let fs = require('fs');
                let files = fs.readdirSync(dir,'utf8');
                if( files) {
                    files.forEach(file => {
                        let part = file.split('.')[0];            
                        let type = file.split('.')[1];
                        let id = part.split('~')[0];            
                        let parent = part.split('~')[1];
                        let ext = part.split('~')[2];
                        if((pType == '*')  || (type ==  pType)){
                            savefile(type,id,parent,ext,dir+file, (err,data)=>{
                                if (DEBUG ){
                                    console.log("%s,%s ,%s ,%s\n%s",type,id,parent,ext,err)
                                }      
                            }) ;   
                        }   
                    });
                }
            } catch(e){
                console.trace(e);
            }
            res.send("OK "+pType);
        };
        //----------------------------------------------------------------------------------
        exports.save1 = function(req,res){
            var pars = getParams(req);   
            save(pars.type,pars.id,pars.parent,pars.ext,pars.cargo);
            res.send("200 OK");
        };
        //-----------------------------------------------------------------------------------
        exports.list = (req,res)=>{
            var pars = getParams(req);   
            let where = buildWhere(pars);
            //console.log("%j,%s",pars,where);
            //dba.query(`select * from dict ${where} group by type,id,parent,ext `,(err,rows)=>{
            dba.query(`select * from dict ${where} `,(err,rows)=>{
                res.send(err||rows);
            })
            return 0;
        }
        //-----------------------------------------------------------------------------------
        let load = exports.load = (file,callback)=> {
            let fs = require('fs');
            if ( fs.existsSync(file) ) {
                //mysql_import(file,(err,rows)=>{
                dba.import(file,(err,rows)=>{
                    if (callback) {
                        callback(err,rows);
                    } 
                });
            } else {
                console.log("Dictionary export file not found: " + file);        
            }
        };
        //-----------------------------------------------------------------------------------
        let dump = exports.dump = (file,type,id,parent,ext,callback)=> {
            //mysql_export(file,type,id,parent,ext,(err,rows)=>{
            dba.dicexport(file,type,id,parent,ext,(err,rows)=>{
                if (callback) {
                    callback(err,rows);
                } 
            }) ;
        };
        //---------------------------------------------------------------------------------
        let get_ = (type,id,parent,ext,callback)=> {
            if (typeof type =='object') { // pass in param = json
                o = type;
                if (typeof id == 'function'){
                    callback = id;
                }
                id = o.id;       
                parent = o.parent;       
                ext = o.ext;       
                type = o.type;
            }   
            if ( dba.connected() ) {
                dba.get(type,id,parent,ext,(err,data)=>{
                    if(data && data.length > 0 ) {
                        if(DEBUG) {
                            console.log("dict.get len =%s data= %j",data.length,data)
                        } 
                        if (callback) {
                            //callback(err,data[0].cargo);
                            callback(err,data);
                        }            
                    } else {
                        callback(err,"");
                    }
                });
            } else {
                callback("Database not connected","");        
            }
        };
        //---------------------------------------------------------------------------------
        let get = exports.get = (type,id,parent,ext,callback)=> {
            get_(type,id,parent,ext,(err,data)=>{
                if (! isEmpty(data)) {
                    callback(err,data[0].cargo);
                } else {
                    callback(err,'');            
                }
            });
        };
        //----------------------------------------------------------------------------------
        // save a value in cargo  update or insert
        let save = exports.save = (type,id,parent,ext,cargo,callback)=> {
            if (typeof cargo == 'object' ){
                let CJSON = require("circular-json"); 
                cargo = CJSON.stringify(cargo);
            }
            try{
                if ( dba.connected() ) {
                    dba.save(type,id,parent,ext,cargo,'',(err,result)=>{ 
                        if( DEBUG) {
                            console.log("dict.dba_save result =%s",result);
                        }
                        if (callback) {
                            callback(err,result);
                        }
                    });
                } else {
                    callback("Database not connected",cargo);                
                } 
            } catch(ex) {
                console.log("dict.dba_save Error %s",ex)
                if (callback) {
                    callback(ex,cargo);
                }
            }
            return true;
        };
        //----------------------------------------------------------------------------------
        // get or set a value in cargo - mimics clipper set
        let set = exports.set = (type,id,parent,ext,cargo,callback)=> { 
            if(DEBUG) {
                console.log("dict.set type=%s id=%s parent=%s ext=%s cargo=%s",type,id,parent,ext,cargo);
            }
            if (isEmpty(callback)){
                if (! isEmpty(cargo) && typeof cargo === 'function'){
                    callback = cargo;
                    cargo = '';
                }
                if (! isEmpty(ext) && typeof  ext ==='function' ){        
                    callback = ext;   
                    ext = "";
                }
            }
            try {
                if ( dba.connected() ) {
                    get(type,id,parent,ext,(err,data)=> {
                        if (! isEmpty(data)) {    
                            if (callback){
                                callback(err,data);
                            }           
                        } else {
                            if (callback){
                                callback(err,cargo);
                            }
                        }
                        if (! isEmpty(cargo) ) {
                            save(type,id,parent,ext,cargo);
                        };                            
                    });
                }else {
                    if (callback) {
                        callback("Database not connected",cargo);
                    }     
                }
            } catch(ex) {
                console.log("dict.set Error %s",ex)
                if (callback) {
                    callback(ex,cargo);
                }
            }
            return true;
        };
        //----------------------------------------------------------------------------------
        let dict = exports.dict = function(req,res){  // generic dictionary GET
            var pars = getParams(req);
            if ( DEBUG) {
            console.log("DEBUG,dict,dict.js  %j",pars);
            }
            //let fn = eval(db+"_get")
            //fn(pars.type,pars.id,pars.parent,pars.ext,(err,data)=>{
            dba.get(pars.type,pars.id,pars.parent,pars.ext,(err,data)=>{
                res.send(err||data);
            });
        };
        //----------------------------------------------------------------------------------
        let savefile = exports.savefile = function(type,id,parent,ext,cargofile, callback){
            if (! isEmpty(cargofile)) {
                try{
                if (dba.connected() ) {
                        dba.savefile(type,id,parent,ext,cargofile,(err,data)=>{
                            if (callback) {
                                callback(err,data);
                            };
                        })
                } else {
                    console.log("Error : no connection to database","");
                    if (callback) {
                            callback("Error : no connection to database","");
                    }               
                }
                } catch(ex){
                    console.log(ex);
                    if(callback) {
                        callback(ex,"");
                    }
                }
            };
            return true;
        };
        //----------------------------------------------------------------------------------
        exports.getSQN = function(id,callback) {
            let type = "SQN";
            let parent = "";
            let result = 1;
            let ext = "";
            if ( isEmpty(id) ) {
                id = "TASKMANGER"
            }
            let incr = 1;     
            dba.get(type,id,parent,ext,(err,rows)=>{
                //console.log("sqn = %j",rows)
                if (err)  console.log("Error getting Sequence Number : %s ",err );
                if ( rows && rows.length > 0 ){
                    // incr = parseInt(rows[0].ext);
                    result = parseInt(rows[0].parent);
                }
                result += incr;
                result = "1"+("000000000" + result).slice(-9)
                let qry = `update dict set parent = '${result}' where type = '${type}' and id = '${id}' `
                dba.execute(qry,(err)=>{
                    callback( err,result);               
                })
                //dba.save(type,id,result,incr,"","");    
            });    
            return true;
        }
        //----------------------------------------------------------------------------------
        exports.saveGRP = function(id,cargo,parent,callback){
            var type = 'GRP';
            var ext = '';
            save(type,id,parent,ext,cargo,(err,data)=>{
                if (callback) {
                    callback(err,data);
                }
                
            });
        }
        //----------------------------------------------------------------------------------
        exports.getGRP = function(id,callback){  
            var type = "GRP";
            dba.get(type,id,(err,rows)=>{ 
                if ( err) {
                    callback(rows,require('../routes/login').getRoles() ,err);
                    console.log("Error dict.getGRP : %s ",err );
                } else {
                    if (rows && rows.length > 0 ){
                        callback(rows[0].cargo.split(','),require('../routes/login').getRoles(),err);       
                    } else {
                        callback('','',id+" Not Found");
                    }      
                }
            });    
        }
        //----------------------------------------------------------------------------------
        exports.getUSERS = function(callback){  
            let type = "USER";
            let parent = "CREDENTIALS";
            let qry = "select  id as iduser, parent, notes, CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and parent = '"+parent+"'" ;
            dba.get(type,'',parent,'',(err,data)=>{
                if (err)  console.log("Error getting result 05 : %s ",err );
                //console.log(data)
                callback(err,data);               
            });    
        }
        //----------------------------------------------------------------------------------
        exports.ISUSER = function(userid,callback){  
            var type = "USER";
            var parent = "CREDENTIALS";
            //var qry = "select count(*) as qty from "+DICT_TABLE+" where type= '"+type+"' and parent = '"+parent+"' and id = '"+userid+"'" ;
            var result = false;
            dba.get(type,userid,parent,'',(err,rows)=>{
                if (err)  console.log("Error getting result 06 : %s ",err );
                result = ( rows && rows.length > 0 );
                callback( result   ,err);               
            });    
        }
        //----------------------------------------------------------------------------------
        exports.getHTM = function(req,res){  
            var id = req.params.id;
            var parent = req.params.parent;
            var ext = req.params.ext;
            var type = "HTM";
            var qry = `select  cargo from ${DICT_TABLE} where type= '${type}' and id ='${id}' and parent = '${parent}' and ext = '${ext}'` ;
            dba.get(type,id,parent,ext,(err,rows)=>{    
                if (err)  console.log(`Error getting result 05 : ${err}` );
                if ( rows && rows.length > 0 ) {
                    res.setHeader('content-type', 'text/html');
                    res.status(200).send(rows[0].cargo);               
                } else {
                    res.status(400).send(id+parent+ext+" Help not found");               
                }
            });    
            return;
        }
        //----------------------------------------------------------------------------------
        exports.getPDF = function(req,res){  
            var id = req.params.id;
            var parent = req.params.parent;
            var ext = req.params.ext;
            var type = "PDF";
            dba.get(type,id,parent,ext,(err,rows)=>{
                if (err)  console.log("Error getting result 05 : %s ",err );
                // set header
                if ( rows && rows.length > 0 ) {
                    res.setHeader('content-type', 'application/pdf');
                    res.status(200).send(rows[0].cargo);               
                } else {
                    res.status(400).send(id+parent+ext+" PDF not found");               
                }
            });    
        }
        //----------------------------------------------------------------------------------
        exports.getJPG = function(req,res){  
            var id = req.params.id;
            var parent = req.params.parent;
            var ext = req.params.ext;
            var type = "JPEG";
            dba.get(type,id,parent,ext,(err,rows)=>{
                if (err)  console.log("Error getting result 05 : %s ",err );
                // set header
                if ( rows && rows.length > 0 ) {
                    res.setHeader('content-type', 'application/jpeg');
                    res.status(200).send(rows[0].cargo);               
                } else {
                    res.status(400).send(id+parent+ext+" JPEG not found");               
                }
            });    
        }

        //----------------------------------------------------------------------------------
        exports.saveTOKN = function(id,parent,cargo){
            var type = 'TOKN';
            var ext = '';
            var datetime = moment().format('YYYYMMDD');
            if ( isEmpty( parent )){ parent='PPTOKEN' ; } ;
            set(type,id,parent,ext,cargo,(err,data)=>{
                //console.log(err||data);
            });
            return("OK");
        };
        //----------------------------------------------------------------------------------
        let saveUser = exports.saveUSER = function(token,callback){ // cargo = token
            var type = 'USER';
            var ext = '';
            var parent = "CREDENTIALS";
            var id = token.userid;
            if (! isEmpty(id)) {
                //console.log("savaing%s",token)
                save(type,id,parent,ext,token,(err,data)=>{
                    if (callback){
                        callback(err,data);
                    }
                });
            } else{
                console.log("Error saving toke %j",token)
                callback("Error: empty userid",token)
            }
            return 'OK';
        };
        //----------------------------------------------------------------------------------
        exports.savePWDHistory = function(token,callback){ // cargo = token
            var type = 'PWD';
            var ext = '';
            var parent = "PWDHISTORY";
            var id = token.userid;
            //console.log('Save PWD HISTORY= %j',token);
            save(type,id,parent,ext,token,(err,rows)=>{
                if (callback){
                    callback(err,rows);
                }
            });                    
            return 'OK';
        };
        //----------------------------------------------------------------------------------
        exports.getPWDHistory = function(userid,callback){ // cargo = token
            var type = 'PWD';
            var ext = '';
            var parent = "PWDHISTORY";
            var datetime = moment().format('YYYYMMDD');
            var id = userid;
            //console.log('get PWD HISTORY='+userid);
            dba.get(type,id,parent,ext,(err,rows)=>{
                if (err)  console.log("Error getting pwd history : %s ",err );
                callback(err,rows);               
            }); 
            return 'OK';
        };
        //----------------------------------------------------------------------------------
        let getLANG = function(id,parent,lang1,lang2,transl,callback){
            if (isEmpty(lang1)) {
                lang1 ='en-US' ;
            } 
            if (isEmpty(lang2)) {
                lang2 ='es-ES' ;
            }  
            let type = "MESG";
            let ext = lang1;           
let qry = `select 
type,
parent,
id as msgid,
cargo as lang1 ,
(select cargo from dict where type = '${type}' and ext = '${lang2}' and d.id = id and d.parent = parent ) as lang2,
(select ext from dict where type = '${type}' and ext = '${lang2}' and d.id = id and d.parent = parent ) as ext2,
ext,
cargo
from dict d 
where type = '${type}' and ext = '${lang1}'
order by id `
            dba.query(qry,function(err, rows, fields){
                if (err) {
                    console.log("Error getting Messages : %s ",err );
                } 
                let cnt = rows.length ;
                let count = 0;
                if (cnt > 0 ) {
                    rows.forEach(function(element, index) { // convert blob to text from buffer
                            rows[index].cargo = element.cargo.toString()  
                            if (element.lang1) {  
                                rows[index].lang1 = element.lang1.toString()
                            }
                            if (element.lang2) {  
                                rows[index].lang2 = element.lang2.toString()
                            }  
                            if (isEmpty(element.ext2)){
                                rows[index].ext2 = lang2
                            } 
                            if (transl > 0 ) {
                                if (isEmpty(element.lang2)){
                                    // ms lang translate
                                    const BingAppId = '599FA87C780A9743C56B85FA00AF65BA6ED641D3';//'73C8F474CA4D1202AD60747126813B731199ECEA';                                
                                    let http = require('http');
                                    let url = `http://api.microsofttranslator.com/v2/Http.svc/Translate?appId=${BingAppId}&text=${rows[index].lang1}&from=${lang1}&to=${lang2}` ;
                                    http.get(url, (resp) => {
                                        let data = '';
                                        // A chunk of data has been recieved.
                                        resp.on('data', (chunk) => {
                                            data += chunk;
                                        });
                                        // The whole response has been received. Print out the result.
                                        resp.on('end', () => {
                                            count++
                                            //console.log("Result data = %s",data);
                                            if ( data.substring(0,7) == '<string' ) {
                                                let tag = data.match(/<string [^>]+>([^<]+)<\/string>/)[1];
                                                //let tag = utils.Tags(data,'<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">','</string>')[0].name ;
                                                rows[index].lang2 = tag;
                                            } else {
                                                rows[index].lang2 = lang2.substr(0,2) + " not supported - needs translating "//rows[index].lang1                                    
                                            }
                                            if( transl == 2 ) {
                                                try{ 
                                                    let type = rows[index].type ;
                                                    let id = rows[index].msgid ;
                                                    let parent = rows[index].parent ;
                                                    let ext1 = lang1 ;
                                                    let ext2 = lang2 ;
                                                    let cargo1 = rows[index].lang1;
                                                    let cargo2 = rows[index].lang2;
            console.log("SAVE TRANSLATE LANGUAGE \ntype=%s \nid=%s \nparent=%s \next1=%s \next2=%s \ncargo1=%s \ncargo2=%s",type,id,parent,ext1,ext2,cargo1,cargo2)
                                                    
                                                    set(type,id,parent,ext1,cargo1,()=>{
                                                        set(type,id,parent,ext2,cargo2);
                                                    });
                                                } catch(e) {
                                                    console.error(e)
                                                }
                                            }
                                            if (cnt == count) {
                                                callback(err,rows);
                                            }
                                        });        
                                    }).on("error", (err) => {
                                        count++
                                        console.log("Error: " + err.message);
                                        rows[index].lang2 = lang2.substr(0,2) + " not supported - needs translating "//rows[index].lang1
                                        if (cnt == count) {
                                            callback(err,rows);
                                        }
                                    });
                                    /* // google translate
                                    let translate = require('google-translate-api')
                                    translate(rows[index].lang1,{from:`${lang1.substr(0,2)}`,to:`${lang2.substr(0,2)}`}).then(res => {
                                        count ++    
                                        rows[index].lang2 = res.text ;
                                        // save 
                                        if( transl == 2 ) {
                                            try{ 
                                                set(type,id,parent,lang1,rows[index].lang1,(err,data)=>{
                                                    set(type,id,parent,lang2,rows[index].lang2)
                                                } );
                                            } catch(e) {
                                                console.error(e)
                                            }
                                        }
                                        if (cnt == count) {
                                            callback(err,rows);
                                        }
                                    }).catch(err=> {
                                        count++;
                                        rows[index].lang2 = lang2.substr(0,2) + " not supported - needs translating "//rows[index].lang1
                                        if (cnt == count) {
                                            callback(err,rows);
                                        }
                                    });
                                    */
                                } else {
                                    count ++
                                }
                            } else {
                                count ++
                            }
                            if (cnt == count) {
                                console.log("Exit reached")
                                callback(err,rows);
                            }
                    });
                }  else {
                    callback("Error",[]);                    
                }      
            });       
        };
        //----------------------------------------------------------------------------------
        exports.getLANG = (req,res)=>{
            let id = req.query.id;
            let parent = req.query.parent;
            let ext = req.query.ext;
            let lang1 = req.query.lang1;
            let lang2 = req.query.lang2;
            let translate = req.query.translate;
            let type = "MESG";
            if (isEmpty(translate)) {
          //      translate =false ;
            } 
            console.log("\n%s \n%s \n%s \n%s\n%s",id,lang1,lang2,parent,translate)
            getLANG(id,parent,lang1,lang2,translate,(err,data)=>{
                res.send(JSON.stringify(data))
            })
        } 
        //----------------------------------------------------------------------------------
        exports.saveLANG = (req,res)=>{
            let input = JSON.parse(JSON.stringify(req.body));    
            console.log("INPUT:\n%j",input);  
            let id = req.query.id;
            let parent = req.query.parent;
            let ext = req.query.ext;
            let lang1 = req.query.lang1;
            let lang2 = req.query.lang2;
            let type = "MESG";
            if(isEmpty(input.type)) {
                input.type = "MESG"
            }
            if(isEmpty(input.ext)) {
                input.ext = "en-US"
            }
            console.log("\n%s \n%s \n%s \n%s \n%s \n%s \n%s",input.type,input.id,input.lang1,input.lang2,input.parent,input.ext,input.ext2)
            set(input.type,input.id,input.parent,input.ext,input.lang1,()=>{
                set(input.type,input.id,input.parent,input.ext2,input.lang2)
            })
            res.send("OK")
        }
        //----------------------------------------------------------------------------------
        let lookup_lang = exports.lookup_lang = (option,id,parent,callback)=>{
            let langs = require('../languages1.json').lang;
            if(isEmpty(id)) {
                id = "languagelist"
            }
            if(isEmpty(parent)) {
                parent = "languagelookup"
            }
            if(isEmpty(option)) {
                option = 0;
            }
            let olist = "";
            let sellist = "";
            let count = Object.keys(langs).length ;
            let counter = 0 ;
            Object.keys(langs).forEach(function(element,key) {
                counter ++;
                olist += `<option value="${element}">${langs[element]}</option>\n`
                if (counter == count) {
                    if (option) {
                        sellist = olist
                    } else {
                        sellist = `<select name ="${id}" id="${id}" class="${parent}">  ${olist}  </select>` 
                    }
                    callback(sellist);            
                }
            });
        }
        //--------------------------------------------------------------------------------------
        exports.getLangList = (req,res)=>{
            let id = req.query.id;
            let parent = req.query.parent;
            let option = req.query.option;
            lookup_lang(option,id,parent,(sel)=>{
                res.send(sel);
            })
        }        
        //----------------------------------------------------------------------------------
        exports.getURL = function(id,parent,defurl,callback){  
            var type = "URL";
            var ext = '';
            if ( isEmpty(parent) ) {
                parent = "RESTURL";
            }
            set(type,id,parent,ext,defurl,(err,data)=>{
                callback(err,data);
            });
        };
        //----------------------------------------------------------------------------------
        exports.getSQL = function(id,parent,defsql,callback){  
            var type = "SQL";
            var ext = '';
            if ( isEmpty(parent) ) {
                parent = "PP_SQL";
            }
            if ( isEmpty( defsql )){ defsql="select * from portal.users" } ;
            set(type,id,parent,ext,defsql,(err,data)=>{
                callback(err,data);
            });    
        };
        //----------------------------------------------------------------------------------
        exports.getSOAP = function(id,parent,defenv,callback){  
            var type = "XML";
            var ext = '';
            if ( isEmpty(parent) ) {
                parent = "SOAPENV";
            }
            if ( isEmpty( defenv )){ defenv="" } ;
            var qry = "select count(*) as qty from "+DICT_TABLE+" where type = '"+type+"' and id ='"+id+"' and parent='"+parent+"'" ; 
            dba.query(qry,function(err, rows, fields){
                if (err) {
                    callback(defenv,"{Error:'soap envelope not found'}")         
                    console.log("Error getting soap envelope : %s ",err );
                }
                if (rows) {
                    if (rows && rows.length > 0 ){
                        var qty= rows[0].qty
                        if ( qty > 0 ) {
                            qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= '"+type+"' and id ='" +id+"' and parent='"+parent+"'" ;
                            var query = dba.query(qry, function (err, rows) {
                                    if (err) {
                                        callback(defenv,JSON.stringify({error:err}));
                                    } else {
                                        callback( rows[0].cargo ,"");
                                    }
                            });
                        } else {
                            if (! isEmpty(defenv) ) {
                                save( type,id,parent,ext, defenv, (err,result)=>{
                                    callback(result,err);
                                });
                            } else {
                                    callback("","No Envelope to save");
                            }
                        };
                    }
                }
            });                    
        };
        //---------------------------------------------------
        exports.GetWebMsg = function(req, res){
            let id = req.params.id;
            let parent = req.params.parent;
            let lang = req.params.lang;
            let msgdef = req.params.msgdef;
            console.log("Params = %j",req.params)
            console.log("Query = %j",req.query)
            if( isEmpty(parent)) {
                parent = req.query.parent ;
            }
            if (isEmpty(lang)) {
               lang = req.headers["accept-language"] ;
            }
            getMesg(id,parent,lang,msgdef,function(data,err){
                res.send( data);
            });      
        };
        //----------------------------------------------------
        exports.getMSG = getMesg = function(id,parent,lang,def,callback){
            set("MESG",id,parent,lang,def,(err,data)=> {
                    callback(data);
            });
        };
        //----------------------------------------------------------------------------------
        let getEmail = exports.getEmail = (id,parent,ext,cargo,callback)=>{  
            let type = "JSON";
            if ( isEmpty( parent )){ parent="MAIL" } ;
            if ( isEmpty( id )){ id="PP_MAIL" } ;
            if ( isEmpty( ext )){ ext="en-US" } ;    
            get(type,id,parent,ext,(err,data)=>{
                if (isEmpty(data)  ){
                    if ( ! isEmpty(cargo) ) {            
                        save(type,id,parent,ext,cargo,(err,data)=>{
                            if (callback) { callback(err,data ); }
                        });
                    } else {
                        if (callback) { callback(err,data ); }
                    }
                } else {
                    if (callback) {callback(err, data );}
                }
            });
        };
        //----------------------------------------------------------------------------------
        let getJS = exports.getJS = (id,parent,ext,cargo,callback)=>{  
            let type = "JSCRIPT";
            if ( isEmpty( id )){ id="PP_JAVASCRIPT" } ;
            if ( isEmpty( parent )){ parent="PP_PORTAL" } ;
            if ( isEmpty( ext )){ ext="MAIN" } ;    
            get(type,id,parent,ext,(err,data)=>{
                if (isEmpty(data)  ) {
                    if ( ! isEmpty(cargo) ) {            
                        save(type,id,parent,ext,cargo,(err,data)=>{
                            if (callback) { callback(err,data ); }
                        });
                    } else {
                        if (callback) { callback(err,data ); }
                    }
                } else {
                    if (callback) {callback(err, data );}
                }
            });
        };
        //----------------------------------------------------------------------------------
        exports.getUSER = function(id,callback){  
            var type = "USER";
            var ext = "";
            var parent = "CREDENTIALS";
            get(type,id,parent,ext,(err,data)=>{
                if (! isEmpty(data)) {
                    if(callback) {
                        callback(err,data);
                    }
                } else {
                    let login = require('../routes/login');
                    let token = login.newToken();
                    token.userid = id; 
                    if(callback) {
                        callback("user not found",JSON.stringify(token));
                    }
                }
            });
            return 0;
        };
        //----------------------------------------------------------------------------------
        exports.getTOKN = function(id,callback){  
            var parent='PPTOKEN' ; 
            //var qry = "select  CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type= 'TOKN' and id ='"+id+"' and parent ='"+parent+"'" ;
            dba.get("TOKN",id,parent,'',(err,rows)=>{
                if (DEBUG) {        
                    console.log("dict.getTOKN = %j err=%s",rows,err);
                }
                if(rows && rows.length > 0 ) {
                    callback( err,rows[0].cargo);
                } else {
                    callback(err,"")    
                }
            });
            return 0;
        }
        //----------------------------------------------------------------------------------
        exports.getFLOW = function(id,parent,callback){  
            let type = 'JSCRIPT' ;
            let ext = ''; 
            if (isEmpty(id)) {
                id = 'AUTHENTICATE'
            }
            if (isEmpty(parent)) {
                parent='FLOW' ;
            }
            get(type,id,parent,ext,(err,rows)=>{
                if (DEBUG) {        
                    console.log("dict.getFLOW = %j err=%s",rows,err);
                }
                callback( err,rows[0].cargo); // should be a function
            });
            return 0;
        }
        //----------------------------------------------------------------------------------
        exports.removeExpiredTOKN = function(parent){
            if ( isEmpty( parent )){ parent='PPTOKEN' ; } ;
            let pType = 'TOKN';
            let qry = "select CONVERT(cargo USING utf8) as cargo from "+DICT_TABLE+" where type = '"+pType+"' and parent ='"+parent+"'" ;
            dba.get(pType,'',parent,'',(err,rows)=>{
                if(err) {
                    console.log("removeExpiredTOKN:Error Selecting : %s ",err )
                };
                let tokenExpireFormat = 'YYYYMMDDHHmm';
                let now = moment().format(tokenExpireFormat);
                if ((!isEmpty(rows)) && (rows.length > 0)) {
                    for (var i = 0, len = rows.length; i < len; i++) {
                        try {
                            value = JSON.parse(rows[i].cargo);
                            let expire = value.expire;
                            let id  = value.tokenid;
                            let userid = value.userid;
                            if (now > expire )   {         
                                dba.remove(pType,id,'','',(err,rows)=>{
                                    //console.log("Deleted "+rows)
                                });                        
                            }    
                        } catch (err)  {
                            console.trace("removeExpiredTOKN Error: %s",err)
                        }                
                    };                
                }
            });
            return true;
        }
        //----------------------------------------------------------------------------------
        exports.getMsg = function(req,res){
            var pars = getParams(req);
            pars.type = 'MSG'; 
            return dict(req,res,pars.type,pars.id,pars.parent,pars.ext);
        }
        //----------------------------------------------------------------------------------
        exports.getJson = function(req,res){
            var pars = getParams(req);
            pars.type = 'JSON'; 
            return dict(req,res,pars.type,pars.id,pars.parent,pars.ext);
        }

        //----------------------------------------------------------------------------------
        exports.dicDelete = function(req,res){
            var pars = getParams(req);   
            return del(req,res,pars.type,pars.id,pars.parent,pars.ext);
        }
        //----------------------------------------------------------------------------------
        let remove = exports.remove = (type,id,parent,ext,callback)=>{
            dba.remove(type,id,parent,ext,(err,rows)=>{
                if(callback){
                    callback(err,rows) ;
                }
            });
        }
        //----------------------------------------------------------------------------------
        let del = function(req,res,type,id,parent,ext) {   
            remove(type,id,parent,ext,(err,rows)=>{
                if (err) {
                        res.status(400).send('Error'+err)   
                } else {
                        res.status(200).send('OK'+JSON.stringify(rows) ); 
                }
            });
        }
        //----------------------------------------------------------------------------------
        exports.isDict = function (req,res) {
            var schema = req.query.schema;
            var table = req.query.table;
            var result = 0;
            if (isEmpty( table )){       
                table='dict';
            }
            if ( isEmpty( schema )){
                schema='dict' ;
            }       
            var qry = "SELECT COUNT(*) AS isTable FROM information_schema.tables WHERE TABLE_SCHEMA = '"+schema+"' AND TABLE_NAME = '"+table+"'" ;
            dba.query(qry,(err, rows, fields)=>{           
                if(err) {           
                    result=0;    
                    res.status(400).send('Error')                       
                } else {
                    result = rows[0].isTable ;
                    res.status(200).send(result > 0 ? "Pong " : "No Dictionary found " )  
                }
            });        
            return result;
        }    

        //----------------------------------------------------------------------------------
        // upload file by POST
        /* sample form for upload
            <div id="uploaddialog">
                <form ref='uploadForm' 
                id='uploadForm' 
                action='/upload' 
                method='post' 
                encType="multipart/form-data">
                    <input type="file" name="sampleFile" />
                    <input type='submit' value='Upload!' />
                </form>		
            </div>
        */
        exports.upload = function(req, res) {
            if (! req.files) {
                res.send('No files to upload.');
                return;
            }
            var sampleFile = req.files.sampleFile;
            var ext =  sampleFile.name.split('.').pop();
            if (DEBUG) {
                console.log(req.files);
                console.log(sampleFile.name);
            }
            var qry = ` INSERT INTO ${DICT_TABLE} SET ? `;
            let values = {
                        type:ext.toUpperCase(),
                        id: sampleFile.name,
                        parent:sampleFile.mimetype,
                        ext:ext.toUpperCase(),
                        cargo: sampleFile.data
                    };
            dba.query(qry, values, (err, data) => {
                if(err) {           
                    console.log('upload error');
                    res.status(400).send('Error')                       
                } else {
                    console.log('Uploaded OK ') 
                    res.status(200).send('Uploaded')  
                }
            });
            return  ;
        };
        //-----------------------------------------------------------------------------------
        let setuser = function(userid){
            if( isEmpty(userid)){
                userid = 'saadmin' ; 
            }
            try {
                dba.get("USER",userid,"CREDENTIALS",'',(err,data)=>{
                    if (DEBUG) {
                        console.log("setuser default err= %s\n data= %j",err,data);
                    }
                    if (err || data.length == 0 ) {
                        let login = require('../routes/login');
                        let token = login.newToken(userid,'Remember!1');
                        if (DEBUG){
                            console.log("setuser new token = %j",token);
                        }
                        token.valid = 1;
                        token.active = "Yes" ;
                        token.pwdReset = "No";
                        token.group ="SUPERADMIN";
                        token.roles = "PP_ALL",
                        login.newuser(token,(err,data)=>{
                            //console.log(err||data)
                        }) ;            
                        save("USER",userid,"CREDENTIALS",'',token,(err,data)=>{
                            //console.log("setuser token =  %j result=%s",token,err||data);
                        });                
                    }
                });
            } catch(ex){
                console.trace("Error creating user :"+ex)
            }
        }
        //--------------------------------------------------------------------------
        exports.get_ejsTemplate = (name,path,callback)=>{
            if (typeof path == 'function' && isEmpty(callback)){
                callback = path;
                path = config.templatePath;
            }    
            if (isEmpty(path)) {
                path = config.templatePath;
            } 
            get("EJS",name,"TEMPLATE","",(err,template)=>{ //get ejs template from dictionary
                if ( isEmpty(template)){  // fallback to file based if empty
                    try {
                        let fs = require('fs');
                        let filePath = path + `/${name}.ejs`;
                        if ( fs.existsSync(filePath) ) {
                            template = fs.readFileSync(filePath, 'utf8');
                            save("EJS",name,"TEMPLATE","",template);
                            callback("",template);
                        } else {
                            callback(err,template);
                            console.error("DICT:Template File not found:"+filePath );                    
                        }               
                    } catch(e){
                        console.error(e);
                        callback(e,"");
                    }
                } else {
                    callback(err,template);
                }
            });
        }
        //---------------------------------------------------------------------------
        // run first time to set a default user
        setTimeout(function () {
            setuser(); 
        }, 1300);
}();





