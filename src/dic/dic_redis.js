
require('console-stamp')(console, 'yyyymmddHHmmss.l');
let moment = require('moment'); // for date time
let redisClient = require('../redis/redisapi');
let fs = require('fs') ;
let connected = true ;

//----------------------------------------------------------------------------------
exports.connected = ()=>{
    return connected;
}
//----------------------------------------------------------------------------------
let isEmpty = (str)=> { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//----------------------------------------------------------------------------------
let buildWhere = (type,id,parent,ext)=>{
    if (typeof type == 'object'){
        let o = type;
        id = o.id;
        parent = o.parent;
        ext = o.ext ;
        type = o.type;
    }
    return type+"~"+id+"~"+parent+"~"+ext;   
}
//----------------------------------------------------------------------------------
let savefile = exports.savefile = function(type,id,parent,ext,cargofile, callback){
    if (! isEmpty(cargofile)) {
        let d = new Date(); 
        let date = moment(d).format('YYYYMMDD');   
        let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();        
        let fs = require('fs');
        fs.readFile( cargofile, function (err, data) {           
            if (err) {
                console.trace(err); 
            } else {                
                save(type,id,parent,ext,data,'',(err,result)=>{ // save details  
                    if (callback) {
                        callback(err,result);
                    };
                });
            };
        });
    };
};
//---------------------------------------------------------------------------------
let query = exports.query = function(qry,callback){
    //redisClient.DB1.set(parent+'_'+token.tokenid,JSON.stringify(token));
}
//----------------------------------------------------------------------------------
let insert = exports.insert = function(type,id,parent,ext,cargo,notes,callback){
    try {
        if(DEBUG) {
            console.log("redis.insert qry=%s") ;
        }
        redisClient.DB1.set(type+"~"+id+"~"+parent+'~'+ext+"~",cargo);
        if (callback) {
            callback(err,cargo); // return the sent cargo
        }else {
            return {err,rows};  
        }          
    } catch(ex){
        console.trace("dic_redis_save Error: %s",ex);
        callback(ex,[])
    }                  
    return true;
}
//----------------------------------------------------------------------------------
let save = exports.save = function(type,id,parent,ext,cargo,notes,callback){
    let d = new Date(); 
    let date = moment(d).format('YYYYMMDD');   
    let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
    try {
        let where = buildWhere(type,id,parent,ext);
        redisClient.DB1.set(where,cargo);     
    } catch(ex){
        console.trace("dict_pg.save Error: %s",ex);
        callback(ex,[])
    }                  
    return true;   
};
//----------------------------------------------------------------------------------
let get = exports.get = (type,id,parent,ext,callback)=> {
    let result= 0;
    if (isEmpty(callback)){
        if (isEmpty(ext) && typeof parent === 'function'){
            callback = parent;
            parent = "";
        }
        if (typeof  ext === 'function' ){        
            callback = ext;
            ext = "";
        }
    }
    try {
        let where = buildWhere(type,id,parent,ext) ;        
        redisClient.DB1.get(where, function (err, reply) {
            let res = [{
            type:type,
            id:id,
            parent:parent,
            ext:ext,
            cargo:reply}]
            console.log("Redis get.reply %j",res);
            callback(err,res); 
        });        
        
    } catch(ex){
        callback(ex,""); 
        console.trace("catch:"+ex);
    }
    return result;
};
//----------------------------------------------------------------------------------
let dicexport = exports.dicexport =(file,type,id,parent,ext,callback) => {
    let date = new moment().format('YYYYMMDDHHmmss') ;
    let where = buildWhere(type,id,parent,ext) ;
    if ( isEmpty(file) ) {
        file = `/tmp/dict_${date}.csv` ;
    }
  
};
//----------------------------------------------------------------------------------
let dicimport = exports.dicimport = (file,callback) => {
    if ( isEmpty(file) ) {
        file = `/tmp/dict.bak` ;
    }
    if ( fs.existsSync(file) ) {
      
    } else {
        console.log("Import File %s not Found",file);
    }
};
//----------------------------------------------------------------------------------
// backup rows in database for editing tools
let backup = exports.backup = (type,id,parent,ext,callback) => {
    let d = new Date(); 
    let date = moment(d).format('YYYYMMDD');   
    let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
    let where = buildWhere(type,id,parent,ext) ;
    if ( isEmpty(where) ) { // for backup all
        where = " where type not like '~%'"; 
    }
  
};
//----------------------------------------------------------------------------------
let remove = exports.remove = (type,id,parent,ext,callback)=> {   
    let where = buildWhere(type,id,parent,ext) ;

}



   





