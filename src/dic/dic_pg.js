
require('console-stamp')(console, 'yyyymmddhhmmss.l');
let moment = require('moment'); // for date time
const DICT_TABLE = 'dict';
const DICT_DB = 'dict'
const DEBUG = false ;
let pg = require('pg');

let fs = require('fs') ;
var db = '';


//----------------------------------------------------------------------------------
let isEmpty = (str)=> { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//----------------------------------------------------------------------------------
let buildWhere = (type,id,parent,ext)=>{
    if (typeof type == 'object'){
        let o = type;
        type = o.type;
        id = o.id;
        parent = o.parent;
        ext = o.ext ;
    }
    let where = "";
    if ( ! isEmpty(type) ) {
        if( typeof type == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` type = '${type}'`;
        }
    }
    if ( ! isEmpty(id)  ) {
        if( typeof id == 'string' ){
            where += ((where == "") ? "where " : " and " ) +`id = '${id}'`;
        }
    }
    if (! isEmpty(parent)) {
        if( typeof parent == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` parent = '${parent}'`
        }
    }
    if (! isEmpty(ext)) {
        if( typeof ext == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` ext = '${ext}'`;
        }
    } 
    return where;   
}
//----------------------------------------------------------------------------------
let savefile = exports.savefile = function(type,id,parent,ext,cargofile, callback){
    if (! isEmpty(cargofile)) {
        let d = new Date(); 
        let date = moment(d).format('YYYYMMDD');   
        let date_time = new moment(d).format('YYYYMMDD-hh:mm:ss') + '.' + d.getMilliseconds();        
        let fs = require('fs');
        fs.readFile( cargofile, function (err, data) {           
            if (err) {
                console.trace(err); 
            } else {                
                save(type,id,parent,ext,data,'',(err,result)=>{ // save details  
                    if (callback) {
                        callback(err,result);
                    };
                });
            };
        });
    };
};
//---------------------------------------------------------------------------------
let query = exports.query = function(qry,callback){
    //et db = open(DICT_DB,DICT_TABLE);
     db.serialize(function() {
         console.log(qry)
        db.all(qry, function(err, rows) {
            console.log("%s,%j",err,rows)
            if (callback) {
                callback(err,rows) ;
            }
        });         
     });        
}
//----------------------------------------------------------------------------------
let insert = exports.insert = function(type,id,parent,ext,cargo,notes,callback){
    let d = new Date(); 
    let date = moment(d).format('YYYYMMDD');   
    let date_time = new moment(d).format('YYYYMMDD-hh:mm:ss') + '.' + d.getMilliseconds();
    try {
        if(DEBUG) {
            console.log("sqlite.insert qry=%s") ;
        }
     //   let connectionString = `postgres://admin:Remember!1@localhost:5432/${DICT_DB}`
    //db = new pg.Client(connectionString);
   // db.connect();
        let qry = `INSERT INTO ${DICT_TABLE} (type,id,parent,ext,cargo,notes,date,date_time) VALUES ($1,$2,$3,$4,$5,$6,$7,$8) `;
        db.query(qry,[type,id,parent,ext,cargo,notes,date,date_time]);       
    } catch(ex){
        console.trace("dic_pg.mysql_save Error: %s",ex);
        callback(ex,[])
    }                  
    return true;
}
//----------------------------------------------------------------------------------
let save = exports.save = function(type,id,parent,ext,cargo,notes,callback){
    let d = new Date(); 
    let date = moment(d).format('YYYYMMDD');   
    let date_time = new moment(d).format('YYYYMMDD-hh:mm:ss') + '.' + d.getMilliseconds();
    try {
        let where = buildWhere(type,id,parent,ext);
        let qry =  `select count(*) as qty from ${DICT_TABLE} ${where} `
        if ( DEBUG) {
            console.log("dic_pg.save qry=%s",qry) ;
        }
    db.connect();
       let query = db.query(qry);
       query.on("row",(row,result)=>{
            console.log("row = %j",row);
            result.addRow(row);
                if(! DEBUG) {
                    console.log("dic_pg.row =%j",row) ;
                }
                
                if (row && row.qty > 0) { // update    
                    let qry = `UPDATE ${DICT_TABLE} set cargo = ? ${where} ` ;
                    if (DEBUG) {
                        console.log("dic_pg.update qry=%s",qry) ;
                    }
                    
                    
                } else {
                    
                    insert(type,id,parent,ext,cargo,notes);
                }
                
        });       
        query.on("end",(result) =>{
            console.log(result);
                if(result && result.rows[0].qty > 0){
                    console.log("result.rows=%j",result.rows)
                    // insert(type,id,parent,ext,cargo,notes);
                }
        });
        
    } catch(ex){
        console.trace("dict_pg.save Error: %s",ex);
        callback(ex,[])
    }                  
    return true;   
};
//----------------------------------------------------------------------------------
let get = exports.get = (type,id,parent,ext,callback)=> {
    let result= 0;
   
    if (isEmpty(callback)){
        if (isEmpty(ext) && typeof parent === 'function'){
            callback = parent;
            parent = "";
        }
        if (typeof  ext === 'function' ){        
            callback = ext;
            ext = "";
        }
    }
    try {
    //console.log(db)
        let where = buildWhere(type,id,parent,ext) ;
        let qry = `select  type, id as dicid ,parent,ext, cargo from ${DICT_TABLE} ${where} ` ;
        //let qry = `select cargo from ${DICT_TABLE} ${where} `
        let query = db.query(qry);
        console.log("query=%j",query)
        query.on("row", (row,result) =>{
            result.addRow(row);
        }); 
        query.on("end",(result)=>{
            console.log(JSON.stringify(result.rows));
            db.end();
        })
        
    } catch(ex){
        console.trace("catch:"+ex);
    }
    return result;
};
//----------------------------------------------------------------------------------
let dicexport = exports.dicexport =(file,type,id,parent,ext,callback) => {
    let date = new moment().format('YYYYMMDDhhmmss') ;
    let where = buildWhere(type,id,parent,ext) ;
    if ( isEmpty(file) ) {
        file = `/tmp/dict_${date}.csv` ;
    }
    let qry = `SELECT * FROM ${DICT_TABLE} ${where} INTO OUTFILE '${file}' `;
    console.log(qry);
    /*
    conn.query(qry, function (err, rows) {
        if (callback) {
            callback(err,rows);
        } 
        if (DEBUG ){
            console.log(err||rows)
        }
    });
    */
};
//----------------------------------------------------------------------------------
let dicimport = exports.dicimport = (file,callback) => {
    if ( isEmpty(file) ) {
        file = `/tmp/dict.bak` ;
    }
    if ( fs.existsSync(file) ) {
        let qry = `LOAD DATA INFILE '${file}' INTO TABLE ${DICT_TABLE} `;
        /*
        conn.query(qry, function (err, rows) {
            if (callback) {
               callback(err,rows);
            };
            if (DEBUG ){
                console.log(err||rows)
            }
        });
        */
    } else {
        console.log("Import File %s not Found",file);
    }
};
//----------------------------------------------------------------------------------
// backup rows in database for editing tools
let backup = exports.backup = (type,id,parent,ext,callback) => {
    let d = new Date(); 
    let date = moment(d).format('YYYYMMDD');   
    let date_time = new moment(d).format('YYYYMMDD-hh:mm:ss') + '.' + d.getMilliseconds();
    let where = buildWhere(type,id,parent,ext) ;
    if ( isEmpty(where) ) { // for backup all
        where = " where type not like '~%'"; 
    }
    let qry = `select type,id,parent,ext,cargo,notes,flags from ${DICT_TABLE} ${where} ` ;
    /*
    conn.query(qry, function (err, rows) {
        if (rows && rows.length > 0 ) {
            rows.forEach(function(element, index) {
                let post = {id:element.id, type:'~'+element.type,parent:element.parent, ext:element.ext, cargo:element.cargo,notes:element.notes, date_time:date_time, date:date}; 
                qry = "INSERT INTO "+DICT_TABLE+" SET ? "
                let query = conn.query(qry,post, function (err, rows) {
                     //console.log(rows);
                }, this);
            });
            if (callback) {
                callback(err,rows)
            }                     
        }     
    });
    */
};
//----------------------------------------------------------------------------------
let remove = exports.remove = (type,id,parent,ext,callback)=> {   
    let where = buildWhere(type,id,parent,ext) ;
    var qry = `delete from ${DICT_TABLE} ${where} ` ;
    conn.query(qry,function(err, rows, fields){
        if (callback) {
            callback(err,rows) ;
        }
    });
}
//----------------------------------------------------------------------------------
let close = exports.close = ()=>{
    db.close();
}
//----------------------------------------------------------------------------------
let open = exports.create = (schema,table)=>{
    let fs = require('fs');
    if ( isEmpty( schema )){
        schema = 'dict.dic' ;
    }  
    if ( isEmpty( table )){
        table = 'dict' ;
    }        
    let config = {
        user: 'admin', //env var: PGUSER
        database: 'dict', //env var: PGDATABASE
        password: 'Remember!1', //env var: PGPASSWORD
        host: 'localhost', // Server hosting the postgres database
        port: 5432, //env var: PGPORT
        max: 10, // max number of clients in the pool
        idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
    };
    let connectionString = `postgres://admin:Remember!1@localhost:5432/${DICT_DB}`
    db = new pg.Client(connectionString);
    db.connect();
    /*
    pg.connect(connectionString, (err, client, done)=>{
        db = client;
        //Err- This means something went wrong connecting to the database.
        if (err) {
                console.error(err);
                process.exit(1);
        }
        let qry = "CREATE TABLE IF NOT EXISTS dict( \
                type CHAR(10) ,\
                id CHAR(20) ,\
                parent CHAR(20) ,\
                ext CHAR(10) ,\
                cargo BYTEA,\
                date timestamp ,\
                notes TEXT,\ 
                date_time CHAR(22) ,\
                flags INT ); \
                CREATE INDEX type on dict (type,id,parent,ext);"
        try{                
            db.query(qry);
            db.query("CREATE INDEX type ON dict (type, id,parent,ext);");
        } catch(ex){
            console.trace(ex);
        }
        //client.end();         
        console.log("Starting Postgress Server %s",connectionString)      
        return db ;
    });
    */
    return db;

}     

db = open();


   





