
//var arr = [249, 13, 105, 170];
//var sql = "INSERT INTO b SET `data` = CHAR( " + arr + " )";
//client.query(sql);

//new Date().getTime()

/*
Of note: mysql-node automatically converts Blob objects into javascript Buffer objects.
the simplest way to just read it as a string in node : myObject.myBlobAttr.toString('utf-8')
For your convenience, this driver will cast mysql types into native JavaScript types by default. The following mappings exist:
TINYBLOB MEDIUMBLOB LONGBLOB BLOB BINARY VARBINARY BIT (last byte will be filled with 0 bits as necessary)
*/
/* usage for mysql
conn.connect();
conn.query('SELECT 1 + 1 AS solution', function(err, rows, fields) {
  if (err) throw err;

  console.log('The solution is: ', rows[0].solution);
});
// store image from file
fs.readFile("/path/to/some/jpg", function (err, data) { // <-- data is a buffer
    conn.query("UPDATE test_table SET photo = ? WHERE id = 2", [ data ], function (err) {
        // err = null
        // photo is a LONGBLOB, id = 1 has the same photo but inserted using Sequel Pro
        // both getting
    });
});

conn.end();

force mysql to use an index
SELECT a, b, c FROM my_table FORCE INDEX(index_name) WHERE a = {value};

*/
require('console-stamp')(console, 'yyyymmddHHmmss.l');
//let config = require('../../app').config;
let config =  require('../utils').getConfig().conf;
let moment = require('moment'); // for date time
let mysql = require('mysql');
let db = 'mysql';
let login = require('../routes/login');
let connected = false;
let DEVENV = "PROD";
const DICT_TABLE = 'dict';
/*
let mysql_config = {
    host     : '192.168.160.151',
    user     : 'root',
    password : '',
    port : 3306,
    typeCast :true,
    database : 'dict'
}

let xmysql_config = {
    host     : 'localhost',
    user     : 'root',
    password : 'EndorKroner#1',
    port : 3306,
    typeCast :true,
    database : 'dict'
}

let config = {devEnv:"STAGE",
    "DEBUG":true,    
    mysqlconfig:{
    'host'     : '192.168.160.151',
    'user'     : 'root',
    'password' : '',
    'port' : 3306,
    'typeCast' :true,
    'database' : 'dict'},
    mssqlconnection:"mssql://stella_user:heartbeat@192.168.130.109/SW_Reporting"
};
*/ 

config.DEBUG = false;

let conn = null ;
//----------------------------------------------------------------------------------
try {
//    conn = mysql.createConnection(mysql_config,'single');
    conn = mysql.createConnection(config.mysqlconfig,'single');
    conn.connect(function(err) {
        if (err) {
            connected = false;
            console.trace(err)
        } else {
            console.log('Dictionary using mySql %s', config.mysqlconfig.host)
            connected = true;
        }
    })
} catch(ex){
    connected = false;
    console.trace(ex)
}
//----------------------------------------------------------------------------------
module.exports.connected = ()=> {
    return connected;
}
//----------------------------------------------------------------------------------
let isEmpty = (str)=> { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//----------------------------------------------------------------------------------
let buildWhere = (type,id,parent,ext)=>{
    if (typeof type == 'object'){
        let o = type;
        id = o.id;
        parent = o.parent;
        ext = o.ext ;
        type = o.type;
    }
    let where = "";
    if ( ! isEmpty(type) ) {
        if( typeof type == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` type = '${type}'`;
        }
    }
    if ( ! isEmpty(id)  ) {
        if( typeof id == 'string' ){
            where += ((where == "") ? "where " : " and " ) +`id = '${id}'`;
        }
    }
    if (! isEmpty(parent)) {
        if( typeof parent == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` parent = '${parent}'`
        }
    }
    if (! isEmpty(ext)) {
        if( typeof ext == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` ext = '${ext}'`;
        }
    } 
    return where;   
}
//----------------------------------------------------------------------------------
let savefile = exports.savefile = function(type,id,parent,ext,cargofile, callback){
    if (! isEmpty(cargofile)) {
        let fs = require('fs');
        fs.readFile( cargofile, function (err, data) {           
            if (err) {
                console.trace(err); 
            } else {                
                save(type,id,parent,ext,'','',(err,result)=>{ // save details  
                    let where = buildWhere(type,id,parent,ext); // update and add cargo - if not done this way saves stream as data 
                    conn.query(`UPDATE ${DICT_TABLE} SET cargo = ? ${where}  ` , [ data ], function (err) { }); 
                    if (! isEmpty(callback)) {
                        callback(err,data);
                    };
                });                    
            };
        });              
    };
};
//----------------------------------------------------------------------------------
exports.list = (type,id,parent,ext)=>{
    let where = buildWhere(type,id,parent,ext);
    let qry = `select * from dict ${where} `     
    query(qry,(err,rows)=>{
        res.send(err||rows);
    })
    return 0;
}
//---------------------------------------------------------------------------------
let query = exports.query = function(qry,callback){
    conn.query(qry,function(err, rows, fields){
        if(callback){
            callback(err,rows,fields);
        }
    });
}
//----------------------------------------------------------------------------------
let insert = exports.insert = function(type,id,parent,ext,cargo,notes,callback){
    let d = new Date(); 
    let date = moment(d).format('YYYYMMDD');   
    let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
    try {
        let post = {id, type,parent, cargo,notes, date_time, date, ext}; // es6 format 
        qry = ` INSERT INTO ${DICT_TABLE} SET ? `
        let query = conn.query(qry,[post], function (err, rows) { 
            if (callback) {
                callback(err,cargo); // return the sent cargo
            }else {
                return {err,rows};  
            }  
        });
    } catch(ex){
        console.log("dict.mysql_insert Error: %s",ex);
        callback(ex,[])
    }                  
    return true;
}
//----------------------------------------------------------------------------------
let save = exports.save = function(type,id,parent,ext,cargo,notes,callback){
    let d = new Date(); 
    let date = moment(d).format('YYYYMMDD');   
    let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
    try {
        let where = buildWhere(type,id,parent,ext);
        let qry =  `select count(*) as qty from ${DICT_TABLE} ${where} `
        if (config.DEBUG) {
            console.log("dic_mysql_qry count = %s where = %s",qry,where) ;
        }        
        conn.query(qry,function(err, rows){
            if (err)  console.log("Error getting result mysql_dicSave : %s for %s ",err,qry );
            // backup record - should  work only on update not insert as it should not be there
            backup(type,id,parent,ext);          
            let qty= 0;
            if (rows && rows.length > 0 ) {  
                qty = rows[0].qty;
            }            
            let post = {id, type,parent, cargo,notes, date_time, date, ext}; // es6 format 
            if ( qty > 0 ) {
                post = {cargo,notes,date_time,ext,date} // es6
                qry = `UPDATE ${DICT_TABLE} SET ? ${where}` ;
            } else {
                qry = ` INSERT INTO ${DICT_TABLE} SET ? `
            };
            if (config.DEBUG) {
                console.log("dic_mysql_qry save = %s post = %j",qry,post) ;
            }
            let query = conn.query(qry,[post], function (err, rows) { 
                if (callback) {
                    callback(err,cargo); // return the sent cargo
                }else {
                    return {err,rows};  
                }  
            });
            if (config.DEBUG) {
                console.log("mysql_save qry=",query.sql) ;
            }         
        });                
    } catch(ex){
        console.trace("dict.mysql_save Error: %s",ex);
        callback(ex,[])
    }                  
    return true;   
};
//----------------------------------------------------------------------------------
let get = exports.get = (type,id,parent,ext,callback)=> {
    let result= 0;
    let where = buildWhere(type,id,parent,ext) ;
    if (isEmpty(callback)){
        if (isEmpty(ext) && typeof parent === 'function'){
            callback = parent;
            parent = "";
        }
        if (typeof  ext === 'function' ){        
            callback = ext;
            ext = "";
        }
    }
    let qry = `select  type,id as dicid ,parent,ext,CONVERT(cargo USING utf8) as cargo from ${DICT_TABLE} ${where}` ;
    if (config.DEBUG) { 
        console.log("EXT = %s",ext);
        console.log("callback = \n%s",callback);
        console.log(qry);
    }
    try {
        conn.query(qry,function(err, rows, fields){    
          
            if (err) { console.log("dict.mysql_get Error getting result : %s ",err );}
            if (config.DEBUG) {
                console.log("dict.mysql_get ROWS=%j",rows);
            }        
            if (rows && rows.length > 0 ) {
                callback(err,rows);
            } else {
                callback(err,rows);
            }
        });
    } catch(ex){
        callback(ex,[]);
        console.log("dic_mysql.get %s",ex);
    }
    return result;
};
//----------------------------------------------------------------------------------
/*
let dicexport = exports.dicexport = (file,type,id,parent,ext,callback) => {
    let date = new moment().format('YYYYMMDDHHmmss') ;
    let where = buildWhere(type,id,parent,ext) ;
    if ( isEmpty(file) ) {
        file = `/tmp/dict_${date}.csv` ;
    }
    let qry = `SELECT * FROM ${DICT_TABLE} ${where} INTO OUTFILE '${file}' `;
    console.log(qry);
    conn.query(qry, function (err, rows) {
        
        if (callback) {
            callback(err,rows);
        } 
        if (config.DEBUG ){
            console.log(err||rows)
        }
    });
};
*/
//----------------------------------------------------------------------------------
let dicexport = exports.dicexport = (type,id,parent,ext,callback) => {
    let date = new moment().format('YYYYMMDDHHmmss') ;
    let where = buildWhere(type,id,parent,ext) ;
    if ( isEmpty(file) ) {
        file = `/tmp/dict_${date}.csv` ;
    }
    let qry = `SELECT * FROM ${DICT_TABLE} ${where} INTO OUTFILE '${file}' `;
    console.log(qry);
    conn.query(qry, function (err, rows) {
        rows.forEach(function(element, index) {
            fs = require('fs');
            file = `/tmp/${element.id}~${element.parent}~${element.ext}.${element.type}` ;
            fs.writeFile(file,element.cargo, function (err) {
                if (err) {
                    console.log(err);
                }
            });
        });
        if (callback) {
            callback(err,rows);
        } 
    });
};
//----------------------------------------------------------------------------------
let dicimport = exports.dicimport = (file,callback) => {
    if ( isEmpty(file) ) {
        file = `/tmp/dict.bak` ;
    }
    if ( fs.existsSync(file) ) {
        let qry = `LOAD DATA INFILE '${file}' INTO TABLE ${DICT_TABLE} `;
        conn.query(qry, function (err, rows) {
            if (callback) {
               callback(err,rows);
            };
            if (config.DEBUG ){
                console.log(err||rows)
            }
        });
    } else {
        console.log("Import File %s not Found",file);
    }
};
//----------------------------------------------------------------------------------
// backup rows in database for editing tools
let backup = exports.backup = (type,id,parent,ext,callback) => {
    let d = new Date(); 
    let date = moment(d).format('YYYYMMDD');   
    let date_time = new moment(d).format('YYYYMMDD-HH:mm:ss') + '.' + d.getMilliseconds();
    let where = buildWhere(type,id,parent,ext) ;
    if ( isEmpty(where) ) { // for backup all
        where = " where type not like '~%'"; 
    }
    let qry = `select type,id,parent,ext,cargo,notes,flags from ${DICT_TABLE} ${where} ` ;
    conn.query(qry, function (err, rows) {
        if (rows && rows.length > 0 ) {
            rows.forEach(function(element, index) {
                let post = {id:element.id, type:'~'+element.type,parent:element.parent, ext:element.ext, cargo:element.cargo,notes:element.notes, date_time:date_time, date:date}; 
                qry = "INSERT INTO "+DICT_TABLE+" SET ? "
                let query = conn.query(qry,post, function (err, rows) {
                     //console.log(rows);
                }, this);
            });
            if (callback) {
                callback(err,rows)
            }                     
        }     
    });
};
//----------------------------------------------------------------------------------
var remove = exports.remove = (type,id,parent,ext,callback)=> {   
    let where = buildWhere(type,id,parent,ext) ;
    if (! isEmpty(where)) {
        var qry = `delete from ${DICT_TABLE} ${where} ` ;
        conn.query(qry,function(err, rows, fields){
            if (callback) {
                callback(err,rows) ;
            }
        });
    }
}
//----------------------------------------------------------------------------------
exports.create = function(config,schema,table,force,callback){  
    createdict(config,schema,table,force,(err,result)=>{
        if( callback) {
            callback(err,result);
        }
    });        
    return 0;
}     
//----------------------------------------------------------------------------------
let isDict = (conn,schema,table,callback) => {
    let result = 0;
    if (isEmpty( table )){       
        table='dict';
    }
    if ( isEmpty( schema )){
        schema='dict' ;
    }       
    let qry = "SELECT COUNT(*) AS isTable FROM information_schema.tables WHERE TABLE_SCHEMA = '"+schema+"' AND TABLE_NAME = '"+table+"'" ;
    query(qry,(err, rows, fields)=>{
        if(rows && rows.length > 0 ){           
            result =  rows[0].isTable ;
            if(callback){
                callback(err,result)
            }
        }
    });        
    return result;
}    
//----------------------------------------------------------------------------------  
let createdict = function (config,schema,table,force,callback){
    let schematable = '';
    // get connection
    if ( isEmpty( schema )){
        schema = 'dict'
    }
    if ( isEmpty( table )){
        table = "dict";
    }
    if (! isEmpty(force)) {
        if (typeof force == 'function' && isEmpty(callback)) {
            callback = force ;
            force = false;
        }
    } else {
        force = false;
    }
    if (isEmpty(config)) {
        config = {
            host     : 'localhost',
            user     : 'root',
            password : 'EndorKroner#1',
            port : 3306,
            typeCast :true,
            database : schema
        }
    }
    try{
        let connection = mysql.createConnection(config,'single');
        connection.connect((err)=>{
            if (! err) {
                isDict(connection,schema,table,(err,result)=>{
                if (result = 0 || force) { 
                        let qry = `CREATE TABLE ${schematable} ( \
                                    type varchar(10) ,\
                                    id varchar(20) ,\
                                    parent varchar(20) ,\
                                    ext varchar(10) ,\
                                    cargo blob,\
                                    date datetime ,\
                                    notes text,\
                                    date_time varchar(22) ,\
                                    flags int(10) unsigned ,\
                                    KEY data (date_time), \
                                    KEY type (type,id,parent),\
                                    KEY parent (type,parent,id)) \
                                    ENGINE=MyISAM  DEFAULT CHARSET=utf8`;       
                        connection.query(qry,function(err, rows, fields){
                            connection.close()       
                            callback(err||rows,1);    
                        });
                    }
                });
            } else {
                connected = false;
                callback("Error Connweting:"+schematable,0)
            }
        });
    } catch(ex) {
        connected = false;
        callback(ex,0);    
    }
    return 0;
}








