

// add timestamps in front of log messages
require('console-stamp')(console, 'yyyymmddHHmmss.l');

let dict = require('./dic/dict');
let CJSON = require('circular-json');
let moment = require('moment'); // for date time
let mysql = require('mysql');
let users = require('./routes/users');
let documents = require('./routes/documents');
let email = require('./mail');
//let redisClient = require('./redis/redisapi');
let config = require('./utils').getConfig().conf ;

let DEBUG = true;

let conn = mysql.createConnection(config.mysqlconfig,'single');

const FLOW_TABLE = 'portal.workflow_scheduler';
const aStataStatusus = ["REFRESH_START","REFRESH_REQUESTED","REFRESH_ATTEMPT_1","REFRESH_ATTEMPT_2","REFRESH_FAILED","REFRESH_FULFILLED","REFRESH_FAILED_NO_ID","REFRESH_FAILED_NO_PHRID","REFRESH_FAILED_USER_NOT_FOUND"] ;
//-----------------------------------------------------------
let updateflow = function(user,status,updatedoc) {
    if (isEmpty(updatedoc)) {
        updatedoc = false;
    }
    let user_id = user.user_id;
    let qry =`update  ${FLOW_TABLE} set last_updated = NOW() ,status = '${status}' `;    
    if (updatedoc &&  (status == "REFRESH_FULFILLED" )){    
        qry += " , last_successfully_updated = NOW() ";
    }
    qry += ` where user_id = '${user_id}' and  upper(sche_type)='DOC_REFRESH'` ;
    if (DEBUG) {
        console.log("Doc Refresh udateFLOW qry="+qry);
    }
    conn.query(qry,[user_id], function(err, result)  {
            if (DEBUG) {
                console.log("Doc Refresh updateflow result = %s",err||JSON.stringify(result));
            }
    });
    if (status == "REFRESH_FAILED" ) {
        refreshFailed(user);
    }
    return 0;
};
//-----------------------------------------------------------
let flowlist = exports.flowlist = function(user_id,callback) {
    var qry = `Select user_id,upper(sche_type) as sche_type,upper(status) as status,last_updated,last_successfully_updated from ${FLOW_TABLE} order by last_updated desc `;    
    //console.log("FLOW_LIST qry="+qry);
    conn.query(qry,[], function(err, result)  {
        if (DEBUG) {
            //console.log("DocRefresh flowlist result = %s",err||JSON.stringify(result));
        }
       if (callback) {
           callback(result,err);
       }
    });
    return 0;
};
//---------------------------------------------------
exports.list = function(req,res){
    flowlist('',(data,err)=>{
        res.send(data);            
    });
    return 0;
}
//---------------------------------------------------
exports.save_edit = function(req,res){
    let data = req.body;
    let qry = `select count(*) as qty from ${FLOW_TABLE} where user_id = '${data.user_id}' and sche_type = '${data.sche_type}' `
    conn.query(qry, function(err, rows)  {
        if (err)  console.log("Error Updating : %s ",err );                        
        if (rows && rows.length > 0 && rows[0].qty > 0 ){ //} then update        
            qry = `update ${FLOW_TABLE} set status = '${data.status}' , last_updated = NOW() `;
            if (data.status == 'REFRESH_FULFILLED'){
                qry += `,last_successfully_updated = NOW() `
            }    
            qry +=  ` where user_id = '${data.user_id}' and sche_type = '${data.sche_type}' ` ;
        } else { 
            qry = `insert into ${FLOW_TABLE}(user_id,status, sche_type,last_updated ) Values('${data.user_id}','${data.status}','${data.sche_type}' ,NOW())`;
        }
        console.log("DOCREFRESH save = %s",qry)
        conn.query(qry, (err, result) => {
            if (! err ) {
                // send email
                users.userlist(data.user_id,'',(list)=> { // should only be one
                    if (list.length > 0 ) {
                        let user = list[0];
                        if (DEBUG) {
                            console.log("send Email save_edit for %j",user);
                        }
                        //emailNewDocRequest(user);
                        res.send("OK");
                    }
                })
            } else {
                res.send("Error")
            }
            if (DEBUG) {
                console.log("Doc Refresh insertflow result = %s",err||JSON.stringify(result));
            }
        });
    });
    return 0;
}
//---------------------------------------------------
exports.insert = function(req,res){
    let data = req.body;
    //let qry = `insert into ${FLOW_TABLE}(user_id,status, sche_type,last_updated ) Values('${data.user_id}','${data.status}','${data.sche_type}' ,NOW())`;
    let qry = `select count(*) as qty from ${FLOW_TABLE} where user_id = '${data.user_id}' and sche_type = '${data.sche_type}' `
    conn.query(qry, function(err, rows)  {
        if (err)  console.log("Error Updating : %s ",err );                        
        if (rows && rows.length > 0 && rows[0].qty > 0 ){ //} then update        
            qry = `update ${FLOW_TABLE} set status = '${data.status}' , last_updated = NOW() `;
            if (data.status == 'REFRESH_FULFILLED'){
                qry += `,last_successfully_updated = NOW() `
            }    
            qry +=  ` where user_id = '${data.user_id}' and sche_type = '${data.sche_type}' ` ;
        } else { 
            qry = `insert into ${FLOW_TABLE}(user_id,status, sche_type,last_updated ) Values('${data.user_id}','${data.status}','${data.sche_type}' ,NOW())`;
        }
        conn.query(qry, (err, result) => {
            if (! err ) {
                // send email
                users.userlist(data.user_id,'',(list)=> { // should only be one
                    if (list.length > 0 ) {
                        let user = list[0];
                        if (DEBUG) {
                            console.log("send Email save_edit for %j",user);
                        }
                        //emailNewDocRequest(user);
                        res.send("OK");
                    }
                })
            } else {
                res.send("Error")
            }
            if (DEBUG) {
                console.log("Doc Refresh insertflow result = %s",err||JSON.stringify(result));
            }
        });
    });
    return 0;
}
//---------------------------------------------------
exports.html = function(req, res){
    res.render('ppDocRefresh',{page_title:"Patient Portal Doc Refresh "});                        
    return 0;  
};
//---------------------------------------------------
exports.tab = function(req, res){
    res.render('ppDocRefresh1',{page_title:"Patient Portal Doc Refresh "});                        
    return 0;  
};
//--------------------------------------------------
let getHSDoc = (doc_id,count,callback) =>{
    console.log("Get HS DOC\ndoc_id = %s\ncount = %s",doc_id,count)
    setTimeout( () => {
        documents.SPRLDocReg(doc_id,(data,resp)=>{
            console.log("Response for DocID %s",doc_id)
            if(callback){
                callback(data,resp)
            }
        });
    }, count * 300 );
}
//---------------------------------------------------
let DocProcess = exports.DocProcess = function(){
    let sets = require("./sets");
    let set = new sets.Set( ["REFRESH_START","REFRESH_REQUESTED","REFRESH_ATTEMPT_1","REFRESH_ATTEMPT_2"] ); // only allow these in
    let counter = 0 ;    
    try {
        //let test = `
        //let config = require('./utils').getConfig().conf ;
        if (DEBUG) {
            //console.log("Doc Process Called for %s debug=%s flw=%s",config.devEnv,DEBUG,config.RefreshFlowDoc);
        }
        let qry = "Select user_id,upper(sche_type) as sche_type,upper(status) as status,last_updated,last_successfully_updated from "+FLOW_TABLE+" where upper(sche_type) = 'DOC_REFRESH' ";    
        conn.query(qry, function(err, rows)  {
            if (rows && rows.length > 0 ) {
                rows.forEach( function(userrow, index, theArray) {
                    let now = moment().format('YYYYMMDDHHmm') ;
                    let s = userrow.status;
                    let user_id = userrow.user_id;                 
                    let status = userrow.status;                 
                    let lastdate = moment(userrow.last_updated).add(config.RefreshFlowDoc, 'minutes').format('YYYYMMDDHHmm');
                    // do not process unneeded
                    if ( set.contains([status]) ) {  // only call needed
                        // check if time < than 20 minutes don't processes otherwise
                        console.log("\nstatus=%s\nNOW =    %s \nupdate = %s\nuserid=%s\nnow>upd %s",status,now,lastdate,user_id,(now > lastdate))
                        if (now > lastdate) {
                            users.userlist(user_id,'',(list)=>{ // should only be one
                                counter ++;
                                console.log("userrow = \n%j \ncounter= %s",userrow,counter)
                                console.log("userlist = \n%j",list)
                                if (list.length > 0 ) {
                                    let user = list[0];      
                                    if (! isEmpty(user.verizon_id) && user.verizon_id != 'undefined'){              
                                        if( DEBUG) {
                                            console.log("DocRefresh Process now= %s , expire= %s",now,lastdate);
                                            console.log("DocRefresh Processing %s for %s phrid=%s ",s,user_id,user.verizon_id);
                                        }
                                        let doc_id = user.verizon_id ; 
                                        doc_id += isEmpty(user.client_OID) ? "" : "~"+user.client_OID ;
                                        if( DEBUG) {
                                            console.log("doc_id= %s",doc_id)
                                        }     
                                        if (s == "REFRESH_START") {  //refresh start send a sprl request
                                            getHSDoc(doc_id,counter,(data,err)=>{
                                                //console.log("Doc request Result err= %s",CJSON.stringify(err))
                                            });         
                                            updateflow(user,"REFRESH_ATTEMPT_1"); //
                                            //documents.SPRLDocReg(doc_id,(data,err)=>{});         
                                        }
                                        if (s == "REFRESH_REQUESTED") {  //refresh requested 
                                            getHSDoc(doc_id,counter,(data,err)=>{
                                                //console.log("Doc request Result err= %s",CJSON.stringify(err))
                                            });         
                                            updateflow(user,"REFRESH_ATTEMPT_1"); //
                                            //documents.SPRLDocReg(doc_id,(data,err)=>{});         
                                        } else if (s == "REFRESH_ATTEMPT_1") { 
                                            getDoc(user,(doc)=>{
                                               if( DEBUG) {
                                                    console.log("DocProcess %s for %s doc %s",s,user_id,doc);
                                               }
                                                if ( ! doc ) {                                
                                                    getHSDoc(doc_id,counter,(data,err)=>{         
                                                    //documents.SPRLDocReg(doc_id,(data,err)=>{                                        
                                                        if (DEBUG) {
                                                            //console.log("Doc request Result err= %s",CJSON.stringify(err))
                                                        }                                         
                                                    });
                                                    updateflow(user,"REFRESH_ATTEMPT_2"); // attempt 2                                   
                                                } else {
                                                    refreshFullfilled(user,true);  
                                                }
                                            });
                                        } else if (s == "REFRESH_ATTEMPT_2") {
                                            getDoc(user,( doc)=>{
                                                if( DEBUG) {
                                                    console.log("DocProcess %s for %s doc %s",s,user_id,doc);
                                                }
                                                if ( ! doc ) {                                
                                                    // build new api with ~
                                                    //documents.SPRLDocRegister(user_id,(data,err)=>{
                                                    getHSDoc(doc_id,counter,(data,err)=>{         
                                                    //documents.SPRLDocReg(doc_id,(data,err)=>{                                        
                                                        if (DEBUG) {
                                                            //console.log("Doc request Result err= %s",CJSON.stringify(err))
                                                        }                                        
                                                    });
                                                    updateflow(user,"REFRESH_FAILED"); // failed                                 
                                                } else {
                                                    refreshFullfilled(user,true);  
                                                };
                                            });
                                        }
                                    } else {
                                        console.error("NO phr id for user %j",user)    
                                        updateflow(user,"REFRESH_FAILED_NO_PHRID"); // failed     
                                    }// is empty verizon
                                } else {
                                    console.error("NO user found for id %s",user_id)    
                                    updateflow(user,"REFRESH_FAILED_USER_NOT_FOUND"); // failed                                         
                                }
                            });
                        };
                    };
                });
            }
        }); 
        //`
        //eval(test);         
        //evalFlow('DOC_FLOW',test,(data)=>{
        //    eval(data);
        //});        
    } catch(ex) {
        console.trace(ex)
    }
    return 0;
}
//-----------------------------------------------------
let evalFlow = (status,defJScript,callback)=>{
    dict.get("JSCRIPT",status,"PP_FLOW",'',(err,data)=>{  // get javascript code for the flow
        if ( isEmpty(data) ) {
            console.log(err);
            dict.set("JSCRIPT",status,"PP_FLOW",'',defJScript);       
            callback(defJScript);
        } else {    
            callback(data);
        }
    }); 
    return 0;
}
//---------------------------------------------------------------------------------------
let getDoc = function(user,callback) {
   documents.userDocList(user.user_id,'Approved',(docs,response)=>{
        let doc = "";
        if (DEBUG) {
            console.log("ppDocRefresh for %s \ngetDoc obj= >%j<",user.user_id,docs)
        }
        if ( typeof docs === 'string'){
            doc = JSON.parse(docs);
            console.log("STRING doc =JSON.parse(docs)  doc=%j ",doc)
        }
        if (typeof docs === 'object' ) {
            //{"type":"Buffer","data":[91,93]}
            if (docs.type && docs.type == 'Buffer') { 
                doc = JSON.parse(docs.toString())                       
                console.log("BUFFER JSON.parse(docs.toString())  doc=%j ",doc)
                doc = JSON.parse(docs)                       
                console.log("BUfFER JSON.parse(docs)  doc=%j ",doc)
                
            } else {
                doc = docs;
                console.log("ppDocRefresh object jdocs= >%j<",doc)
                console.log("ppDocRefresh object sdocs= >%s<",doc)
            }
        }
        if (DEBUG) {
            console.log("ppDocRefresh \nsDoc= >%s<",doc)
            console.log("ppDocRefresh \njDoc= >%j<",doc)
        }
        if (doc && ! isEmpty(doc)){
            console.log("ppDocRefresh Found DOC ");
            callback(true) ;
        } else {
            console.log("ppDocRefresh NOT Found DOC ");
            callback(false);
        }    
    });
    return 0;
}
//---------------------------------------------------------------------------------------
exports.test = function(req,res){
    if (DEBUG) {        
        console.log("Doc Proccess Started"); 
    }
    let intervalId = setInterval(() => { 
        DocProcess();
    },10*60*1000 );               
    res.send("Process Started");
    return 0;
}
//---------------------------------------------------------------------------------------
exports.test1 = function(req,res){
    //refreshFullfilled('bpoulos',false);
    users.userlist('snosal','',(data,err)=>{
        let user = data[0] ;
        refreshFailed(user);
        refreshFullfilled(user);
    });
    res.send("ccOK");
    return 0;
}
//---------------------------------------------------------------------------------------
let refreshFullfilled = function(user,updatedoc) {
    if ( isEmpty(updatedoc) ) {
        updatedoc = false;
    }
    updateflow(user,"REFRESH_FULFILLED",updatedoc); //refresh fulfilled
    // get doc into redis  
    fetchDoc(user);
    //send email   
    emailFullfilled(user);
    /*
    let oMail = {
                text:   
`Dear %(first_name)s %(last_name)s, 
Per your request, your clinical information was updated successfully.
Please follow this link %(client_url)s  to login and view your most recent medical data.
If you have any questions, please do not hesitate to contact us.

Thank you,
NYPP Support team 

1.888.633.6706
nyppsupport@nyehealth.org
` ,
                from:   "nyppsupport@nyehealth.org", 
                to:     '%(email)s',
                cc:     "",
                bcc:    "nyppsupport@nyehealth.org;dpal@nyehealth.org",
                subject: "Do not Reply: PP Account Document Refresh"
            }   
            email.sendEmail(user,'PP_REFRESHDOC_OK',oMail);
    */
    return 0;
};
//---------------------------------------------------------------------------------------
let emailFullfilled = function(user) {
    //send email   
    let oMail = {
                text:   
`Dear %(first_name)s %(last_name)s, 
Per your request, your clinical information was updated successfully.
Please follow this link %(client_url)s  to login and view your most recent medical data.
If you have any questions, please do not hesitate to contact us.

Thank you,
NYPP Support team 

1.888.633.6706
nyppsupport@nyehealth.org
` ,
                from:   "nyppsupport@nyehealth.org", 
                to:     '%(email)s',
                cc:     "",
                bcc:    "nyppsupport@nyehealth.org;dpal@nyehealth.org",
                subject: "Do not Reply: PP Account Document Refresh"
            }   
            email.sendEmail(user,'PP_REFRESHDOC_OK',oMail);
            console.log("Refresh Fulfilled and email sent")
    return 0;
};
//--------------------------------------------------------------------------------------
let fetchDoc = function(user){ // get ondemand document
    // get mpi for user
    users.userlist(user.user_id,'',(data,err)=>{
        let mpiid = data[0].mpiid ;
        let verzid = data[0].verizon_id;
        if (DEBUG) {
            console.log("REDIS GET DOC verzid= %s, MPIID= %s",verzid,mpiid);
        }
        documents.getOndemandDoc(mpiid,'XML',(doc,err)=>{
            // send data to redis
            let k = 'nyecDocumentTempKey+'+verzid;
            if (DEBUG) {
                console.log("REDIS KEY = %s",k);
            }
            if( typeof doc == 'object' || typeof doc == 'array'){
                doc = JSON.stringify(doc);
            }
            let redisClient = require('./redis/redisapi');
            redisClient.DB0.set(k,doc);
        });    
    });
    return 0;
};
                    
//---------------------------------------------------------------------------------------
let emailNewDocRequest = function(user) {
    //send email
            let oMail = {
                text:   
`Dear %(first_name)s %(last_name)s,

A Document refresh was sent.

Please follow this link %(client_url)s to login and view your most recent medical data.

If you have any questions, please do not hesitate to contact us.

Thank you,
NYPP Support team

1.888.633.6706
nyppsupport@nyehealth.org
` ,
                from:   "nyppsupport@nyehealth.org", 
                to:     "%(email)s",
                cc:     "",
                bcc:    "nyppsupport@nyehealth.org;dpal@nyehealth.org",
                subject: "Do not Reply: PP Account Document Refresh"
            };   
            email.sendEmail(user,"PP_REFRESHDOC_FAILED",oMail);
    return 0;
};


//---------------------------------------------------------------------------------------
let refreshFailed = function(user) {
    //send email
            let oMail = {
                text:   
`Dear %(first_name)s %(last_name)s,
Per your request, we tried and failed to refresh your clinical information,
We have escallated the issue to the NYeC operations team and you will notified through email on successful update.
Please follow this link %(client_url)s to login and view your most recent medical data.

If you have any questions, please do not hesitate to contact us.

Thank you,
NYPP Support team

1.888.633.6706
nyppsupport@nyehealth.org
` ,
                from:   "nyppsupport@nyehealth.org", 
                to:     "%(email)s",
                cc:     "",
                bcc:    "nyppsupport@nyehealth.org;dpal@nyehealth.org",
                subject: "Do not Reply: PP Account Document Refresh"
            };   
            email.sendEmail(user,"PP_REFRESHDOC_FAILED",oMail);
    return 0;
};









