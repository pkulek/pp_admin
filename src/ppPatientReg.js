

// add timestamps in front of log messages
require('console-stamp')(console, 'yyyymmddHHmmss.l');

const SCHEDULER_NAME = "PATIENT_REGISTER"
//const FLOW_TABLE = 'portal.doc_refresh';
const FLOW_TABLE = 'portal.workflow_scheduler';
const QEs =  [ 'HEALTHLINKNY','HEALTHIX','HEALTHELINK' ,'HIXNY','BRONX','NYCIG', 'HEALTHECONN' ,'GRRHIO'];

let dict = require('./dic/dict');
let CJSON = require('circular-json');
let moment = require('moment'); // for date time
let mysql = require('mysql');
let users = require('./routes/users');
let documents = require('./routes/documents');
let email = require('./mail');
let sprl = require('./routes/sprl');
let config = require('./utils').getConfig().conf ;

let DEBUG = false;

let conn = mysql.createConnection(config.mysqlconfig,'single');

 let selectppReg = {value:" : ;PP_REGISTERED:PP_REGISTERED;HS_REGISTER_ATTEMPT1:HS_REGISTER_ATTEMPT1;HS_REGISTER_ATTEMPT2:HS_REGISTER_ATTEMPT2;PP_REGISTER_FULLFILLED:PP_REGISTER_FULLFILLED;SMPI_MANUALLINK:SMPI_MANUALLINK;PP_REGISTER_FAILED:PP_REGISTER_FAILED;PP_ACCEPTED:PP_ACCEPTED;SMPI_REGISTER_FAILED:SMPI_REGISTER_FAILED"}

let  PP_REGISTERED = `                          
if ( ! isEmpty(user.mpiid) && user.mpiid != "n/a" ){
    //get approved documents for user Approved and latest date
    getDocs(user,(docs)=>{
        console.log("getDocs_PPREGISTRED %s",docs);  
        if (docs==true){
            // update HS docs from sprl    
            // see if there is sprl match for mppid
            patientSPRLMatch(user.mpiid,(err,found,isSMPI)=>{
                if (found ){ // linked
                    updateflow(user,"REFRESH_REQUESTED","DOC_REFRESH");           
                    users.updateFlowstatus(user_id,"PP_ACCEPTED")
                    email_registerFullfilledNoDoc(user) ;
                    updateflow(user,"PP_REGISTER_FULLFILLED");
                } else { // no match
                    // check if SMPI in list
                    if (isSMPI) {
                        //send email to operations for manual linking
                        email_OperationsManualLink(user) ;
                        updateflow(user,"SMPI_MANUALLINK");
                        users.updateFlowstatus(user_id,"PP_ACCEPTED");
                    } else {
                        updateflow(user,"SMPI_ATTEMPT1");
                        users.registerUser(user);                                    
                    }
                }
            });
        } else {
            email_registerFullfilled(user) ;
            updateflow(user,"PP_REGISTER_FULLFILLED");
            users.updateFlowstatus(user_id,"PP_ACCEPTED");
        }
    });
} else {
    updateflow(user,"HS_REGISTER_ATTEMPT1");
    users.registerUser(user);                                    
}
`
   
let HS_REGISTER_ATTEMPT1 = `
console.log("STATUS =%s ",status) ;
if ( ! isEmpty(user.mpiid) && user.mpiid != "n/a" ){
    getDocs(user,(docs)=>{
        if (docs==true){
            patientSPRLMatch(user.mpiid,(err,found,isSMPI)=>{
                if (found ){ // linked
                    updateflow(user,"REFRESH_REQUESTED","DOC_REFRESH");           
                    users.updateFlowstatus(user_id,"PP_ACCEPTED");
                    email_registerFullfilledNoDoc(user) ;
                    updateflow(user,"PP_REGISTER_FULLFILLED");
                } else { // no match 
                    if (isSMPI) {
                        email_OperationsManualLink(user) ;
                        updateflow(user,"SMPI_MANUALLINK");
                        users.updateFlowstatus(user_id,"PP_ACCEPTED");

                    } else {
                        updateflow(user,"SMPI_ATTEMPT1");
                        users.registerUser(user);                                    
                    }
                }
            });
        } else {
            email_registerFullfilled(user) ;
            updateflow(user,"PP_REGISTER_FULLFILLED");
            users.updateFlowstatus(user_id,"PP_ACCEPTED");
        }
    });
} else {
    updateflow(user,"HS_REGISTER_ATTEMPT2");
    users.registerUser(user);                                    
};
`
let HS_REGISTER_ATTEMPT2 = `
if  ( isEmpty(user.mpiid) || user.mpiid == "n/a" ) {
        updateflow(user,"PP_REGISTER_FAILED");
        email_HSRegisterFailed(user);
} else {
    getDocs(user,(docs)=>{
        if (docs==true){
            patientSPRLMatch(user.mpiid,(err,found,isSMPI)=>{
                if (found ){ // linked
                    updateflow(user,"REFRESH_REQUESTED","DOC_REFRESH");           
                    users.updateFlowstatus(user_id,"PP_ACCEPTED")
                    email_registerFullfilledNoDoc(user) ;
                    updateflow(user,"PP_REGISTER_FULLFILLED");
                } else { // no match
                    if (isSMPI) {
                        email_OperationsManualLink(user) ;
                        updateflow(user,"SMPI_MANUALLINK");
                    } else {
                        updateflow(user,"SMPI_ATTEMPT1");
                        users.registerUser(user);            
                        //updateflow(user,"HS_REGISTER_FAILED");                
                        // send email to operatio for register SMPI failed
                        //email_SMPIRegisterFailed(user);
                    }
                }
            });
        } else {
            email_registerFullfilled(user) ;
            updateflow(user,"PP_REGISTER_FULLFILLED");
            users.updateFlowstatus(user_id,"PP_ACCEPTED")

        }
    });
}
`
let SMPI_ATTEMPT1 = `
if  ( isEmpty(user.mpiid) || user.mpiid == "n/a" ) {
        updateflow(user,"PP_REGISTER_FAILED");
        email_HSRegisterFailed(user);
} else {
        patientSPRLMatch(user.mpiid,(err,found,isSMPI)=>{
                if (found ){ // linked
                    updateflow(user,"REFRESH_REQUESTED","DOC_REFRESH");           
                    users.updateFlowstatus(user_id,"PP_ACCEPTED")
                    email_registerFullfilledNoDoc(user) ;
                    updateflow(user,"PP_REGISTER_FULLFILLED");
                } else { // no match
                    if (isSMPI) {
                        email_OperationsManualLink(user) ;
                        updateflow(user,"SMPI_MANUALLINK");
                    } else {
                        updateflow(user,"SMPI_ATTEMPT2");
                        users.registerUser(user);            
                    }
                }
        });
};         

` 

let SMPI_ATTEMPT2 = `
if  ( isEmpty(user.mpiid) || user.mpiid == "n/a" ) {
        updateflow(user,"PP_REGISTER_FAILED");
        email_HSRegisterFailed(user);
} else {                                     
    patientSPRLMatch(user.mpiid,(err,found,isSMPI)=>{           
        if (found ){ // linked
            updateflow(user,"REFRESH_REQUESTED","DOC_REFRESH");           
            users.updateFlowstatus(user_id,"PP_ACCEPTED")
            email_registerFullfilledNoDoc(user) ;
            updateflow(user,"PP_REGISTER_FULLFILLED");
        } else { // no match                             
            if (isSMPI) {
                email_OperationsManualLink(user) ;
                updateflow(user,"SMPI_MANUALLINK");
            } else {
                updateflow(user,"SMPI_REGISTER_FAILED");                
                // send email to operatio for register SMPI failed
                email_SMPIRegisterFailed(user);
            }
        }
    });                                                            
};                                                                      
`
let SMPI_MANUALLINK = `
    console.log("dummy flow SMPI_MANUALLINK");
`
let PP_REGISTER_FULLFILLED = `
    console.log("dummy flow PP_REGISTER_FULLFILLED");
`
let PP_REGISTER_FAILED = `
    console.log("dummy flow PP_REGISTER_FAILED");
`
let SMPI_REGISTER_FAILED = `
    console.log("dummy flow SMPI_REGISTER_FAILED");
`

//conn.connect();
//--------------------------------------------------
exports.testproc = (req,res)=>{
    PatientRegProcess();
}
//--------------------------------------------------
exports.test1 = (req,res)=>{
    let user_id = 'snosal';
    users.userlist(user_id,'',(rows)=>{
        let user = rows[0] ;
        email_OperationsManualLink(user);
        email_SMPIRegisterFailed(user);
        email_HSRegisterFailed(user);
        email_registerFullfilledNoDoc(user);
        email_registerFullfilled(user); 
        email_registerFailed(user);
        email_linkFailed(user);

        res.send(user)
    });
}
//---------------------------------------------------------------------------------------
exports.test = function(req,res){
    if (DEBUG) {        
        console.log("Pateint Reg Proccess Started");
    }
    let intervalId = setInterval(() => { 
        PatientRegProcess();
    },10*60*1000 );               
    res.send("Process Started");
}
//---------------------------------------------------------------------------------------
exports.test3 = function(req,res){
    let query = req.query ;
    let params = req.params;

    let mpiid = params.mpiid;
    if (! mpiid) {
        mpiid = query.mpiid;
    }
    patientSPRLMatch(mpiid,(err,bSet,isSMPI,list)=> {
        let o = {
        mpiid,
        bSet,
        isSMPI,
        list
        }
        res.send(o)
    })
}
//----------------------------------------------------------------------------------
let isEmpty = (str)=> { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//-----------------------------------------------------------
let updateflow = function(user,status,sched_type=`${SCHEDULER_NAME}`) {
    let user_id = user.user_id ;
    let qry = `select count(*) as qty from ${FLOW_TABLE} where user_id = '${user_id}' and upper(sche_type) = '${sched_type}' `
    conn.query(qry,[user_id], function(err, res)  {
        //var qry =`insert into ${FLOW_TABLE} set user_id = '${user_id}', last_updated = NOW() ,status = '${status}',sche_type = '${sched_type}' `;            
        let qry1 =`insert into ${FLOW_TABLE}(user_id,last_updated,status,sche_type) VALUES('${user_id}', NOW() ,'${status}','${sched_type}') `;
        console.log("updateFlow qry = %s",qry1)   ;         
        if ( res && res.length > 0 && res[0].qty > 0 ) {        
            qry1 =`update  ${FLOW_TABLE} set last_updated = NOW() ,status = '${status}' `
            if (status == 'PP_REGISTER_FULLFILLED' || status == 'PP_ACCEPTED' ) {
                qry1 += " , last_successfully_updated = NOW() ";
            }
            qry1 += ` where user_id = '${user_id}' and  upper(sche_type)='${sched_type}'` ;
        } 
        if (DEBUG) {
            console.log("PPREG updateFLOW qry="+qry1);
        }
        conn.query(qry1,[user_id], function(err, result)  {
            if (DEBUG) {
                console.log("PPRegister updateflow result = %s",err||JSON.stringify(result));
            }
        });
        if (status == 'PP_REGISTER_FAILED' ) {
            email_registerFailed(user);
        }
    })
    return 0;
};
//--------------------------------------------------
let patientSPRLMatch = (mpiid,callback)=> {
    let qenames = [];
    // call SPRL PixQeury
    sprl.getPixUsersMPI(mpiid,(err,list)=>{
        // go through list and build set of qe's
        list.forEach( (element,index)=>{
            qenames.push(element.qe );
        });
        let sets = require("./sets");
        let set = new sets.Set(qenames);
        let bSet = set.contains(QEs) ;
        let isSMPI = set.has('SMPI') ;
        if(DEBUG){
            console.log("user smpi = %s,list= %j,  qenames = %s, bSet= %s , isMPI = %s",mpiid,list, qenames,bSet,isSMPI);
        }
        callback(err,bSet,isSMPI);
    });
    return 0;
};
//---------------------------------------------------
let checkFlow = function(callback) {
    users.userlist('','PP_REGISTERED',(rows)=>{
        rows.forEach( (user,index) => {
            if (! isEmpty(user.user_id) ) {
                let user_id = user.user_id ;
                let qry = `select count(*) as qty from ${FLOW_TABLE} where user_id = '${user_id}' and upper(sche_type) = '${SCHEDULER_NAME}' `
                conn.query(qry,[user_id], function(err, res)  {
                    if ( res && res.length > 0 && res[0].qty > 0 ) {
                        // already there do nothing
                    } else { //add to flow and send doc request
                        updateflow(user,"PP_REGISTERED")
                        users.registerUser(user);    
                    };
                }) ;
            }
        });
        callback("OK");
    });
    return "OK"
}
//-----------------------------------------------------
let evalFlow = (status,defJScript,callback)=>{
    if (isEmpty(defJScript)) {
       //defJScript = eval(status) ;
    }
    dict.get("JSCRIPT",status,"PP_FLOW",'',(err,data)=>{  // get javascript code for the flow
        if (err || isEmpty(data)) {
            if (isEmpty(defJScript)) {
                callback('');
            } else {
                dict.set("JSCRIPT",status,"PP_FLOW",'',defJScript,(err,defdata)=>{ });       
                callback(defJScript);
            }
        } else {    
            callback(data);
        }
    }); 
    return 0;
}
//-----------------------------------------------------
// start the work flow // should be loop not called
let PatientRegProcess = exports.PatientRegProcess = function(){
    let sets = require("./sets");
    let set = new sets.Set(["PP_REGISTERED","HS_REGISTER_ATTEMPT1","HS_REGISTER_ATTEMPT2","SMPI_ATTEMPT1","SMPI_ATTEMPT2"]); // only allow these in    
    //get list of users that are PP_REGISTERED and update if not found
    if (DEBUG) {
        //console.log("Patient Register Process Called for %s",config.devEnv);
    }
    checkFlow((data)=>{ // update the flow if new pp_register
        // go through the workflow
        let qry = `Select user_id,upper(sche_type) as sche_type,upper(status) as status,last_updated,last_successfully_updated from ${FLOW_TABLE} where upper(sche_type) = '${SCHEDULER_NAME}' `;    
        conn.query(qry, (err, rows) => {
            rows.forEach((user,index) =>{
                let now = moment().format('YYYYMMDDHHmm') ;
                let lastdate = moment(user.last_updated).add(config.RefreshFlowpReg, 'minutes').format('YYYYMMDDHHmm');               
                let user_id = user.user_id;
                let status = user.status;   
                if ( set.contains([status]) ) {  // only call if needed
                    if (now > lastdate) {
                        if(DEBUG) {
                            console.log("BEFORE PPRegister call - userid = %s, status = %s,  now= %s , expire= %s ",user_id,status,now,lastdate);
                        }                
                        // get user details for flow            
                        users.userlist(user_id,'',(list)=>{ // should only be one
                            if(DEBUG) {                        
                               console.log("PRE PatientRegProcess called, Status = %s\n list= %j",status,list );
                            }
                            if (list.length > 0 ) {
                                try {
                                    let user = list[0]; // this is read ny the proc via closure
                                    let proc = eval(status) ;  //eg SMPI_ATTEMPT2 = text
                                    if (DEBUG) {
                                        console.log("PROC PatientRegProcess called, Status = %s , USER = %j \nproc=%j",status ,user,proc);
                                    }
                                    evalFlow(status,proc,(data)=> {
                                        if ( isEmpty(data)) {
                                            eval(proc);
                                        } else {
                                            eval(data);
                                        }
                                    }); 
                                } catch(ex) {
                                    console.error(ex);
                                }
                            }
                        });
                    }
                }
            });
        });
    });
    return 0;
};
//---------------------------------------------------------------------------------------
let getDocs = function(user,callback) {

    /*
    documents.userDocList(user.user_id,'Approved',(docs)=>{    
        if( DEBUG ) {
            console.log("getDocs %s",JSON.parse(docs));
        }
        let doc = JSON.parse(docs);    
        if (doc && doc.length > 0 && ! isEmpty(doc[0]) ){
            callback(doc ) ;
        } else {
            callback('');
        }    
    });
    */
    documents.userDocList(user.user_id,'Approved',(docs,response)=>{
        let doc = "";
        if (DEBUG) {
            console.log("ppPatientReg for %s \ngetDoc obj= >%j<",user.user_id,docs)
        }
        if ( typeof docs === 'string') {
            doc = JSON.parse(docs);
            console.log("STRING doc =JSON.parse(docs)  doc=%j ",doc)
        }
        if (typeof docs === 'object' ) {
            //{"type":"Buffer","data":[91,93]}
            if (docs.type && docs.type == 'Buffer') { 
                doc = JSON.parse(docs.toString())                       
                console.log("BUFFER JSON.parse(docs.toString())  doc=%j ",doc)
                doc = JSON.parse(docs)                       
                console.log("BUFER JSON.parse(docs)  doc=%j ",doc)
                
            } else {
                doc = docs;
                console.log("ppPatientReg object jdocs= >%j<",doc)
                console.log("ppPatientReg object sdocs= >%s<",doc)
            }
        }
        if (DEBUG) {
            console.log("ppPatientReg \nsDoc= >%s<",doc)
            console.log("ppPatientReg \njDoc= >%j<",doc)
        }
        if (doc && ! isEmpty(doc)){
            console.log("ppPatientReg Found DOC ");
            callback(true) ;
        } else {
            console.log("ppPatientReg NOT Found DOC ");
            callback(false);
        }    
    });
    return 0;
}
//---------------------------------------------------------------------------------------
exports.testUsers = function(req,res){
    let type = req.params.type;
    if (isEmpty(type)){
        type = 'PP_REGISTERED';
    }
    //users.updateFlowstatus('snosal',"PP_ACCEPTED")
    users.userlist('snosal',type,(rows)=>{
        if( rows && rows.length > 0 ) {
            let user = rows[0] ;
            //rgisterFullfilled(user);
            email_registerFailed(user);
            //linkFailed(user);
        }
        res.send(rows);
    });
    return 0;
}; 
//---------------------------------------------------------------------------------------
let email_OperationsManualLink = (user) => {
    let oMail = { 
        text:    
`Dear Operations Team,

The sMPI Registration was succsseful for %(user_id)s %(first_name)s %(last_name)s  .
but a link was not established, please manually link the user.

Thanks.


from PP Admin application
` ,
        from:    "nyppsupport@nyehealth.org", 
        to:      "nyppsupport@nyehealth.org",
        cc:      "",
        subject: "NY Patient Portal – sMPI link was not completed"
    };
    email.sendEmail(user,'PP_SMPI_MANUAL_LINK',oMail);
    return 0;
}
//---------------------------------------------------------------------------------------
let email_SMPIRegisterFailed = (user) => {
    let oMail = { 
        text:     
`Dear Operations Team,

SMPI Registration failed for %(user_id)s %(first_name)s %(last_name)s  .

Thanks.

from PP Admin application
` ,
        from:    "nyppsupport@nyehealth.org", 
        to:      "nyppsupport@nyehealth.org",
        cc:      "",
        subject: "NY Patient Portal, SMPI Registration Failed"
    };
    email.sendEmail(user,'PP_SMPIREGISTERFAIL',oMail);
    return 0;
}
//---------------------------------------------------------------------------------------
let email_HSRegisterFailed = (user) => {
    let oMail = { 
        text:    
`Dear Operations Team,

HS Registration failed for %(user_id)s %(first_name)s %(last_name)s  .

Thanks.

from PP Admin application
` ,
        from:    "nyppsupport@nyehealth.org", 
        to:      "nyppsupport@nyehealth.org",
        cc:      "",
        subject: `NY Patient Portal – HS Registration Failed`
    };
    email.sendEmail(user,'PP_HSREGISTERFAILED',oMail);
    return 0;
}
//---------------------------------------------------------------------------------------
let email_registerFullfilledNoDoc = (user,updatedoc)=>{
   if ( isEmpty(updatedoc) ) {
        updatedoc = false;
    }
    let user_id = user.user_id;
    //send email
    let oMail = { 
        text:    
`Dear %(first_name)s %(last_name)s, 

Your registration to the New York Patient Portal account was completed successfully.
System is in the process of retrieving your clinical information, you will be notified on completion. 
Please follow this link %(client_url)s to login.

If you have any questions, please do not hesitate to contact us.

NYPP Support Center
New York eHealth Collaborative (NYeC)
40 Worth Street, 5th Floor
New York, NY 10013-2988
Tel: 888-633-6706
nyppsupport@nyehealth.org
www.nyehealth.org
 
Better Healthcare Through Technology
` ,
        from:    "nyppsupport@nyehealth.org", 
        to:      "%(email)s",
        cc:      "",
        bcc:     "nyppsupport@nyehealth.org",
        subject: "NY Patient Portal – Registration Completed Successfully"
    };
    email.sendEmail(user,'PP_REGISTEROK_NODOC',oMail);
    return 0;
}
//-----------------------------------------------------------------
let email_registerFullfilled = function(user,updatedoc) {
    if ( isEmpty(updatedoc) ) {
        updatedoc = false;
    }
    let user_id = user.user_id;
    //send email
    let oMail = { 
        text:    
`Dear %(first_name)s %(last_name)s, 

Your registration to the New York Patient Portal account was completed successfully.
Please follow this link %(client_url)s to login and view your most recent medical data.

If you have any questions, please do not hesitate to contact us.

NYPP Support Center
New York eHealth Collaborative (NYeC)
40 Worth Street, 5th Floor
New York, NY 10013-2988
Tel: 888-633-6706
nyppsupport@nyehealth.org
www.nyehealth.org
 
Better Healthcare Through Technology
` ,
        from:    "nyppsupport@nyehealth.org", 
        to:      "%(email)s",
        cc:      "",
        bcc:     "nyppsupport@nyehealth.org",
        subject: "NY Patient Portal, Registration Completed Successfully"
    };
    email.sendEmail(user,'PP_REGISTERFULFILLED',oMail);
    return 0;
};
//---------------------------------------------------------------------------------------
let email_registerFailed = function(user) {
  let oMail = { 
        text:    
`
Dear %(first_name)s %(last_name)s, 

Your account is set-up and you can follow this  %(client_url)s to log into the portal, but you will not be able to view your clinical information until the issue is fixed.

Unfortunately, there was a technical issue while updating your clinical information in the portal. 

Our support team is working on fixing the problem and will contact you once the issue is resolved.

If you have any questions, please do not hesitate to contact us.


NYPP Support Center
New York eHealth Collaborative (NYeC)
40 Worth Street, 5th Floor
New York, NY 10013-2988
Tel: 888-633-6706
nyppsupport@nyehealth.org
www.nyehealth.org
 
Better Healthcare Through Technology
` ,
        from:    "nyppsupport@nyehealth.org", 
        to:      "%(email)s",
        cc:      "",
        subject: " New York Patient Portal, Your Account is Setup, Clinical Data is not Available "
    };
    email.sendEmail(user,'PP_REGISTERFAILED',oMail);
    return 0;
};
//---------------------------------------------------------------------------------------
var email_linkFailed = function(user) {
  let oMail = { 
        text:   
`Dear Operations Team,

The sMPI Registration failed for %(user_id)s %(first_name)s %(last_name)s  .

Please open a ticket and look into the problem.

Thanks.

from PP Admin application
`
,
        from:    "nyppsupport@nyehealth.org", 
        to:      "nyppsupport@nyehealth.org",
        cc:      "",
        subject: `NYPP , sMPI Registration failed for %(user_id)s %(first_name)s %(last_name)s`
    };
    email.sendEmail(user,'PP_LINKFAILED',oMail);
    return 0;
};
//---------------------------------------------------







