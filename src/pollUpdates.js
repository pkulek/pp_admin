const DEBUG = true;
// add timestamps in front of log messages
require('console-stamp')(console, 'yyyymmddHHmmss.l');

var CJSON = require('circular-json');
var sprl = require('./routes/sprl');


var intervalId = 0;
var counter = 0;
var tasklist = [];
const task_XCAQUERY = {"taskid":0,"name":"XCAQUERY","task": "XCAQuerytask();","duration":60000*4,"timestarted":0}; // 
const task_XCARETRIEVE = {"taskid":0,"name":"XCARETRIEVE","task": "XCARetrievetask();","duration":60000*10,"timestarted":0}; // 
const task_DocRefresh = {"taskid":0,"name":"DOC_REFRESH","task": "DocRefreshTask();","duration":60000*10,"timestarted":0}; // 

//----------------------------------------------------------------------------------
function isEmpty(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//----------------------------------------------------------------------------------
exports.tasklist = function(req, res){
    var out = [];
    tasklist.forEach( (element) => {
        out.push({"name":element.name,"task":element.task,"duration":element.duration/60000,"timestarted":element.timestarted,"element":CJSON.stringify(element) });    
    }, this);  
    res.send(out);
};
//---------------------------------------------------
exports.html = function(req, res){
       res.render('tasks_view',{page_title:"tasks Editor "});                        
};

//-----------------------------------------------------------------------------
let XCARetrievetask = function(){
    sprl.XCARetrieveAll();
}
//-----------------------------------------------------------------------------
let XCAQuerytask = function(){
    sprl.XCAQuery();
}          
//-----------------------------------------------------------------------------
exports.taskstart = function(req, res){
    console.log("taskstart BODY "+JSON.stringify(req.body));
    var taskname = req.params.taskname ;
    var task = req.params.task;
    var duration = req.params.duration;
    if (DEBUG) { 
        console.log(taskname);
        console.log(task);
        console.log(duration);
    }
    if ( isEmpty(duration)) {
        duration = 1000*60*60 ;
    };
    if ( isEmpty(task)) {
        task = "userstatustask('PP_REGISTRED');";
    }
    if ( isEmpty(taskname)) {
        taskname = 'HSREGISTER';
    }    
    var o = startTask(taskname,duration,task)
    res.send(CJSON.stringify(o));
};
//-----------------------------------------------------------------------------
var startTask = exports.startTask = function(task){
    if (DEBUG) {
        console.log('start '+task.name);
    }
    var intervalId = setInterval(() => { 
         eval(task.task); 
    },task.duration );
    task.taskid = intervalId ;         
    task.timestarted = new Date();
    tasklist.push(task);
    if (DEBUG) {
        console.log(tasklist);
    }
    //return(o) ;
};
//-----------------------------------------------------------------------------
exports.stopTask = function(taskname){
    console.log('stop '+ taskname);
    tasklist.forEach( (element) => {
        if ( element.name == taskname ){
            clearInterval(element.taskid);
        }        
    }, this);
};



