
require('console-stamp')(console, 'yyyymmddHHmmss.l');
const DEBUG = true ;
const XCA_TIMEOUT = 1200 ;
const devEnv = "PROD"; //STAGE  or PROD//
let CJSON = require("circular-json");
let dict = require('../dic/dict');
let Client = require('node-rest-client').Client;
let logger = require('./logging');
let fs = require('fs')
let email = require('../mail');
let stopwatch = require("node-stopwatch").Stopwatch.create();
let config =  require('../utils').getConfig().conf;
let moment = require('moment'); // for date time

let client = new Client();
let DEVENV = config.devEnv;

//-----------------------------------------------------------------------------
const livingSubjectQuery = {
        "extension":"1360358721" ,
        "root":"2.16.840.1.113883.3.458.10.3.1",
        "Gender":"F",
        "DOB":"19220406",
        "family":"NOSAL",
        "given":"SHERRY",
        "middle":"A",
        "streetAddressLine":"2173 JARVISVILLE RD RTRT",
        "city":"HUNTINGTON",
        "state":"NY",
        "country":"USA",
        "postalCode":"11743"
}
//-----------------------------------------------------------------------------
        //var rim1 = "'urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1'";   // DocumnetType1
        //var rim2 = "'urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248'";   // DocumentType2
        //var rim3 = "'urn:oasis:names:tc:ebxml-regrep:StatusType:Approved'"; // DocumentStatus
        //,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved"
let QEs = ['BRONX','HIXNY','HEALTHECONN','HEALTHIX','HEALTHLINKNY','NYCIG','GRRHIO','HEALTHELINK'];
//var QEs = ['HEALTHLINKNY','HEALTHIX','HEALTHELINK','HIXNY','BRONX','NYCIG','HEALTHECONN','GRRHIO'];

let STAGE_HIEProperties = [
        {QE_Name:"BRONX",MPI_ID:"000257887182",QE_ID:"2.16.840.1.113883.3.371.1.2.3",MPI_OID:"000257887182^^^&amp;2.16.840.1.113883.3.371.1.1.3&amp;ISO",Repository_Id:"2.16.840.1.113883.3.371.1.2.3",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HIXNY",MPI_ID:"36191",QE_ID:"2.16.840.1.113883.4.319",MPI_OID:"36191^^^&amp;2.16.840.1.113883.4.319&amp;ISO",Repository_Id:"2.16.840.1.113883.4.319.1.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHECONN",MPI_ID:"6193709",QE_ID:"2.16.840.1.113883.3.1834.2",MPI_OID:"6193709^^^&amp;2.16.840.1.113883.3.1834.2&amp;ISO",Repository_Id:"2.16.840.1.113883.3.1834.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHIX",MPI_ID:"5470835",QE_ID:"2.16.840.1.113883.13.61",MPI_OID:"5470835^^^&amp;2.16.840.1.113883.13.61&amp;ISO",Repository_Id:"2.16.840.1.113883.13.61.100.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHLINKNY",MPI_ID:"1020217012",QE_ID:"2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5",MPI_OID:"1020217012^^^&amp;2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5&amp;ISO",Repository_Id:"2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"NYCIG",MPI_ID:"1046652",QE_ID:"2.16.840.1.113883.3.2591.100.2",MPI_OID:"1046652^^^&amp;2.16.840.1.113883.3.2591.100.2&amp;ISO",Repository_Id:"2.16.840.1.113883.5.25",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"GRRHIO",MPI_ID:"1360358721",QE_ID:"2.16.840.1.113883.3.458.10.3.3",MPI_OID:"1360358721^^^&amp;2.16.840.1.113883.3.458.10.3.3&amp;ISO",Repository_Id:"",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHELINK",MPI_ID:"000649637162",QE_ID:"2.25.256133121442266547198931747355024016667.1.2.1",MPI_OID:"000649637162^^^&amp;2.25.256133121442266547198931747355024016667.1.2.1&amp;ISO",Repository_Id:"2.25.256133121442266547198931747355024016667.1.1.1.1040",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"}
    ];
let PROD_HIEProperties = [
        {QE_Name:"BRONX",MPI_ID:"002412926061",QE_ID:"2.16.840.1.113883.3.371.1.1.3",MPI_OID:"'002412926061^^^&amp;2.16.840.1.113883.3.371.1.1.3&amp;ISO'",Repository_Id:"2.16.840.1.113883.3.371.1.1.5.12",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"'urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1'",DocumentType2:"'urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248'",DocumentStatus:"'urn:oasis:names:tc:ebxml-regrep:StatusType:Approved'",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HIXNY",MPI_ID:"12953892",QE_ID:"2.16.840.1.113883.4.319",MPI_OID:"12953892^^^&amp;2.16.840.1.113883.4.319&amp;ISO",Repository_Id:"2.16.840.1.113883.4.319.2.1.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHECONN",MPI_ID:"9458451",QE_ID:"2.16.840.1.113883.3.1834.1",MPI_OID:"9458451^^^&amp;2.16.840.1.113883.3.1834.1&amp;ISO",Repository_Id:"1.3.6.1.4.1.21367.2010.1.2.1040",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHIX",MPI_ID:"5160563",QE_ID:"2.16.840.1.113883.13.61",MPI_OID:"5160563^^^&amp;2.16.840.1.113883.13.61&amp;ISO",Repository_Id:"2.16.840.1.113883.13.61.100.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHLINKNY",MPI_ID:"1021759186",QE_ID:"2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5",MPI_OID:"1021759186^^^&amp;2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.5&amp;ISO",Repository_Id:"2.16.840.1.113883.3.234.1.4.1.17.1.6.3.1.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"NYCIG",MPI_ID:"6109116",QE_ID:"2.16.840.1.113883.3.2591.100.3",MPI_OID:"6109116^^^&amp;2.16.840.1.113883.3.2591.100.3&amp;ISO",Repository_Id:"2.16.840.1.113883.3.2591.100.3.1.2",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"GRRHIO",MPI_ID:"1360910431",QE_ID:"2.16.840.1.113883.3.458.10.1.1",MPI_OID:"1360910431^^^&amp;2.16.840.1.113883.3.458.10.1.1&amp;ISO",Repository_Id:"2.16.840.1.113883.3.458.10.1.1",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"},
        {QE_Name:"HEALTHELINK",MPI_ID:"2000001795808",QE_ID:"2.25.256133121442266547198931747355024016667.1.1.1",MPI_OID:"2000001795808^^^&amp;2.25.256133121442266547198931747355024016667.1.1.1&amp;ISO",Repository_Id:"2.25.256133121442266547198931747355024016667.1.1.1.1040",Document_Id:"n/a",Command:"",Result:"",Failure_Description:"",Count:0,ret_count:0,pix_count:0,qry_count:0,xcpd_count:0,DocumentType1:"urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1",DocumentType2:"urn:uuid:34268e47-fdf5-41a6-ba33-82133c465248",DocumentStatus:"urn:oasis:names:tc:ebxml-regrep:StatusType:Approved",email:"",text:"",url:"https://sprl.shinnyapi.org:9443/sXCPDQuery"}
    ];

//dict.set("JSON","HIEProperties","SPRL","PROD",PROD_HIEProperties);
//dict.set("JSON","HIEProperties","SPRL","STAGE",STAGE_HIEProperties);
//----------------------------------------------------------------------------------
let isEmpty = (str)=> { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//----------------------------------------------------------------------------------
let msinsert = function(data,count,jobID,ResponseTime) {
    if(DEBUG) { 
        console.log("data=%s : count=%s",data,count);
    }
    try{
        let mssql = require('mssql');
        let request = new mssql.Request();
        if (isEmpty(count)) {
            count = data.Count;
        }
        let qry = "insert into Heartbeat_Results (QE_Name,QE_ID,Repository_Id,Document_Id,MPI_ID,Command,TimeStamp,Result,Failure_Description,Count,SetID,ResponseTime)  VALUES('" +data.QE_Name+"','"+
                data.QE_ID+"', '"+data.Repository_Id+"','"+data.Document_Id+"','"+data.MPI_ID+"','"+data.Command+"',GETDATE(),'"+
                data.Result+"','"+data.Failure_Description+"',"+count+","+jobID+","+ResponseTime+ ")";
        request.query(qry);
    } catch(Ex) {
        console.error("Insert Error %s",Ex);
    }         
};
//----------------------------------------------------------------------------------------------------------------------------
let postXCAQuery= function(url,args,oHIE,jobID){
    let cl = new Client();
    let status = "n/a";        
    let docId = "n/a";
    stopwatch.start();
    try {
            if (DEBUG) {                    
                //console.log(url);
                fs.writeFile("/tmp/XCA_Query_"+oHIE.QE_Name, url, function(err) {
                    if(err) { console.log(err); }
                });         
            }     
            cl.post(url, args, function (data, response) {            
                data = data.toString();
                if (DEBUG) {                    
                    fs.writeFile("/tmp/XCA_Query_response"+oHIE.QE_Name, response, function(err) {
                        if(err) { console.log(err); }
                    });         
                }        
                if ( ! isEmpty(data) ) {
                    let select = require('xpath.js') ;
                    let dom = require('xmldom').DOMParser ;
                    let doc = new dom().parseFromString(data) ;   
                    let errornodes = select(doc, "//*[local-name()='RegistryError']");                    
                    if (errornodes.length > 0 ) {
                        status = errornodes[0].toString();
                        oHIE.Count += 1;
                        oHIE.qry_count += 1;
                        if ( oHIE.qry_count =3 ){
                            // sendemail to rhio ERROR
                            qry_error_mail(oHIE);
                        }
                        oHIE.Result = "Failure" ;
                        oHIE.Command = "XCA_Query" ; 
                        oHIE.Document_Id = docId ;
                        oHIE.Failure_Description = status ;
                        //console.log(status);
                        msinsert( oHIE,oHIE.qry_count,jobID,stopwatch.elapsedMilliseconds);                     
                    } else { 
                        let nodes = select(doc, "//*[local-name()='ExternalIdentifier']"); 
                        if (nodes.length > 0 ) {
                            nodes.forEach(function(element, index, theArray) {
                                let attr = select(element,"@identificationScheme").toString().split("=")[1].replace(/"/g,'') ;
                                let value = select(element,"@value").toString().split("=")[1].replace(/"/g,'') ;
                                if (attr =="urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427" ) {
                                    MPI = value;
                                }
                                if (attr =="urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab" ) {
                                    docId = value;
                                }
                            });
                            oHIE.Result = "Success" ;
                            oHIE.Count = 0;
                            if ( oHIE.qry_count =3 ){
                                // sendemail to rhio OK
                                //qry_ok_mail(oHIE);
                            }
                            oHIE.qry_count =0 ;
                            oHIE.Failure_Description = "";
                            oHIE.Document_Id = docId ;
                            oHIE.Command = "XCA_Query" ;
                            msinsert( oHIE,oHIE.qry_count,jobID,stopwatch.elapsedMilliseconds);
                            fs.writeFile("/tmp/HIEProperties_"+oHIE.QE_Name, JSON.stringify(oHIE), function(err) {
                                if(err) { console.log(err); }
                            });                                                      
                        }  else {
                            oHIE.Result = "Failure" ;
                            oHIE.Count += 1;
                            oHIE.qry_count += 1;
                            if ( oHIE.qry_count =3 ){
                                // sendemail to rhio ERROR
                                qry_error_mail(oHIE);
                            }
                            oHIE.Command = "XCA_Query" ;
                            oHIE.Document_Id = docId ;
                            oHIE.Failure_Description = "";
                            msinsert( oHIE,oHIE.qry_count,jobID,stopwatch.elapsedMilliseconds);
                            if (DEBUG) {
                                fs.writeFile("/tmp/XCA_Query_"+oHIE.QE_Name, "XCA Query response Failure =>"+oHIE.QE_Name, function(err) {
                                    if(err) { console.log(err); }
                                });         
                            }
                        }
                    }    
                } else {
                    oHIE.Result = "Failure" ;
                    oHIE.Count += 1;
                    oHIE.qry_count += 1;
                    if ( oHIE.qry_count =3 ){
                        // sendemail to rhio ERROR
                        //qry_error_mail(oHIE);
                    }
                    oHIE.Document_Id = docId ;
                    oHIE.Command = "XCA_Query" ;
                    oHIE.Failure_Description =  "Query Error: no Data for :"+JSON.stringify(args) ;  
                    msinsert( oHIE,oHIE.qry_count,jobID,stopwatch.elapsedMilliseconds);
                    if (DEBUG){
                        fs.writeFile("/tmp/XCA_Query_Error_"+oHIE.QE_Name, "XCA Query response Failure =>"+JSON.stringify(args), function(err) {
                            if(err) { console.log(err); }
                        });        
                    } 
                }
                // test if updated
                if ( DEBUG ) {
                     console.log("DEBUG QE => %s DOCID => %s",oHIE.QE_Name,oHIE.Document_Id) ;
                }                
            });
        } catch (Ex) {
                oHIE.Command = "XCA_Query" ;
                oHIE.Document_Id = docId ;
                oHIE.Failure_Description = "XCA Query Error:"+err ;
                oHIE.Count += 1;
                oHIE.qry_count += 1;
                if ( oHIE.qry_count =3 ){
                    //qry_error_mail(oHIE);
                    // sendemail to nyec saying cannot post a query
                }
                msinsert( oHIE,oHIE.qry_count,jobID,stopwatch.elapsedMilliseconds);
                fs.writeFile("/tmp/XCA_Query_Error_"+oHIE.QE_Name, "XCA Query response Failure =>"+oHIE.QE_Name, function(err) {
                    if(err) { console.log(err); }
                });         
        };                  
    stopwatch.stop();
};
//----------------------------------------------------------------------------------------------------------------------------
let postXCARetrieve= function(url,args,oHIE,jobID){
    let cl = new Client();
    dict.getSQN("TASKMANAGER",(sqn,err)=>{
        let Sqn = sqn;
        if (DEBUG) {
            console.log("Call POST XCA Retrieve %s for %s for %s",url,args,Sqn);
        }
        args.sqn = Sqn;
        //logger.LOG('TaskManager',"AUDIT",Sqn,"XCARetrieve-Query","",url,CJSON.stringify(args));
        try{
            stopwatch.start();
            fs.writeFile("/tmp/XCA_Retrieve_SOAPCall_"+oHIE.QE_Name, CJSON.stringify(args), function(err) {
                if(err) { console.log(err); }
            });  
            cl.post(url, args, function (data, response) {            
                data = data.toString();
                if (DEBUG) {                    
                    fs.writeFile("/tmp/XCA_Retrieve_Data_"+oHIE.QE_Name,data, function(err) {
                        if(err) { console.log(err); }
                    });         
                }        
                if ( ! isEmpty(data) ) {
                    //logger.LOG('TaskManager',"AUDIT",args.sqn,"XCARetrieve-Result","",data,response);
                    let docId = "n/a";
                    let MPI = "n/a" 
                    let status = 'n/a'
                    let failure = 'n/a';       
                    let select = require('xpath.js') ;
                    let dom = require('xmldom').DOMParser ;
                    let doc = new dom().parseFromString(data) ;   
                    let errNodes = select(doc, "//*[local-name()='RegistryError']");
                    if (errNodes.length > 0) {
                        errNodes.forEach(function(element, index, theArray) { 
                            oHIE.Failure_Description = ""+ errNodes[0].toString() 
                            oHIE.Result = "Failure" ;
                            oHIE.Count += 1;
                            oHIE.ret_count += 1;
                            if ( oHIE.ret_count =3 ){
                                ret_error_mail(oHIE);
                            }
                            fs.writeFile("XCA_Retrieve_","XCA Retrieve failure =>" +oHIE.QE_Name, function(err) {
                                if(err) { console.log(err); }
                            });         
                        });
                    } else  {
                        var nodes = select(doc, "//*[local-name()='RegistryResponse']");
                        if (nodes.length > 0 ){
                            nodes.forEach(function(element, index, theArray) {
                                status = select(element,"@status").toString().split("=")[1].replace(/"/g,'');
                                status = status.split(":").pop(-1);
                                oHIE.Failure_Description = ""  ;
                                oHIE.Count= 0; 
                                if ( oHIE.ret_count =3 ){
                                    ret_ok_mail(oHIE);
                                }
                                oHIE.ret_count = 0;
                                oHIE.Result = status ;
                            });
                        } else {
                            oHIE.Failure_Description = "No Data in Registry Response" 
                            oHIE.Result = "Failure" ; 
                            oHIE.Count += 1;
                            oHIE.ret_count += 1;
                            if ( oHIE.ret_count =3 ){
                                ret_error_mail(oHIE);
                            }
                        }
                    }
                    oHIE.Command = "XCA_Retrieve" ;   
                    msinsert(oHIE,oHIE.ret_count,jobID,stopwatch.elapsedMilliseconds);
                    if (DEBUG) {
                        console.log("XCARETRIEVE EVAL",oHIE  );
                    }
                } else {
                    oHIE.Count += 1;
                    oHIE.ret_count += 1;
                    if ( oHIE.ret_count =3 ){
                        ret_error_mail(oHIE);
                    }
                    oHIE.Result = "Failure" ;
                    oHIE.Command = "XCA_Retrieve" ;
                    oHIE.Failure_Description = "XCA Retirieve: no Data "+response ;                    
                    msinsert( oHIE,oHIE.ret_count,jobID,stopwatch.elapsedMilliseconds);
                    if (DEBUG){
                        fs.writeFile("/tmp/XCARetrieve_Fail_"+oHIE.QE_Name, "XCA Query response Failure =>", function(err) {
                            if(err) { console.log(err); }
                        });        
                    } 
                }   
            }); 
            stopwatch.stop();
        } catch(eX){
            console.log("XCA ERROR Retrieve %s ",eX);
        }      
    });
};
//----------------------------------------------------------------------------------
exports.XCA_Query = (req,res)=>{
    let rhio = req.query.rhio ;
    let dev = req.query.dev ;
    if(isEmpty(rhio)) {
        rhio = req.params.rhio ;        
    }
    if(isEmpty(dev)) {
        dev = req.params.dev ;        
    }
    if(isEmpty(dev)) {
        dev = "PROD" ;        
    }
    if (! isEmpty(rhio) ){
        let data = eval(dev+"_HIEProperties[index]") ;
        dict.set("JSON","HIEPROPERTIES","SPRL",dev,data,(err,data) => {
            if (data){
                data.forEach(function(element, index, theArray) {
                    if (rhio == element.QE_NAME ){
                        runxca("XCA_QUERY",dev,oHIE,(err,oH)=>{
                            res.send(oH);
                            return 1;
                        });
                    } ;
                }); 
            }
        });
    } else {
        res.send({error:"No rhio provided"});
    }
    return 0;
}  
//----------------------------------------------------------------------------------
let runxca = (name,dev,oHIE,callback)=>{
    if (isEmpty(name)) {
        name = "XCA_QUERY" ;
    }    
    if (isEmpty(dev)) {
        dev = devEnv ;
    }        
    let oid = oHIE.MPI_OID ;
    let jobID = new moment().format('YYYYMMDDHHmm') ;
    getXCA_Query1('XCA_QUERY',dev,(err,xmldata) => {
        let sprintf = require("sprintf-js").sprintf;                
        let xml = sprintf(xmldata, oHIE) ;
        let args = {
            data: xml,
            async:false,
            headers: {  
                "Content-Type": "text/xml; charset=utf-8" 
            }  
        };
        postXCAQuery1(url,args,oHIE,jobID,(err,oH)=>{
            if(callback){
                callback(err,oH) ;
            }
        });
    });    
};
//----------------------------------------------------------------------------------------------------------------------------
let postXCAQuery1= function(url,args,oHIE,jobID,callback){
    let cl = new Client();
    let status = "n/a";        
    let docId = "n/a";
    try {
        stopwatch.start();
        if (DEBUG) {                    
            //console.log(url);
            fs.writeFile("/tmp/XCA_Query_"+oHIE.QE_Name, url, function(err) {
                if(err) { console.log(err); }
            });         
        }     
        cl.post(url, args, function (data, response) {            
            data = data.toString();
            if (DEBUG) {                    
                fs.writeFile("/tmp/XCA_Query_response"+oHIE.QE_Name, response, function(err) {
                    if(err) { console.log(err); }
                });         
            }        
            if ( ! isEmpty(data) ) {
                let select = require('xpath.js') ;
                let dom = require('xmldom').DOMParser ;
                let doc = new dom().parseFromString(data) ;   
                let errornodes = select(doc, "//*[local-name()='RegistryError']");                    
                if (errornodes.length > 0 ) {
                    let status = errornodes[0].toString();
                    oHIE.Count += 1;
                    oHIE.qry_count += 1;
                    oHIE.Result = "Failure" ;
                    oHIE.Command = "XCA_Query" ; 
                    oHIE.Document_Id = docId ;
                    oHIE.Failure_Description = status ;
                    if (callback){
                        callback("Error",oHie) ;
                    }
                } else { 
                    let nodes = select(doc, "//*[local-name()='ExternalIdentifier']"); 
                    if (nodes.length > 0 ) {
                        nodes.forEach(function(element, index, theArray) {
                            let attr = select(element,"@identificationScheme").toString().split("=")[1].replace(/"/g,'') ;
                            let value = select(element,"@value").toString().split("=")[1].replace(/"/g,'') ;
                            if (attr =="urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427" ) {
                                MPI = value;
                            }
                            if (attr =="urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab" ) {
                                docId = value;
                            }
                        });
                        oHIE.Result = "Success" ;
                        oHIE.Count = 0;
                        oHIE.qry_count =0 ;
                        oHIE.Failure_Description = "" ;
                        oHIE.Document_Id = docId ;
                        oHIE.Command = "XCA_Query" ;
                        if (callback){
                            callback("",oHie) ;
                        }
                    }  else {
                        oHIE.Result = "Failure" ;
                        oHIE.Count += 1;
                        oHIE.qry_count += 1;
                        oHIE.Command = "XCA_Query" ;
                        oHIE.Document_Id = docId ;
                        oHIE.Failure_Description =  "Xpath Error: no Data for :"+args.toString() ;
                        if (callback){
                            callback("Error",oHie) ;
                        }
                        if (DEBUG) {
                            fs.writeFile("/tmp/XCA_Query_"+oHIE.QE_Name, "XCA Query response Failure =>"+oHIE.QE_Name, function(err) {
                                if(err) { console.log(err); }
                            });         
                        }

                    }
                }    
            } else {
                oHIE.Result = "Failure" ;
                oHIE.Count += 1;
                oHIE.qry_count += 1;
                oHIE.Document_Id = docId ;
                oHIE.Command = "XCA_Query" ;
                oHIE.Failure_Description =  "Query Error: no Data for :"+JSON.stringify(args) ;  
                if (callback){
                    callback("Error",oHie) ;
                }
                if (DEBUG){
                    fs.writeFile("/tmp/XCA_Query_Error_"+oHIE.QE_Name, "XCA Query response Failure =>"+JSON.stringify(args), function(err) {
                        if(err) { console.log(err); }
                    });        
                } 
            }
        });
        stopwatch.stop();
    } catch (Ex) {
            oHIE.Command = "XCA_Query" ;
            oHIE.Document_Id = docId ;
            oHIE.Failure_Description = "XCA Query Error:"+err ;
            oHIE.Count += 1;
            oHIE.qry_count += 1;
            if (callback){
                callback("Error:"+Ex,oHie) ;
            }
            fs.writeFile("/tmp/XCA_Query_Error_"+oHIE.QE_Name, "XCA Query response Failure =>"+oHIE.QE_Name, function(err) {
                if(err) { console.log(err); }
            });         
    };
};
//----------------------------------------------------------------------------------
exports.XCAQuery = function(){
    dict.get("JSON","HIEPROPERTIES","SPRL",devEnv,(err,data) => {    
        let jobID = new moment().format('YYYYMMDDmmss') ;
        if (isEmpty(data) ) {
            data = eval(devEnv+"_HIEProperties") ;
            dict.set("JSON","HIEPROPERTIES","SPRL",devEnv,data)    
        }
        data.forEach(function(element, index, theArray) {
            // send request o HIE to get doc ID
            let oid = element.MPI_OID ;
            getXCA_Query('XCA_QUERY',element,(err,xmldata) => {
                let sprintf = require("sprintf-js").sprintf;                
                let xml = sprintf(xmldata, oid) ;
                //let defurl = "http://192.168.180.193:9445";
                let defurl = "http://192.168.130.23:9448";
                if (devEnv == 'PROD') {
                    defurl = "http://192.168.180.193:9445";
                }                
                dict.get("URL","SPRL_XCA","SPRL_"+devEnv,(error,url) => {
                    if( isEmpty(url) ){
                        url = defurl ;                        
                        dict.set("URL","SPRL_XCA","SPRL_"+devEnv,defurl);
                    }
                    url+="/sXCAQuery";                  
                    let args = {
                        data: xml,
                        async:false,
                        headers: {  
                            "Content-Type": "text/xml; charset=utf-8" 
                        }  
                    };
                    let timeout = XCA_TIMEOUT ; 
                    //var oHIE = eval(devEnv+"_HIEProperties[index]") ;
                    let oHIE = element;
                    setTimeout(function () {
                        postXCAQuery(url,args,oHIE,jobID);
                    }, timeout);
                });
            });
        });
    });
};
//----------------------------------------------------------------------------------
exports.XCARetrieveAll = function(repository,docId){
    let timeout = XCA_TIMEOUT * 5 ;
    let data = eval(devEnv+"_HIEProperties") ;
    let jobID = new moment().format('YYYYMMDDHHmmss') ;
    if (data){
        data.forEach(function(element, index, theArray) {
            let docId = element.Document_Id ;
            let repository = element.Repository_Id;
            //console.log("Call XCA Retrieve %s for %s",repository,docId);
            setTimeout(function () {
                _XCARetrieve(repository,docId,index,jobID);
            }, timeout);
        }); 
    };
};    
//----------------------------------------------------------------------------------
let _XCARetrieve = (repository,docId,index)=>{
    let oHIE = eval(devEnv+"_HIEProperties[index]") ;
    getXCA_Retrieve('XCA_RETRIEVE',repository,docId,(xmldata) => {          
        let defurl = "http://192.168.130.23:9448";
        if (devEnv == 'PROD') {
            defurl = "http://192.168.180.193:9445";
        }
        //dict.GETSET("URL","SPRL_XCA","SPRL_"+devEnv,defurl,(error,data) => {        
        dict.get("URL","SPRL_XCA","SPRL_"+devEnv,(error,url) => {
            if (isEmpty(url)) {
                url = defurl ; 
                dict.set("URL","SPRL_XCA","SPRL_"+devEnv,defurl)
            }
            url +="/sXCARetrieve";            
            let args = {
                data: xmldata,
                async:false,
                headers: {  
                    "Content-Type": "text/xml; charset=utf-8" 
                }  
            };
            let timeout = index * XCA_TIMEOUT ; 
            setTimeout(function () { 
                postXCARetrieve(url,args,oHIE);
            }, timeout);
        });
    });
}     
//-------------------------------------------------------------------------------------
exports.updateHIEDocs = function(req, res){
    let id = req.query.id;
    let mpiid = req.query.mpiid;
    let name = req.query.name;
    UpdateSPRLDocId();   
    res.send("job started");
}
//----------------------------------------------------------------------------------
let storeSoapEnvelope = function() {
    dict.saveFile("XML","XCA_QUERY","SOAPENV",'',"../../../data/XCA_QUERY.xml")
}
//----------------------------------------------------------------------------------
let getXCA_Query1 = function (name,env,callback){
    if (isEmpty(name)) {
        name = "XCA_QUERY";
    }
    if (isEmpty(env)) {
        env  = "PROD";
    }
	dict.get("XML",name,"SOAPENVELOPE",env,(err,data)=>{
	    if(! isEmpty(data)) {
            callback(err,data);
        } else {
            let soapEnv =  
`<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
<SOAP-ENV:Header>
    <wsa:Action SOAP-ENV:mustUnderstand="true">urn:ihe:iti:2007:CrossGatewayQuery</wsa:Action>
    <wsa:MessageID>urn:uuid:2E8C5DF4-1140-11E6-BF83-5056AC0A4F00</wsa:MessageID>
    <wsa:ReplyTo>
        <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
    </wsa:ReplyTo>
    <wsa:To>%(url)s</wsa:To>
    <Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
        <Timestamp wsu:Id="Timestamp-2E8CB8BC-1140-11E6-BF83-5056AC0A4F00" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:oas="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
            <Created>2016-05-03T15:03:23Z</Created>
            <Expires>2016-05-03T15:08:23Z</Expires>
        </Timestamp>
        <saml:Assertion ID="XId-2E8C86BC-1140-11E6-BF83-5056AC0A4F00" IssueInstant="2016-05-03T15:03:23.791Z" Version="2.0" xmlns="" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">
            <saml:Issuer Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=sprl.shinnyapi.org,OU=Domain Control Validated</saml:Issuer>
            <saml:Subject>
            <saml:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=sprl.shinnyapi.org,OU=Domain Control Validated</saml:NameID>
            </saml:Subject>
            <saml:AuthnStatement AuthnInstant="2016-05-03T15:03:23.791Z">
            <saml:SubjectLocality Address="192.168.120.98" DNSName="SPRL.SHINNYAPI.ORG"/>
            <saml:AuthnContext>
                <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml:AuthnContextClassRef>
            </saml:AuthnContext>
            </saml:AuthnStatement>
            <saml:AttributeStatement>
            <saml:Attribute FriendlyName="SubjectID" Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                <saml:AttributeValue xsi:type="s:string">SystemMonitor</saml:AttributeValue>
            </saml:Attribute>
            <saml:Attribute FriendlyName="SubjectOrganization" Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                <saml:AttributeValue xsi:type="s:string">sPRLMonitoringTool</saml:AttributeValue>
            </saml:Attribute>
            <saml:Attribute FriendlyName="SubjectOrganizationID" Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                <saml:AttributeValue xsi:type="s:string">2.16.840.1.113883.3.2591.400</saml:AttributeValue>
            </saml:Attribute>
            <saml:Attribute FriendlyName="HomeCommunityID" Name="urn:nhin:names:saml:homeCommunityId" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                <saml:AttributeValue xsi:type="s:string">urn:oid:2.16.840.1.113883.3.2591.400</saml:AttributeValue>
            </saml:Attribute>
            <saml:Attribute FriendlyName="SubjectRole" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                <saml:AttributeValue>
                    <s01:Role code="265950004" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="IT Professional" xsi:type="s01:CE" xmlns:s01="urn:hl7-org:v3"/>
                </saml:AttributeValue>
            </saml:Attribute>
            <saml:Attribute FriendlyName="PurposeOfUse" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                <saml:AttributeValue>
                    <s01:PurposeOfUse code="OPERATIONS" codeSystem="2.16.840.1.113883.3.18.7.1" codeSystemName="nhin-purpose" displayName="OPERATIONS" xsi:type="s01:CE" xmlns:s01="urn:hl7-org:v3"/>
                </saml:AttributeValue>
            </saml:Attribute>
            </saml:AttributeStatement>
        </saml:Assertion>
    </Security>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
    <query:AdhocQueryRequest xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0">
        <query:ResponseOption returnComposedObjects="true" returnType="LeafClass"/>
        <rim:AdhocQuery id="urn:uuid:14d4debf-8f97-4251-9a74-a90016b0af0d" xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0">
            <rim:Slot name="$XDSDocumentEntryPatientId">
            <rim:ValueList>
                <rim:Value>'%(MPI_OID)s'</rim:Value>
            </rim:ValueList>
            </rim:Slot>
            <rim:Slot name="$XDSDocumentEntryStatus">
            <rim:ValueList>
                <rim:Value>'%(DocumentStatus)s'</rim:Value>
            </rim:ValueList>
            </rim:Slot>
            <rim:Slot name="$XDSDocumentEntryType">
            <rim:ValueList>
                <rim:Value>'%(DocumentType1)s'</rim:Value>
                <rim:Value>'%(DocumentType2)s'</rim:Value>
            </rim:ValueList>
            </rim:Slot>
        </rim:AdhocQuery>
    </query:AdhocQueryRequest>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>`;        
            dict.set("XML",name,"SOAPENVELOPE",env,soapEnv);
            callback(err,soapEnv);
        }
    });
}

//----------------------------------------------------------------------------------
let getXCA_Query = function (name,element,callback){
    if (isEmpty(name)) {
        name = "XCA_QUERY";
    }
	dict.get("XML",name,"SOAPENVELOPE",devEnv,(err,data)=>{
	    if(! isEmpty(data)) {
            callback(err,data);
        } else {
    let soapEnv =  '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:wsa="http://www.w3.org/2005/08/addressing"\ xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
        <SOAP-ENV:Header>\
            <wsa:Action SOAP-ENV:mustUnderstand="true">urn:ihe:iti:2007:CrossGatewayQuery</wsa:Action>\
            <wsa:MessageID>urn:uuid:2E8C5DF4-1140-11E6-BF83-5056AC0A4F00</wsa:MessageID>\
            <wsa:ReplyTo>\
                <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>\
            </wsa:ReplyTo>\
            <wsa:To>https://sprl.shinnyapi.org:9443/sXCPDQuery</wsa:To>\
            <Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">\
                <Timestamp wsu:Id="Timestamp-2E8CB8BC-1140-11E6-BF83-5056AC0A4F00" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:oas="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">\
                    <Created>2016-05-03T15:03:23Z</Created>\
                    <Expires>2016-05-03T15:08:23Z</Expires>\
                </Timestamp>\
                <saml:Assertion ID="XId-2E8C86BC-1140-11E6-BF83-5056AC0A4F00" IssueInstant="2016-05-03T15:03:23.791Z" Version="2.0" xmlns="" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">\
                    <saml:Issuer Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=sprl.shinnyapi.org,OU=Domain Control Validated</saml:Issuer>\
                    <saml:Subject>\
                    <saml:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=sprl.shinnyapi.org,OU=Domain Control Validated</saml:NameID>\
                    </saml:Subject>\
                    <saml:AuthnStatement AuthnInstant="2016-05-03T15:03:23.791Z">\
                    <saml:SubjectLocality Address="192.168.120.98" DNSName="SPRL.SHINNYAPI.ORG"/>\
                    <saml:AuthnContext>\
                        <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml:AuthnContextClassRef>\
                    </saml:AuthnContext>\
                    </saml:AuthnStatement>\
                    <saml:AttributeStatement>\
                    <saml:Attribute FriendlyName="SubjectID" Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                        <saml:AttributeValue xsi:type="s:string">SystemMonitor</saml:AttributeValue>\
                    </saml:Attribute>\
                    <saml:Attribute FriendlyName="SubjectOrganization" Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                        <saml:AttributeValue xsi:type="s:string">sPRLMonitoringTool</saml:AttributeValue>\
                    </saml:Attribute>\
                    <saml:Attribute FriendlyName="SubjectOrganizationID" Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                        <saml:AttributeValue xsi:type="s:string">2.16.840.1.113883.3.2591.400</saml:AttributeValue>\
                    </saml:Attribute>\
                    <saml:Attribute FriendlyName="HomeCommunityID" Name="urn:nhin:names:saml:homeCommunityId" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                        <saml:AttributeValue xsi:type="s:string">urn:oid:2.16.840.1.113883.3.2591.400</saml:AttributeValue>\
                    </saml:Attribute>\
                    <saml:Attribute FriendlyName="SubjectRole" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                        <saml:AttributeValue>\
                            <s01:Role code="265950004" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="IT Professional" xsi:type="s01:CE" xmlns:s01="urn:hl7-org:v3"/>\
                        </saml:AttributeValue>\
                    </saml:Attribute>\
                    <saml:Attribute FriendlyName="PurposeOfUse" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                        <saml:AttributeValue>\
                            <s01:PurposeOfUse code="OPERATIONS" codeSystem="2.16.840.1.113883.3.18.7.1" codeSystemName="nhin-purpose" displayName="OPERATIONS" xsi:type="s01:CE" xmlns:s01="urn:hl7-org:v3"/>\
                        </saml:AttributeValue>\
                    </saml:Attribute>\
                    </saml:AttributeStatement>\
                </saml:Assertion>\
            </Security>\
        </SOAP-ENV:Header>\
        <SOAP-ENV:Body>\
            <query:AdhocQueryRequest xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0">\
                <query:ResponseOption returnComposedObjects="true" returnType="LeafClass"/>\
                <rim:AdhocQuery id="urn:uuid:14d4debf-8f97-4251-9a74-a90016b0af0d" xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0">\
                    <rim:Slot name="$XDSDocumentEntryPatientId">\
                    <rim:ValueList>\
                        <rim:Value>'+element.MPI_OID+'</rim:Value>\
                    </rim:ValueList>\
                    </rim:Slot>\
                    <rim:Slot name="$XDSDocumentEntryStatus">\
                    <rim:ValueList>\
                        <rim:Value>'+element.DocumentStatus+'</rim:Value>\
                    </rim:ValueList>\
                    </rim:Slot>\
                    <rim:Slot name="$XDSDocumentEntryType">\
                    <rim:ValueList>\
                        <rim:Value>'+element.DocumentType1+'</rim:Value>\
                        <rim:Value>'+element.DocumentType2+'</rim:Value>\
                    </rim:ValueList>\
                    </rim:Slot>\
                </rim:AdhocQuery>\
            </query:AdhocQueryRequest>\
        </SOAP-ENV:Body>\
        </SOAP-ENV:Envelope>';        

         dict.set("XML",name,"SOAPENVELOPE",devEnv,soapEnv);
         callback("",soapEnv);
         if (DEBUG ) {
            fs.writeFile("/tmp/XCA_Query_SOAP_"+element.QE_Name, soapEnv, function(err) {
                if(err) { console.log(err); }
            });        
        }
    }
    });
}

//----------------------------------------------------------------------------------
let getXDS_Query = function (name,url,callback){ 
    if (isEmpty(name)) {
        name = "XDS_QUERY";
    }
    let soapEnv = `<?xml version="1.0" encoding="utf-8"?>
        <soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope"> 
            <soapenv:Header xmlns:wsa="http://www.w3.org/2005/08/addressing"> 
            <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                <saml2:Assertion ID="OHT-SAML-ASSERTION" IssueInstant="2016-05-02T18:38:05.797Z" Version="2.0" xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xenc="http://www.w3.org/2001/04/xmlenc#" xmlns:exc14n="http://www.w3.org/2001/10/xml-exc-c14n#" xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
                    <saml2:Issuer Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=*.sthl.saas.infor.com, OU=PremiumSSL Wildcard, O=Infor Enterprise Solutions Holdings, L=Alpharetta, ST=GA, C=US</saml2:Issuer>
                    <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
                    <SignedInfo>
                        <CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                        <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"/>
                        <Reference URI="#OHT-SAML-ASSERTION">
                            <Transforms>
                                <Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
                                <Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                            </Transforms>
                            <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/>
                            <DigestValue>enrOov7e3rzINJXTLd4kdf0RhsA=</DigestValue>
                        </Reference>
                    </SignedInfo>
                    <SignatureValue>VaCxLtiGt7V85oEDp5vswm51PGGelilmdg92dcIGsB1uEEZi0jNg6Gesj+69rMOkw4S3FIrRY16p dgfvPnFdBIktaQKx523ux3eViUEZUmVvOQeLn97YaURL3QmRhuEY1wNxa72UG6zIPyRBcl1sFagc GmK/mre9CKwWbmuAl+Pn5QuD7lPsaAINO9jXHi3LCto+ICk72lL+BabVzTPhp+d9bMCc5ukwXRxp K4byxK3seyPHST93PC7iN6P15R1B0RJf+jvas9+6+Kk/UCeZm1m4L60KYA+2p4HBxZoctLbDXoT6 TUDdt8q5LHCekn7LgZgRMEpMQZCzqJ/QXZ6Ndg==</SignatureValue>
                    <KeyInfo>
                        <X509Data>
                            <X509Certificate>MIIGVDCCBTygAwIBAgIRAOy/M08iokL5bVYLRB4D7HwwDQYJKoZIhvcNAQELBQAwgZYxCzAJBgNV BAYTAkdCMRswGQYDVQQIExJHcmVhdGVyIE1hbmNoZXN0ZXIxEDAOBgNVBAcTB1NhbGZvcmQxGjAY BgNVBAoTEUNPTU9ETyBDQSBMaW1pdGVkMTwwOgYDVQQDEzNDT01PRE8gUlNBIE9yZ2FuaXphdGlv biBWYWxpZGF0aW9uIFNlY3VyZSBTZXJ2ZXIgQ0EwHhcNMTUwMjA0MDAwMDAwWhcNMjAwMjAzMjM1 OTU5WjCCAToxCzAJBgNVBAYTAlVTMQ4wDAYDVQQREwUzMDAwNDELMAkGA1UECBMCR0ExEzARBgNV BAcTCkFscGhhcmV0dGExEzARBgNVBAkTClN1aXRlIDQxMDAxGjAYBgNVBAkTETEzNTYwIE1vcnJp cyBSb2FkMTIwMAYDVQQKEylJbmZvciBFbnRlcnByaXNlIFNvbHV0aW9ucyBIb2xkaW5ncywgSW5j LjELMAkGA1UECxMCSVQxSTBHBgNVBAsTQElzc3VlZCB0aHJvdWdoIEluZm9yIEVudGVycHJpc2Ug U29sdXRpb25zIEhvbGRpbmdzLCBJbmMuIEUtUEtJIE0xHDAaBgNVBAsTE1ByZW1pdW1TU0wgV2ls ZGNhcmQxHjAcBgNVBAMUFSouc3RobC5zYWFzLmluZm9yLmNvbTCCASIwDQYJKoZIhvcNAQEBBQAD ggEPADCCAQoCggEBAJvYMh65WO6u6XMEqc7ThmqMq9iU/VtvoWckTE2Ba0Tm505TRoakmDiImdCo 9tbQ4jqT75z7lo6bNdBNWyBh3T1hA+w9NzwO52vpPy/x79rB4ulLZAu+6n5XiPS+GqdU2ONnp7Iw 7iPdVmaGfRjvI0PjTXuGpZOjYNEj3YT3l4OAXQ2w2RrbcYB5age54vDBJTf2d4hNx6+Thf6bptGO 5548nTIdr8BTf2ixBDa/7uNM91KNvEInVgMQ5y0zuzoWn9mRLiZAeRHNupAo+hVVLhCsScV8y2ap 4JhAkMfIe5nZ1u0JEWvYLr3Jez9zlA196rWlTrCxUl0X/QHrXYgAik0CAwEAAaOCAfQwggHwMB8G A1UdIwQYMBaAFJrzK9rPrU+2L7sqSEgqErcbQsEkMB0GA1UdDgQWBBQJGPjHgPWDcrSwtOQrjtWb s3/fDTAOBgNVHQ8BAf8EBAMCBaAwDAYDVR0TAQH/BAIwADAdBgNVHSUEFjAUBggrBgEFBQcDAQYI KwYBBQUHAwIwUAYDVR0gBEkwRzA7BgwrBgEEAbIxAQIBAwQwKzApBggrBgEFBQcCARYdaHR0cHM6 Ly9zZWN1cmUuY29tb2RvLmNvbS9DUFMwCAYGZ4EMAQICMFoGA1UdHwRTMFEwT6BNoEuGSWh0dHA6 Ly9jcmwuY29tb2RvY2EuY29tL0NPTU9ET1JTQU9yZ2FuaXphdGlvblZhbGlkYXRpb25TZWN1cmVT ZXJ2ZXJDQS5jcmwwgYsGCCsGAQUFBwEBBH8wfTBVBggrBgEFBQcwAoZJaHR0cDovL2NydC5jb21v ZG9jYS5jb20vQ09NT0RPUlNBT3JnYW5pemF0aW9uVmFsaWRhdGlvblNlY3VyZVNlcnZlckNBLmNy dDAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuY29tb2RvY2EuY29tMDUGA1UdEQQuMCyCFSouc3Ro bC5zYWFzLmluZm9yLmNvbYITc3RobC5zYWFzLmluZm9yLmNvbTANBgkqhkiG9w0BAQsFAAOCAQEA slLz7uqf58ePHPHQujQZ4vwd5zuiZOEoNA5Gy+zJZTO9JFXD32TUWA0LI8jKgicHEhFU7lV8fjhf Sbi00FSG4ELucFPe1JU9Jn1f++CdS7/xWweE5VtZyoKW3je+6FNJ77ZzekpRD1ExhNBSrNCHm+cq af4ChsAt0B3iwvqnqFNlkDGTfTg16yayfEam7ZvqKDt/BSdTOkCT5F5NrKo1EVE4QrVhuV1p3zoq kgG9hO4/H+6IByO6SGbQdOs6cms2XcMfmUjIPpkVrN/38tKABkTpuI2HiFQT7m9G7XGanWKe4Lks YnuLwdn2NZv/7zw2128qEcGjHknvrwIa2soR0A==</X509Certificate>
                        </X509Data>
                        <KeyValue>
                            <RSAKeyValue>
                                <Modulus>m9gyHrlY7q7pcwSpztOGaoyr2JT9W2+hZyRMTYFrRObnTlNGhqSYOIiZ0Kj21tDiOpPvnPuWjps1 0E1bIGHdPWED7D03PA7na+k/L/Hv2sHi6UtkC77qfleI9L4ap1TY42ensjDuI91WZoZ9GO8jQ+NN e4alk6Ng0SPdhPeXg4BdDbDZGttxgHlqB7ni8MElN/Z3iE3Hr5OF/pum0Y7nnjydMh2vwFN/aLEE Nr/u40z3Uo28QidWAxDnLTO7Ohaf2ZEuJkB5Ec26kCj6FVUuEKxJxXzLZqngmECQx8h7mdnW7QkR a9guvcl7P3OUDX3qtaVOsLFSXRf9AetdiACKTQ==</Modulus>
                                <Exponent>AQAB</Exponent>
                            </RSAKeyValue>
                        </KeyValue>
                    </KeyInfo>
                    </Signature>
                    <saml2:Subject>
                    <saml2:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=*.sthl.saas.infor.com, OU=PremiumSSL Wildcard, O=Infor Enterprise Solutions Holdings, L=Alpharetta, ST=GA, C=US</saml2:NameID>
                    <saml2:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:holder-of-key">
                        <saml2:SubjectConfirmationData>
                            <ds:KeyInfo>
                                <ds:KeyValue>
                                <ds:RSAKeyValue>
                                    <ds:Modulus>m9gyHrlY7q7pcwSpztOGaoyr2JT9W2+hZyRMTYFrRObnTlNGhqSYOIiZ0Kj21tDiOpPvnPuWjps1 0E1bIGHdPWED7D03PA7na+k/L/Hv2sHi6UtkC77qfleI9L4ap1TY42ensjDuI91WZoZ9GO8jQ+NN e4alk6Ng0SPdhPeXg4BdDbDZGttxgHlqB7ni8MElN/Z3iE3Hr5OF/pum0Y7nnjydMh2vwFN/aLEE Nr/u40z3Uo28QidWAxDnLTO7Ohaf2ZEuJkB5Ec26kCj6FVUuEKxJxXzLZqngmECQx8h7mdnW7QkR a9guvcl7P3OUDX3qtaVOsLFSXRf9AetdiACKTQ==</ds:Modulus>
                                    <ds:Exponent>AQAB</ds:Exponent>
                                </ds:RSAKeyValue>
                                </ds:KeyValue>
                            </ds:KeyInfo>
                        </saml2:SubjectConfirmationData>
                    </saml2:SubjectConfirmation>
                    </saml2:Subject>
                    <saml2:Conditions NotBefore="" NotOnOrAfter=""/>
                    <saml2:AuthnStatement AuthnInstant="2016-05-02T18:38:05.797Z">
                    <saml2:SubjectLocality Address="10.1.100.35" DNSName="ShinnyClient"/>
                    <saml2:AuthnContext>
                        <saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:X509</saml2:AuthnContextClassRef>
                        <saml2:AuthenticatingAuthority>CN=*.sthl.saas.infor.com, OU=PremiumSSL Wildcard, O=Infor Enterprise Solutions Holdings, L=Alpharetta, ST=GA, C=US</saml2:AuthenticatingAuthority>
                    </saml2:AuthnContext>
                    </saml2:AuthnStatement>
                    <saml2:AttributeStatement>
                    <saml2:Attribute FriendlyName="SubjectID" Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                        <saml2:AttributeValue xsi:type="xs:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">jspears</saml2:AttributeValue>
                    </saml2:Attribute>
                    <saml2:Attribute FriendlyName="SubjectOrganization" Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                        <saml2:AttributeValue xsi:type="xs:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">HealthlinkNY</saml2:AttributeValue>
                    </saml2:Attribute>
                    <saml2:Attribute FriendlyName="SubjectOrganizationID" Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                        <saml2:AttributeValue xsi:type="xs:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">2.16.840.1.113883.3.234.1.4.1.17</saml2:AttributeValue>
                    </saml2:Attribute>
                    <saml2:Attribute FriendlyName="HomeCommunityID" Name="urn:nhin:names:saml:homeCommunityId" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                        <saml2:AttributeValue xsi:type="xs:string" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">urn:oid:2.16.840.1.113883.3.234.1.4.1.17.1.5.3.1.3</saml2:AttributeValue>
                    </saml2:Attribute>
                    <saml2:Attribute FriendlyName="SubjectRole" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                        <saml2:AttributeValue>
                            <hl7:Role code="224608005" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="Administrative Healthcare Staff" xsi:type="hl7:CE" xmlns:hl7="urn:hl7-org:v3" xmlns="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
                        </saml2:AttributeValue>
                    </saml2:Attribute>
                    <saml2:Attribute FriendlyName="PurposeOfUse" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                        <saml2:AttributeValue>
                            <hl7:PurposeOfUse code="TREATMENT" codeSystem="2.16.840.1.113883.3.18.7.1" codeSystemName="nhin-purpose" displayName="TREATMENT" xsi:type="hl7:CE" xmlns:hl7="urn:hl7-org:v3" xmlns="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
                        </saml2:AttributeValue>
                    </saml2:Attribute>
                    </saml2:AttributeStatement>
                </saml2:Assertion>
                <wsu:Timestamp ID="TIMESTAMP" wsu:Id="TIMESTAMP" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                    <wsu:Created>2016-05-02T18:38:05.853Z</wsu:Created>
                    <wsu:Expires>2016-05-02T18:43:05.853Z</wsu:Expires>
                </wsu:Timestamp>
                <Signature xmlns="http://www.w3.org/2000/09/xmldsig#">
                    <SignedInfo>
                    <CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                    <SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"/>
                    <Reference URI="#TIMESTAMP">
                        <Transforms>
                            <Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
                        </Transforms>
                        <DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/>
                        <DigestValue>ybt/04eBenHr9+/CPFbYrhQo5nI=</DigestValue>
                    </Reference>
                    </SignedInfo>
                    <SignatureValue>BjMW2Td2gel+8H1G9SWrKi8f+QpkC/8diSG2d4DN1a71ptQ1XTi/NU+nDf2sGCVYPVpkJkN0T6QS 68/3iTIuD0ODnDW2Y4THghlzzqadkgdXBryt8L7LLYV91JG+Mggk5D1mFSZNL7Y5fO2UXCXkYV9x qlx7QVdJbkmEXH3X//Qh0SRoj+HLvarBhrR4mt5PLm1pjm5c+QVI3dLPdkuC2sy98zb4eIPgXBcn 5Ok7OI1DkYUEIv3EEC1+wK1dWGfbmFbmtogVpATn1NEnphc8oy+WIJhgAx+Bbg7Bfz1EzsiGAKXs EpmLPT17bYv43DWgecNJA2hZfX3CSnwojCXMBQ==</SignatureValue>
                    <KeyInfo>
                    <wsse:SecurityTokenReference wsse11:TokenType="http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0" xmlns:wsse11="http://docs.oasis-open.org/wss/oasis-wss-wssecurity-secext-1.1.xsd">
                        <wsse:KeyIdentifier ValueType="http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLID">OHT-SAML-ASSERTION</wsse:KeyIdentifier>
                    </wsse:SecurityTokenReference>
                    </KeyInfo>
                </Signature>
            </wsse:Security>
            <wsa:To>https://stagsprl.shinnyapi.org:9443/sXCPDQuery</wsa:To>
            <wsa:ReplyTo>
                <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
            </wsa:ReplyTo>
            <wsa:MessageID>urn:uuid:1ba185ae-7b9e-2c74-c72c-db8c55d46e80</wsa:MessageID>
            <wsa:Action>urn:hl7-org:v3:PRPA_IN201305UV02:CrossGatewayPatientDiscovery</wsa:Action>
        </soapenv:Header>
        <soapenv:Body>
            <v3:PRPA_IN201305UV02 ITSVersion="XML_1.0" xmlns:v3="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <v3:id extension="-146cafe5:1533e98506b:-3c68" root="2.16.840.1.113883.3.234.1.4.1.17.1.6.4.1.1.100103036033006191.1462214285861.65"/>
                <v3:creationTime value="20160502183805"/>
                <v3:interactionId extension="PRPA_IN201305UV02" root="2.16.840.1.113883.1.6"/>
                <v3:processingCode code="P"/>
                <v3:processingModeCode code="T"/>
                <v3:acceptAckCode code="AL"/>
                <v3:receiver typeCode="RCV">
                    <v3:device classCode="DEV" determinerCode="INSTANCE">
                    <v3:id root="2.16.840.1.113883.3.2591.600.1.99.3.1"/>
                    </v3:device>
                </v3:receiver>
                <v3:sender typeCode="SND">
                    <v3:device classCode="DEV" determinerCode="INSTANCE">
                    <v3:id root="2.16.840.1.113883.3.234.1.4.1.17.1.6.4.1.1"/>
                    </v3:device>
                </v3:sender>
               classCode="CACT" moodCode="EVN">
                    <v3:code code="PRPA_TE201305UV02" codeSystem="2.16.840.1.113883.1.18"/>
                    <v3:queryByParameter>
                    <v3:queryId extension="%(extension)s" root="%(root)s"/>
                    <v3:statusCode code="new"/>
                    <v3:responseModalityCode code="R"/>
                    <v3:responsePriorityCode code="I"/>
                    <v3:parameterList>
                    <livingSubjectId>
                            <value extension="1360358721" root="2.16.840.1.113883.3.458.10.3.1"/>
                            <semanticsText representation="TXT">LivingSubject.id</semanticsText>
                        </livingSubjectId>
                        <v3:livingSubjectAdministrativeGender>
                            <v3:value code="%(Gender)s"/>
                            <v3:semanticsText>LivingSubject.administrativeGender</v3:semanticsText>
                        </v3:livingSubjectAdministrativeGender>
                        <v3:livingSubjectBirthTime>
                            <v3:value value="%(DOB)s"/>
                            <v3:semanticsText>LivingSubject.birthTime</v3:semanticsText>
                        </v3:livingSubjectBirthTime>
                        <v3:livingSubjectName>
                            <v3:value xsi:type="v3:PN">
                                <v3:family>%(family)s</v3:family>
                                <v3:given>%(given)s</v3:given>
                                <v3:given>%(middle)s</v3:given>
                            </v3:value>
                            <v3:semanticsText>LivingSubject.name</v3:semanticsText>
                        </v3:livingSubjectName>
                        <v3:patientAddress>
                            <v3:value>
                                <v3:streetAddressLine>%(streetAddressLine)s</v3:streetAddressLine>
                                <v3:city>%(city)s</v3:city>
                                <v3:state>%(state)s</v3:state>
                                <v3:country>%(country)s</v3:country>
                                <v3:postalCode>%(postalCode)s</v3:postalCode>
                            </v3:value>
                            <v3:semanticsText>Patient.addr</v3:semanticsText>
                        </v3:patientAddress>
                        </v3:parameterList>
                    </v3:queryByParameter>
                </v3:controlActProcess>
            </v3:PRPA_IN201305UV02>
        </soapenv:Body>
    </soapenv:Envelope>`
    //dict.GETSET("XML",name,"SPRLSOAPENV",devEnv,soapEnv,(error,result) => {
    dict.get("XML",name,"SPRLSOAPENV",devEnv,(error,result) => {
        if (isEmpty(result) ) {
            result = soapEnv ;
            dict.set("XML",name,"SPRLSOAPENV",devEnv,soapEnv)
        }
        if(callback) {
            callback(result,error)
        } else {
            return result;
        }
    });
}

//-------------------------------------------------------------------------------------
let getXCA_Retrieve = function (name,repository,docId,callback){
    if (isEmpty(name)) {
        name = "XCA_RETRIEVE";
    }
    let soapEnv = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">\
          <soap:Header>\
            <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soap:mustUnderstand="true">\
              <saml2:Assertion xmlns:saml2="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:xs="http://www.w3.org/2001/XMLSchema" ID="_655651ee4e7c4c76a678f5e4d7f279a6" IssueInstant="2016-05-02T15:58:31.015Z" Version="2.0">\
                <saml2:Issuer Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=SAML User,OU=SU,O=SAML User,L=Los Angeles,ST=CA,C=US</saml2:Issuer>\
                <saml2:Subject>\
                  <saml2:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=sprl.shinnyapi.org, OU=Domain Control Validated</saml2:NameID>\
                  </saml2:Subject>\
                <saml2:AuthnStatement AuthnInstant="2016-05-02T15:58:30.000Z" SessionIndex="987">\
                  <saml2:SubjectLocality Address="158.147.185.168" DNSName="158.147.185.168"/>\
                  <saml2:AuthnContext>\
                    <saml2:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:X509</saml2:AuthnContextClassRef>\
                  </saml2:AuthnContext>\
                </saml2:AuthnStatement>\
                <saml2:AttributeStatement>\
                  <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">SystemMonitor</saml2:AttributeValue>\
                  </saml2:Attribute>\
                  <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">sPRLMonitoringTool</saml2:AttributeValue>\
                  </saml2:Attribute>\
                  <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">urn:oid:2.16.840.1.113883.3.2591.400</saml2:AttributeValue>\
                  </saml2:Attribute>\
                  <saml2:Attribute Name="urn:nhin:names:saml:homeCommunityId" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">urn:oid:2.16.840.1.113883.3.2591.400</saml2:AttributeValue>\
                  </saml2:Attribute>\
                  <saml2:Attribute Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue>\
                      <hl7:Role xmlns:hl7="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="265950004" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="IT Professional" xsi:type="hl7:CE"/>\
                    </saml2:AttributeValue>\
                  </saml2:Attribute>\
                  <saml2:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">\
                    <saml2:AttributeValue>\
                      <hl7:PurposeOfUse xmlns:hl7="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="OPERATIONS" codeSystem="2.16.840.1.113883.3.18.7.1" codeSystemName="nhin-purpose" displayName="OPERATIONS" xsi:type="hl7:CE"/>\
                    </saml2:AttributeValue>\
                  </saml2:Attribute>\
                </saml2:AttributeStatement>\
              </saml2:Assertion>\
              <wsu:Timestamp wsu:Id="TS-460585">\
                <wsu:Created>2017-05-02T15:58:31.014Z</wsu:Created>\
                <wsu:Expires>2017-05-02T16:58:31.014Z</wsu:Expires>\
              </wsu:Timestamp>\
            </wsse:Security>\
            <Action xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">urn:ihe:iti:2007:CrossGatewayRetrieve</Action>\
            <MessageID xmlns="http://www.w3.org/2005/08/addressing">urn:uuid:48fccf69-1640-4774-9dd3-de1fc2a7614b</MessageID>\
            <To xmlns="http://www.w3.org/2005/08/addressing">https://sprl.shinnyapi.org:9443/sXCARetrieve</To>\
            <ReplyTo xmlns="http://www.w3.org/2005/08/addressing" soap:mustUnderstand="true">\
              <Address>http://www.w3.org/2005/08/addressing/anonymous</Address>\
            </ReplyTo>\
          </soap:Header>\
          <soap:Body>\
            <RetrieveDocumentSetRequest xmlns="urn:ihe:iti:xds-b:2007" xmlns:ns2="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0" xmlns:ns3="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0" xmlns:ns4="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0" xmlns:ns5="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0">\
              <DocumentRequest>\
                <HomeCommunityId>urn:oid:2.16.840.1.113883.3.2591.400</HomeCommunityId>\
                <RepositoryUniqueId>'+repository+'</RepositoryUniqueId>\
                <DocumentUniqueId>'+docId+'</DocumentUniqueId>\
              </DocumentRequest>\
            </RetrieveDocumentSetRequest>\
          </soap:Body>\
        </soap:Envelope>';
   
    if (callback) {
        callback(soapEnv,"");
    }
    return soapEnv
};

//----------------------------------------------------------------------------------
let getPIX_Query = function (name,callback){ 
    if (isEmpty(name)) {
        name = "PIX_QUERY";
    }
    let soapEnv = 
`<SOAP-ENV:Envelope xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope">
        <SOAP-ENV:Header>
          <wsa:Action SOAP-ENV:mustUnderstand="true">urn:hl7-org:v3:PRPA_IN201309UV02</wsa:Action>
          <wsa:MessageID>urn:uuid:806A4F53-C99F-4C19-8D64-1BFBDD96F876</wsa:MessageID>
          <wsa:ReplyTo>
            <wsa:Address>http://www.w3.org/2005/08/addressing/anonymous</wsa:Address>
          </wsa:ReplyTo>
          <wsa:To xmlns:regex="http://exslt.org/regular-expressions" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:s="http://www.w3.org/2003/05/soap-envelope" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">https://sprl.shinnyapi.org:9443/pixmanager</wsa:To>
          <Security xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
           
            <saml:Assertion ID="XC932A8AF-F707-4313-ADBE-3BDD8F37991B" IssueInstant="2016-05-12T16:16:32.696Z" Version="2.0" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns="">
              <saml:Issuer Format="urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName">CN=*.hixny.com,OU=COMODO SSL Wildcard,OU=Domain Control Validated</saml:Issuer>
                          
              <saml:AttributeStatement>
                <saml:Attribute FriendlyName="SubjectID" Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml:AttributeValue xsi:type="s:string">HIXNYQA1008</saml:AttributeValue>
                </saml:Attribute>
                <saml:Attribute FriendlyName="SubjectOrganization" Name="urn:oasis:names:tc:xspa:1.0:subject:organization" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml:AttributeValue xsi:type="s:string">HIXNY</saml:AttributeValue>
                </saml:Attribute>
                <saml:Attribute FriendlyName="SubjectOrganizationID" Name="urn:oasis:names:tc:xspa:1.0:subject:organization-id" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml:AttributeValue xsi:type="s:string">2.16.840.1.113883.4.319</saml:AttributeValue>
                </saml:Attribute>
                <saml:Attribute FriendlyName="HomeCommunityID" Name="urn:nhin:names:saml:homeCommunityId" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml:AttributeValue xsi:type="s:string">2.16.840.1.113883.4.319</saml:AttributeValue>
                </saml:Attribute>
                <saml:Attribute FriendlyName="SubjectRole" Name="urn:oasis:names:tc:xacml:2.0:subject:role" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml:AttributeValue>
                    <s01:Role code="112247003" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="Medical doctor" xsi:type="s01:CE" xmlns:s01="urn:hl7-org:v3"/>
                  </saml:AttributeValue>
                </saml:Attribute>
                <saml:Attribute FriendlyName="PurposeOfUse" Name="urn:oasis:names:tc:xspa:1.0:subject:purposeofuse" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
                  <saml:AttributeValue>
                    <s01:PurposeOfUse code="EMERGENCY" codeSystem="2.16.840.1.113883.3.18.7.1" codeSystemName="nhin-purpose" displayName="Emergency" xsi:type="s01:CE" xmlns:s01="urn:hl7-org:v3"/>
                  </saml:AttributeValue>
                </saml:Attribute>
              </saml:AttributeStatement>
              
            </saml:Assertion>
            
          </Security>
        </SOAP-ENV:Header>
        <SOAP-ENV:Body>
          <PRPA_IN201309UV02 ITSVersion="XML_1.0" xmlns:hl7="www.hl7.org" xmlns="urn:hl7-org:v3">
            <id root="397E4D7B-BDE6-43EA-9509-2757078074D4"/>
            <creationTime value="20160512121632-0500"/>
            <interactionId root="2.16.840.1.113883.1.6" extension="PRPA_IN201309UV02"/>
            <processingCode code="P"/>
            <processingModeCode code="T"/>
            <acceptAckCode code="AL"/>
            <receiver typeCode="RCV">
              <device classCode="DEV" determinerCode="INSTANCE">
                <id root="2.16.840.1.113883.3.2591.500.1"/>
              </device>
            </receiver>
            <sender typeCode="SND">
              <device classCode="DEV" determinerCode="INSTANCE">
                <id root="2.16.840.1.113883.4.319"/>
              </device>
            </sender>
            <controlActProcess classCode="CACT" moodCode="EVN">
              <code code="PRPA_TE201309UV02" codeSystem="2.16.840.1.113883.1.6"/>
              <authorOrPerformer typeCode="AUT">
                <assignedPerson classCode="ASSIGNED"/>
              </authorOrPerformer>
              <queryByParameter>
                <queryId root="3E2985AF-C494-471D-A22F-96644B54511B" extension="1"/>
                <statusCode code="new"/>
                <responsePriorityCode code="I"/>
                <parameterList>
                  <patientIdentifier>
                    <value root="2.16.840.1.113883.3.2591.700.1" extension="%s"/>
                    <semanticsText>Patient.id</semanticsText>
                  </patientIdentifier>
                </parameterList>
              </queryByParameter>
            </controlActProcess>
          </PRPA_IN201309UV02>
        </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>`
    dict.get("XML",name,"SOAPENV","", (err,data) => {
        if( isEmpty(data)) {
            dict.set("XML",name,"SOAPENV","",soapEnv);
            if (callback) {
                callback(err,soapEnv);
            }
        } else {
            if (callback) {
                callback("",data);
            }            
        }
    });
    return soapEnv
}
//---------------------------------------------------------------
exports.testmpi = function(req,res){
    let mpiid = req.params.mpiid;    
    console.log(mpiid)
    if (isEmpty(mpiid)) {
        mpiid = '100000180';
    }
    console.log("ENTER TestMPI");
     getPixUsersMPI(mpiid,(err,data)=>{
         res.send(data) ;
     }); 
};
//----------------------------------------------------------------------------------
module.exports.htmlsprllist = function(req, res){  // output to jqgrid
   let mpiid = req.params.mpiid ;
   setup_MPIcolumns( (colSetup) => {   
        res.render('mpi',{mpiid:mpiid,page_title:"HeartBeat Monitor ",colsetup:colSetup});                        
   });        
}
//---------------------------------------------------
let setup_MPIcolumns = function(callback) {
            let colsetup = { 
                url:'/SPRL/testmpi', 
                altRows: false, 
                deepempty: true,
                autowidth: true,
                ignoreCase: true,
                datatype: "json",
                mtype: "GET",       
                height:'auto',
                width:'auto',
                colModel:[
                    {label:'QE',name:'qe', editable:false ,width:150},
                    {label:'oid',name:'root', editable:false , width:250},
                    {label:'mpi',name:'ext', editable:false ,width:100 },
                    {label:'', width:1,search:false}    
                ],
                loadonce:true,
                toppager:true,
                rowNum: 25,
                rowTotal:5000,
                viewrecords: true,
                gridview: true,
                autoencode: true,            
                caption: "Config"          
            }
            callback( colsetup);  
};
//-------------------------------------------------------------------------------------
let getPixUsersMPI = exports.getPixUsersMPI = (mpiid,callback)=>{
    let cl = new Client();
    //let STAGE_url = 'http://192.168.180.193:9445/PIXManager';
    //let PROD_url = 'http://192.168.180.23:9448/PIXManager';
    //let defurl = eval(devEnv+"_url");
    try {
        //dict.set("URL","PIX_MANAGER","SPRL_"+devEnv,'',defurl,(err,data) => {
            //let url = defurl;
            //if (! isEmpty(data)){
            //    url = data;
            //}
            let url = config.SPRL_url +"/PIXManager" ;
            console.log(url)  
            getPIX_Query('PIX_QUERY',(err,xml)=>{
                //console.log(xmldata);
                let sprintf = require("sprintf-js").sprintf;
                let xmldata = sprintf(xml, mpiid)             
                let args = {
                    data: xmldata,
                    headers: {  
                        "Content-Type": "text/xml; charset=utf-8"
                    }  
                };
                stopwatch.start();  
                cl.post(url, args, function (data, response) {            
                    data = data.toString();
                    if ( ! isEmpty(data) ) {
                        let select = require('xpath.js') ;
                        let dom = require('xmldom').DOMParser ;
                        let doc = new dom().parseFromString(data) ;   
                        let qryRes = select(doc, "//*[local-name()='queryResponseCode']/@code").toString().split("=")[1].replace(/"/g,'');
                        //console.log("RES %s",qryRes);
                        if (qryRes != "OK" ) {
                            callback("No Patients found",[])
                            console.log("No Patients found") ;            
                        } else { 
                                //get the number of AA's 
                                let alist = [];
                                let patients = select(doc, "//*[local-name()='patient']/*[local-name()='id']");
                                if (patients.length > 0 ){
                                patients.forEach(function(element, index, theArray) {
                                    let qename = select(element,"@assigningAuthorityName").toString().split("=")[1].replace(/"/g,'');
                                    let ext = select(element,"@extension").toString().split("=")[1].replace(/"/g,'');
                                    let root = select(element,"@root").toString().split("=")[1].replace(/"/g,'');
                                    alist.push({"qe":qename,"ext":ext,"root":root})
                                });
                                }
                            console.log(alist);
                            callback("",alist) ; 
                        }    
                    } else {
                            callback("Error No Patients ",[])
                    }          
                });
                stopwatch.stop();            
            });
        //});
    } catch (Ex) {
       callback("getPixUsersMPI Error: "+Ex,[])        
    };
    return true
}

//-------------------------------------------------------------------------------------
exports.getUsers = function(req, res){
    let id = req.query.id;
    let mpiid = req.query.mpiid;    
    let oid = req.query.oid;
    let name = req.query.name;
    let input = JSON.parse(JSON.stringify(req.body)); //POsT DATA
    console.log("INPUT->:%s",input);
    if (isEmpty(name)) { 
        name = "SPRLPATIENTS";
    }
    if (isEmpty(oid)) {
        oid = "5160563^^^&amp;2.16.840.1.113883.13.61&amp;ISO"
    }
    var url ="http://192.168.130.23:9448/sXCPDQuery";
    //var url = "http://192.168.180.193:9445/sXCAQuery";
    var defurl = "http://192.168.130.23:9448";
    if (devEnv == 'PROD') {
            defurl = "http://192.168.180.193:9445";
    }
    //dict.GETSET("URL","SPRL_XCA","SPRL_"+devEnv,defurl,(error,data) => {
    dict.get("URL","SPRL_XCA","SPRL_"+devEnv,(error,data) => {
        if (isEmpty(data)){

        }
        var url = data+"/sXCPDQuery";  
        getXDS_Query('XDS_QUERY',url,(xmldata,err)=>{
            //console.log(xmldata);
            var sprintf = require("sprintf-js").sprintf;
            var soapEnv = sprintf(xmldata, livingSubjectQuery)             
            var args = {
                data: xmldata,
                headers: {  
                    "Content-Type": "text/xml; charset=utf-8"
                }  
            };
            var cReq = client.post(url, args, function (data, response) {                
                var select = require('xpath.js') ;
                var dom = require('xmldom').DOMParser ;
                var doc = new dom().parseFromString(data.toString()) ;   
                //console.log(doc);
                var nodes = select(doc, "//*[local-name()='subject']");
                //var nodes = select(doc, "//*[local-name()='ExternalIdentifier']");
                //var nodes = select(doc, "//*[local-name()='ExternalIdentifier']");
                //console.log("node: " + nodes);
                console.log(nodes.length);            
                var parser = require('xml2json');
                nodes.forEach(function(element) {
                    var json = parser.toJson(element.toString());
                    //console.log("attr5 = %s/n ",element.attributes[4]);  
                }, this);                
                //console.log( nodes[0].toString() );                
                // xml to json 
                //var parser = require('xml2json');    
                var val = nodes[1].attributes[4].toString().replace("=","").replace('"','').replace("value","").replace('"','') ;
                //val = JSON.parse(val) ;
                //var json = parser.toJson(nodes[0].toString());
                //console.log("to json -> %s", json);                
                res.setHeader('content-type', 'text/json');     
                res.send(val);
            });    
            cReq.on('requestTimeout', function (req) {
                console.log("Register User request has timed out");
                req.abort();
            });
            cReq.on('responseTimeout', function (res) {
                console.log("Register User - request has expired");
                req.abort();
            });   

        });
    });
};
//---------------------------------------------------
exports.listsoap = function(req, res){
    var id = req.params.id;
    dict.get("XML",'','','',(err,data)=>{
        res.send(data);
    });
};

//---------------------------------------------------------------------------------------
let sendEmail = (oHIE,mailtype,oMail)=>{
   dict.get("JSON",mailtype,"MAIL",'en-US',(err,maildata) => {
        if (! isEmpty(maildata)) {
            if (typeof maildata == 'object') {
                oMail = maildata;
            } else {
                oMail = JSON.parse(maildata) ;
            }
        } else {                    
            dict.set("JSON",mailtype,"MAIL",'en-US',oMail);
        }
        let sprintf = require("sprintf-js").sprintf;
        if (! isEmpty(oHIE.email)) {
            oMail.to = oHIE.email;
        }
        if (! isEmpty(oMail.text)) {
            oMail.text = sprintf(oMail.text,oHIE.QE_Name)
        } else {
            oMail.text = oHIE.text ;
        }    
        email.send(oMail, (err, message) => {
            if (DEBUG) { 
                console.log("email message result =%j  /n Error=%s",message,err);
            }
        });
    });           
    return 0;
}
//---------------------------------------------------------------------------------------
let ret_error_mail = function(oHIE) {
        oMail = {
            text:    
`%s XCA Document Retrieve from Heartbeat Monitor has failed on 3 consecutive occasions.
Could you please check that the services are operational?

NYeC Support team 
1.855.669.6932
operationscenter@nyehealth.org`
            ,
            from:    "operationscenter@nyehealth.org", 
            to:      "dashboard@nyehealth.org", // defult if rhoi is missing
            cc:      "operationscenter@nyehealth.org,dashboard@nyehealth.org",
            bcc:     "dpal@nyehealth.org,sshi@nyehealth.org",
            subject: "XCA Document Retrieve Failure"
        }       
    sendEmail(oHIE,"XCA_RETRIEVE_ERROR",oMail);
    return 0;
};
//---------------------------------------------------------------------------------------
let qry_error_mail = function(oHIE) {
    let oMail = { 
        text:    
`%s XCA Query from Heartbeat Monitor has failed.
Could you please check that the server is running.


NYeC Support team 
1.855.669.6932
operationscenter@nyehealth.org`
        ,
        from:    "operationscenter@nyehealth.org", 
        to:      "dashboard@nyehealth.org", // defult if rhoi is missing
        cc:      "operationscenter@nyehealth.org,dashboard@nyehealth.org",
        bcc:     "dpal@nyehealth.org,sshi@nyehealth.org",
        subject: "XCA Dcoumnet Retrieve Failure"
    };
    sendEmail(oHIE,"XCA_QUERY_ERROR",oMail);
    return 0;
};

//---------------------------------------------------------------------------------------
let qry_ok_mail = function(oHIE) {
    let oMail = { 
        text:    
`%s XCA Query from Heartbeat Monitor has succeeded after Previous Failure.

NYeC Support team 
1.855.669.6932
operationscenter@nyehealth.org`
         ,
        from:    "operationscenter@nyehealth.org", 
        to:      "dashboard@nyehealth.org", // defult if rhoi is missing
        cc:      "operationscenter@nyehealth.org,dashboard@nyehealth.org",
        bcc:     "dpal@nyehealth.org,sshi@nyehealth.org",
        subject: "XCA Dcoumnet Retrieve Failure"
    };
    sendEmail(oHIE,"XCA_QUERY_OK",oMail);
    return 0;
};
//---------------------------------------------------------------------------------------
let ret_ok_mail = function(oHIE) {
    let oMail = { 
        text:    
`%s XCA Document Retrieve from Heartbeat Monitor has succeeded after Previous Failure.

NYeC Support team 
1.855.669.6932
operationscenter@nyehealth.org`
         ,
        from:    "operationscenter@nyehealth.org", 
        to:      "dashboard@nyehealth.org", // defult if rhoi is missing
        cc:      "operationscenter@nyehealth.org,dashboard@nyehealth.org",
        bcc:     "dpal@nyehealth.org,sshi@nyehealth.org",
        subject: "XCA Dcoumnet Retrieve Failure"
    };
    sendEmail(oHIE,"XCA_RETRIEVE_OK",oMail);
    return 0;
};
//----------------------------------------------------
exports.list = function(req,res){
   let type=  req.params.type;
   getlist(type,(err,data)=>{
       res.send(data);
   });
}
//----------------------------------------------------
let getlist = exports.getlist = function(type,callback){
    if (isEmpty(type)) {
        type = 'XML';
    }
    let id = '';
    let parent = '';
    let ext = '' ;
    dict.get(type,id,parent,ext,(err,rows)=>{
        callback(err,rows) ;
    });
};
//-----------------------------------------------------
exports.soapedit = function(req, res){
       res.render('sprl_soap',{page_title:"soap Source Editor "});                        
};
//------------------------------------------------------








