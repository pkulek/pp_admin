
/*
 * GET CID users listing.
 */
var Client = require('node-rest-client').Client;
// configure basic http auth for every request
var options_auth = { user: "HS_Services", password: "HS_Services" };
var client = new Client(options_auth);
//----------------------------------------------------------------------------------
function isEmpty(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
}
//---------------------------------------------------
exports.list = function(req, res){
    var id = req.params.id;
    var tokenid = req.params.tokenid;
    var type = req.params.type;
    if (isEmpty( type )){       
        type="JSON";
    }      
    var args = {
        requesConfig: { timeout: 1000 },
        responseConfig: { timeout: 2000 }
    };
    var cReq = client.get("http://192.168.180.173:57772/csp/healthshare/hsppedge/pp/NYeC.PP.Api/CIDUsers/8888", args, function (data, response) {
        //        console.log(response);
  	    res.send(data);
    });    
    cReq.on('requestTimeout', function (req) {
        console.log("CIDUsers - request has expired");
        req.abort();
    });
    cReq.on('responseTimeout', function (res) {
        console.log("CIDUsers - response has expired");
    });

};
//---------------------------------------------------
exports.htmllist = function(req, res){
    var tokenid = req.params.tokenid ;
    var type = req.params.type;
    if (isEmpty( type )){       
        type="JSON";
    } 
    var args = {
        requesConfig: { timeout: 1000 },
        responseConfig: { timeout: 2000 }
    };
    var cReq = client.get("http://192.168.180.173:57772/csp/healthshare/hsppedge/pp/NYeC.PP.Api/CIDUsers/888"+tokenid, args, function (data, response) {
        var colSetup =   setup_CID(data,req,res);     
  	    res.render('CIDusers',{page_title:"CID Portal Users ",data:data,colsetup:colSetup,tokenid:tokenid});    
    });    
};

//---------------------------------------------------
function setup_CID (rows,req,res) {
    var colsetup = {
        url:'/CIDusers',
        loadonce:true,
        altRows: true,
        deepempty: true,
        ignoreCase: true,
        datatype: "json",
        mtype: "GET",       
        height:'auto',
        width:'auto',
        autowidth: true,
        colModel:[
            {label:'ID',name:'ID'},
            {label:'VerizonMrn',name:'VerizonMrn'},
            {label:'FirstName',name:'FirstName'},
            {label:'LastName',name:'LastName'},
            {label:'MiddleName',name:'MiddleName'},
            {label:'DOB',name:'DOB'},
            {label:'SSN',name:'SSN'},
            {label:'HomeEmail',name:'HomeEmail'},
            {label:'Street',name:'Street'},
            {label:'City',name:'City'},
            {label:'State',name:'State'},
            {label:'Zip',name:'Zip'},
            {label:'Country',name:'Country'},
            {label:'UpdatedDateTime',name:'UpdatedDateTime'},
            {label:''} 
        ],
        //pager: "#CIDuserspager",
        toppager:true,
        rowNum: 25,
        rowTotal:5000,
        sortname: "ID",
        sortorder: "asc",
        viewrecords: true,
        gridview: true,
        autoencode: false,            
        caption: "Patient Portal CID Users"       
    }      
    return colsetup ;  
};

