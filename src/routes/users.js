
require('console-stamp')(console, 'yyyymmddHHmmss.l');
let config =  require('../utils').getConfig().conf;
let DEVENV = config.devEnv ;
let DEBUG = false ;
let devenv = "STAGE";
let dict = require('../dic/dict');
let utils = require('../utils');
let login = require('./login');
let logger = require('./logging');
let moment = require('moment');
let email = require('../mail');
let crypto = require('crypto');
let crypt = 'mUQ3cT9Sq1Ha4Y';
let lastsel = '0' ;

let Client = require('node-rest-client').Client;
let  client = new Client(config.HS_auth);

let mysql = require('mysql');
let conn = mysql.createConnection(config.mysqlconfig,'single');
let isEmpty = utils.isEmpty ;
//---------------------------------------------------
exports.gethash = function(req, res){
    let id = req.query.id;
    let qry = "select password from portal.users where user_id ='"+id+"'" ; 
    conn.query(qry,function(err,rows)  {                
        if(err) {
            console.log("Error Selecting : %s ",err );
            res.send('Error in retrieving data');
        } else {
            res.send(rows[0].password);
        };      
    });
};
//---------------------------------------------------
let registerUser = exports.registerUser = (json_data,callback)=>{
    let verizon_id = json_data.verizon_id;
    let url =  config.HSECR_CIDApi + "/userRegister/"+verizon_id+""
    let args = {
        data: json_data,
        headers: { "Content-Type": "application/json" }
    };
    let cReq = client.post(url, args, function (data, response) {
        if (DEBUG) {
            console.log("RegisterUser data:"+data);
        }
        if (callback ){
            callback("",data);
        }
    });    
    cReq.on('requestTimeout', function (req) {
        if (DEBUG) {
            console.log("Register User request has expired");
        }
        if (callback ){
            callback(req,"");
        }
    });
    cReq.on('responseTimeout', function (res) {
        if (DEBUG) {
            console.log("Register User - request has expired");
        }
        if (callback ){
            callback(res,"");
        }
    });   
}
//---------------------------------------------------
exports.register_user = function(req,res){
    var id = req.body.verizon_id ;
    var jsondata = JSON.stringify(req.body);
    console.log("Register_User data BODY:"+jsondata);
        //var url = data+"/userRegister/"+id+"";
        let url =  config.HSECR_CIDApi +"/userRegister/"+id+"";

        var args = {
            data: jsondata,
            headers: { "Content-Type": "application/json" }
        };
        var cReq = client.post(url, args, function (data, response) {
            if (DEBUG) {
                console.log("Register_User data:"+data);
            }
            res.send(data);
        });    
        cReq.on('requestTimeout', function (req) {
            console.error("Register_User request has expired");
            req.abort();
        });
        cReq.on('responseTimeout', function (res) {
            console.error("Register_User - response timeout");
            req.abort();
        });   
};
//------------------------------------------------------------------------
exports.users = function(req,res){
    let user_id= req.params.id;
    console.log("users.params = %j",req.params)
    console.log("users.query = %j",req.query)
    userlist(user_id,'',(data)=>{
        res.send(data);
    })
}
//-------------------------------------------------------------------------
let userlist = exports.userlist = function(user_id,flowstatus ,callback){
    _PPusers(function(rows,err){
        if(err) {
            console.log("Error in users.userlist: %s ",err );
            callback([],err);
        } else {
            var o = [];
            if ( isEmpty(user_id) ) {
                if (! isEmpty(flowstatus)) {
                    var o = [];
                    rows.forEach(function(part, index) {
                        if (part.status == flowstatus ){
                            o.push(part,'');
                        }
                    });                  
                    callback(o,'');       
                } else {
                    callback(rows,''); // get all
                }
            } else {
                rows.forEach(function(part, index) {
                    if (part.user_id == user_id ){
                        o.push(part,'');
                    }
                });                  
                callback(o,'');       
            };
        };   
    });
};
//-----------------------------------------------
var getClientList = function(callback){
    var qry = "select client_id,url as client_url,OID as client_OID,name,client_code,parent,type,api_id from portal.clients " ;
    conn.query(qry,function(err,rows)  {
        // get HS MPIID for verizon id and replace
        if ( rows && rows.length > 0 ){
            callback(rows,err);
        } else {
            callback([],"Error") ;
        }  
    });  
}
//------------------------------------------------
exports.ppclients = function(req,res){
    getClientList((data)=>{
        res.send(data) ;
    });        
}
//---------------------------------------------------
exports.htmlclients = function(req, res){
    setupuserclients_columns( function(colSetup){
        dict.get_ejsTemplate('userclients',req.app.get('views'),(err,template)=>{
            if(! isEmpty(template)){
                let ejs = require('ejs');
                res.send(ejs.render(template,{page_title:"Patient Portal Clients ",colsetup:colSetup}));
            } else {
                res.render('userclients',{page_title:"Patient Portal Clients ",colsetup:colSetup});                        
            }     
        })   
        //res.render('userclients',{page_title:"Patient Portal Clients ",colsetup:colSetup});                        
    });        
};
//------------------------------------------------
let updateFlowstatus = exports.updateFlowstatus = (user_id,status)=>{
    if (isEmpty(status) ){
        status = "PP_ACCEPTED";
    }
    conn.query(`select count(*) as qty from portal.users_flow_statuses where user_id = '${user_id}' `,  (err, rows) => {
        if (err)  console.log("Error Updating FlowSatatu : %s ",err );
        let qry = `INSERT into portal.users_flow_statuses set flow_status ='${status}' , user_id='${user_id}'`               
        if (rows && rows[0].qty > 0 ) { 
            qry = `UPDATE  portal.users_flow_statuses set flow_status = '${status}' where user_id = '${user_id}'  `                        
        }
        if (DEBUG) {
            console.log(qry);
        }
        conn.query(qry, (err,rows)=>{} );
    });
    return "OK";
}
//--------------------------------------------------------------
var getVerizonID = exports.getVerizonID = function(user_id,callback){
    // get verizonid
    var qry = "SELECT user_id,external_user_id FROM portal.users_apis where user_id ='"+user_id+"' " ;
    try {
        //console.log("getVerizonID qry="+qry);
        conn.query(qry,function(err,rows)  {
           // console.log(err||rows)
            // get HS MPIID for verizon id and get document
            if ( rows && rows.length > 0 ) {      
                callback(rows[0].external_user_id,err )
            } else {
                callback("",'Error');
            }
        });
    } catch(Ex) {
        callback("",Ex);
    }
};
//------------------------------------------------
var _PPusers = function(callback){
    var qry = ` 
    select user_id as user_id,
    (select CAST(AES_DECRYPT(first_name,'${crypt}') as CHAR(60)) decfname from portal.users_demographics where user_id = PU.user_id limit 1) first_name ,   
    (select CAST(AES_DECRYPT(middle_name,'${crypt}') as CHAR(60)) decmname from portal.users_demographics where user_id = PU.user_id limit 1) middle_name ,   
    (select CAST(AES_DECRYPT(last_name,'${crypt}') as CHAR(60)) declname from portal.users_demographics where user_id = PU.user_id limit 1) last_name ,   
    (select gender from portal.users_demographics where user_id = PU.user_id limit 1) as gender ,   
    (select STR_TO_DATE(CONVERT(AES_DECRYPT(date_of_birth,'${crypt}') using utf8),'%Y%d%m') from portal.users_demographics where user_id = PU.user_id limit 1) dob ,   
    (select CAST(AES_DECRYPT(phone_1,'${crypt}') as CHAR(20)) from portal.users_demographics where user_id = PU.user_id limit 1) phone_1 ,   
    (select CAST(AES_DECRYPT(phone_2,'${crypt}') as CHAR(20)) from portal.users_demographics where user_id = PU.user_id limit 1) phone_2 ,   
    (select CAST(AES_DECRYPT(phone_3,'${crypt}') as CHAR(20)) from portal.users_demographics where user_id = PU.user_id limit 1) phone_3 ,   
    (select CAST(AES_DECRYPT(primary_address_street_address,'${crypt}') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) primary_address_street_address ,   
    (select CAST(AES_DECRYPT(primary_address_city,'${crypt}') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) primary_address_city ,   
    (select CAST(AES_DECRYPT(primary_address_state,'${crypt}') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) primary_address_state ,   
    (select CAST(AES_DECRYPT(primary_address_zip,'${crypt}') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) primary_address_zip,   
    (select CAST(AES_DECRYPT(primary_address_country,'${crypt}') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) primary_address_country ,   
    (select CAST(AES_DECRYPT(social_sec,'${crypt}') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) ssn9 ,   
    (select CAST(AES_DECRYPT(email,'${crypt}') as CHAR(60))  decemail from portal.users_demographics where user_id = PU.user_id limit 1) email ,   
    client_id , 
    (select url from portal.clients where client_id = PU.client_id limit 1) as client_url ,   
    (select OID from portal.clients where client_id = PU.client_id limit 1) as client_OID ,   
    (select flow_status from portal.users_flow_statuses where user_id = PU.user_id limit 1) status ,   
    (select external_user_id from portal.users_apis where user_id = PU.user_id limit 1) verizon_id ,   
    (select role_name from portal.users_roles where user_id = PU.user_id limit 1) roles ,   
    cast(active as signed ) as active , 
    cast(is_admin as signed) as is_admin ,
    'n/a' as mpiid ,
    (select date_time_updated from portal.users_flow_statuses where user_id = PU.user_id limit 1) datetime   
    from portal.users PU  
     ` ; 
    conn.query(qry,function(err,rows)  {
        // get HS MPIID for verizon id and replace
        if(err) {
            console.log("err=%s\nrows=%j",err,rows);
            callback([],err);                                        
        } else {
            if ( rows && rows.length > 0 ){
                let url =  config.HSHUB_APIurl +"/Patient/MPIID";
                let cReq = client.get(url, function (data, response) {
                    try{
                        if(data){
                            data = JSON.parse(data);                        
                            rows.forEach(function(part, index) {
                                var x = part.verizon_id ;
                                var r = 'n/a';
                                data.forEach(function(obj,i){
                                    if (obj.MRN == x) {
                                        r = obj.MPIID ;
                                        return r;
                                    }
                                });    
                                rows[index].mpiid = r;
                            });
                            callback(rows,err);                
                        } else {
                            callback([],"Error: No user data");                                        
                        }
                    } catch(ex){
                        callback([],"Error in data: "+ex);                                        
                    } 
                });                    
            }
        }
    });
};
/*
//------------------------------------------------
var _PPusers = function(callback){
    var qry = "select user_id as user_id,\
                (select CAST(AES_DECRYPT(first_name,'"+crypt+"') as CHAR(60)) decfname from portal.users_demographics where user_id = PU.user_id limit 1) first_name ,   \
                (select CAST(AES_DECRYPT(middle_name,'"+crypt+"') as CHAR(60)) decmname from portal.users_demographics where user_id = PU.user_id limit 1) middle_name ,   \
                (select CAST(AES_DECRYPT(last_name,'"+crypt+"') as CHAR(60)) declname from portal.users_demographics where user_id = PU.user_id limit 1) last_name ,   \
                (select gender from portal.users_demographics where user_id = PU.user_id limit 1) as gender ,   \
                (select STR_TO_DATE(CONVERT(AES_DECRYPT(date_of_birth,'"+crypt+"') using utf8),'%Y%d%m') from portal.users_demographics where user_id = PU.user_id limit 1) dob ,   \
                (select CAST(AES_DECRYPT(phone_1,'"+crypt+"') as CHAR(20)) from portal.users_demographics where user_id = PU.user_id limit 1) phone_1 ,   \
                (select CAST(AES_DECRYPT(phone_2,'"+crypt+"') as CHAR(20)) from portal.users_demographics where user_id = PU.user_id limit 1) phone_2 ,   \
                (select CAST(AES_DECRYPT(phone_3,'"+crypt+"') as CHAR(20)) from portal.users_demographics where user_id = PU.user_id limit 1) phone_3 ,   \
                (select CAST(AES_DECRYPT(primary_address_street_address,'"+crypt+"') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) primary_address_street_address ,   \
                (select CAST(AES_DECRYPT(primary_address_city,'"+crypt+"') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) primary_address_city ,   \
                (select CAST(AES_DECRYPT(primary_address_state,'"+crypt+"') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) primary_address_state ,   \
                (select CAST(AES_DECRYPT(primary_address_zip,'"+crypt+"') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) primary_address_zip,   \
                (select CAST(AES_DECRYPT(primary_address_country,'"+crypt+"') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) primary_address_country ,   \
                (select CAST(AES_DECRYPT(social_sec,'"+crypt+"') as CHAR(60)) from portal.users_demographics where user_id = PU.user_id limit 1) ssn9 ,   \
                (select CAST(AES_DECRYPT(email,'"+crypt+"') as CHAR(60))  decemail from portal.users_demographics where user_id = PU.user_id limit 1) email ,   \
                client_id , \
                (select url from portal.clients where client_id = PU.client_id limit 1) as client_url ,   \
                (select OID from portal.clients where client_id = PU.client_id limit 1) as client_OID ,   \
                (select flow_status from portal.users_flow_statuses where user_id = PU.user_id limit 1) status ,   \
                (select external_user_id from portal.users_apis where user_id = PU.user_id limit 1) verizon_id ,   \
                (select role_name from portal.users_roles where user_id = PU.user_id limit 1) roles ,   \
                cast(active as signed ) as active , \
                cast(is_admin as signed) as is_admin ,\
                'n/a' as mpiid ,\
                (select date_time_updated from portal.users_flow_statuses where user_id = PU.user_id limit 1) datetime   \
                from portal.users PU  " ; 
    conn.query(qry,function(err,rows)  {
        // get HS MPIID for verizon id and replace
        if ( rows && rows.length > 0 ){
            let url =  config.HSHUB_APIurl +"/Patient/MPIID";
            let cReq = client.get(url, function (data, response) {
                try{
                    if(data){
                        data = JSON.parse(data);                        
                        rows.forEach(function(part, index) {
                            var x = part.verizon_id ;
                            var r = 'n/a';
                            data.forEach(function(obj,i){
                                if (obj.MRN == x) {
                                    r = obj.MPIID ;
                                    return r;
                                }
                            });    
                            rows[index].mpiid = r;
                        });
                        callback(rows,err);                
                    } else {
                        callback([],"Error: No user data");                                        
                    }
                } catch(ex){
                    callback([],"Error in data: "+ex);                                        
                } 
            });                    
        }
    });
};
*/
//---------------------------------------------------
exports.list = function(req, res){
    var type = req.params.type;
    var status = req.params.status;

    if (isEmpty( type )){       
        type="JSON";
    } 
    if (isEmpty( status )){       
        status="PP_REGISTERED";
    } 
    _PPusers(function(rows,err){
        if(! isEmpty(err) ) {
            //console.log("Error: %s ",err );
            res.send([{user_id:err}]);
        } else {
            if (type == "JSON" ){
                res.send(rows);
            } else if (type == "XML"){
                //TODO: convert to XML
                res.send(rows);                        
            } else {
                res.send(rows);                                                
            }               
        };                   
    });    
};
//---------------------------------------------------
exports.htmllist = function(req, res){
   let tokenid = req.params.tokenid ;
   login.authorised(tokenid,['PP_USERSVIEW'],function(data,roles,token){
       if (data) {
            /*
            dict.get_ejsTemplate('users',req.app.get('views'),(err,template)=>{
                if(! isEmpty(template)){
                    let ejs = require('ejs');
                    res.send(ejs.render(template,{page_title:"Patient Portal Users ",tokenid:tokenid,roles:roles}));
                } else {
                   res.render('users',{page_title:"Patient Portal Users ",tokenid:tokenid,roles:roles});                        
                }     
            })
            */
            res.render('users',{page_title:"Patient Portal Users ",tokenid:tokenid,roles:roles,theme:token.theme});                        

       } else {
            res.send("Access Denied - no authorisation for view");                                   
       }
   }); 
};
//---------------------------------------------------
exports.add = function(req, res){
     res.render('add_customer',{page_title:"Add Customers - Node.js"});
};
//---------------------------------------------------
exports.edit = function(req, res){
    var id = req.params.id;
    console.log(" edit "+JSON.stringify(req.body));
    conn.query('SELECT * FROM customer WHERE id = ?',[id],function(err,rows)  {
            if(err) console.log("Error Selecting : %s ",err );
            res.render('edit_customer',{page_title:"Edit Customers - Node.js",data:rows});
         });
};
//---------------------------------------------------
/*Save the user POST req*/
var save = function(req,res){
    var input = JSON.parse(JSON.stringify(req.body));
    var id = input.user_id ;
    var expdate = moment().add(90, 'days').format("YYYY/MM/DD"); 
    var manaid = utils.genToken(15);      
    console.log("save"+JSON.stringify(input));
    var qry = "select count(*) as qty from portal.users where user_id = '"+id+"'"
    conn.query(qry, function(err, rows)  {
        if (err)  console.log("Error Updating : %s ",err );                        
        if (rows[0].qty == 0 ){ //} then insert
                qry = `INSERT into portal.users set user_id = '${id}',
                        passwd_expire_dt = '${expdate}' ,
                        is_admin = '${input.is_admin}' ,
                        active = '${input.active}', 
                        password='AKLnAAGCvHh+yc/RAt3drfHuPXv2/ByhUdNM7UdQ+84=',
                        saltnpepa='eybncR82GhbGnZmvS04tc9ZG7I6YvTY3jtH49A0B',
                        mana_id='${manaid}',
                        client_id='${input.client_id}',
                        is_external = 0 "
                        ` ; 
        } else {
                qry = `UPDATE portal.users set active = '${input.active}', client_id = '${input.client_id}', is_admin = '${input.is_admin}'' where user_id = '${id}'`;                 
        }
        conn.query(qry, function(err, rows)  {         
            if (err)  console.log("Error Updating1 : %s ",err );
                //---------------------------------------------------------------------------------------------                
                qry = "select count(*) as qty from portal.users_flow_statuses where user_id = '"+id+"'"  
                conn.query(qry, function(err, rows)  {
                    if (err)  console.log("Error Updating1 : %s ",err );
                    if (rows[0].qty == 0 ){ //} then insert
                        qry = "INSERT into portal.users_flow_statuses set flow_status ='"+input.status+"' , user_id='"+id+"' "            
                    } else {  // update
                        qry = "UPDATE  portal.users_flow_statuses set flow_status ='"+input.status+"' where user_id='"+id+"' "                        
                    }
                    conn.query(qry, function(err, rows)  {
                        if (err)  console.log("Error Updating2 portal.users_flow_statuses : %s %s",qry,err );
                        //---------------------------------
                        qry = "select count(*) as qty from portal.users_demographics where user_id = '"+id+"'"  
                        conn.query(qry, function(err, rows)  {
                            if (err)  console.log("Error Updating3 : %s ",err );
                            if (rows[0].qty == 0 ){ //} then insert
                                qry = "INSERT INTO  portal.users_demographics set first_name = AES_ENCRYPT('" +input.first_name + "','"+crypt+"') ,\
                                            last_name = AES_ENCRYPT('"+ input.last_name + "','"+crypt+"'), \
                                            email = AES_ENCRYPT('"+ input.email + "','"+crypt+"'),\
                                            date_of_birth = AES_ENCRYPT('"+ input.dob + "','"+crypt+"'),\
                                            user_id= '"+input.user_id + "'";                 
                            } else {
                                qry = "UPDATE  portal.users_demographics set first_name = AES_ENCRYPT('" +input.first_name + "','"+crypt+"') ,\
                                            last_name = AES_ENCRYPT('"+ input.last_name + "','"+crypt+"'), \
                                            email = AES_ENCRYPT('"+ input.email + "','"+crypt+"'),\
                                            WHERE user_id= '"+input.user_id + "'";                 

                            }            
                            conn.query(qry, function(err, rows)  {                   
                                if (err)  console.log("Error Updating2 : %s ",err );
                                var qry = "INSERT INTO portal.users set active = "+input.active+" , is_admin = "+input.is_admin+" "; 
                                console.log(qry); 
                                conn.query(qry, function(err, rows)  {
                                    if (err)  console.log("Error Updating3 : %s ",err );
                                    var qry = "insert into portal.users_apis  set  user_id = "+input.user_id+", external_user_id= "+input.phrid+" "
                                    conn.query(qry, function(err, rows)  {
                                        if (err)  console.log("Error4 Updating : %s ",err );
                                    });
                                });
                            });
                        });
                    });      
                });
            });
        });           
};

//---------------------------------------------------------------------------------------
exports.VerifyUser = function (req,res){ // new user as post
    //var now = moment();    
    var input = JSON.parse(JSON.stringify(req.body));
    var user_id = input.user_id ;
    var email = input.email ;
    var result = {"data":input,
                  "result":false,
                  "msg":"OK",  
                  "testuser_id":false,
                  "testmail":false
                 };
    var valid = false;
    if ( isEmpty( user_id )){
        user_id ='' ;
    };
    if ( isEmpty( email )){
        email ='' ;
    };
    console.log("Verifyuser input="+JSON.stringify(input));
    var qry = "select count(*) as qty from portal.users where user_id = '"+user_id+"' ";    
    conn.query(qry, function(err, rows)  {
        if (err) {
            var msg ="SQL Error Validating user_id : "+err ;
            console.log(msg);
        }           
        if (rows && rows.length > 0) {                            
            valid = (rows[0].qty == 0 ) ;
        }
        if (valid ) {
            qry = "select count(*) as qty from portal.users_demographics where CAST(AES_DECRYPT(email,'"+crypt+"') as CHAR(60)) = '" + email+"'" ;  
            conn.query(qry, function(err, rows)  {
                valid = false;                 
                if (err) {
                    var msg = "SQL Error Validating email : "+err ; 
                    result.valid = false;
                    result.msg = msg;
                    console.log( msg );
                };
                if (rows && rows.length > 0) {       
                    valid = (rows[0].qty == 0 ) ;
                }   
                if(! valid ) {
                    result.msg = "email is already in use";
                    result.testmail = true;
                }            
                result.valid = valid;
                console.log("Result out = %s",JSON.stringify(result));
                res.send(result) ;
            });
        } else {
           result.testuser_id = true;
           result.valid = false;
           result.msg = "User is already registered";            
           res.send(result) ;
        };         
    });
};    
//---------------------------------------------------
exports.save_edit = function(req,res){ // POST from edit jqgrid
    console.log("save edit "+JSON.stringify(req.body));
    var input = JSON.parse(JSON.stringify(req.body));
    var oper = input.oper ;
    var id = input.user_id ;
    console.log("Operator %s",input.oper);
    if (oper === 'del' ){
        delete_user(req,res,id);
    } else {
        // send email if PP_ACCEPTED 
        if (input.status == "PP_ACCEPTED"){
            // need to get the HIE url here
            var hie = input.client_id ;
            if (isEmpty(hie)){
                hie = 'nyec';
            } 
            var urlid = "PP_PORTAL"+hie;
            console.log(hie);
            dict.getURL(urlid.toUpperCase(),"SUPPORT_EMAIL","nyppsupport@nyehealth.org~1888-633-6706",function(data,error){
                    var url = data; 
                    var msg = 'Dear %s %s,'+
                        '\nYour New York Patient Portal account has been activated. You can now log in and have a look at your medical data'+
                        '\nYou now have access to a broad range of your medical records online. Please follow this link %s to log-in and take a look.'+
                        '\nIf you are having issues with the NY Patient Portal, please do not hesitate to contact us.'+ 
                        '\n\nThank you,'+
                        '\nNYPP Support team'+ 
                        '\n\n 1.888.633.6706'+
                        '\nnyppsupport@nyehealth.org'
                    dict.set("MESG","PP_ACCEPTED","MAIL",'en-US',msg,(err,data)=> {
                        var sprintf = require("sprintf-js").sprintf;
                        msg = sprintf(data,input.first_name,input.last_name,input.client_url)
                      
                        console.log("EMAIL to "+input.email+"\n MSG="+msg);           
                        email.send({
                            text:    msg ,
                            from:    "pp_admin@nyecemail.com", 
                            to:      input.email,
                            cc:      "",
                            bcc:    "nyppsupport@nyehealth.org",
                            subject: "Do no Reply: PP Account Activation"
                        }, function(err, message)  { 
                            console.log("err="+err);
                            console.log("message="+JSON.stringify(message));
                        });
                });
            });
        }
        _save(req,res);
        //save(req,res);
        res.send("OK")
    };
};
//---------------------------------------------
exports.sendemail = function(req,res){
   var input = JSON.parse(JSON.stringify(req.body));    
   console.log(JSON.stringify(req.body));
   if ( isEmpty(input.cc)) {
       input.cc = "";
   }
   if ( isEmpty(input.bcc)) {
       input.bcc = "nyppsupport@nyehealth.org";
   }
    email.send({
        text:    input.msg ,
        from:    "pp_admin@nyecemail.com", 
        to:      input.email,
        cc:      input.cc,
        bcc:    input.bcc,
        subject: "Do no Reply: PP Account Activation"
    }, function(err, message)  { 
        console.log("err="+err);
        console.log("message="+JSON.stringify(message));
    });
  res.send("Sent");
};
//----------------------------------------------
exports.del_user = function(req,res){
    //console.log("delete user "+JSON.stringify(req.query.id));
    delete_user(req,res);
};
//---------------------------------------------------
exports.save = function(req,res){
    _save(req,res);
}

//---------------------------------------------------
var _save = function(req,res){
    let input = req.body; //POsT DATA
    console.log("user._save input %s ",JSON.stringify(input));
    let id = input.user_id ;
    let client_id = input.client_id ;
    let expdate = moment().add(90, 'days').format("YYYY/MM/DD"); 
    let qry = "select count(*) as qty from portal.users where user_id = '"+id+"'"
    let manaid = utils.genToken(15);  
    //console.log(manaid+"  -- "+qry);    
    conn.query(qry, function(err, rows)  {
        if (err)  console.log("Error Updating : %s ",err );                        
        if (rows[0].qty == 0 ){ //} then insert
                qry = `INSERT into portal.users set user_id = '${id}',
                        passwd_expire_dt = '${expdate}' ,
                        is_admin = '${input.is_admin}' ,
                        active = '${input.active}', 
                        password='AKLnAAGCvHh+yc/RAt3drfHuPXv2/ByhUdNM7UdQ+84=',
                        saltnpepa='eybncR82GhbGnZmvS04tc9ZG7I6YvTY3jtH49A0B',
                        mana_id='${manaid}',
                        client_id='${input.client_id}',
                        is_external = 0 ` ; 
        } else {
                qry = `UPDATE portal.users set active = '${input.active}', client_id = '${input.client_id}', is_admin = '${input.is_admin}' where user_id = '${id}'`;                 
        }
        conn.query(qry, function(err, rows)  {
            if (err)  console.log("Error Updating portal.users : %s %s ",qry,err );
            //---------------------------------------------------------------------------------------------                
            qry = `select count(*) as qty from portal.users_flow_statuses where user_id = '${id}'`  
            conn.query(qry, function(err, rows)  {
                    if (err)  console.log("Error Updating1 : %s ",err );
                    if (rows[0].qty == 0 ){ //} then insert
                        qry = `INSERT into portal.users_flow_statuses set flow_status ='${input.status}' , user_id='${id}' `            
                    } else {  // update
                        qry = `UPDATE  portal.users_flow_statuses set flow_status ='${input.status}' where user_id='${id}' `                        
                    }
                    conn.query(qry, function(err, rows)  {
                        if (err)  console.log("Error Updating2 portal.users_flow_statuses : %s %s",qry,err );
                        //---------------------------------
                        qry = `select count(*) as qty from portal.users_demographics where user_id = '${id}'`  
                        conn.query(qry, function(err, rows)  {
                            if (err)  console.log("Error Updating3 : %s ",err );
                            if (rows[0].qty == 0 ){ //} then insert
                                qry = "INSERT INTO  portal.users_demographics set first_name = AES_ENCRYPT('" +input.first_name + "','"+crypt+"') ,\
                                                last_name = AES_ENCRYPT('"+ input.last_name + "','"+crypt+"'), \
                                                middle_name = AES_ENCRYPT('"+ input.middle_name + "','"+crypt+"'), \
                                                email = AES_ENCRYPT('"+ input.email + "','"+crypt+"'),\
                                                phone_1 = AES_ENCRYPT('"+ input.phone_1 + "','"+crypt+"'),\
                                                date_of_birth = AES_ENCRYPT('"+ input.dob + "','"+crypt+"'),\
                                                primary_address_street_address = AES_ENCRYPT('"+ input.primary_address_street_address + "','"+crypt+"'),\
                                                primary_address_city = AES_ENCRYPT('"+ input.primary_address_city + "','"+crypt+"'),\
                                                primary_address_zip = AES_ENCRYPT('"+ input.primary_address_zip + "','"+crypt+"'),\
                                                primary_address_country = AES_ENCRYPT('"+ input.primary_address_country + "','"+crypt+"'),\
                                                gender = '"+ input.gender + "',\
                                                user_id= '"+id + "'";                 
                            } else {
                                qry = "UPDATE portal.users_demographics set first_name = AES_ENCRYPT('" +input.first_name + "','"+crypt+"') ,\
                                    last_name = AES_ENCRYPT('"+ input.last_name + "','"+crypt+"'), \
                                    middle_name = AES_ENCRYPT('"+ input.middle_name + "','"+crypt+"'), \
                                    email = AES_ENCRYPT('"+ input.email + "','"+crypt+"'),\
                                    phone_1 = AES_ENCRYPT('"+ input.phone_1 + "','"+crypt+"'),\
                                    date_of_birth = AES_ENCRYPT('"+ input.dob + "','"+crypt+"'),\
                                    primary_address_street_address = AES_ENCRYPT('"+ input.primary_address_street_address + "','"+crypt+"'),\
                                    primary_address_city = AES_ENCRYPT('"+ input.primary_address_city + "','"+crypt+"'),\
                                    primary_address_zip = AES_ENCRYPT('"+ input.primary_address_zip + "','"+crypt+"'),\
                                    primary_address_country = AES_ENCRYPT('"+ input.primary_address_country + "','"+crypt+"'),\
                                    gender = '"+ input.gender + "' \
                                    WHERE user_id = '"+id+"'" ;                             
                            }            
                            conn.query(qry, function(err, rows)  {
                                if (err)  console.log("Error Updating/Inserting  portal.users_demographics : %s %s ",qry,err );
                                qry = "select count(*) as qty from  portal.users_apis where user_id = '"+id+"'"                                  
                                conn.query(qry, function(err, rows)  {                               
                                    if (rows[0].qty == 0 ){ //} then insert only - new user
                                        qry = "INSERT into  portal.users_apis set external_user_id ='"+input.phrid+"' , user_id='"+id+"' , api_id = 2 ,Data_status='COMPLETED'"            
                                        conn.query(qry, function(err, rows)  {
                                            if (err)  console.log("Error Inserting  portal.users_apis : %s %s",qry,err );
                                        });
                                    };
                                });  
                                qry = "select count(*) as qty from  portal.users_statistics where user_id = '"+id+"'";
                                    conn.query(qry, function(err, rows)  {                               
                                        if (rows[0].qty == 0 ){ //} then insert only - new user
                                            qry = "INSERT into  portal.users_statistics set joined ='2017-02-07' , Last_logged_in ='2017-02-07' , Failed_logged_in_attempts=0,user_id='"+id+"',password_change_required =0"                                                                
                                            conn.query(qry, function(err, rows)  {
                                                if (err)  console.log("Error Updating  portal.users_statistics : %s %s",qry,err );
                                            });
                                        };
                                    })
                                qry = "select count(*) as qty from  portal.users_preferences where user_id = '"+id+"'"
                                    conn.query(qry, function(err, rows)  {                               
                                        if (rows[0].qty == 0 ){ //} then insert only - new user
                                            qry = " insert into portal.users_preferences set language='en-us',user_id='"+id+"',dashboard_overlay=1,\
                                            data_overlay=1,messages_overlay=1,notes_overlay=1,documents_overlay=1,allow_contact=1,premade_dashboard_screen=1"
                                            conn.query(qry, function(err, rows)  {
                                                if (err)  console.log("Error Inserting  portal.users_preferences : %s %s",qry,err );
                                            });
                                        };
                                    })
                                qry = "select count(*) as qty from  portal.users_contracts where user_id = '"+id+"'"
                                    conn.query(qry, function(err, rows)  {                               
                                        if (rows[0].qty == 0 ){ //} then insert only - new user
                                            qry = "insert into portal.users_contracts set user_id= '"+id+"' , document_id = 1 , signed = 'Y'"
                                            conn.query(qry, function(err, rows)  {
                                                if (err)  console.log("Error Updating  portal.users_contracts : %s %s",qry,err );
                                                qry = "insert into portal.users_contracts set user_id= '"+id+"' , document_id = 2 , signed = 'Y'"
                                                conn.query(qry, function(err, rows)  {
                                                    if (err)  console.log("Error Inserting  portal.users_contracts : %s\n %s",qry,err );
                                                });                                                
                                            });
                                        };
                                    })
                                    qry = "select count(*) as qty from  portal.users_roles  where user_id = '"+id+"'"
                                    conn.query(qry, function(err, rows)  {                               
                                        if (rows[0].qty == 0 ){ //} then insert only - new user
                                            qry = "INSERT INTO portal.users_roles (role_name,user_id) values('patient','"+id+"') "
                                            conn.query(qry, function(err, rows)  {
                                                if (err)  console.log("Error Inserting  portal.users_roles : %s \n%s",qry,err );
                                            });
                                        };
                                    })
                            });      
                        });
                    });       
                });      
            });       
       });               
};
//---------------------------------------------------
var delete_user = (req,res,id)=> {      
    let tokenid = req.query.tokenid ;
    if ( isEmpty(id) ){
        id = req.query.id;

    };
     console.log("Deleteuser id= "+id+"  tokenid = "+tokenid);   
     login.authorised(tokenid,['PP_USERDELETE'],function(data,roles){
        if ( data ) {
            console.log("deletetin User ID :"+id);
                var qry = "DELETE FROM portal.users_preferences  WHERE user_id = '"+id+"'";        
                //login.authorised(tokenid,['PP_DELETEUSER'],function(data,roles){
                                           
                conn.query(qry,[id], function(err, rows)  {            
                    if(err) console.log("1-Error deleting : %s ",err );
                    var qry = "DELETE FROM portal.users_demographics  WHERE user_id = '"+id+"'";
                    conn.query(qry,[id], function(err, rows)  {            
                        if(err)  console.log("2-Error deleting : %s ",err );
                        var qry = "DELETE FROM portal.users_settings WHERE user_id = '"+id+"'";
                        conn.query(qry,[id], function(err, rows)  {            
                        if(err)  console.log("3-Error deleting : %s ",err );
                        var qry = "DELETE FROM portal.users_roles  WHERE user_id = '"+id+"'";
                        conn.query(qry,[id], function(err, rows)  {            
                            if(err)    console.log("4-Error deleting : %s ",err );
                            var qry = "DELETE FROM portal.users_security_questions  WHERE user_id = '"+id+"'";
                            conn.query(qry,[id], function(err, rows)  {            
                                if(err) console.log("5-Error deleting : %s ",err );
                                var qry = "DELETE FROM portal.users_apis  WHERE user_id = '"+id+"'";
                                conn.query(qry,[id], function(err, rows)  {            
                                    if(err) console.log("6-Error deleting : %s ",err );
                                    var qry = "DELETE FROM portal.users_contracts  WHERE user_id = '"+id+"'";
                                    conn.query(qry,[id], function(err, rows)  {            
                                        if(err) console.log("7-Error deleting : %s ",err );
                                        var qry = "DELETE FROM portal.users_encounters  WHERE user_id = '"+id+"'";
                                        conn.query(qry,[id], function(err, rows)  {            
                                            if(err) console.log("8-Error deleting : %s ",err );
                                            var qry = "DELETE FROM portal.users_organizations  WHERE user_id = '"+id+"'";
                                            conn.query(qry,[id], function(err, rows)  {            
                                                if(err) console.log("9-Error deleting : %s ",err );
                                                var qry = "DELETE FROM portal.users_notifications  WHERE user_id = '"+id+"'";
                                                conn.query(qry,[id], function(err, rows)  {            
                                                    if(err) console.log("10-Error deleting : %s ",err );
                                                    var qry = "DELETE FROM portal.users_direct_accounts  WHERE user_id = '"+id+"'";
                                                    conn.query(qry,[id], function(err, rows)  {            
                                                        if(err) console.log("11-Error deleting : %s ",err );
                                                        var qry = "DELETE FROM portal.users_statistics  WHERE user_id = '"+id+"'";
                                                        conn.query(qry,[id], function(err, rows)  {            
                                                            if(err) console.log("12-Error deleting : %s ",err );
                                                            var qry = "DELETE FROM portal.section_notes   WHERE user_id = '"+id+"'";
                                                            conn.query(qry,[id], function(err, rows)  {            
                                                                if(err) console.log("13-Error deleting : %s ",err );
                                                                var qry = "DELETE FROM portal.user_address_book  WHERE user_id = '"+id+"'";
                                                                conn.query(qry,[id], function(err, rows)  {            
                                                                    if(err) console.log("14-Error deleting : %s ",err );
                                                                    var qry = "DELETE FROM portal.users_provided_demographics  WHERE user_id = '"+id+"'";
                                                                    conn.query(qry,[id], function(err, rows)  {            
                                                                        if(err) console.log("15-Error deleting : %s ",err );
                                                                        var qry = "DELETE FROM portal.invites  WHERE created_by = '"+id+"'";
                                                                        conn.query(qry,[id], function(err, rows)  {            
                                                                            if(err) console.log("16-Error deleting : %s ",err );
                                                                            var qry = "DELETE FROM portal.users_flow_statuses  WHERE user_id = '"+id+"'";
                                                                            conn.query(qry,[id], function(err, rows)  {            
                                                                                if(err) console.log("17-Error deleting : %s ",err );
                                                                                var qry = "DELETE FROM portal.users_flow_statuses_archive  WHERE user_id = '"+id+"'";
                                                                                conn.query(qry,[id], function(err, rows)  {            
                                                                                    if(err) console.log("18-Error deleting : %s ",err );
                                                                                    var qry = "DELETE FROM portal.previous_admin_passwords  WHERE user_id = '"+id+"'";
                                                                                    conn.query(qry,[id], function(err, rows)  {            
                                                                                        if(err) console.log("19-Error deleting : %s ",err );
                                                                                        var qry = "DELETE FROM portal.notes  WHERE user_id = '"+id+"'";
                                                                                        conn.query(qry,[id], function(err, rows)  {            
                                                                                            if(err) console.log("19-Error deleting : %s ",err );
                                                                                            var qry = "DELETE FROM portal.users_documents  WHERE user_id = '"+id+"'";
                                                                                            conn.query(qry,[id], function(err, rows)  {            
                                                                                                if(err) console.log("20-Error deleting : %s ",err );
                                                                                                var qry = "DELETE FROM portal.users  WHERE user_id = '"+id+"'";
                                                                                                conn.query(qry,[id], function(err, rows)  {            
                                                                                                    if(err) {
                                                                                                        console.log("21-Error deleting : %s ",err );
                                                                                                        res.send(err);
                                                                                                    } else {
                                                                                                        res.send("OK");
                                                                                                    }
                                                                                                });
                                                                                            });
                                                                                        });
                                                                                    }); 
                                                                                });
                                                                            });
                                                                        });
                                                                    });                
                                                                });
                                                            });                                                                    
                                                        });
                                                    });    
                                                });                       
                                            });
                                        });                       
                                    });                       
                                });                       
                            });                       
                        });        
                        });                   
                    });        
                });        
        } else {
            res.send("Access denied");
        }
    });
};
//---------------------------------------------------
var setupuserclients_columns = (callback)=> {
    dict.get("JSON","USERCLIENTCOLUMNS","JQGRID","", (err,data) => {
        if ( isEmpty(data)) {
            var colsetup = {
                url:'/users/clients',
                loadonce:true,
                altRows: false,
                deepempty: true,
                ignoreCase: true,
                datatype: "json",
                mtype: "GET",       
                height:'auto',
                width:'auto',
                autowidth: false,
                colModel:[
                    {label:'Client ID',name:'client_id',width:100, editable:true },
                    {label:'name',name:'name',width:160, editable:true },
                    {label:'url',name:'url',width:160, editable:true },
                    {label:'name',name:'name',width:130, editable:true },
                    {label:'Client Code',name:'client_code' ,width:110, editable:true },
                    {label:'Parent',name:'parent',width:90, editable:true },
                    {label:'', width:1,editable:false ,search:false}
                ],             
                toppager:true,
                rowNum: 25,
                rowTotal:5000,
                viewrecords: true,
                gridview: true,
                autoencode: true,            
                caption: "Patient Portal Clients"
            } ;
            // save to dictionary
            //console.log("Using columns from code");
            dict.set("JSON","USERCLIENTCOLUMNS","JQGRID","",JSON.stringify(colsetup));
            callback( colsetup);
    } else {
            //console.log("Using columns from dictionary"+data);
            callback(JSON.parse(data)); 
        }       
    });
}

