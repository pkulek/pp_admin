let saltChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
let saltCharsCount = saltChars.length;      
let captchapng = require('captchapng');
let tokenExpire = 5;//in minutes
let tokenExpireFormat = 'YYYYMMDDHHmm';
let crypto = require('crypto');
let dict = require('../dic/dict');
let moment = require('moment'); // for date time
let connection  = require('express-myconnection'); 
//let redisClient = require('../redis/redisapi');
let passwordHash = require('../auth/password-hash');
let logger = require('./logging');
let Client = require('node-rest-client').Client;
let client = new Client();

let crypt = 'lKeo4F$aT6i';

//redisClient.DB1.set("DB01","test"); //test
//  redisClient.DB1.get('PPTOKEN_'+tokenid, function (err, reply) {
//            console.log(reply); 
//            return reply;
//        });

let PP_ADMIN = ['PP_ALL'] ;
let PP_USER = ['PP_VIEW'] ;

let _roles = ['PP_ALL','PP_ADD','PP_EDIT','PP_VIEW','PP_REPORTSVIEW','PP_REPORTS'] ;            
let _groups = [{'PP_ADMIN':['PP_ALL']} ,{'PP_USER':['PP_VIEW']}] ;    
//------------------------------------------------------------------------------		                
function saveToken(token,parent) {
    if ( isEmpty( parent )){ parent='PPTOKEN' ; }  
    //redisClient.DB1.set(parent+'_'+token.tokenid,JSON.stringify(token));
    dict.saveTOKN(token.tokenid,parent,JSON.stringify(token));
    return token.tokenid;
}
//------------------------------------------------------------------------------		                
function getToken(tokenid,callback) {
    var parent='PPTOKEN' ;  
    var token = newToken();
    removeExpired();
    if ( ! isEmpty(tokenid)){ 
       /*
        redisClient.DB1.get(parent+'_'+tokenid, function (err, reply) {
            console.log("login.getToken="+reply); 
            callback( reply,err) ;
        });
        */       
        dict.getTOKN(tokenid,function(result,err){
            //console.log("DICT test token="+result);
            callback( result,err) ;
        });
    } else {
        token.msg = 'Invalid token';
        callback( token ,"Error");
    };
}
//-----------------------------------------------------------------------------
exports.VerifyToken = function (req,res){ 
    var tokenid = req.query.tokenid;
    if ( isEmpty( tokenid )) {
        // send to login screen
        //res.send(false);

    }    
    // lookup user
    getToken(tokenid,function(token,err){
        if (! isEmpty(err)) {
            res.send( false);
        } else {
            token = JSON.parse(token);
            res.send( token.tokenid == tokenid ); 
        }    
    });
}
//-----------------------------------------------------------------------------
/*
    2 lookup token in redis and then see if still active check against datetime
           if error send to login screen
    2 refreshh time and store in redis        

*/
verifyUser = function(userid) {
    dict.redis(userid,function(token){
            var result = false;
            if (! isEmpty(token)) {
                result = token;
            }
            return result;
    });
}
//------------------------------------------------------------------------------
// get registered user details and save to dict /Redis
exports.register = function(req,res){
    var token = newToken();
    var pwd = req.query.pwd;
    var userid = req.query.id;
    var fname = req.query.fname;
    var lname = req.query.lname;
    var email = req.query.email;
    token.fname = fname;
    token.userid = userid,
    token.lname = lname;
    token.userid = userid;
    token.password = pwd;
    // save token and user user
    dict.log(req,res);
    passwordHash(pwd).hash(function(error, hash,salt,key) {
            token.salt = salt;
            token.hash = hash;
            token.password = pwd;
            saveUser(token);
            res.send('OK');    
    }); 
}
//------------------------------------------------------------------------------		
exports.resetpwd = function(req,res){  // change password in the Admin tool
    var userid = req.query.userid;
    var newpassword = req.query.newpassword ;     
    if ( isEmpty( userid )){ userid='' ; }    
    if ( isEmpty( newpassword )){ newpassword='Remember!' ;  }
    dict.getUSER(userid,function(data,err){
        if  ( ! isEmpty( err ) ) {
            res.send(err);
        } else {
            var token = JSON.parse(data);
            passwordHash(newpassword).hash(function(error, hash,salt,key) {
                if (error) {
                    res.send(error);
                } else {
                    token.salt = salt;
                    token.hash = hash;
                    token.password = newpassword;
                    saveUser(token);
                    dict.savePWDHistory(token.userid,token);   
                    res.send('OK');
                }    
            });
        }       
    });
}
//-----------------------------------------------------------------------------------
function genToken(len) {
  len = len ||20;
  if (crypto.randomBytes) {
    return crypto.randomBytes(Math.ceil(len / 2)).toString('hex').substring(0, len);
  } else {
    for (var i = 0, salt = ''; i < len; i++) {
      salt += saltChars.charAt(Math.floor(Math.random() * saltCharsCount));
    }
    return salt;
  }
}
//-----------------------------------------------------------------------------------
exports.forgotPasswordEmail = function(req,res){
    //var id = req.query.id;
    //var lang = req.query.langid;
    var email = req.query.email;
    var provider = req.query.provider;
    var dat = {"email":email,"provider":provider };
    //console.log("Enter forgotPasswordEmail"+dat);
    var args = {
    	data: dat,
	    headers: { "Content-Type": "application/json" }
    }; 
    client.put("http://192.168.160.131:9000/api/forgot-password-email", args, function (data, response) {
//        console.log(response);
  	     res.send(data.toString());
    });
}
//------------------------------------------------------------------------------		
exports.changepwd = function(req,res){  // change password on the PP mysql
    var userid = req.query.userid;
    var email = req.query.email;
    var provider = req.query.provider;
    var hash = req.query.hash;
    var newpwd = req.query.newpwd;
    var currentpwd = req.query.currentpwd;
    var dat = {"userid":userid, "email":email, "hash":hash, "provider":provider, "new-pwd":newpwd, "confirm-new-pwd":newpwd,"current-pwd":currentpwd };
    var args = {
    	data: dat,
	    headers: { "Content-Type": "application/json" }
    }; 
    client.put("http://localhost:9000/api/pwd/reset", args, function (data, response) {
  	     res.send(data.toString());
    });
}
//------------------------------------------------------------------------------		
exports.securityQuestions = function(req, res){
    var id = req.query.id;
    var lang = req.query.langid;
    if (isEmpty(lang) ) {
        lang = "en-US";
    }
    req.getConnection(function(err,connection){
         var qry = "select id,bank,language_id,question from portal.security_questions where language_id =LOWER('"+lang+"')" ;
         var query = connection.query(qry,function(err,rows)  {                
                if(err) {
                    res.send('Error in retrieving data');
                } else {
                    //console.log("SEND user List " + JSON.stringify(rows));
                    res.send(rows);
                };                   
         });
    });                 
}
//------------------------------------------------------------------------------		
exports.securityAnswers = function(req, res){
     var id = req.query.id;
     var lang = req.query.langid;
     console.log(id);
     req.getConnection(function(err,connection){
         var qry = "select user_id,CAST(AES_DECRYPT(answer,'"+crypt+"') as CHAR(60)) answer from portal.users_security_questions where user_id ='"+id+"'" ;
         console.log(qry);
         var query = connection.query(qry,function(err,rows)  {                
                if(err) {
                    console.log("Error Selecting : %s ",err );
                    res.send('Error in retrieving data');
                } else {
                    //console.log("SEND user List " + JSON.stringify(rows));
                    res.send(rows);
                };                   
         });
    });                 
}

//------------------------------------------------------------------------------		
exports.index = function(req, res){
      res.render('index', { title: 'Hello World' });
};                 
//------------------------------------------------------------------------------		     
exports.captchapng = function(req, res){         
    var capt = captchaImg();                         
    var validcode = new Buffer(capt.img).toString('base64');
    var imgbase64 = new Buffer(capt.img,'base64');
    res.send(imgbase64);
};
//------------------------------------------------------------------------------		     
exports.captcha = function(req, res){         
    var capt = captchaImg();                         
    var validcode = new Buffer(capt.img).toString('base64');       
    //res.render('login', { page_title: 'Captcha',image:capt.img, num:capt.num});
    res.send({ num:capt.num, image:capt.img});
};//------------------------------------------------------------------------------		                
var captchaImg = function(){                   
      var num = parseInt(Math.random()*9000+1000);                                   
      var p = new captchapng(70,25,num); // width,height,numeric captcha
      p.color(115, 95, 197, 100);  // First color: background (red, green, blue, alpha)
      p.color(30, 104, 21, 255); // Second color: paint (red, green, blue, alpha)
      var img = p.getBase64();
   // dict.Log(captchsImg,img,"INFO")
      return {img:img,num:num};
} ;
//------------------------------------------------------------------------------		                
function isEmpty(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//------------------------------------------------------------------------------		                
function newToken() {
    let _token = { 
            tokenid: genToken(),
            userid: userid,
            password: password,
            pwdExpire:moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ,
            pwdReset:"Yes",
            fname:'',
            lname:'',
            email:'',
            phone:'',
            hash:'',
            salt:'',
            link: '/login/html',
            callurl: '/login/html',
            count: 0,
            lastAccess: '',
            msg: '',
            theme: 'redmond',
            langid:'en-US',
            valid: 0,
            active: "No",
            baseurl:'',
            group:"PP_OPERATIONS",
            roles:['PP_VIEWDOCUMENTLIST','PP_USERSVIEW','PP_ONDEMANDVIEW','PP_REPOSITORYVIEW','PP_USEREDIT','PP_USERDELETE','PP_USERREGISTER'],
            expire:moment().add(tokenExpire, 'minutes').format(tokenExpireFormat)
    };
    return _token;
}
//------------------------------------------------------------------------------
function removeExpired(){
    //redisRemoveExpired();
    dict.removeExpiredTOKN() ;     
}
//------------------------------------------------------------------------------
function redisRemoveExpired(){      
    var alist = [];                     
    redisClient.DB1.keys('PPTOKEN*', function (err, keys) {
        if (err) return console.log(err);
        var now = moment().format(tokenExpireFormat);    
        for(var i = 0, len = keys.length; i < len; i++) {
                alist.push(keys[i]);
        }
        //console.log(alist);
        alist.forEach(function(value){
             redisClient.DB1.get(value, function (err, reply) {
                    var expire = JSON.parse(reply).expire;
                    if ( now > expire  ){                    
                       console.log('deleting expired:'+now+" expire="+expire);
                       redisClient.DB1.del(value); 
                    };
             });
        });
    });   
}
//------------------------------------------------------------------------------		                
function saveUser(token) {
    dict.saveUSER(token.userid,JSON.stringify(token),true);
    return token.tokenid;
}
//------------------------------------------------------------------------------		                
function getUser(userid) {
    dict.getUSER(userid,function(data){
           return data; 
    });
}

//------------------------------------------------------------------------------		
exports.setpwd = function(req,res){
     //params = {tokenid:'84676847edu',userid:'admin',password:'admin'};
    var token = newToken();
    var tokenid = req.param('tokenid');
    var userid = req.param('userid');
    var password = req.param('password');
    var newpassword = req.param('newpassword') ;     
    if ( isEmpty( tokenid )){ tokenid='' ; }    
    if ( isEmpty( userid )){ userid='' ; }    
    if ( isEmpty( password )){ password='' ; }       
    if ( isEmpty( newpassword )){ newpassword='' ;  }         
    //if (!password || !userid || !newpassword) { res.send("incorrect  parameters in change password"); return }; 
    var salt = passwordHash.genSalt();
    var hash = passwordHash.genHash(newpassword,salt);
    var now = moment().format('YYYY/MM/DD'); // current data time = new Date()
    var qry = 'select user_id,password,saltnpepa from portal.users where user_id = ?';
    req.getConnection(function(err,connection){       
        var query = connection.query(qry,[userid],function(err,rows)  {
            if(err) {
                console.log("Error Selecting : %s ",err );
            } else {     
                token = getToken(tokenid);   
                token.userid = userid;
                token.password = hash;  
                token.expire = moment().add(tokenExpire, 'minutes').format(tokenExpireFormat);
                var pwdexpire = moment().add(90, 'days').format("YYYY/MM/DD");
/*
console.log('salt   ='+rows[0].saltnpepa);
console.log('hash   ='+rows[0].password);
console.log('pwd    ='+password)
console.log('Newhash='+passwordHash.genHash(password,rows[0].saltnpepa));
res.send('test');
*/

                if (rows.length > 0 ){ //} then update 
                    // check passwords in the used passwords list
                    // portal.previous_admin_passwords user_id, password,saltnpepa,changed
                    qry = "select  user_id, password,saltnpepa,changed from portal.previous_admin_passwords where user_id = '"+userid+"'";
                    connection.query(qry, function(err, rows)  {
                        if (err)  console.log("Error Updating : %s ",err );
                        if ( rows.length > 0 ) {
                            // go through the list and see if duplicate password
                            rows.forEach(function(value){
                                var duphash = passwordHash.genHash(newpassword,value.saltnpepa);
                                if (value.password == duphash ){
                                    //console.log('Password has already been used');
                                }
                            });
                            //----------- add user to history passwords
                            qry = "INSERT into  portal.previous_admin_passwords set password = '"+hash+"'"+
                                            ", saltnpepa = '"+salt+"'"+
                                            ", changed='" +now+"'"+
                                            " ,user_id = '"+userid+"'";   
                            connection.query(qry, function(err, rows)  {
                                if (err)  console.log("Error Updating : %s ",err );
                                // add users details
                                //--------------------------------------------------------     
                                qry = "UPDATE portal.users set password = '"+hash+"'"+
                                            ", saltnpepa = '"+salt+"'"+
                                            ", passwd_expire_dt='" +pwdexpire+"'"+
                                            " where user_id = '"+userid+"'";   
                                connection.query(qry, function(err, rows)  {
                                    if (err)  console.log("Error Updating : %s ",err );
                                    saveToken(token);
                                    res.send(token.tokenid);
                                });   
                            });           
                        }
                    });
                } else {  // do an insert
                    res.send("user not found");
                }         

            }
        });
    });  
};
//------------------------------------------------------------------------------
exports.loginpost = function (req,res){ //POST returns tokenid
    console.log("POST DATA = "+req.body);

}

//------------------------------------------------------------------------------		
/**
 * 1 verify userid and password in table portal.users
 * 2 if OK create a token with data
 * 3 store data in redis as token ID
 * 4 return token ID 
 */
exports.login = function (req,res){ //GET returns tokenid
        //params = {tokenid:'asfdasdasdffdsadfsa',userid:'admin',password:'admin',langid:'en-US'};
        var tokenid = req.query.tokenid;
        var userid = req.query.userid;
        var password = req.query.password;
        var langid = req.query.langid ;  
        var token =  getUser(userid); // get from dict
        if ( isEmpty( tokenid )){
            tokenid='' ;
        }    
        if ( isEmpty( userid )){
            userid='' ;
        }    
        if ( isEmpty( password )){
            password='' ;
        }       
        if ( isEmpty( langid )){
            langid='en-US' ;
        }
        // check if token is set
        if (token.valid) {

        };    
        var now = moment(); // current data time = new Date()
        token.lastAccess = now;
        token.userid = userid;
        token.tokenid = tokenid;
        logger.LOG(userid,"AUDIT",tokenid,'LOGIN REQ','','','');
        req.getConnection(function(err,connection){
            var qry =   "select  (select language from portal.users_preferences where user_id = '"+userid+"' limit 1) langid " +  
                        ", (select role_name from portal.users_roles where user_id = '"+userid+"' limit 1) roles "+
                        ", user_id\
                        ,password\
                        ,saltnpepa\
                        ,is_admin\
                        ,active\
                        ,passwd_expire_dt from portal.users where user_id = ?";
            var query = connection.query(qry,[userid],function(err,rows)  {
                if(err) { 
                     logger.LOG(userid,"AUDIT",tokenid,'LOGIN ERROR','',err,'');
                     console.log("Error Selecting : %s ",err )

                };
             // userid and password exists
                    if ( rows.length > 0 ) {
                        // check if expired 
                        var expire = moment(rows[0].passwd_expire_dt);
                        if ( moment(expire).isBefore(now) ){
                                // expired
                              token.msg = 'Password Expired';
                        };
                        // check if in redis or dict
                        if (isEmpty(token.tokenid)){
                            token.tokenid = passwordHash.genSalt(16).toUpperCase();
                            token.valid = 0;
                            token.langid = rows[0].langid ;
                            token.roles= rows[0].roles;
                        }else{
                            token = redisClient.DB1.get('PPTOKEN_'+token.tokenid);
                        }
                        // check passwords match
                        //--------------------------------------------------------                  
                        if (passwordHash.verifyhash(password, rows[0].password, rows[0].saltnpepa) ){
                            token.valid = 1;
                            token.msg = "Authentication OK";
                            console.log('verified:'+userid);
                        } else {
                            token.valid = 0;
                            token.msg = "Authentication Failed";
                            console.log('Verification failed:'+userid);
                        }
                        //----------------------------------------------------------
                        if (token.valid ) {
                                // send to Redis or dict
                                logger.LOG(userid,"AUDIT",token.tokenid,'LOGIN VALIDATED','',err,'');
                                console.log('Vlidated:'+userid);
                                saveToken(token);
                        } else {
                            token.tokenid ='';
                        }

                    } else {                        
                        logger.LOG(userid,"AUDIT",tokenid,'LOGIN ERROR','user not found',err,'');
                        //res.send("Error");    
                    }
                
                res.send(token.tokenid);
            });
        });               
};

//-----------------------------------------------------------------------------
exports.loginVerifyUser = function (req,res){ // new login
    var tokenid = req.query.token;
    var userid = req.query.userid;
    var password = req.query.password;
    var newpassword = req.query.newpassword;
    if ( isEmpty( userid )){
        userid='' ;
    }    
    if ( isEmpty( password )){
        password='' ;
    }       
    if ( isEmpty( newpassword )){
        newpassword='' ;
    }       
    // lookup user
    var token = "";
    dict.getUSER(userid,function(data,err){
        if  ( ! isEmpty( err ) ) {
            res.send("Error");
            return 0;
        } else {
            token = JSON.parse(data);            
            // Creating hash and salt
            tokexpire = moment().add(tokenExpire, 'minutes').format(tokenExpireFormat);
            token.valid = 0;
            token.expire = tokexpire ;
            token.tokenid=genToken() ;
            passwordHash(password).verifyAgainst(token.hash, function(error, verified) {              
                if(error)
                    token.msg = 'Error in verification, Something went wrong!';
                if(!verified) {
                    token.msg = "Not Verified";
                } else {
                    token.valid = 1;
                    saveToken(token);                    
                }                
                res.send(token);
            });     
        }       
    });
};

//------------------------------------------------------------------------------		
exports.htmllogin = function(req,res){
    var tokenid = req.query.tokenid;
    // params = {token:'asfdasdasdffdsadfsa',userid:'admin',password:'admin',langid:'en-US'};
	// params = setParam(req);	
    // console.log("HTML params="+JSON.stringify(params));
    //res.render('login',{page_title:"PP Login ",data:token,token:params.token});                  
    //res.render('login',{page_title:"PP Login ",data:params,token:params.token,langid:params.langid});                  
    res.render('login',{page_title:"PP Admin Login "});           
    //res.render('login', { data:token,page_title: 'PP Admin Login' });          
};

//------------------------------------------------------------------------------		
function set_Param(req){    
    var token = req.query.token;
    var tokenid = req.query.tokenid;
    var userid = req.query.userid;
    var password = req.query.password;
    var langid = req.param.quuerylangid;
    if (isEmpty( tokenid )){       
        tokenid = '' ;
    }
    if (isEmpty( token )){       
        token = newToken() ;
    }
    if ( isEmpty( userid )){
        userid='' ;
    }    
    if ( isEmpty( password )){
        password='' ;
    }       
    if ( isEmpty( langid )){
        langid='en-US' ;
    }    
    return {token:token,userid:userid,password:password,langid:langid};// emulate multiple return values
};
//------------------------------------------------------------------------------	
exports.setParams = function(req,res){
        return set_Param(req);
};

//------------------------------------------------------------------------------		
exports.authUser = function(req,res){
	return 1;	
};
//------------------------------------------------------------------------------
		
