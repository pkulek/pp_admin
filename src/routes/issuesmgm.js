
require('console-stamp')(console, 'yyyymmddHHmmss.l');
let config = require('../utils').getConfig().config;
let moment = require('moment'); // for date time

let connected = false;
let dict = require('../dic/dict');
let sqlite3 = require('sqlite3').verbose();
var dbissues ;
const isEmpty =  require('../utils').isEmpty;
//---------------------------------------------------------------------
let buildWhere = (type,id,parent,ext)=>{
    if (typeof type == 'object'){
        let o = type;
        id = o.id;
        parent = o.parent;
        ext = o.ext ;
        type = o.type;
    }
    let where = "";
    if ( ! isEmpty(type) ) {
        if( typeof type == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` type = '${type}'`;
        }
    }
    if ( ! isEmpty(id)  ) {
        if( typeof id == 'string' ){
            where += ((where == "") ? "where " : " and " ) +`id = '${id}'`;
        }
    }
    if (! isEmpty(parent)) {
        if( typeof parent == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` parent = '${parent}'`
        }
    }
    if (! isEmpty(ext)) {
        if( typeof ext == 'string' ){
            where += ((where == "") ? "where " : " and " ) +` ext = '${ext}'`;
        }
    } 
    return where;   
}
//-------------------------------------------------------------  
exports.index = function(req, res){
    let id = req.query.id;
    let userid = req.query.userid;
    let qe = req.query.qe;
    let theme = req.query.theme;
    if (isEmpty(theme)){
        theme = 'redmond'
    }    
    res.render('main_issues',{page_title:'Issues Management',theme:theme,qe:qe,userid:userid});
}     
//-------------------------------------------------------------  
exports.html = function(req, res){
    let id = req.query.id;
    let userid = req.query.userid;
    let qe = req.query.qe;
    let theme = req.query.theme;
    let tokenid = req.query.tokenid;
    let login = require('../routes/login');
    console.log("ISSUES THML \nquery=%j\nparams=%j",req.query,req.params)
    login.getToken(tokenid,(token,err)=>{
        if(typeof token == 'string'){
            token = JSON.parse(token);
        }
        theme = token.theme;
        userid = token.userid ;
        if (isEmpty(theme)){
            theme = 'redmond'
        }    
        res.render('issues',{page_title:'Issues Management',theme:theme,qe:qe,userid:userid,tokenid:tokenid});
    })
}      
//-------------------------------------------------------------  
exports.list = function(req, res){  // post
    let type = req.query.type;
    let id = req.query.id;
    let parent = req.query.parent;
    let ext = req.query.ext;
    let userid = req.query.userid;
    let qe = req.query.qe;
    let theme = req.query.theme;
    if (isEmpty(type)) {
        type= 'ISSUE'
    }
    getlist(type,id,parent,ext,(err,rows)=>{
        res.send(rows) ;
    });
};
//-----------------------------------------------------------------
let lookup = exports.lookup = (id,parent,cargo,callback) =>{  // type = select get list of objects
    if (isEmpty(id)) {
        id = `ISSUES`; 
    }
    if (isEmpty(parent)) {
        parent = `HTML_SELECT`;    
    }
    let type = `JSON`
    let obj = {};
    dict.get(type,id,parent,"",(err,data)=>{  // returns cargo should be json list
        if(err) {
            console.error(err);
        }
        if(data) {
            obj = JSON.parse(data);
        } else {
            if( isEmpty(cargo)){
                cargo = `{"ISSUE":"ISSUE","OPENED":"OPENED","TASKED":"TASKED","ASSIGNED":"ASSIGNED","IN_PROGRESS":"IN_PROGRESS","COMPLETED":"COMPLETED","REJECTED":"REJECTED"}`;
            }
            // add to dictionary
            dict.save(type,id,parent,"",cargo) ;
            console.log(cargo);
            obj = JSON.parse(cargo) ;
        }
        let sellist = ""//  `<select name ="${id}_select" id="${id}_select" class="${parent}">`
        Object.keys(obj).forEach(function(element,key) {
            let val = element[key];
            console.log("e=%s,k=%s",element,key);
            sellist += `<option value="${element}">${element}</option>\n`
        });
        //sellist +=  '</select>'
        console.log("sellist=\n%s",sellist)
        callback(sellist);

    })
}
//------------------------------------------------------------------
exports.select = (req,res) =>{
    let id = req.query.id;
    let parent= req.query.parent;
    let option= req.query.option;
    if (isEmpty(option)) {
        option = false; 
    }
    if (isEmpty(id)) {
        id = `ISSUES`; 
    }
    if (isEmpty(parent)) {
        parent = `HTML_SELECT`;    
    }
    if(id==="ISSUES") {
        lookup(id,parent,"",(sel)=>{
            let sellist =   `<select name ="${id}_select" id="${id}_select" class="${parent}">  ${sel}  </select>` 
            if (option) {
                sellist = sel
            }
            console.log("select= %s",sellist);
            res.send(sellist);
        })
    }
    if(id==="USERID") {
        let users = require('./adminusers');
        users.usersids((list)=>{
            let sellist = "";
            if (! option) {
                sellist =   `<select name ="${id}_select" id="${id}_select" >`
            }            
            if (list.length > 0 ){
                list.forEach((element,i)=>{
                    sellist += `<option value="${element}">${element}</option>\n`
                })
            }
            if (! option) {
                sellist +=  '</select>'
            }
            res.send(sellist)
        })
    }
}
//---------------------------------------------------------------
let getlist = (type,id,parent,ext,callback)=>{
    let where = buildWhere(type,id,parent,ext);
    console.log(where);
    let qry = `select * from incidents ${where} order by datetime desc `
    if(dbissues && dbissues.open) {
        try{                
            dbissues.serialize(function() {
                dbissues.all(qry, (err, rows) => {
                    console.log("return err= %s rows=%j",err,rows)
                    rows.forEach( (row,i) => {
                        //rows[i].cargo = {"name":"peter kulek","userid":"pkulek","phone":"908 994 666"};
                        try {
                            //console.log("rows[i].cargo = %s",row.cargo);
                            if(row.cargo) {
                                rows[i].cargo = JSON.parse(row.cargo) ;
                            } else {
                                rows[i].cargo = {status:"Error in cargo",name:"peter kulek",userid:"pkulek",phone:"908 994 666"};
                            }
                        } catch(ex) {
                            console.error(ex);
                            rows[i].cargo = {status:"Error",name:"peter kulek",userid:"pkulek",phone:"908 994 666"};
                        }
                    })
                    callback(err,rows);
                });
            });
        } catch(ex){
            callback(ex,[]);
            console.trace(ex);
        }
    } else {
        console.log("Error no Connection to incidents db=%j",dbissues);
    }
    return 0
}
//-------------------------------------------------------------  
exports.tasks = function(req, res){  // post
    let type = req.query.type;
    let id = req.query.id;
    let parent = req.query.parent;
    let ext = req.query.ext;
    let userid = req.query.userid;
    let qe = req.query.qe;
    let theme = req.query.theme;
    gettasks(type,id,parent,ext,(err,rows)=>{
        res.send(rows) ;
    });
};
//---------------------------------------------------------------
let gettasks = (type,id,parent,ext,callback)=>{
    let qry = `select * from incidents where type='TASK' and id='${id}' `
    if(dbissues && dbissues.open) {
        try{                
            dbissues.serialize(function() {
                dbissues.all(qry, (err, rows) => {
                    //console.log("return err= %s rows=%j",err,rows)
                    callback(err,rows);
                });
            });
        } catch(ex){
            callback(ex,[ex]);
            console.trace(ex);
        }
    } else {
        callback("Error no Connection to incidents",["Error no Connection to incidents"]);
        console.log("Error no Connection to incidents db=%j",dbissues);
    }
    return 0
}
//-------------------------------------------------------------
exports.update = function(req, res){  //
    let data = req.body;
    let userid = data.userid;    
    if (typeof req.body == 'string') {
        data = JSON.parse(req.body);
    }
    console.log("issue data = %j",data)
    updateissue(data,(err,data)=>{
        res.send('OK')
    });
};
//-------------------------------------------------------------
exports.taskupdate = function(req, res){  //
    let data = req.body; // get data from issue for issue id
    if (typeof req.body == 'string') {
        data = JSON.parse(req.body);
    }
    console.log("taskupdate data = %j",data)
    updatetask(data,(err,data)=>{
        if(err) {
            console.error(err);
        }
        res.send("OK")
    });
};
//-------------------------------------------------------------------------
let updateissue = (data,callback )=>{
    dict.getSQN("ISSUES",(err,sid)=>{
        let now = moment().format('YYYY MM DD HH:mm:ss');
        let qry = `select count(*) as qty from incidents where type='TISSUE' and id='${data.id}' `
        let cargo = {
            type:data.type,
            id:sid,
            parent:data.parent,
            ext:data.ext,
            userid:data.userid,
            qe:data.qe,
            issueid:data.issueid,
            status:data["cargo.status"],
            priority:data["cargo.priority"],
            notes:data.notes ,
            lastupdate:now
        };
        //let qry = `insert into incidents set id = '${sid}',userid='${data.userid}', type='ISSUE',issueid ='${data.issueid}', parent='${data.parent}', ext='${data.ext}', lastupdate='${now}', notes='${data.notes}'  `
        if(dbissues && dbissues.open ) {            
            dbissues.serialize(function() {
                console.log("id=>%s< sid =%s",data.id,sid)
                if (isEmpty(data.id)) {
                    cargo.status = "OPENED" ;
                    qry = `insert into incidents(id,userid,type,issueid, parent, ext, lastupdate, notes,qe,cargo) VALUES( '${sid}','${cargo.userid}','ISSUE','${cargo.issueid}','${cargo.parent}', '${cargo.ext}','${now}','${cargo.notes}','${cargo.qe}','${JSON.stringify(cargo)}')   `
                } else {
                    qry = `update incidents set userid='${data.userid}', type='ISSUE',issueid ='${data.issueid}', parent='${data.parent}', ext='${data.ext}', datetime='${now}',lastupdate='${now}', notes='${data.notes}' ,qe= '${data.qe}',cargo ='${JSON.stringify(cargo)}' where id ='${data.id}'  `             
                }
                console.log("ISSUE qry = \n%s",qry);
                dbissues.run(qry,(err,rows)=>{
                    // now add a task entry with OPENED
                    dict.getSQN("ISSUES",(err,taskid)=>{
                        cargo.status = "OPENED";
                        cargo.id = taskid;
                        cargo.parent = sid ;
                        qry = `insert into incidents(id,userid,type,issueid, parent, ext, lastupdate, notes,qe,cargo) VALUES( '${cargo.id}','${cargo.userid}','TASK','${cargo.issueid}','${cargo.parent}', '${cargo.ext}','${now}','${cargo.notes}','${cargo.qe}','${JSON.stringify(cargo)}')   `
                        dbissues.run(qry,(err,rows)=>{
                            callback(err,rows)
                        })
                    })
                })
            });
        }
    })
}
//-------------------------------------------------------------------------
let updatetask = (data,callback )=>{
    dict.getSQN("ISSUES",(err,sid)=>{
        data.id = sid;
        let now = moment().format('YYYY MM DD HH:mm:ss');
        let qry = `select count(*) as qty from incidents where type='TASK' and parent='${data.parent}' and id='${data.id}' `
        console.log("\nDATA=\n%j",data)
        let cargo = {
            type:data.type,
            id:sid,
            parent:data.parent,
            ext:data.ext,
            userid:data.userid,
            qe:data.qe,
            issueid:data.issueid,
            status:data["cargo.status"],
            priority:data["cargo.priority"],
            lastupdate:now
        };
        if(dbissues && dbissues.open) {            
            dbissues.serialize(function() {
                dbissues.each(qry, function(err, row) {
                    if (row.qty == 0 ) {
                        qry = `insert into incidents(id,userid,type,issueid, parent, ext, lastupdate, notes,qe,cargo) VALUES( '${sid}','${data.userid}','${data.type}','${data.issueid}','${data.parent}','${data.ext}','${now}','${data.notes}','${data.qe}','${JSON.stringify(cargo)}' )   `
                    } else {
                        qry = `update incidents set userid='${data.userid}', issueid ='${data.issueid}',  ext='${data.ext}', datetime='${now}',lastupdate='${now}', notes='${data.notes}' ,qe= '${data.qe}',cargo='${JSON.stringify(cargo)}'  where id ='${id}'  `                    
                    }
                    console.log("TASK qry = \n%s",qry);
                    dbissues.run(qry,(err,rows)=>{
                        callback(err,rows)
                    })
                });                
            });
        }
    })
}
//-------------------------------------------------------------------------
exports.createdb = (req,res)=>{
    createIssuetracker("issueTracker.db","incidents");
    res.send("OK")
}
//-------------------------------------------------------------------------
let openIssuetracker = exports.createIssuetracker = (schema,table)=>{
    let fs = require('fs');
    if ( isEmpty( schema )){
        schema = 'issueTracker.db' ;
    }  
    if ( isEmpty( table )){
        table = 'incidents' ;
    }        
   if ( fs.existsSync(schema)) {
        try {
            dbissues = new sqlite3.Database(schema);
            connected = true ;
        } catch(ex){
            connected = false ;
        }
    } else {
        dbissues = new sqlite3.Database(schema);
        // check if table exists
        // types = INCIDENT, assignedto, 
        let qry = `CREATE TABLE ${table} ( 
                id CHAR(20),
                type CHAR(10),
                parent CHAR(20),
                ext CHAR(10),
                issueid CHAR(20),
                userid CHAR(20),
                qe CHAR(20),
                status CHAR(20),
                cargo BLOB,
                notes TEXT,
                datetime CHAR(22) ,
                flags INT ); 
                CREATE INDEX type on incidents (type,id,parent,ext);
                CREATE INDEX type on incidents (datetime);`
       try{                
            connected = true ;
            dbissues.serialize(function() {                    
                db.run(qry);    
            });
            //dbissues.close();
        } catch(ex){
            connected = false ;
            console.trace(ex);
        }
    }
    console.log("starting sqllite %j",dbissues);
    dbissues.on('error',(err)=>{
        console.log("sqlite Error: %s",err);
        connected = false ;
    });
    return dbissues ;
}

openIssuetracker("issueTracker.db","incidents");


