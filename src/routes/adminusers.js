
/*
 * GET users listing.
 */

var CJSON = require('circular-json');
var moment = require('moment'); // for date time
var dict = require('../dic/dict');
var login = require('./login');
var logger = require('./logging');
var passwordHash = require('../auth/password-hash');
var email = require('../mail');
var moment = require('moment'); // for date time
const pwdExpire = 90*60*24;//90 days in minutes
const tokenExpireFormat = 'YYYYMMDDHHmm';
//----------------------------------------------------------------------------------
function isEmpty(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
}
//----------------------------------------------------------------------------------
function getParams(req){
    var type = req.query.type;
    var userid = req.query.userid;
    var id = req.query.id;
    var parent = req.query.parent;
    var ext = req.query.ext;  
    var notes = req.query.notes;  
    var cargo = req.query.cargo;  
    var theme = req.query.theme;  
    var lang = req.headers["accept-language"] ;
    //console.log("logging HEADERS:="+JSON.stringify(req.headers));
    if (isEmpty( type )){       
        type="";
    }
    if ( isEmpty( userid )){
        userid="system" ;
    }    
    if ( isEmpty( id )){
        id="" ;
    }    
    if ( isEmpty( parent )){
        parent=lang ;
    }    
    if ( isEmpty( ext )){
        ext="" ;
    }     
    if ( isEmpty( cargo )){
        cargo="" ;
    }       
    if ( isEmpty( theme )){
        theme="" ;
    }       
    if ( isEmpty( notes )){
        notes="" ;
    }       
    return {type:type,id:id,parent:parent,ext:ext,cargo:cargo,notes:notes,lang:lang,theme:theme};// emulate multiple return values
}
//----------------------------------------------------------------------------------
exports.disable = (req,res) => { // 
    //logger.LOG("","AUDIT",tokenid,'DISABLE USER','','',req);
    var userid = req.query.userid;
    var tokenid = req.query.tokenid;
    //console.log("adminuser.disable.qquery="+JSON.stringify(req.query));
    dict.getUSER(userid,function(err,cargo){
        var token = JSON.parse(cargo);
        token.active = "No"; 
        token.valid = 0; 
        token.pwdReset = "Yes"; // need to reset password
        login.saveUser(token,function(err,data){    
            res.send("OK "+JSON.stringify(data));
        });        
    });
};
//----------------------------------------------------------------------------------
exports.update = function(req,res){ // 
    //logger.LOG("","AUDIT",tokenid,'CREATE USER','','',req);
    let data = req.body;
    let userid = data.userid;
    if (typeof req.body == 'string') {
        data = JSON.parse(req.body);
    }
    //console.log("adminuser.update="+JSON.stringify(data));
    //console.log("adminuser.reqquery="+JSON.stringify(req.query));
    dict.getUSER(userid,(err,cargo) =>{
        //console.log("get token cargo =%s",cargo)
            var token = JSON.parse(cargo);
            token.userid = data.userid;
            token.fname = data.fname;
            token.lname = data.lname;
            token.email = data.email;
            token.phone = data.phone;
            token.group = data.group;
            token.roles = data.roles;            
            token.group = data.group;
            token.active = data.active; 
            token.pwdReset = data.pwdReset; // need to reset password
            token.pwdExpire = data.pwdExpire ;
            token.theme = data.theme;
            //console.log("update data = %j",token);
            //console.log("update token = %j",data);
            login.saveUser(token,function(err,resdata){    
               res.send(err||resdata);
            });        
    });
};
//----------------------------------------------------------------------------------
exports.pwdreset = function(req,res){ //    
    let data = req.body;
    let userid = req.query.userid;
    if (typeof data == 'string') {
        data = JSON.parse(data);
    }
    let password = login.genPwd(8);
    //console.log("adminuser.pwdreset data="+JSON.stringify(data));
    console.log("adminuser.pwdreset query="+JSON.stringify(req.query));
    dict.getUSER(userid,(err,cargo) => {
       //console.log("adminuser.pwdreset old token="+cargo);
        let token = JSON.parse(cargo);
        passwordHash(password).hash( (error, hash,salt,key) => {
            token.salt = salt;
            token.hash = hash;
            token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
            token.valid = 1; // activate
            token.active = "Yes"; // activate
            token.pwdReset ="Yes" ; // need to reset password
            token.password = password;
            //console.log("adminuser.pwdreset new token="+JSON.stringify(token));
            login.saveUser(token,(err,data) => {    
                //console.log("Admin.saveuser= "+data);
                dict.savePWDHistory(token,(err,result) => {
                    email_pwdReset(token);

                    /*    
                    var msg = 'Your password has been reset for the Patient Portall\n'+
                        '\n\t\t\tuser id : %s' +
                        '\n\t  temporary password : %s' + 
                        '\n\t\t\t       url : %s'+
                        '\n\n\nPlease Change your password at next login.'
                    let email_config = {
                            text:    msg ,
                            from:    "pp_admin@nyecemail.com", 
                            to:      `${token.email}`,
                            cc:      "",
                            subject: "PP admin password reset"
                        }; 
                    dict.set("EMAIL","PP_ADMINPWDCHANGE","PP_ADMIN","en-US",email.config);    
                    dict.getMSG("PP_ADMINPWDCHANGE","MAIL",msg,'en-US',(data)=> {
                        var sprintf = require("sprintf-js").sprintf;
                        // insert variables.
                        msg = sprintf(data,token.userid,password,token.email);
                        console.log("EMAIL to "+token.email+"\n MSG="+msg);                               
                        email.send({
                            text:    msg ,
                            from:    "pp_admin@nyecemail.com", 
                            to:      token.email,
                            cc:      "",
                            subject: "PP admin password reset"
                        }, function(err, message)  { 
                            //console.log("err="+err);
                            //console.log("message="+JSON.stringify(message));
                        });
                    });
                    */
                    res.send("OK "+JSON.stringify(data));
                });    
            });                                
        });
    });
};
//---------------------------------------------------------------------------------
let newuser = exports.newuser = (token,callback)=>{
    login.newuser(token,(err,data)=>{
        if(callback){
            callback(err,data) ;
        }

    })
    /*
    if (isEmpty(token.password)) {
        token.password = login.genPwd(8);
    }
    passwordHash(token.password).hash(function(error, hash,salt,key) {
        token.salt = salt;
        token.hash = hash;
        token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
        token.valid = 1; // activate
        token.active = "Yes"; // activate
        token.pwdReset ="Yes" ; // need to reset password
        login.saveUser(token,function(err,data){});
    });
    */
}
//----------------------------------------------------------------------------------
exports.save = function(req,res){ // get for when post does not work
    var password = login.genPwd(8);
    //console.log("adminuser.reqquery="+JSON.stringify(req.query));
    dict.getUSER(req.query.userid,function(err,cargo){
        var token = JSON.parse(cargo);
        token.userid =  req.query.userid;
        token.fname = req.query.fname;
        token.lname = req.query.lname;
        token.email = req.query.email;
        token.phone = req.query.phone;
        token.group = req.query.group;
        token.roles = req.query.roles;
        token.theme = req.query.theme
        //token.active = data.active;
        //token.pwdReset = data.pwdReset;
        //token.pwdExpire = data.pwdExpire;
        passwordHash(password).hash(function(error, hash,salt,key) {
            token.salt = salt;
            token.hash = hash;
            token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
            token.valid = 1; // activate
            token.active = "Yes"; // activate
            token.pwdReset ="Yes" ; // need to reset password
            token.password = password;
            token.roles = "PP_OPERATIONS" ;
            var groups = login.getGroups();
            if (token.group == "SUPERADMIN"){
                token.roles = groups[0].SUPERADMIN;        
            } else if (token.group == "PP_ADMIN"){
                token.roles = groups[1].PP_ADMIN;        
            } else if (token.group="PP_OPERATIONS"){
                token.roles = groups[3].PP_OPERATIONS ;        
            }     
            login.saveUser(token,function(err,data){    
                //console.log("Admin.saveuser= "+data);
                dict.savePWDHistory(token);    
                let msg = `You have been registered as a user on the Patient Administation Portal
                    user id : ${token.userid}
                    temporary password : ${password} 
                    url : "http://192.168.180.131:4300"
                    Please Change your password at first login.`
                //dict.getMSG("PP_ADMINREGISTERED","MAIL",msg,'en-US',function(data) {
                    console.log("EMAIL to "+token.email+"\n MSG="+msg);                               
                    email.send({
                        text:    msg ,
                        from:    "pp_admin@nyecemail.com", 
                        to:      token.email,
                        cc:      "",
                        subject: "PP admin registration"
                    }, function(err, message)  { 
                     //   console.log("err="+err);
                     //   console.log("message="+JSON.stringify(message));
                    });
                //});
                
                res.send("OK "+JSON.stringify(data));
            });
        });
        
    });
};
//----------------------------------------------------------------------------------
function _save(id,parent,cargo,notes,callback){ // parent= type LOG INFO DEBUG ERROR etc
    var ext = "";
    var type="USER" ; 
    if ( isEmpty( parent )){ 
        parent='USERLOGIN' ; 
    } ;
    if ( isEmpty( id )){ 
        id ='admin' ; 
    } ;
    if ( isEmpty( notes)){ 
        notes ='' ; 
    } ;
    if ( isEmpty( cargo)){ 
        cargo ='' ; 
    } ;
    dict.set(type,id,parent,ext,cargo,notes,function(data){
         
    }) ;
}
//---------------------------------------------------
exports.user = function(req, res){
    var userid = req.params.userid;
    dict.ISUSER(userid,function(data,err){
        res.send(data);
    });
};
//---------------------------------------------------
exports.list = function(req, res){
    dict.getUSERS(function(err,data){
        //console.log("ADMIN users List " + JSON.stringify(data));
        if (typeof data == 'object') {
            data.forEach(function(obj,i){
                if (typeof data[i].cargo == 'string'){
                    user = JSON.parse(data[i].cargo) ;
                    data[i].lastAccess = user.lastAccess ;                
                    data[i].roles = user.roles ;
                    data[i].expires = user.expire ;
                    data[i].group = user.group ;
                    data[i].lname = user.lname ;
                    data[i].fname = user.fname ;        
                    data[i].email = user.email ;        
                    data[i].phone = user.phone ;        
                    data[i].active = user.active ;        
                    data[i].userid = user.userid ;        
                    data[i].theme = user.theme ;        

                    data[i].lastAccess = user.lastAccess ;                        
                    data[i].pwdReset = user.pwdReset ;
                    data[i].pwdExpire = user.pwdExpire ;
                }
            });    
        }
        res.send(data);
    });
};
//---------------------------------------------------
let usersids = exports.usersids = function(callback){
    dict.getUSERS(function(err,data){
        //console.log("ADMIN users List " + JSON.stringify(data));
        let aList = [];
        if (typeof data == 'object') {
            data.forEach(function(obj,i){
                if (typeof data[i].cargo == 'string'){
                    user = JSON.parse(data[i].cargo) ;
                    aList.push(user.userid) ;
                }
            });    
        }
        callback(aList)
    });
};
//---------------------------------------------------
exports.useridlist = function(req, res){
    usersids((list)=>{
        res.send(list)
    })
};
//---------------------------------------------
exports.htmllist = function(req, res){
   var tokenid = req.params.tokenid ;
   var type = req.params.type;
   if (isEmpty( type )){       
        type="JSON";
   } 
   login.authorised(tokenid,['PP_USERSVIEW'],function(data,roles){
       if ( data) {
            adminuser_columns(function(data){
                res.render('adminusers',{page_title:"PP admin user config setup",colsetup:data});      
            });   
       } else {
            res.send("Access Denied");                                   
       }
   }); 
};
//---------------------------------------------------
adminuser_columns = function(callback) {
   // dict.get("JSON","USERADMINCOLUMNS","JQGRID","",(err,data)=> {
   //     if (  isEmpty(data)) {
   let themelist = require('../utils').UIThemeList('',4);
   console.log(themelist)
            var colsetup = { 
                url:'/adminusers/list', 
                editurl:'/adminuser/update',
                altRows: false, 
                deepempty: true,
                autowidth: true,
                ignoreCase: true,
                datatype: "json",
                mtype: "GET",       
                height:'auto',
                width:'auto',
                colModel:[
                    {label:'id',name:'userid', editable:true , width:85, editoptions:{size:26,readonly: "readonly"},formoptions:{label:'User ID'}, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                    {label:'active',name:'active', search:false,editable:true , width:45,formoptions:{label:'Active'}, formatoptions: { disabled: false}, edittype: "checkbox", editoptions: {value: "Yes:No"} },
                    {label:'pwd Reset',name:'pwdReset', search:false,editable:true , width:65,formoptions:{label:'Force Pwd Reset'}, formatoptions: { disabled: false}, edittype: "checkbox", editoptions: {value: "Yes:No"} },
                    {label:'pwd expires',name:'pwdExpire',search:false, editable:true ,width:85,editoptions: {size:20, dataInit: function(el) { $(el).datepicker({ controlType: 'select', dateFormat: "yyyymmdd" } ) } }, formoptions:{label:'Password Expiry'} },
                    {label:'fname',name:'fname', editable:true , width:85, editoptions:{size:26},formoptions:{label:'First Name'}, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                    {label:'lname',name:'lname', editable:true , width:85, editoptions:{size:26},formoptions:{label:'Last Name'}, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                    {label:'email',name:'email', editable:true ,width:85,editoptions:{size:26},formoptions:{label:'Email'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'phone',name:'phone', editable:true ,width:85,editoptions:{size:26},formoptions:{label:'Phone'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'group',name:'group',search:false, editable:true ,width:85,editoptions:{size:26},formoptions:{label:'group'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'roles',name:'roles',search:false, editable:true,hidden: true,editrules: { required: true,edithidden:true} ,width:185,edittype:"textarea",editoptions:{size:285,rows:"1",cols:"20"},formoptions:{label:'roles'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'last Access',name:'lastAccess', search:false,editable:false ,width:85,editoptions:{size:26},formoptions:{label:'Last Access'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'theme',name:'theme', editable:true , width:85, edittype:"select",editoptions:{value:`${themelist}`}},
                    //{label:'notes',name:'notes',index:'notes', editable:true, hidden: true,,width:450, edittype:"textarea", editoptions:{size:400,rows:"12",cols:"100"},sortable:false,searchoptions:{sopt:['cn','bw','eq','bn','nc','ew','en']}},
                    {label:'', width:1,search:false}    
                ],
                loadonce:true,
                toppager:true,
                rowNum: 25,
                rowTotal:5000,
                viewrecords: true,
                gridview: true,
                autoencode: false,            
                caption: "Admin Users"          
            }
            dict.set("JSON","USERADMINCOLUMNS","JQGRID","",colsetup);
            callback( colsetup);
   //     } else {
   //         callback(JSON.parse(data)); 
   //     }       
   // });      
};

let email_pwdReset = function(token) {
  let oMail = { 
        text:   
 `
 ${token.fname} ${token.lname}

 Your password has been reset for the Patient Portal

             user id:  ${token.userid}
  temporary password:  ${token.password}
                url : http://192.168.180.131:4300


Please Change your password at next login.`
,
        from:    "pp_admin@nyecemail.com", 
        to:      token.email,
        cc:      "",
        subject: "PP Admin Portal Password Reset"
    };
    email.sendEmail(token,'PPADMIN_PWD_RESET',oMail);
    return 0;
};
