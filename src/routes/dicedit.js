
let dict = require('../dic/dict');
let utils = require('../utils');

//----------------------------------------------------
exports.list = function(req,res){
   var type = req.params.type;
   getlist(type,(data,err)=>{
       res.send(data);
   });
}
//----------------------------------------------------
let getlist = exports.getlist = function(type,callback){
    if (utils.isEmpty(type)) {
        type = 'URL';
    }
    var id = '';
    var parent = '';
    var ext = '';
    dict.get(type,id,parent,ext,(rows,err)=>{
        callback(rows,err) ;
    });
};
//----------------------------------------------------
exports.editor = function(req, res){
   var tokenid = req.params.tokenid ;
   var type = req.params.type;
   if (isEmpty( type )){       
        type="JSON";
   } 
};
//---------------------------------------------------
exports.html = function(req, res){
    setup_DicColumns( function(colSetup){   
       res.render('dicedit1',{page_title:"Source Editor ",colsetup:colSetup});                        
    });        
};

//----------------------------------------------------
exports.save = function(req, res){
   let tokenid = req.params.tokenid ;
   let type = req.params.type;
   let input = JSON.parse(JSON.stringify(req.body));
   console.log(JSON.stringify(input));
   if (isEmpty( type )){       
        type="JSON";
   }
};
//---------------------------------------------------
let setup_DicColumns = function(callback) {
let colsetup = { 
        url:'/EDIT/list/HTM', 
        altRows: false, 
        deepempty: true,
        autowidth: false,
        ignoreCase: true,
        multiselect:false,
        multiboxonly:true,
        datatype: "json",
        mtype: "GET",       
        height:'auto',
        width:'auto',
        scrollOffset:0,
        colModel:[
            {label:'Type',name:'type', editable:false,hidden:true ,width:88 },
            {label:'ID',name:'id', editable:false ,width:155 },
            {label:'parent',name:'parent', editable:false,hidden:true ,width:155 },                    
            {label:'ext',name:'ext', editable:false ,hidden:true,width:30   },
            {label:'cargo',name:'cargo', editable:true,hidden:true ,width:150 },
            {label:'notes',name:'cargo', editable:true,hidden:true ,width:150 },
            {label:'-', width:1,search:false}              
        ],
        loadonce:true,
        toppager:true,
        rowList: [],        // disable page size dropdown
        pgbuttons: false,     // disable page control like next, back button
        pgtext: null,         // disable pager text like 'Page 0 of 10'
        viewrecords: false ,   // disable current view record text like 'View 1-10 of 100' 
        gridview: false,
        autoencode: true,           
        caption: "Dic edit"          
    };    
    dict.set("JSON","DICEDITCOLUMNS","JQGRID","",colsetup,function(err,data) {
        if ( isEmpty(data)) {
            callback( colsetup);
        } else {
            callback(JSON.parse(data)); 
        }       
    });      
};


