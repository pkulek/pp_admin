
/*
 * GET users listing.
 */


var moment = require('moment'); // for date time
var dict = require('../dic/dict');


//----------------------------------------------------------------------------------
function isEmpty(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
}
//----------------------------------------------------------------------------------
function getParams(req){
    var type = req.query.type;
    var userid = req.query.userid;
    var id = req.query.id;
    var parent = req.query.parent;
    var ext = req.query.ext;  
    var notes = req.query.notes;  
    var cargo = req.query.cargo;  
    var lang = req.headers["accept-language"] ;
    //console.log("logging HEADERS:="+JSON.strinigy(req.headers));
    if (isEmpty( type )){       
        type="";
    }
    if ( isEmpty( userid )){
        userid="system" ;
    }    
    if ( isEmpty( id )){
        id="" ;
    }    
    if ( isEmpty( parent )){
        parent=lang ;
    }    
    if ( isEmpty( ext )){
        ext="" ;
    }     
    if ( isEmpty( cargo )){
        cargo="" ;
    }       
    if ( isEmpty( notes )){
        notes="" ;
    }       
    return {type:type,id:id,parent:parent,ext:ext,cargo:cargo,notes:notes,lang:lang};// emulate multiple return values
}
//----------------------------------------------------------------------------------
exports.save = function(req,res){ // POST
    var pars = getParams(req);
    console.log("config.js-save"+req.body);
    if (isEmpty(pars.notes)) {
        pars.notes = JSON.stringify(req.body);
    }
    if (isEmpty(pars.cargo)) {
        pars.cargo = JSON.stringify(res);
    }
    _logconfig(pars.userid, pars.type, pars.id, pars.parent, pars.notes ,pars.cargo);
    res.send("OK");
};
//----------------------------------------------------------------------------------
function _save(id,parent,cargo,notes,callback){ // parent= type LOG INFO DEBUG ERROR etc
    var ext = "";
    var type="CONFIG" ; 
    if ( isEmpty( parent )){ 
        parent='PATIENTPORTAL' ; 
    } ;
    if ( isEmpty( id )){ 
        id ='APP001_URL' ; 
    } ;
    if ( isEmpty( notes)){ 
        notes ='' ; 
    } ;
    if ( isEmpty( cargo)){ 
        cargo ='' ; 
    } ;
    dict.set(type,id,parent,ext,cargo,notes,function(data){
         
    }) ;
}
//---------------------------------------------------
exports.list = function(req, res){
     var input = JSON.parse(JSON.stringify(req.params));
     var id = req.params.id;
     dict.getCONFIG('','',(data,err)=>{
         //console.log("CONFIG List " + JSON.stringify(data));
         res.send(data);
     });
};
//---------------------------------------------------
exports.htmllist = function(req, res){
    config_columns(function(data){
        //console.log("config html list="+JSON.stringify(data));
        res.render('config',{page_title:"PP config setup",colsetup:data});                           
    });     
};

//---------------------------------------------------
config_columns = function(callback) {
    dict.get("JSON","CONFIGCOLUMNS","JQGRID","",function(err,data) {
        if ( isEmpty(data)) {
            var colsetup = { 
                url:'/config', 
                altRows: false, 
                deepempty: true,
                autowidth: true,
                ignoreCase: true,
                datatype: "json",
                mtype: "GET",       
                height:'auto',
                width:'auto',
                colModel:[
                    {label:'id',name:'idconf',index:'idconf', editable:false , width:85, editoptions:{size:26},formoptions:{label:'First Name'}, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                    {label:'parent',name:'parent',index:'parent', editable:false ,width:85,editoptions:{size:26},formoptions:{label:'Last Name'},searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'notes',name:'notes',index:'notes', editable:true ,width:450, edittype:"textarea", editoptions:{size:400,rows:"12",cols:"100"},sortable:false,searchoptions:{sopt:['cn','bw','eq','bn','nc','ew','en']}},
                    {label:'cargo',name:'cargo',index:'cargo', editable:true ,width:450,  editoptions:{size:326},searchoptions:{sopt:['cn','bw','eq','bn','nc','ew','en']}},
                    {label:'', width:1,search:false}    
                ],
                loadonce:true,
                toppager:true,
                rowNum: 25,
                rowTotal:5000,
                viewrecords: true,
                gridview: true,
                autoencode: false,            
                caption: "Config"          
            }
            // save to dictionary
            //console.log("Using columns from code");
            dict.save("JSON","CONFIGCOLUMNS","JQGRID","",JSON.stringify(colsetup));
            callback( colsetup);
        } else {
            //console.log("Using columns from dictionary"+data);
            callback(JSON.parse(data)); 
        }       
    });      
};


