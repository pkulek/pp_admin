
/*
 * documents for patient.
 */


// add timestamps in front of log messages
require('console-stamp')(console, 'yyyymmddHHmmss.l');

let config = require('../utils').getConfig().conf ;

const JSONC = require("circular-json");
let logger = require('./logging');
let dict = require('../dic/dict');
let login =require('./login');
let users =require('./users');
let Client = require('node-rest-client').Client;
// configure basic http auth for every request

//var options_auth = { user: "HS_Services", password: "HS_Services" };
//var options_auth = { user: "HS_Services", password: "$Nyec123" };
let devenv = "STAGE";
/*
let PROD_options_auth = { user: "HS_Services", password: "HS_Services" };
let STAGE_options_auth = { user: "HS_Services", password: "$Nyec123" };
var options_auth = eval(devenv+"_options_auth");
let  client = new Client(options_auth);
*/
let  client = new Client(config.HS_auth);

//---------------------------------------------------
isEmpty = function(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//
exports.test = function(req,res) {
    userDocList("william.wiegner",'Approved',(data)=>{
        res.send(data)
    })
}
//---------------------------------------------------
exports.userDocList = userDocList = function(user_id,type,callback){   // get document list by pp user_id
    if ( isEmpty( type )) {
        type = "Approved"
    }
    //var PROD_url = "http://192.168.180.174:57772/csp/healthshare/pphub/api/NYeC.PP.Api/Patient/DocumentList" ;
    //var STAGE_url = "http://192.168.160.174:57772/csp/healthshare/hsregistry/api/NYeC.PP.Api/Patient/DocumentList" ;
    //var url = eval(devenv+"_url") + "/"+mrn+"/"+type;

    // get mrn from pp_userid
    users.getVerizonID(user_id,(mrn,err)=>{
        console.log("mrn=%s, type=%s, err=%s",mrn,type,err)
        let url =  config.HSHUB_APIurl + `/Patient/DocumentList/${mrn}/${type} `;
        //console.dir("userDocList="+url);
        var cReq = client.get(url, function (data, response) {
  	            callback(data,response);
        });    
    });   
};
//-------------------------------------------------------------------------------
exports.HSlist = function(req, res){ 
    var id = req.params.id;
    var type = req.params.type;
    var tokenid = req.params.tokenid;
    // jqgrid params
    var page = req.params.page;
    var rows = req.params.limit;
    var sidx = req.params.sidx;
    var sord = req.params.sord;
    //
    var params = "";
    if ( isEmpty( id )) {
        id = ""
    }
    if ( isEmpty( type )) {
        type = ""
    }
    if (id != "") {
        params += "/"+id
        if ( type != "") {
            params += "/"+type
        }
    }
    //console.log("HSList Params ="+params+"-"+page+"-"+rows+"-"+sidx+"-"+sord);
    /*
    var PROD_url = "http://192.168.180.174:57772/csp/healthshare/pphub/api/NYeC.PP.Api/Patient/DocumentList" ;
    var STAGE_url = "http://192.168.160.174:57772/csp/healthshare/hsregistry/api/NYeC.PP.Api/Patient/DocumentList" ;
    var url = eval(devenv+"_url") + params;
*/
    let url =  config.HSHUB_APIurl + `/Patient/DocumentList${params}`
console.log(url)
    var cReq = client.get(url, function (data, response) {
        //logger.LOG(tokenid ,"TRACE","documents.HSList","TIMMINGS","TRACE END");
        if(typeof data == 'object' ) {
            //console.log("data is object %j",data)
            if (data.type && data.type == 'Buffer' ) {
                data = data.data.toString('utf8').replace(/\0/g, 'Error');
            }
            
        } else {
            console.log("data is NOT object %s",data)

        }
        //let cdata = data.toString('utf8');
        //res.send( cdata.replace(/\0/g, 'Error') );
        //res.send( cdata.replaceAll('\0', 'Error') ); //same as above - defined in utils
  	    res.send(data);
    });    
    cReq.on('requestTimeout', function (req) {
        console.log("HS Documents - request has expired");
        req.abort();
    });
    cReq.on('responseTimeout', function (res) {
        console.log("HS Documents - request has expired");
        req.abort();
    });
};
//---------------------------------------------------
exports.HShtmllist = function(req, res){ // display document list in jqgrid
    // get token ID for req
    var tokenid = req.params.tokenid;
    var id = req.params.id;    
    login.Token(tokenid,function(data,err) {     
        if (isEmpty(err) ){
            var token = JSON.parse(data);
            token.password = "**********";
            logger.LOG(token.userid,"AUDIT",tokenid,'LIST DOCUMENTS REQ','',token,res);    
            login.authorised(tokenid,['PP_VIEWDOCUMENTLIST'],function(result,roles){
                if (result) {
                    /*      
                    dict.get_ejsTemplate('HSdocuments',req.app.get('views'),(err,template)=>{
                        if(! isEmpty(template)){  
                            let ejs = require('ejs');
                            res.write(ejs.render(template,{filename:"HSdocuments.ejs",page_title:"HS Patient Documents ",tokenid:tokenid,id:id}));                    
                            res.end();                            
                        //res.send(ejs.render(template,{page_title:"HS Patient Documents ",tokenid:tokenid,id:id}));
                        } else {
                            res.render('HSdocuments',{page_title:"HS Patient Documents ",tokenid:tokenid,id:id});    
                        }     
                    })
                    */   
                    res.render('HSdocuments',{page_title:"HS Patient Documents ",tokenid:tokenid,id:id});    
                } else {
                    res.send(false);    
                }
            });
        } else {
            res.send(false);
        }
    });         
};
//---------------------------------------------------
exports.HSDoc = function(req, res){ // a redirect to HS
    var tokenid = req.params.tokenid;
    var id = req.params.id;
    var type = req.params.type;
    var type1 = req.query.type;
    //console.log("documents.HSDOC params = %j",req.params);
    login.Token(tokenid,function(data,err)  {
        if (isEmpty(err) ){
            var token = JSON.parse(data);
            token.password = "**********";
            logger.LOG(token.userid,"AUDIT",tokenid,'ONDEMAND VIEW','',token,res);    
            login.authorised(tokenid,['PP_ONDEMANDVIEW'],(data,roles)=>{
                if ( isEmpty( type )) {
                    type = "HTML"
                }
                let url =  config.HSREPOSITORY_APIurl + `/Patient/Document/${id}/${type}`;
                var cReq = client.get(url, function (data, response) {
                    //res.render('HSdoc',{page_title:"HS document ",data:data,tokenid:tokenid,id:id });
                    res.setHeader('content-type', 'text/'+type.toLowerCase());
                    res.send(data);
                });    
                cReq.on('requestTimeout', function (req) {
                    console.log("HS Documents - request has expired");        
                    req.abort();
                });
                cReq.on('responseTimeout', function (res) {
                    console.log("HS Documents - request has expired");
                    req.abort();
                });
             });
        } else {
            res.send(false);
        }
   });
};
//---------------------------------------------------
exports.RegisterSPRLDoc = function(req, res){ // a redirect to HS
    var tokenid = req.params.tokenid;
    var id = req.params.id;
    var type = req.params.type;  
   
    let url =  config.HSECR_APIurl + `/Patient/SPRLDocument/${id} `

    //console.log(url);
    var cReq = client.get(url, function (data, response) {
        res.setHeader('content-type', 'text/html');
        //res.status(302).send(data)  ;//redirect
        res.send(data);
    });    
    cReq.on('requestTimeout', function (req) {
        console.log("HS Documents - request has expired");        
        req.abort();
    });
    cReq.on('responseTimeout', function (res) {
         console.log("HS Documents - request has expired");
        req.abort();
    });
};
//---------------------------------------------------
var SPRLDocRegister = exports.SPRLDocRegister = function(user_id,callback){ // a redirect to HS
    users.getVerizonID(user_id,(verizonID)=>{
        console.log("SPRLDocRegister verizonid = %s",verizonID);
        //var PROD_url = "http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.Api/Patient/SPRLDocument/"+verizonID
        //var STAGE_url = "http://192.168.160.173:57772/csp/healthshare/ppedge/api/NYeC.PP.Api/Patient/SPRLDocument/"+verizonID
        //var url = eval(devenv+"_url")
        let url =  config.HSECR_APIurl + `/Patient/SPRLDocument/${verizonID} `

        console.log("SPRLDocRegister url ="+url);
        var cReq = client.get(url, function (data, response) {
            if(callback) {
                callback(data,'');
            }
        });    
        cReq.on('requestTimeout', function (req) {
            console.log("HS Documents - request has expired");
            if(callback) {
                callback('','Error');
            }
        });
        cReq.on('responseTimeout', function (res) {
            console.log("HS Documents - request has expired");
            if(callback) {
                callback('','Error');
            }
        });
    });
};
//---------------------------------------------------
var SPRLDocReg = exports.SPRLDocReg = function(verizon_id,callback){ // a redirect to HS
        console.log("SPRLDocRegister verizonid = %s",verizon_id);
        //var PROD_url = "http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.Api/Patient/SPRLDocument/"+verizonID
        //var STAGE_url = "http://192.168.160.173:57772/csp/healthshare/ppedge/api/NYeC.PP.Api/Patient/SPRLDocument/"+verizonID
        //var url = eval(devenv+"_url")
        let url =  config.HSECR_APIurl + `/Patient/SPRLDocument/${verizon_id} `
        console.log("documents.SPRLDocReg ="+url);
        let cReq = client.get(url, function (data, response) {
            if(callback) {
                callback(data,response);
            }
        });    
};

//---------------------------------------------------
exports.SPRLDoc = function(req, res){ // a redirect to HS
    var tokenid = req.params.tokenid;
    var id = req.params.id;
    var type = req.params.type;
    //login.authorised(tokenid,['PP_SPRLVIEW'],function(data,roles,token){
    login.Token(tokenid,function(token){
        token = JSON.parse(token);
        if ( isEmpty( type )) {
            type = "HTML"
        };
        //var url = "http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.Api/Patient/SPRLDoc/"+id
        //var PROD_url = "http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.Api/Patient/SPRLDoc/"+id
        //var STAGE_url = "http://192.168.160.173:57772/csp/healthshare/ppedge/api/NYeC.PP.Api/Patient/SPRLDoc/"+id
        //var url = eval(devenv+"_url")
        let url =  config.HSECR_APIurl + `/Patient/SPRLDoc/${id} `

        logger.LOG(token.userid,"AUDIT",token.tokenid,'GETSPRL START',id,url,"");
        var options_auth = { user: token.userid, password:token.password };
        var cl = new Client(options_auth);
        var cReq = client.get(url, function (data, response) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'GETSPRL RESULT',id,data,response);
            res.setHeader('content-type', 'text/html');
            res.send(data);
        });    
        cReq.on('requestTimeout', function (req) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'GETSPRL REQ TIMEOUT',id,url,"");
            console.log("SPRL Documents - request has expired");        
            req.abort();
        });
        cReq.on('responseTimeout', function (res) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'GETSPRL RES TIMEOUT',id,url,"");
            console.log("HS Documents - request has expired");
            res.status(502).send("HS Documents - request has expired")  
        });
    });
  
};
//---------------------------------------------------
var getOndemandDoc = exports.getOndemandDoc = function(id,type,callback){ // a redirect to HS
    if ( isEmpty( type )) {
        type = "XML"
    };
    //var PROD_url = "http://192.168.180.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api/Patient/ODDocument/"+id+"/"+type
    //var STAGE_url = "http://192.168.160.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api/Patient/ODDocument/"+id+"/"+type
    //var url = eval(devenv+"_url")
    let url =  config.HSACCESS_APIurl + `/Patient/ODDocument/${id}/${type} `
    var cReq = client.get(url,  (data, response) => {
        callback(data,'');
    }).on('error',  (err) => {
        callback('',err);
    });
    cReq.on('requestTimeout',  (res) => {
        callback('',res);
    });
    cReq.on('responseTimeout', (res) => {
        callback('',res);
    });
};
//---------------------------------------------------
exports.OnDemandDoc = function(req, res){ // a redirect to HS
    var tokenid = req.params.tokenid;
    var id = req.params.id;
    var type = req.params.type;  
    login.Token(tokenid,function(token){
        if ( isEmpty( type )) {
            type = "HTML"
        };
        //var PROD_url = "http://192.168.180.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api/Patient/ODDocument/"+id+"/"+type
        //var STAGE_url = "http://192.168.160.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api/Patient/ODDocument/"+id+"/"+type
        //var url = eval(devenv+"_url")
        let url =  config.HSACCESS_APIurl + `/Patient/ODDocument/${id}/${type} `

        logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMAND REQ',id,url,req);
        var cReq = client.get(url, function (data, response) {
            console.log(JSONC.stringify(response));
            if ( data == '' || data == ' ') {
                logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMND RESULT',id,'Retrieve Failed ','check to see if healthshare system is up and user has valid credentials ');
            } else {
                logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMND RESULT',id,'Retrieved OK ',data);
            }
            res.setHeader('content-type', 'text/'+type.toLowerCase()+'');           
            res.send(data);
        }).on('error', function (err) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMND ERROR',id, err.request.options);
            console.log('something went wrong on the request', err.request.options);
        });
        cReq.on('requestTimeout', function (req) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMND REQ TIMEOUT',id,url,"");
            console.log("HS Documents - request has expired");        
            req.abort();
        });
        cReq.on('responseTimeout', function (res) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMND RES TIMEOUT',id,url,"");
            console.log("HS Documents - request has expired");
            res.status(502).send("HS Documents - request has expired")  
        });
    });
};
//---------------------------------------------------
exports.list = function(req, res){ // a redirect to HS
    client.get("http://192.168.160.173:57772/csp/healthshare/hsrepository/api/NYeC.PP.Api/Patient/Document/List/99/100000102", function (data, response) {
            res.send(data);
    });
};
//---------------------------------------------------
exports.htmllist = function(req, res){ // display document list in jqgrid
    // get token ID for req
    if (login.VerifyToken(req) ){    
        var jqgSetup =   HSdocument_jqgrid();     
        res.render('documents',{page_title:"PP Documents",Setup:jqgSetup,tokenid:"peter"});
    } else {   
        res.render('login',{page_title:"PP Login "});
    }                          
};



