const DEBUG = false;

let saltChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
let saltCharsCount = saltChars.length;      
let captchapng = require('captchapng');
let crypto = require('crypto');
let dict = require('../dic/dict');
let moment = require('moment'); // for date time
let passwordHash = require('../auth/password-hash');
let Client = require('node-rest-client').Client;
const options_auth = { user: "HS_Services", password: "HS_Services" };
let client = new Client(options_auth);
let sets = require('../sets');
let logger = require('../routes/logging');
let email = require('../mail');

const tokenExpire = 15;//in minutes
const pwdExpire = 90*60*24;//90 days in minutes
const tokenExpireFormat = 'YYYYMMDDHHmm';
const crypt = 'lKeo4F$aT6i';

//redisClient.DB1.set("DB01","test"); //test
//  redisClient.DB1.get('PPTOKEN_'+tokenid, function (err, reply) {
//            console.log(reply); 
//            return reply;
//        });
const PP_SADMIN = ['PP_ALL'] ;
const PP_ADMIN = ['PP_ALL'] ;
const PP_OPERATIONS = ['PP_VIEWDOCUMENTLIST','PP_USERSVIEW','PP_REPOSITORYVIEW','PP_USEREDIT','PP_USERDELETE','PP_USERREGISTER','PP_SPRLGET'] ;
const PP_USER = ['PP_VIEW'] ;

const _roles = ['PP_ALL','PP_ADD','PP_EDIT','PP_VIEW','PP_AUDIT','PP_REPORTSVIEW','PP_REPORTS','PP_VIEWONDEMAND','PP_VIEWSPRL','PP_VIEWREPOSITORY', 
              'PP_AUDITVIEW','PP_VIEWDOCUMENTLIST','PP_USERSVIEW','PP_ONDEMANDVIEW','PP_RAWVIEW','PP_SPRLVIEW','PP_SPRLGET','PP_REPOSITORYVIEW',
              'PP_USEREDIT','PP_USERDELETE','PP_USERREGISTER','PP_USERADMINVIEW','PP_ADMINUSERCREATE' ];         
const _groups = [ {'SUPERADMIN':PP_SADMIN} ,
                {'PP_ADMIN':PP_ADMIN} ,
                {'PP_USER':PP_USER},
                {'PP_OPERATIONS':PP_OPERATIONS}] ;            
let roles = new sets.Set(_roles);
const isEmpty =  require('../utils').isEmpty;

//---------------------------------------------------------------------------------
let setToken = exports.setToken = (token,callback)=>{
    if (token && isEmpty(token.password)) {
        token.password = login.genPwd(8);
    }
    //console.log("login.newuser = %j",token);
    passwordHash(token.password).hash(function(error, hash,salt,key) {
        token.salt = salt;
        token.hash = hash;
        token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
        token.valid = 1; // activate
        token.active = "Yes"; // activate
        token.pwdReset ="Yes" ; // need to reset password
        if(callback){
            callback(token);
        }
        return token;
    });
};
//-------------------------------------------------------------------------
let generatePassword = (length)=> {
     if (isEmpty(length)){
         var length=12;
    }
    var c='abcdefghijknopqrstuvwxyzACDEFGHJKLMNPQRSTUVWXYZ12345679',
    n=c.length,
    p='!@#$+-*&_',
    o=p.length,
    r='',
    n=c.length,
    s=Math.floor(Math.random() * (p.length-1));
    for(var i=0; i<length; ++i){
        if(s == i){
            r += p.charAt(Math.floor(Math.random() * o));
        }else{
            r += c.charAt(Math.floor(Math.random() * n));
        }
    }
    return r;
}
//------------------------------------------------------------------------------		                
exports.genPwd = function(length){
    return generatePassword(length) ;
}
//------------------------------------------------------------------------------		                
exports.getRoles = function (){ 
    return(_roles);
};
//------------------------------------------------------------------------------		                
exports.getGroups = function (){ 
    return(_groups);
};
//------------------------------------------------------------------------------		                
let saveToken = function(token,parent) {
    if ( isEmpty( parent )){ parent='PPTOKEN' ; }  
    //redisClient.DB1.set(parent+'_'+token.tokenid,JSON.stringify(token));
    dict.saveTOKN(token.tokenid,parent,JSON.stringify(token));
    return token.tokenid;
}
//------------------------------------------------------------------------------
let Token = exports.Token = function(tokenid,callback) {
    getToken(tokenid,function(data,err){
        callback(data,err);
    });
    return 0;
}
//------------------------------------------------------------------------------		                
let getToken = exports.getToken = (tokenid,callback) => {
    removeExpired();
    if ( ! isEmpty(tokenid)){ 
        dict.getTOKN(tokenid,function(err,result){
            //console.log("DICT test token="+result);
            callback( result,err) ;
        });
    } else {
        let token = newToken();
        token.msg = 'Invalid token';
        callback( token ,"Error");
    };
};
//----------------------------------------------------------------------------------------------
let AUTHORISE = `
    let result = false ;
    let status = "LOGIN_AUTHORISE_FAIL" ; 
    if (typeof roles == 'string' ) {
        roles = roles.split(',');
    }
    if (typeof roles == 'array' ) {
        let testroles = token.roles ;
        if (typeof testroles == 'string' ) {
            testroles = testroles.split(',');
        }                    
        let uroles = new sets.Set(testroles) ;
        if ( uroles.contains(["PP_ALL","PP_ADMIN"]) || uroles.contains(roles) ) { 
            status = "LOGIN_AUTHORISE_OK";
        } else {
            status = "LOGIN_AUTHORISE_FAIL";
        }
        token.msg = status;
        saveToken(token) ;
        loginFlow(status,token.tokenid)        
    } else {
        loginFlow(status,token.tokenid)
    }    
` 
//----------------------------------------------------------------------------
let loginFlow = (status,tokenid) => {
    dict.getTOKN(tokenid, function(err,token){
        if (DEBUG) {
           // console.log("jtoken = %j err= %s",token,err)
        }
        if (isEmpty(token)) {
            token = newToken();
        } else {
            if (typeof token == 'string' ) {
                token = JSON.parse(token)                                    
            }
        }
        let flowMsg = token['msg'] ;
        let userid = token.userid;
        let roles = token["roles"]
        dict.getFLOW(status, "LOGIN_FLOW",function(err,data){ // returns a JS function
            logger.LOG(userid,"AUDIT",tokenid,status,"",roles,JSON.stringify(token));
            try {
                let proc = eval(flowStatus) ;  //eg ATTEMPT2 = text
                if (config.DEBUG) {
                    console.log("Login Flow Called, flow status = %s , token = %j ",flowStatus ,token);
                }
                if ( isEmpty(data)) {
                    eval(proc);
                }else{
                    eval(data);
                }
            } catch(ex) {
                console.error(ex);
            }
        }); 
    });
    return false;
};
//----------------------------------------------------------------------------
let authorised = exports.authorised = (tokenid,roles,callback)=>{
    dict.getTOKN(tokenid,function(err,token){
        if ( isEmpty(err)){
            //console.log("login.authorised jtoken = %j err= %s",token,err)
            if (typeof token == 'string' && ! isEmpty(token)) {
                token = JSON.parse(token)
            }
            var userid = token.userid;
            if ( (! isEmpty(callback))  && (! isEmpty(token))  ) {
                token.password='***********';
                logger.LOG(userid,"AUDIT",tokenid,"AUTHENTICATE_START","",roles,JSON.stringify(token));
                        if (roles.constructor === String ) {
                            roles = roles.split(',');
                        }
                        //console.log('login._authorized roles='+roles);
                        if (roles.constructor === Array ) {
                            var testroles = token.roles ;
                            if (testroles.constructor === String ) {
                                testroles = testroles.split(',');
                            }                    
                            //console.log('_authorized testroles='+testroles);
                            var uroles = new sets.Set(testroles) ;
                            var test = ( uroles.contains(["PP_ALL","PP_ADMIN"]) || uroles.contains(roles) ); 
                            logger.LOG(userid,"AUDIT",tokenid,"AUTHENTICATED="+test,"",roles,JSON.stringify(token));
                            callback(test,testroles,token) ;
                        } else {
                            callback(false);
                        }
            } else {
                logger.LOG(userid,"ERROR",tokenid,"AUTHENTICATION","",roles,'security token not found');            
                callback(false);
            }
        } else {
            logger.LOG(userid,"ERROR",tokenid,"AUTHENTICATION","",roles,err);            
            callback(false);
        }
    });
    return false;
} 

//---------------------------------------------------------------------------------
let newuser = exports.newuser = (token,callback)=>{
    if (token && isEmpty(token.password)) {
        token.password = login.genPwd(8);
    }
    if (DEBUG) {
            console.log("login.newuser = %j",token);
    }
    passwordHash(token.password).hash(function(error, hash,salt,key) {
        token.salt = salt;
        token.hash = hash;
        token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
        token.valid = 1; // activate
        token.active = "Yes"; // activate
        token.pwdReset ="Yes" ; // need to reset password
        saveUser(token,function(err,data){
            if(callback){
                callback(err,data);
            }
        });
        return token;
    });
};
//----------------------------------------------------------------------------------
let register = function(token){
    if (isEmpty(token.password)){
        token.password = generatePassword(8);
    }
    passwordHash(token.password).hash(function(error, hash,salt,key) {
        token.salt = salt;
        token.hash = hash;
        token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
        token.valid = 1; // activate
        token.active = "Yes"; // activate
        token.pwdReset ="Yes" ; // need to reset password
        saveUser(token,(err,data)=>{    
            dict.savePWDHistory(token);    
            // send email
            let msg = 'you have been registered as a new user on the Patient Portal administation \n'+
                      '\nTemporary password is ' + password+' for userid '+token.userid +
                      'Please connect to the link 192.168.180.132:4300 and login in.\n'+
                      'You will need to change your password at the first login' ;
             let email_config = {
                text:    msg ,
                from:    "pp_admin@nyecemail.com", 
                to:      `${token.email}`,
                cc:      "pkulek0@gmail.com",
                subject: "PP admin registration"
            }
       	    //console.log("EMAIL to "+token.email+"\n MSG="+msg);
            dict.set("EMAIL","PP_ADMINREGISTRATION","PP_ADMIN","en-US",email_config,(err,data)=>{

            });                          
            email.send({
                text:    msg ,
                from:    "pp_admin@nyecemail.com", 
                to:      "token.email",
                cc:      "pkulek0@gmail.com",
                subject: "PP admin registration"
            }, function(err, message)  { 
                console.log("err="+err);
                console.log("message="+JSON.stringify(message));
            });
            // add to HS
            /*
            var url ="http://"+req.headers.host+"/RegisterHSUser/"+token.userid+"/"+token.password+"/INSERT";
            //var dat = {"pwd":token.password,"userid":token.userid };
            var dat = {"pwd":'HS_Services',"userid":'HS_Services'};
            var args = {
                data: dat,
                headers: { "Content-Type": "application/json" }
            }; 
            var cReq = client.post(url, args, function (data, response) {
                res.send("OK"+token.tokenid);    
            });
            */      
           //res.send("OK"+token.tokenid);
        });
    }); 
    res.send("OK"+token.tokenid);
}
//-----------------------------------------------------------------------------
exports.Roles = function(req,res){ 
    var tokenid = req.query.tokenid;
    // lookup user
    getToken(tokenid,function(token,err){
        if (! isEmpty(err)) {
            res.send( ["NON"] );
        } else {
            token = JSON.parse(token);
            res.send( token.roles ); 
        }    
    });
}
//-----------------------------------------------------------------------------
exports.VerifyToken = function (req,res){ 
    var tokenid = req.query.tokenid;
    // lookup user
    getToken(tokenid,function(token,err){
        if (! isEmpty(err)) {
            res.send( false);
        } else {
            token = JSON.parse(token);
            res.send( token.tokenid == tokenid ); 
        }    
    });
}
//---------------------------------------------------
exports.authenticate = function(req,res){
    var tokenid = req.params.tokenid;
    var roles = req.params.roles;
    authorised(tokenid,roles,function(result ) {
        if(DEBUG){
            console.log("authorised token=%s roles=%s res=%s  ",tokenid,roles,result);
        }
        res.send(result);
    });
};
//------------------------------------------------------------------------------
// get registered user details and save to dict /Redis
// also add this to Healthshare in ens_config.credentials
exports.register = function(req,res){
    var data = req.body;
    var token = newToken();
    var password = generatePassword(8);
    console.log("register type = "+(typeof data))
    if (typeof data == 'string') {
        data = JSON.parse(data);
    }
    console.log("Registrer user = "+JSON.stringify(data));
    token.userid = data.userid;
    token.fname = data.fname;
    token.userid = data.userid;
    token.fname = data.lname;
    token.lname = data.fname;
    token.password = data.password;
    token.phone = data.phone;
    token.email = data.email;
    token.group = data.group;
    //token.password = generatePassword(8);
    // save token and user user
    passwordHash(password).hash(function(error, hash,salt,key) {
        token.salt = salt;
        token.hash = hash;
        token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
        token.valid = 1; // activate
        token.active = "Yes"; // activate
        token.pwdReset ="Yes" ; // need to reset password
        token.password = "*********";
        token.roles = PP_OPERATIONS ;
        if (token.group == "SUPERADMIN"){
            token.roles = PP_SADMIN ;        
        } else if (token.group="PP_OPERATIONS"){
            token.roles = PP_OPERATIONS ;        
        }            
    //console.log("login.register="+token);
        saveUser(token,(err,data)=>{    
                console.log("logindict.saveuser=="+data);    
            dict.savePWDHistory(token);    
            // send email
            let msg = 'you have been registered as a new user on the Patient Portal administation \n'+
                      '\nTemporary password is ' + password+' for userid '+token.userid +
                      'Please connect to the link 192.168.180.132:4300 and login in.\n'+
                      'You will need to change your password at the first login' ;
             let email_config = {
                text:    msg ,
                from:    "pp_admin@nyecemail.com", 
                to:      `${token.email}`,
                cc:      "pkulek0@gmail.com",
                subject: "PP admin registration"
            }
       	    //console.log("EMAIL to "+token.email+"\n MSG="+msg);
            dict.set("EMAIL","PP_ADMINREGISTRATION","PP_ADMIN","en-US",email_config,(err,data)=>{

            });                          
            email.send({
                text:    msg ,
                from:    "pp_admin@nyecemail.com", 
                to:      "token.email",
                cc:      "pkulek0@gmail.com",
                subject: "PP admin registration"
            }, function(err, message)  { 
                console.log("err="+err);
                console.log("message="+JSON.stringify(message));
            });
            // add to HS
            /*
            var url ="http://"+req.headers.host+"/RegisterHSUser/"+token.userid+"/"+token.password+"/INSERT";
            //var dat = {"pwd":token.password,"userid":token.userid };
            var dat = {"pwd":'HS_Services',"userid":'HS_Services'};
            var args = {
                data: dat,
                headers: { "Content-Type": "application/json" }
            }; 
            var cReq = client.post(url, args, function (data, response) {
                res.send("OK"+token.tokenid);    
            });
            */      
           //res.send("OK"+token.tokenid);
        });
    }); 
    res.send("OK"+token.tokenid);
}
//------------------------------------------------------------------------------		
exports.resetpwd = (req,res) => {  // change password in the Admin tool for admin users
    var userid = req.query.userid;
    var newpassword = req.query.newpassword ;     
    if ( isEmpty( userid )){ userid='' ; }    
    if ( isEmpty( newpassword )){ 
        newpassword = generatePassword(8);
    }
    getUser(userid, (err,data) => {
        if  ( ! isEmpty( err ) ) {
            res.send(err);
        } else {
            var token = JSON.parse(data);
            passwordHash(newpassword).hash( (error, hash,salt,key) => {
                if (error) {
                    res.send(error);
                } else {
                    token.salt = salt;
                    token.hash = hash;
                    token.password = newpassword;
                    token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                    token.pwdReset ="No";
                    token.count =1;
                    saveUser(token,(err,data) => {
                        dict.savePWDHistory(token, (err,result) =>{});                         
                        res.send("OK"+token.tokenid);    
                         // update HS 
                         /*
                        var url ="http://"+req.headers.host+"/RegisterHSUser/"+token.userid+"/"+token.password+"/UPDATE";
                        var dat = {"pwd":token.password,"userid":token.userid };
                        var args = {
                            data: dat,
                            headers: { "Content-Type": "application/json" }
                        }; 
                        var cReq = client.post(url, args, function (data, response) {
                            res.send("OK"+token.tokenid);    
                        });
                        */       
                    });
                }    
            });                
        }       
    });
}
//-----------------------------------------------------------------------------------
let genToken = (len) => {
  len = len || 20 ;
  if (crypto.randomBytes) {
    return crypto.randomBytes(Math.ceil(len / 2)).toString('hex').substring(0, len);
  } else {
    for (var i = 0, salt = ''; i < len; i++) {
      salt += saltChars.charAt(Math.floor(Math.random() * saltCharsCount));
    }
    return salt;
  }
}
//-----------------------------------------------------------------------------------
exports.forgotPasswordEmail = function(req,res){
    //var id = req.query.id;
    //var lang = req.query.langid;
    var email = req.query.email;
    var provider = req.query.provider;
    var dat = {"email":email,"provider":provider };
    //console.log("Enter forgotPasswordEmail"+dat);
    var args = {
    	data: dat,
	    headers: { "Content-Type": "application/json" }
    }; 
    client.put("http://192.168.160.131:9000/api/forgot-password-email", args, function (data, response) {
//        console.log(response);
  	     res.send(data.toString());
    });
}
//------------------------------------------------------------------------------		
exports.changepwd = function(req,res){  // change password on the PP mysql
    var userid = req.query.userid;
    var email = req.query.email;
    var provider = req.query.provider;
    var hash = req.query.hash;
    var newpwd = req.query.newpwd;
    var currentpwd = req.query.currentpwd;
    var dat = {"userid":userid, "email":email, "hash":hash, "provider":provider, "new-pwd":newpwd, "confirm-new-pwd":newpwd,"current-pwd":currentpwd };
    var args = {
    	data: dat,
	    headers: { "Content-Type": "application/json" }
    }; 
    client.put("http://localhost:9000/api/pwd/reset", args, function (data, response) {
  	     res.send(data.toString());
    });
}
//------------------------------------------------------------------------------		
exports.securityQuestions = function(req, res){
    var id = req.query.id;
    var lang = req.query.langid;
    if (isEmpty(lang) ) {
        lang = "en-US";
    }
    req.getConnection(function(err,connection){
         var qry = "select id,bank,language_id,question from portal.security_questions where language_id =LOWER('"+lang+"')" ;
         var query = connection.query(qry,function(err,rows)  {                
                if(err) {
                    res.send('Error in retrieving data');
                } else {
                    //console.log("SEND user List " + JSON.stringify(rows));
                    res.send(rows);
                };                   
         });
    });                 
}
//------------------------------------------------------------------------------		
exports.securityAnswers = function(req, res){
     var id = req.query.id;
     var lang = req.query.langid;
     console.log(id);
     req.getConnection(function(err,connection){
         var qry = "select user_id,CAST(AES_DECRYPT(answer,'"+crypt+"') as CHAR(60)) answer from portal.users_security_questions where user_id ='"+id+"'" ;
         console.log(qry);
         var query = connection.query(qry,function(err,rows)  {                
                if(err) {
                    console.log("Error Selecting : %s ",err );
                    res.send('Error in retrieving data');
                } else {
                    //console.log("SEND user List " + JSON.stringify(rows));
                    res.send(rows);
                };                   
         });
    });                 
}

//------------------------------------------------------------------------------		
exports.index = function(req, res){
      res.render('index', { title: 'Hello World' });
};                 
//------------------------------------------------------------------------------		     
exports.captchapng = function(req, res){         
    var capt = captchaImg();                         
    var validcode = new Buffer(capt.img).toString('base64');
    var imgbase64 = new Buffer(capt.img,'base64');
    res.send(imgbase64);
};
//------------------------------------------------------------------------------		     
exports.captcha = function(req, res){         
    var capt = captchaImg();                         
    var validcode = new Buffer(capt.img).toString('base64');       
    //res.render('login', { page_title: 'Captcha',image:capt.img, num:capt.num});
    res.send({ num:capt.num, image:capt.img});
};
//------------------------------------------------------------------		                
let captchaImg = function() {                   
      let num = parseInt(Math.random()*9000+1000);                                   
      let p = new captchapng(70,25,num); // width,height,numeric captcha
      p.color(115, 95, 197, 100);  // First color: background (red, green, blue, alpha)
      p.color(30, 104, 21, 255); // Second color: paint (red, green, blue, alpha)
      var img = p.getBase64();
      return {img:img,num:num};
} ;
//-----------------------------------------------------------------------------
let removeExpired = function(callback){
    //redisRemoveExpired();
    dict.removeExpiredTOKN() ;     
};
/*
//------------------------------------------------------------------------------
let redisRemoveExpired = ()=>{      
    var alist = [];                     
    redisClient.DB1.keys('PPTOKEN*', function (err, keys) {
        if (err) return console.log(err);
        var now = moment().format(tokenExpireFormat);    
        for(var i = 0, len = keys.length; i < len; i++) {
                alist.push(keys[i]);
        }
        //console.log(alist);
        alist.forEach(function(value){
             redisClient.DB1.get(value, function (err, reply) {
                    var expire = JSON.parse(reply).expire;
                    if ( now > expire  ){                    
                       console.log('deleting expired:'+now+" expire="+expire);
                       redisClient.DB1.del(value); 
                    };
             });
        });
    });   
};
*/
//------------------------------------------------------------------------------		                
let saveUser= exports.saveUser = (token,callback)=> {
    dict.saveUSER(token,(err,data)=>{
        if ( err) {
             if(callback){
                callback(err,data);
            }
        } else {
            if(callback){
                callback(err,data);
            }
        }
    });
}
//------------------------------------------------------------------------------		                
let getUser = exports.getUser = function(userid,callback) {
    dict.getUSER(userid,function(err,data){
        if (callback) {
           callback(err,data);
        } 
    });
}
//------------------------------------------------------------------------------		
exports.setpwd = function(req,res){
     //params = {tokenid:'84676847edu',userid:'admin',password:'admin'};
    var token = newToken();
    var tokenid = req.param('tokenid');
    var userid = req.param('userid');
    var password = req.param('password');
    var newpassword = req.param('newpassword') ;

    if ( isEmpty( tokenid )){ tokenid='' ; }    
    if ( isEmpty( userid )){ userid='' ; }    
    if ( isEmpty( password )){ password='' ; }       
    if ( isEmpty( newpassword )){ newpassword='' ;  }         
    //if (!password || !userid || !newpassword) { res.send("incorrect  parameters in change password"); return }; 
    var salt = passwordHash.genSalt();
    var hash = passwordHash.genHash(newpassword,salt);
            var now = moment().format(tokenExpireFormat);  
    var qry = 'select user_id,password,saltnpepa from portal.users where user_id = ?';
    req.getConnection(function(err,connection){       
        var query = connection.query(qry,[userid],function(err,rows)  {
            if(err) {
                console.log("Error Selecting : %s ",err );
            } else {     
                token = getToken(tokenid);   
                token.userid = userid;
                token.password = hash;  
                token.expire = moment().add(tokenExpire, 'minutes').format(tokenExpireFormat);
                token.pwdExpire = moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ;
                var pwdexpire = moment().add(90, 'days').format("YYYY/MM/DD");

console.log('salt   ='+rows[0].saltnpepa);
console.log('hash   ='+rows[0].password);
console.log('pwd    ='+password)
console.log('Newhash='+passwordHash.genHash(password,rows[0].saltnpepa));
res.send('test');


                if (rows.length > 0 ){ //} then update 
                    // check passwords in the used passwords list
                    // portal.previous_admin_passwords user_id, password,saltnpepa,changed
                    qry = "select  user_id, password,saltnpepa,changed from portal.previous_admin_passwords where user_id = '"+userid+"'";
                    connection.query(qry, function(err, rows)  {
                        if (err)  console.log("Error Updating : %s ",err );
                        if ( rows.length > 0 ) {
                            // go through the list and see if duplicate password
                            rows.forEach(function(value){
                                var duphash = passwordHash.genHash(newpassword,value.saltnpepa);
                                if (value.password == duphash ){
                                    //console.log('Password has already been used');
                                }
                            });
                            //----------- add user to history passwords
                            qry = "INSERT into  portal.previous_admin_passwords set password = '"+hash+"'"+
                                            ", saltnpepa = '"+salt+"'"+
                                            ", changed='" +now+"'"+
                                            " ,user_id = '"+userid+"'";   
                            connection.query(qry, function(err, rows)  {
                                if (err)  console.log("Error Updating : %s ",err );
                                // add users details
                                //--------------------------------------------------------     
                                qry = "UPDATE portal.users set password = '"+hash+"'"+
                                            ", saltnpepa = '"+salt+"'"+
                                            ", passwd_expire_dt='" +pwdexpire+"'"+
                                            " where user_id = '"+userid+"'";   
                                connection.query(qry, function(err, rows)  {
                                    if (err)  console.log("Error Updating : %s ",err );
                                    saveToken(token);
                                    res.send(token.tokenid);
                                });   
                            });           
                        }
                    });
                } else {  // do an insert
                    res.send("user not found");
                }         

            }
        });
    });  
};
//------------------------------------------------------------------------------
exports.loginpost = function (req,res){ //POST returns tokenid
    console.log("POST DATA = "+req.body);

}

//------------------------------------------------------------------------------		
/**
 * 1 verify userid and password in table portal.users
 * 2 if OK create a token with data
 * 3 store data in redis/dict as token ID
 * 4 return token ID 
 */
exports.login = function (req,res){ //GET returns tokenid
        //params = {tokenid:'asfdasdasdffdsadfsa',userid:'admin',password:'admin',langid:'en-US'};
        var tokenid = req.query.tokenid;
        var userid = req.query.userid;
        var password = req.query.password;
        var langid = req.query.langid ;  
        var token =  getUser(userid); // get from dict
        if ( isEmpty( userid )){
            userid='' ;
        }    
        if ( isEmpty( password )){
            password='' ;
        }       
        if ( isEmpty( langid )){
            langid='en-US' ;
        }
        // check if token is set
        if (token.valid) {

        };    
        var now = moment().format(tokenExpireFormat);  
        token.lastAccess = now;
        token.userid = userid;
        token.tokenid = tokenid;

        req.getConnection(function(err,connection){
            var qry =   "select  (select language from portal.users_preferences where user_id = '"+userid+"' limit 1) langid " +  
                        ", (select role_name from portal.users_roles where user_id = '"+userid+"' limit 1) roles "+
                        ", user_id\
                        ,password\
                        ,saltnpepa\
                        ,is_admin\
                        ,active\
                        ,passwd_expire_dt from portal.users where user_id = ?";
            var query = connection.query(qry,[userid],function(err,rows)  {
                if(err)   console.log("Error Selecting : %s ",err );
                    // userid and password exists
                    if ( rows.length > 0 ) {
                        // check if expired 
                        var expire = moment(rows[0].passwd_expire_dt);
                        if ( moment(expire).isBefore(now) ){
                                // expired
                              token.msg = 'Password Expired';
                        };
                        // check if in redis or dict
                        if (isEmpty(token.tokenid)){
                            token.tokenid = passwordHash.genSalt(16).toUpperCase();
                            token.valid = 0;
                            token.langid = rows[0].langid ;
                            token.roles= rows[0].roles;
                        }else{
                         //   token = redisClient.DB1.get('PPTOKEN_'+token.tokenid);
                        }
                        // check passwords match
                        //--------------------------------------------------------                  
                        if (passwordHash.verifyhash(password, rows[0].password, rows[0].saltnpepa) ){
                            token.valid = 1;
                            token.msg = "Authentication OK";
                            console.log('verified:'+userid);
                        } else {
                            token.valid = 0;
                            token.msg = "Authentication Failed";
                            console.log('Verification failed:'+userid);
                        }
                        //----------------------------------------------------------
                        if (token.valid ) {
                                // send to Redis or dict
                                console.log('Validated:'+userid);
                                saveToken(token);
                        } else {
                            token.tokenid ='';
                        }

                    } else {                        
                        dict.log("ERROR","login");
                        //res.send("Error");    
                    }
                
                res.send(token.tokenid);
            });
        });               
};

//-----------------------------------------------------------------------------
exports.loginVerifyUser = function (req,res){ // new login as POST
    var now = moment().format(tokenExpireFormat);    
    var tokenid = req.query.token;
    var userid = req.query.userid;
    var password = req.query.password;
    var newpassword = req.query.newpassword;
    if ( isEmpty( tokenid )){
        tokenid='' ;
    }    
    if ( isEmpty( userid )){
        userid='' ;
    }    
    if ( isEmpty( password )){
        password='' ;
    }       
    if ( isEmpty( newpassword )){
        newpassword='' ;
    }       
    var token = "";
    //logger.LOG(userid,"AUDIT",tokenid,'LOGIN REQ','','','');
    //console.log("Login userid =%s pwd =%s",userid,password)
    getUser(userid,function(err,data){
            if  ( ! isEmpty( err ) ) {
                logger.LOG(userid,"AUDIT",tokenid,'LOGIN ERROR','',userid+': not found','');
                res.send("Error");
                return 0;
            } else {
                token = JSON.parse(data);        
                // check id user needs to change password
                //console.log("loginverifyuser token = %j",token);
                //console.log("Avtive = %s",token.active);
                if ( token.active == "No" ){
                    logger.LOG(userid,"AUDIT",tokenid,'LOGIN INACTIVE USER','',userid+'Needs to be activated','');
                    res.send("INACTIVE");
                    return 0;
                }                
                if (token.pwdReset == "Yes"  ){
                    logger.LOG(userid,"AUDIT",tokenid,'LOGIN PWD RESET','',userid+'Password Reset needed','');
                    token.pwdReset = "No";
                    saveUser(token);
                    res.send("RESET PASSWORD");
                    return 0;
                }
                if (now > token.pwdExpire ){
                    logger.LOG(userid,"AUDIT",tokenid,'LOGIN PWD EXPIRED','',userid+'Password Reset needed','');
                    token.pwdExpire =moment().add(tokenExpire, 'minutes').format(tokenExpireFormat);
                    saveUser(token);
                    res.send("EXPIRED PASSWORD");
                    return 0;
                }
                
                // Creating hash and salt
                tokexpire = moment().add(tokenExpire, 'minutes').format(tokenExpireFormat);
                token.valid = 0;
                token.expire = tokexpire ;
                token.tokenid=genToken() ;
                token.lastAccess=now;
                //console.log("exports.loginVerifyUser pwd ="+password);
                passwordHash(password).verifyAgainst(token.hash, function(error, verified) {                   
                    if(error) {
                        token.msg = 'Error in verification, Something went wrong!';
                    }
                    if( ! verified) {
                        logger.LOG(userid,"AUDIT",token.tokenid,'LOGIN ERROR','',userid+' : not verified','');
                        token.msg = "Not Verified";
                    } else {
                        token.valid = 1;
                        saveToken(token);                    
                        token.msg = "Verified OK";
                        logger.LOG(userid,"AUDIT",token.tokenid,'LOGIN OK','',userid+': verified',token);
                    }       
                    res.send(token);
                });     
            }       
    });
};
//-----------------------------------------------------------------------------
exports.logout = function (req,res){ // logou
    var now = moment().format(tokenExpireFormat);  
    var tokenid = req.query.token;
    var userid = req.query.userid;
    var password = req.query.password;
    var newpassword = req.query.newpassword;
    if ( isEmpty( tokenid )){
        tokenid='' ;
    }    
    if ( isEmpty( userid )){
        userid='' ;
    }    
    if ( isEmpty( password )){
        password='' ;
    }       
    if ( isEmpty( newpassword )){
        newpassword='' ;
    }       
    // lookup user
    var token = "";
    //console.log(req.query);
    //console.log(tokenid);
    logger.LOG(userid,"AUDIT",tokenid,'LOGOUT REQ','','','');
    getUser(userid,function(err,data){
            if  ( ! isEmpty( err ) ) {
                logger.LOG(userid,"AUDIT",tokenid,'LOGOUT ERROR','',userid+': not found','');
                res.send("Error");
            } else {
                token = JSON.parse(data);            
                // Creating hash and salt
                tokexpire = moment().add(tokenExpire, 'minutes').format(tokenExpireFormat);
                token.valid = 0;
                token.expire = tokexpire ;
                token.lastAccess=now;
                token.tokenid='' ;
                res.send('OK');
            }       
    });
};
//------------------------------------------------------------------------------		
exports.htmllogin = function(req,res){
    var tokenid = req.query.tokenid;
    // params = {token:'asfdasdasdffdsadfsa',userid:'admin',password:'admin',langid:'en-US'};
	// params = setParam(req);	
    // console.log("HTML params="+JSON.stringify(params));
    //res.render('login',{page_title:"PP Login ",data:token,token:params.token});                  
    //res.render('login',{page_title:"PP Login ",data:params,token:params.token,langid:params.langid});                  
    res.render('login',{page_title:"PP Admin Login "});           
    //res.render('login', { data:token,page_title: 'PP Admin Login' });          
};

//------------------------------------------------------------------------------		
let set_Param = (req)=>{
    
    var token = req.query.token;
    var tokenid = req.query.tokenid;
    var userid = req.query.userid;
    var password = req.query.password;
    var langid = req.query.langid;
    var theme = req.query.theme;
    
    /*
    var token = req.query('token');
    var userid = req.query('userid');
    var password = req.query('password');
    var langid = req.query('langid')
    */

    if (isEmpty( tokenid )){       
        tokenid = '' ;
    }
    if (isEmpty( token )){       
        token = newToken() ;
    }
    if ( isEmpty( userid )){
        userid='' ;
    }    
    if ( isEmpty( password )){
        password='' ;
    }       
    if ( isEmpty( langid )){
        langid='en-US' ;
    }    
    if ( isEmpty( theme )){
        theme='redmond' ;
    }    
    return {token:token,tokenid:tokenid,userid:userid,password:password,langid:langid,theme:theme};// emulate multiple return values
};
//------------------------------------------------------------------------------	
exports.setParams = function(req,res){
    //console.log('set params req='+req);
        return set_Param(req);
};

//------------------------------------------------------------------------------		
exports.authUser = function(req,res){
	return 1;	
};
//------------------------------------------------------------------------------		                
let newToken = exports.newToken = (userid,password)=>{
    let _token = { 
            tokenid: genToken(),
            userid: userid,
            password: password,
            pwdExpire:moment().add(pwdExpire, 'minutes').format(tokenExpireFormat) ,
            pwdReset:"Yes",
            fname:'',
            lname:'',
            email:'',
            phone:'',
            hash:'',
            salt:'',
            link: '/login/html',
            callurl: '/login/html',
            count: 0,
            lastAccess: '',
            msg: '',
            theme: 'redmond',
            langid:'en-US',
            valid: 0,
            active: "No",
            baseurl:'',
            group:"PP_OPERATIONS",
            roles:['PP_VIEWDOCUMENTLIST','PP_USERSVIEW','PP_ONDEMANDVIEW','PP_REPOSITORYVIEW','PP_USEREDIT','PP_USERDELETE','PP_USERREGISTER'],
            expire:moment().add(tokenExpire, 'minutes').format(tokenExpireFormat)
    };
    return _token;
};

//------------------------------------------------------------------------------
		
