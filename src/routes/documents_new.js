
/*
 * documents for patient.
 */

const JSONC = require("circular-json");
var logger = require('./logging');
var dict = require('../dic/dict');
var login =require('./login');
var users =require('./users');
var Client = require('node-rest-client').Client;
// configure basic http auth for every request
let config = require('../utils').getConfig().conf ;

//var options_auth = { user: "HS_Services", password: "HS_Services" };
//var options_auth = { user: "HS_Services", password: "$Nyec123" };
var devenv = "PROD";
//var devenv = require('../../app').config.devenv;
var PROD_options_auth = { user: "HS_Services", password: "HS_Services" };
var STAGE_options_auth = { user: "HS_Services", password: "$Nyec123" };
var options_auth = eval(devenv+"_options_auth");
var client = new Client(options_auth);

//---------------------------------------------------
isEmpty = function(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//---------------------------------------------------
exports.userDocList = function(user_id,type,callback){   // get document list by pp user_id
    if ( isEmpty( type )) {
        type = "Approved"
    }
    //var PROD_url = "http://192.168.180.174:57772/csp/healthshare/pphub/api/NYeC.PP.Api/Patient/DocumentList" ;
    //var STAGE_url = "http://192.168.160.174:57772/csp/healthshare/hsregistry/api/NYeC.PP.Api/Patient/DocumentList" ;
    // get mrn from pp_userid
    users.getVerizonID(user_id,(mrn,err)=>{
        //var url = eval(devenv+"_url") + "/"+mrn+"/"+type;
        let url =  config.HSHUB_APIurl + "/Patient/DocumentList" + "/"+mrn+"/"+type;
        //console.dir("userDocList="+url);
        var cReq = client.get(url, function (data, response) {
  	            callback(data);
        });    
    });   
};
//-------------------------------------------------------------------------------
exports.HSlist = function(req, res){ 
    var id = req.params.id;
    var type = req.params.type;
    var tokenid = req.params.tokenid;
    // jqgrid params
    var page = req.params.page;
    var rows = req.params.limit;
    var sidx = req.params.sidx;
    var sord = req.params.sord;
    //
    var params = "";
    if ( isEmpty( id )) {
        id = ""
    }
    if ( isEmpty( type )) {
        type = ""
    }
    if (id != "") {
        params += "/"+id
        if ( type != "") {
            params += "/"+type
        }

    }
    //console.log("HSList Params ="+params+"-"+page+"-"+rows+"-"+sidx+"-"+sord);
    var PROD_url = "http://192.168.180.174:57772/csp/healthshare/pphub/api/NYeC.PP.Api/Patient/DocumentList" ;
    var STAGE_url = "http://192.168.160.174:57772/csp/healthshare/hsregistry/api/NYeC.PP.Api/Patient/DocumentList" ;
    var url = eval(devenv+"_url") + params;
    //let url =  config.HSHUB_APIurl + "/Patient/DocumentList" + params

    console.dir(url);
    var cReq = client.get(url, function (data, response) {
        //logger.LOG(tokenid ,"TRACE","documents.HSList","TIMMINGS","TRACE END");
        //console.log(data);
  	    res.send(data);
    });    
    cReq.on('requestTimeout', function (req) {
        console.log("HS Documents - request has expired");
        req.abort();
    });
    cReq.on('responseTimeout', function (res) {
         console.log("HS Documents - request has expired");
    });

};
//---------------------------------------------------
exports.HShtmllist = function(req, res){ // display document list in jqgrid
    // get token ID for req
    var tokenid = req.params.tokenid;
    var id = req.params.id;    
    login.Token(tokenid,function(data,err) {     
        if (isEmpty(err) ){
            var token = JSON.parse(data);
            token.password = "**********";
            logger.LOG(token.userid,"AUDIT",tokenid,'LIST DOCUMENTS REQ','',token,res);    
            login.authorised(tokenid,['PP_VIEWDOCUMENTLIST'],function(result,roles){
                if (result) {      
                    HSdocument_jqgrid(function(colSetup){
                        logger.LOG(token.userid,"AUDIT",tokenid,'LIST DOCUMENTS RES','','Retrieved OK',colSetup);    
                        res.render('HSdocuments',{page_title:"HS Patient Documents ",colsetup:colSetup,tokenid:tokenid,id:id});    
                    });
                } else {
                    res.send(false);    
                }
            });
        } else {
            res.send(false);
        }
    });         
};
//---------------------------------------------------
exports.HSDoc = function(req, res){ // a redirect to HS
    var tokenid = req.params.tokenid;
    var id = req.params.id;
    var type = req.params.type;
    var type1 = req.query.type;
    login.Token(tokenid,function(data,err)  {
        if (isEmpty(err) ){
            var token = JSON.parse(data);
            token.password = "**********";
            logger.LOG(token.userid,"AUDIT",tokenid,'ONDEMAND VIEW','',token,res);    
            login.authorised(tokenid,['PP_ONDEMANDVIEW'],function(data,roles){
                if ( isEmpty( type )) {
                    type = "HTML"
                }
                //var url = "http://192.168.180.173:57772/csp/healthshare/hsrepository/api/NYeC.PP.Api/Patient/Document/555/2.25.229466344526257865132203513150355894016"
                /*
                var STAGE_url = "http://192.168.160.173:57772/csp/healthshare/hsrepository/api/NYeC.PP.Api/Patient/Document/"+id+"/"+type
                var PROD_url = "http://192.168.180.173:57772/csp/healthshare/hsrepository/api/NYeC.PP.Api/Patient/Document/"+id+"/"+type
                var url = eval(devenv+"_url")
                */
                let url =  config.HSHUB_APIurl + "/Patient/Document/" +id+"/"+type
                var cReq = client.get(url, function (data, response) {
                    //res.render('HSdoc',{page_title:"HS document ",data:data,tokenid:tokenid,id:id });
                    res.setHeader('content-type', 'text/'+type.toLowerCase());
                    res.send(data);
                });    
                cReq.on('requestTimeout', function (req) {
                    console.log("HS Documents - request has expired");        
                    req.abort();
                });
                cReq.on('responseTimeout', function (res) {
                    console.log("HS Documents - request has expired");
                });
             });
        } else {
            res.send(false);
        }
   });
};
//---------------------------------------------------
exports.RegisterSPRLDoc = function(req, res){ // a redirect to HS
    var tokenid = req.params.tokenid;
    var id = req.params.id;
    var type = req.params.type;  
    
    //console.log("token="+tokenid) ;
    //console.log("ID="+id) ;
    /*
    //var url = "http://192.168.180.173:57772/csp/healthshare/hsrepository/api/NYeC.PP.Api/Patient/Document/555/2.25.229466344526257865132203513150355894016"
    var PROD_url = "http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.Api/Patient/SPRLDocument/"+id
    var STAGE_url = "http://192.168.160.173:57772/csp/healthshare/ppedge/api/NYeC.PP.Api/Patient/SPRLDocument/"+id
    var url = eval(devenv+"_url")
    */
    let url =  config.HSECR_APIurl + "/Patient/SPRLDocument/" +id
    //console.log(url);
    var cReq = client.get(url, function (data, response) {
        res.setHeader('content-type', 'text/html');
        //res.status(302).send(data)  ;//redirect
        res.send(data);
    });    
    cReq.on('requestTimeout', function (req) {
        console.log("HS Documents - request has expired");
        
        req.abort();
    });
    cReq.on('responseTimeout', function (res) {
         console.log("HS Documents - request has expired");
    });
};
//---------------------------------------------------
var SPRLDocRegister = exports.SPRLDocRegister = function(user_id,callback){ // a redirect to HS
    users.getVerizonID(user_id,(verizonID)=>{
        console.log("SPRLDocRegister verizonid = %s",verizonID);
        /*
        var PROD_url = "http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.Api/Patient/SPRLDocument/"+verizonID
        var STAGE_url = "http://192.168.160.173:57772/csp/healthshare/ppedge/api/NYeC.PP.Api/Patient/SPRLDocument/"+verizonID
        var url = eval(devenv+"_url")
        */
        let url =  config.HSECR_APIurl + "/Patient/SPRLDocument/" +verizonID
        console.log("SPRLDocRegister url ="+url);
        var cReq = client.get(url, function (data, response) {
            if(callback) {
                callback(data,'');
            }
        });    
        cReq.on('requestTimeout', function (req) {
            console.log("HS Documents - request has expired");
            if(callback) {
                callback('','Error');
            }
        });
        cReq.on('responseTimeout', function (res) {
            console.log("HS Documents - request has expired");
            if(callback) {
                callback('','Error');
            }
        });
    });
};

//---------------------------------------------------
exports.SPRLDoc = function(req, res){ // a redirect to HS
    var tokenid = req.params.tokenid;
    var id = req.params.id;
    var type = req.params.type;
    //login.authorised(tokenid,['PP_SPRLVIEW'],function(data,roles,token){
    login.Token(tokenid,function(token){
        token = JSON.parse(token);
        if ( isEmpty( type )) {
            type = "HTML"
        };
        /*
        //var url = "http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.Api/Patient/SPRLDoc/"+id
        var PROD_url = "http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.Api/Patient/SPRLDoc/"+id
        var STAGE_url = "http://192.168.160.173:57772/csp/healthshare/ppedge/api/NYeC.PP.Api/Patient/SPRLDoc/"+id
        var url = eval(devenv+"_url")
        */
       let url =  config.HSECR_APIurl + "/Patient/SPRLDoc/" +id
 
        logger.LOG(token.userid,"AUDIT",token.tokenid,'GETSPRL START',id,url,"");
        var options_auth = { user: token.userid, password:token.password };
        var cl = new Client(options_auth);
        var cReq = client.get(url, function (data, response) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'GETSPRL RESULT',id,data,response);
            res.setHeader('content-type', 'text/html');
            res.send(data);
        });    
        cReq.on('requestTimeout', function (req) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'GETSPRL REQ TIMEOUT',id,url,"");
            console.log("SPRL Documents - request has expired");        
            req.abort();
        });
        cReq.on('responseTimeout', function (res) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'GETSPRL RES TIMEOUT',id,url,"");
            console.log("HS Documents - request has expired");
            res.status(502).send("HS Documents - request has expired")  
        });
    });
  
};
//---------------------------------------------------
var getOndemandDoc = exports.getOndemandDoc = function(id,type,callback){ // a redirect to HS
    if ( isEmpty( type )) {
        type = "XML"
    };
    /*
    var PROD_url = "http://192.168.180.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api/Patient/ODDocument/"+id+"/"+type
    var STAGE_url = "http://192.168.160.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api/Patient/ODDocument/"+id+"/"+type
    var url = eval(devenv+"_url")
    */
    let url =  config.HSACCESS_APIurl + "/Patient/ODDocument/" +id+"/"+type
 
    var cReq = client.get(url,  (data, response) => {
        callback(data,'');
    }).on('error',  (err) => {
        callback('',err);
    });
    cReq.on('requestTimeout',  (res) => {
        callback('',res);
    });
    cReq.on('responseTimeout', (res) => {
        callback('',res);
    });
};
//---------------------------------------------------
exports.OnDemandDoc = function(req, res){ // a redirect to HS
    var tokenid = req.params.tokenid;
    var id = req.params.id;
    var type = req.params.type;  
    login.Token(tokenid,function(token){
        if ( isEmpty( type )) {
            type = "HTML"
        };
        /*
        var PROD_url = "http://192.168.180.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api/Patient/ODDocument/"+id+"/"+type
        var STAGE_url = "http://192.168.160.171:57772/csp/healthshare/hsaccess/api/NYeC.PP.Api/Patient/ODDocument/"+id+"/"+type
        var url = eval(devenv+"_url")
        */
        let url =  config.HSACCESS_APIurl + "/Patient/ODDocument/" +id+"/"+type
        logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMAND REQ',id,url,req);
        var cReq = client.get(url, function (data, response) {
            console.log(JSONC.stringify(response));
            if ( data == '' || data == ' ') {
                logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMND RESULT',id,'Retrieve Failed ','check to see if healthshare system is up and user has valid credentials ');
            } else {
                logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMND RESULT',id,'Retrieved OK ',data);
            }
            res.setHeader('content-type', 'text/'+type.toLowerCase()+'');           
            res.send(data);
        }).on('error', function (err) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMND ERROR',id, err.request.options);
            console.log('something went wrong on the request', err.request.options);
        });
        cReq.on('requestTimeout', function (req) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMND REQ TIMEOUT',id,url,"");
            console.log("HS Documents - request has expired");        
            req.abort();
        });
        cReq.on('responseTimeout', function (res) {
            logger.LOG(token.userid,"AUDIT",token.tokenid,'ONDEMND RES TIMEOUT',id,url,"");
            console.log("HS Documents - request has expired");
            res.status(502).send("HS Documents - request has expired")  
        });
    });
};
//---------------------------------------------------
exports.list = function(req, res){ // a redirect to HS
    let url =  config.HSHUB_APIurl + "/Patient/ODDocument/Document/List/99/100000102";
    client.get("http://192.168.160.173:57772/csp/healthshare/hsrepository/api/NYeC.PP.Api/Patient/Document/List/99/100000102", function (data, response) {
            res.send(data);
    });
};
//---------------------------------------------------
exports.htmllist = function(req, res){ // display document list in jqgrid
    // get token ID for req
    if (login.VerifyToken(req) ){    
        var jqgSetup =   HSdocument_jqgrid ();     
        res.render('documents',{page_title:"PP Documents",Setup:jqgSetup,tokenid:"peter"});
    } else {   
        res.render('login',{page_title:"PP Login "});
    }                          
};
//---------------------------------------------------------------------------------------
HSdocument_jqgrid = function(callback) {
let colsetup = {
        url:'/HSDocuments',
        altRows: false,
        deepempty: true,
        autowidth: true,
        autoheight:true,
        ignoreCase: true,
        datatype: "json",
        mtype: "GET",       
        height:'auto',
        width:'auto',
        rownumbers:true,
        colModel:[
            {label:'Patient ID',name:'PatientID', width:85},
            {label:'Name',name:'Name',index:'Name', width:125,hidden:false },
            {label:'MRN',name:'MRN',index:'MRN', width:255,hidden:false },
            {label:'AA',name:'AA',index:'AA', width:90,hidden:false },
            {label:'Doc Id',name:'DocumentID',index:'DocumentID', width:260,hidden:true },
            {label:'IID',name:'IID', width:125,hidden:true},
            {label:'Source ID',name:'SourcePatientID', width:95,hidden:true},
            {label:'Type',name:'MimeType',index:'MimeType',width:65 },
            {label:'Submission Date',name:'SubmissionTime', width:105},   
            {label:'Name',name:'PracticeSettingCode_DisplayName', width:120,hidden:false},   
            {label:'Size',name:'Size', width:55,hidden:false,search:false},   
            {label:'Status',name:'Status', width:70,hidden:false, sortable:true},   
            {label:'Doc ID',name:'DocumentID', width:360,hidden:false,search:true,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},   
            {label:'Format code',name:'FormatCode_IID', width:80,hidden:true},   
            {label:'Doc Class',name:'ClassCode_DisplayName',width:200,hidden:true},               
            {label:'Doc Format',name:'FormatCode_DisplayName',widt:70, hidden:true},   
            {label:'Code Scheme',name:'FormatCode_CodingScheme', width:50,hidden:true},   
            {label:'Doc uID',name:'DocumentUniqueIdentifier_IID', width:400,hidden:true},   
            {label:'Repository ID',name:'RepositoryUniqueID', width:175,hidden:true,search:true},
            {label:'',width:1,search:false}
        ],
        loadonce:true,
        toppager:true,
        rowNum: 25,
        rowTotal:50000,
        //rowList: [10, 20, 30],
        viewrecords: true,
        gridview: true,
        autoencode: true,            
        caption: "Document List",
        grouping: false,
        groupingView : {
            groupField : ['PatientID'],
            groupColumnShow : [true],
            groupText : ['<b>group</b>'],
            groupCollapse : true,
            groupOrder: ['asc'],
            groupSummary : [false],
            groupDataSorted : false
        },
        ondblClickRow:function(rowid,iRow,iCol,e){
            log(e.type);
        return false;
        },
        onSelectRow:function(rowid,status,e){
            log(e.type);
            return false;
        }
    };     
    dict.set("JSON","HSDOCUMENTCOLUMNS","JQGRID","", colsetup,(err,data)=> {
        if ( isEmpty(data) ) {    
            callback( colsetup);
        } else {
            if(typeof data === 'string') {
                callback(JSON.parse(data)); 
            } else if(typeof data === 'object') {
               callback(data); 
            }
        }       
    });   
};



