
/*
 * GET CID users listing.
 */
var Client = require('node-rest-client').Client;
// configure basic http auth for every request
var options_auth = { user: "HS_Services", password: "HS_Services" };
var client = new Client(options_auth);
//----------------------------------------------------------------------------------
function isEmpty(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
}
//---------------------------------------------------
exports.list = function(req, res){
    var id = req.params.id;
    var tokenid = req.params.tokenid;
    var type = req.params.type;
    if (isEmpty( type )){       
        type="JSON";
    }      
    var args = {
        requesConfig: { timeout: 1000 },
        responseConfig: { timeout: 2000 },
        user: "HS_Services", // basic http auth username if required 
        password: "HS_Services" // basic http auth password if required 
    };
    var cReq = client.get("http://192.168.180.174:57772/csp/healthshare/pphub/api/NYeC.PP.Api/PPUsers", args, function (data, response) {
  	    res.send(data);
    });    
    cReq.on('requestTimeout', function (req) {
        console.log("PPUsers - request has expired");
        req.abort();
    });
    cReq.on('responseTimeout', function (res) {
        console.log("PPUsers - response has expired");
    });

};
//---------------------------------------------------
exports.htmllist = function(req, res){
    var tokenid = req.params.tokenid ;
    var type = req.params.type;
    if (isEmpty( type )){       
        type="JSON";
    } 
    var colSetup =   setup_PP(req,res);     
    res.render('PPusers',{page_title:"CID Portal Users ",colsetup:colSetup,tokenid:tokenid});    
};

//---------------------------------------------------
function setup_PP (req,res) {
    var tokenid = req.params.tokenid ;
    var type = req.params.type;
    var colsetup = {
        url:'/PPusers',
        loadonce:true,
        altRows: false,
        deepempty: true,
        ignoreCase: true,
        datatype: "json",
        mtype: "GET",       
        height:'auto',
        width:'auto',
        autowidth: true,
        colModel:[
            {label:'MPIID',name:'MPIID', sortable:true ,width:100, editable:false ,editoptions:{readonly:false} },
            {label:'Name_Family',name:'Name_Family', sortable:true ,width:150, editable:false ,editoptions:{readonly:false} },
            {label:'Name_Given',name:'Name_Given', sortable:true ,width:150, editable:false ,editoptions:{readonly:false} },
            {label:'AssigningAuthority',name:'AssigningAuthority',index:'user_id', sortable:true ,width:110, editable:false ,editoptions:{readonly:false},hidden:true },
            {label:'BirthDateTime',name:'BirthDateTime', sortable:true ,width:140, editable:false ,editoptions:{readonly:false} },
            {label:'BirthMonth',name:'BirthMonth', sortable:true ,width:50, editable:false ,editoptions:{readonly:false} ,hidden:true},
            {label:'BirthYear',name:'BirthYear', sortable:true ,width:50, editable:false ,editoptions:{readonly:false} ,hidden:true},
            {label:'CreatedOn',name:'CreatedOn', sortable:true ,width:130, editable:false ,editoptions:{readonly:false},hidden:true },
            {label:'Facility',name:'Facility', sortable:true ,width:110, editable:false ,editoptions:{readonly:false},hidden:true },
            {label:'FullName',name:'HomeEmail', sortable:true ,width:150, editable:false ,editoptions:{readonly:false},hidden:true },
            {label:'Gender',name:'Gender', sortable:true ,width:50, editable:false ,editoptions:{readonly:false} },
            {label:'LastEnteredOn',name:'LastEnteredOn' ,sortable:true ,width:130, editable:false ,editoptions:{readonly:false} },
            {label:'SSN',name:'SSN', sortable:true ,width:100, editable:false ,editoptions:{readonly:false} },
            {label:'' ,hidden:true} 
        ],
        toppager:true,
        rowNum: 25,
        rowTotal:5000,
        sortname: "ID",
        sortorder: "asc",
        viewrecords: true,
        gridview: true,
        autoencode: false,            
        caption: "Patient Portal HS Users" ,
       
    }      
    return colsetup ;  
};

