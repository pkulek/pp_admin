

var dict = require('../dic/dict');

var Client = require('node-rest-client').Client;
// configure basic http auth for every request
var options_auth = { user: "HS_Services", password: "HS_Services" };
var client = new Client(options_auth);
//----------------------------------------------------------------------------------
function isEmpty(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
}
//---------------------------------------------------
exports.RegisterAdminUser = function(req,res){
    var userid = req.params.userid;
    var lang = req.params.lang;
    var pwd = req.params.pwd;
    var type = req.params.type;
    if (isEmpty( type )){       
        type="INSERT";
    }       
    var defurl = "http://192.168.180.174:57772/csp/healthshare/pphub/api/NYeC.PP.Api";
    dict.getURL("PP_HUB","",defurl,function(urldata,error){ // gets the url from dictionary - if not there store the default
        var url = urldata +"/EnsUser/"+userid+"/"+pwd+"/"+type;
        
        var args = {
            data:{"userid":userid,
                  "pwd":pwd },
            headers: { "Content-Type": "application/json" }
        };
        console.log(url);
        console.log(args);
        var cReq = client.post(url, args, function (data, response) {
            console.log("Register Admin User  data:"+url);
            res.send(data);
        });    
        cReq.on('requestTimeout', function (req) {
            console.log("request has expired");
            req.abort();
        });
        cReq.on('responseTimeout', function (res) {
            console.log("request has expired");
        });   
    });
};
//---------------------------------------------------
exports.GetMsg = function(req, res){
    var type = req.params.type;
    var id = req.params.id;
    var lang = req.params.lang;
    var ext = req.params.ext;
    var msgdef = req.params.msgdef;
    if (isEmpty( type )){       
        type="MSG";
    }      
    if (isEmpty( ext )){       
        ext="TEXT";
    }      
    if (isEmpty( msgdef )){       
        msgdef="undefined message";
    }      
    var cReq = client.get("http://192.168.180.174:57772/csp/healthshare/pphub/api/NYeC.PP.Api/Msg/"+id+"/"+lang+"/"+msgdef, function (data, response) {
        if (isEmpty(data)) {
            data = msgdef;
        }
  	    res.send(data);
    });    
    cReq.on('requestTimeout', function (req) {
        console.log("GetMsg - request has expired");
        req.abort();
    });
    cReq.on('responseTimeout', function (res) {
        console.log("GetMsg - response has expired");
    });

};

//---------------------------------------------------
exports.GetWebMsg = function(req, res){
    var type = req.params.type;
    var ext = req.params.ext;
    var id = req.params.id;
    var lang = req.params.lang;
    var msgdef = req.params.msgdef;
    if (isEmpty( type )){       
        type="MSG";
    }      
    if (isEmpty( ext )){       
        ext="TEXT";
    }      
    if (isEmpty( msgdef )){       
        msgdef="undefined message";
    }      
    var cReq = client.get("http://192.168.180.174:57772/csp/healthshare/pphub/api/NYeC.PP.Api/MsgWeb/"+id+"/"+lang+"/"+msgdef, function (data, response) {
        if (isEmpty(data)) {
            data = msgdef;
        }
  	    res.send(data);
    });    
    cReq.on('requestTimeout', function (req) {
        console.log("GetMsg - request has expired");
        req.abort();
    });
    cReq.on('responseTimeout', function (res) {
        console.log("GetMsg - response has expired");
    });

};



