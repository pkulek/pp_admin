
/*
 * GET home page.
 */
let login = require('../routes/login');
exports.index = function(req, res){
  let token = login.setParams(req);      
  let config =  require('../utils').getConfig().conf;  
  res.render('main', { data:token,page_title: 'PP Admin ',theme:token.theme,devEnv:config.devEnv,config:config});    
};
