
/*
 * GET skins listing.
 */
var connection  = require('express-myconnection'); 
//---------------------------------------------------
exports.list = function(req, res){
     console.log("Report List " + JSON.stringify(req.body));
     req.getConnection(function(err,connection){
         var qry = "select \
                    id,\
                    client_id, \
                    portal_name ,\
                    color_primary ,\
                    color_secondary ,\
                    disclaimer_text , \
                    contact_link ,\
                    terms_conditions_text, \
                    incorrect_data_text ,\
                    delay_threshold ,\
                    logo_type ,\
                    logo_link ,\
                    medallies_terms_condition \
                    from portal.skins";
         var query = connection.query(qry,function(err,rows)  {                
                if(err) {
                    console.log("Error Selecting : %s ",err );
                    res.send('Error in retrieving data');
                } else {
                    //console.log("SEND user List " + JSON.stringify(rows));
                    res.send(rows);
                };                   
         });
    });
};
//---------------------------------------------------
exports.htmllist = function(req, res){
    req.getConnection(function(err,connection){
            var qry = "select \
                    id,\
                    client_id, \
                    portal_name ,\
                    color_primary ,\
                    color_secondary ,\
                    disclaimer_text , \
                    contact_link ,\
                    terms_conditions_text, \
                    incorrect_data_text ,\
                    delay_threshold ,\
                    logo_type ,\
                    logo_link ,\
                    medallies_terms_condition \
                    from portal.skins";
            var query = connection.query(qry,function(err,rows) {            
                    if(err)
                        console.log("Error Selecting : %s ",err );

                    var colSetup =   skin_columns (rows,req,rows);     
                    // res.render('users',{page_title:"PP Users ",data:rows,colsetup:colSetup});                        
                    res.render('skins',{page_title:"PP Client Skins ",data:rows,colsetup:colSetup});                           
            });         
    });  
};                
//---------------------------------------------------
skin_columns = function(rows,req,res) {
    var colsetup = {
        url:'/skins',
        altRows: true,
        deepempty: true,
        autowidth: true,
        ignoreCase: true,
        datatype: "json",
        mtype: "GET",       
        height:'auto',
        width:'auto',
        colNames:['ID', 'client id','portal name','pri color','sec color','disclaimer','contact','terms conditions','incorrect data','delay','logo type','logo link','meddallies terms'],
        colModel:[
            {name:'id',index:'id', width:50  },
            {name:'client_id', editable:true , width:50},
            {name:'portal_name',index:'porat_name', sortable:false, editable:true, width:50},
            {name:'color_primary', editable:true ,width:60 },
            {name:'color_secondary', width:20},
            {name:'disclaimer_text', editable:true},
            {name:'contact_link', editable:true},
            {name:'terms_condition_text', editable:true},
            {name:'incorrect_data_text', editable:true},
            {name:'delay_threshold', editable:true},
            {name:'logo_type', editable:true},
            {name:'logo_link', editable:true},
            {name:'medallies_terms_condition', editable:true},               
        ],
        loadonce:true,
        pager: "#PPskinspager",
        toppager:true,
        rowNum: 25,
        rowTotal:5000,
        //rowList: [10, 20, 30],
        sortname: "id",
        sortorder: "asc",
        viewrecords: true,
        gridview: true,
        autoencode: false,            
        caption: "Skins ",
        onSelectRow: function(id) {
            if(id && id!==lastsel){
                jQuery('#PPskinsgrid').jqGrid('restoreRow',lastsel);
                jQuery('#PPskinsgrid').jqGrid('editRow',id,true);
                lastsel=id;
            }
        }    
    }      
    return colsetup ;  
};


