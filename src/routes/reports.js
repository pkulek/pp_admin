
/*
 * GET users listing.
 */
var crypt = 'mUQ3cT9Sq1Ha4Y';
var connection  = require('express-myconnection'); 
var lastsel = '0' ;
//---------------------------------------------------
exports.list = function(req, res){
     var input = JSON.parse(JSON.stringify(req.params));
     var id = req.params.id;
     console.log("Report List " + JSON.stringify(req.body));
     req.getConnection(function(err,connection){
         var qry = "select id,\
                    date_time, \
                    owner ,\
                    viewer ,\
                    action_taken ,\
                    oid \
                    from portal.vdt";
         var query = connection.query(qry,function(err,rows)  {                
                if(err) {
                    console.log("Error Selecting : %s ",err );
                    res.send('Error in retrieving data');
                } else {
                    //console.log("SEND user List " + JSON.stringify(rows));
                    res.send(rows);
                };                   
         });
    });
};
//---------------------------------------------------
exports.htmllist = function(req, res){
    console.log(JSON.stringify(req.params.page));
    req.getConnection(function(err,connection){
        var qry = "select COUNT(*) from portal.users " ;        
         var qry = "select id,\
                    date_time, \
                    owner ,\
                    viewer ,\
                    action_taken ,\
                    oid \
                    from portal.vdt";
            var query = connection.query(qry,function(err,rows) {            
                    if(err)
                        console.log("Error Selecting : %s ",err );

                    var colSetup =   report_columns (rows,req,rows);     
                    // res.render('users',{page_title:"PP Users ",data:rows,colsetup:colSetup});                        
                    res.render('reports',{page_title:"PP Users Report ",data:rows,colsetup:colSetup});                           
         });         
    });  
};
//---------------------------------------------------
exports.add = function(req, res){
  res.render('add_customer',{page_title:"Add Customers - Node.js"});
};
//---------------------------------------------------
exports.edit = function(req, res){
    var id = req.params.id;
    req.getConnection(function(err,connection){       
        var query = connection.query('SELECT * FROM customer WHERE id = ?',[id],function(err,rows)  {
            if(err)
                console.log("Error Selecting : %s ",err );
     
            res.render('edit_customer',{page_title:"Edit Customers - Node.js",data:rows});
         });
         //console.log(query.sql);
    }); 
};
//---------------------------------------------------

//---------------------------------------------------
report_columns = function(rows,req,res) {
    var colsetup = {
        url:'/reports',
        altRows: true,
        deepempty: true,
        autowidth: true,
        ignoreCase: true,
        datatype: "json",
        mtype: "GET",       
        height:'auto',
        width:'auto',
        colNames:['ID', 'Date','owner','viewer','action_taken','OID'],
        colModel:[
            {name:'id',index:'id', width:50  },
            {name:'date_time', editable:true , width:50},
            {name:'owner',index:'owner', sortable:false, editable:true, width:50},
            {name:'viewer',index:'viewer', editable:false ,width:60 },
            {name:'action_taken',index:'action_taken', width:20},
            {name:'oid',index:'oid', editable:false}   
        ],
        loadonce:true,
        pager: "#PPreportspager",
        toppager:true,
        rowNum: 25,
        rowTotal:5000,
        //rowList: [10, 20, 30],
        sortname: "id",
        sortorder: "asc",
        viewrecords: true,
        gridview: true,
        autoencode: false,            
        caption: "User List",
        onSelectRow: function(id) {
            if(id && id!==lastsel){
                jQuery('#PPreportsgrid').jqGrid('restoreRow',lastsel);
                jQuery('#PPreportsgrid').jqGrid('editRow',id,true);
                lastsel=id;
            }
        }    
    }      
    return colsetup ;  
};


