const CJSON = require("circular-json");
var dict = require('../dic/dict');
var logger = require('./logging');
var mssql = require('mssql');
var fs = require('fs')
var stopwatch = require("node-stopwatch").Stopwatch.create();
var moment = require('moment'); // for date time

//var devEnv = require('../../app').devenv;
//const devEnv = "PROD"; //STAGE  or PROD//

const DEBUG = true ;
const TIMEOUT = 1200 ;
var QEs = ['BRONX','HIXNY','HEALTHECONN','HEALTHIX','HEALTHLINKNY','NYCIG','GRRHIO','HEALTHELINK'];
/*
var STAGE_connect = "mssql://stella_user:heartbeat@192.168.130.109/SW_Reporting";
var PROD_connect = "mssql://stella_user:heartbeat@192.168.180.45:3978/TEMP_SW_Reporting";

const msconfig = eval(devEnv+"_connect");
//------------------------------------------------------------------------------------------------
mssql.connect(msconfig,(err)=> {
        console.log("Starting MS SqlServer %s for %s - %s",err||"OK",msconfig,devEnv);
});
*/
//----------------------------------------------------------------------------------
function isEmpty(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//---------------------------------------------------
var getHBdashboard = function(callback){                                                                                          
        var qry = "select top 1 'HEALTHIX' as RHIO, "+            
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHIX' order by Timestamp desc ) as XCA_Retrieve,"+
                    "(select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHIX' order by Timestamp desc ) as LRS,"+                                                                                               
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHIX' order by Timestamp desc ) as XCA_Query , "+                                                                                             
                    "(select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHIX' order by Timestamp desc ) as LQS , "+
                    "(select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'HEALTHIX' order by Timestamp desc ) as LErr "+                                                                                                         
        " from Heartbeat_Results"+                                                                                         
        " union "+                                                                                                                                                                         
        " select top 1 'BRONX',"+                                                                                                         
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'BRONX' order by Timestamp desc ) as XCA_Retrieve, "+                                                                                              
                    "(select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'BRONX' order by Timestamp desc ) ,   "+                                                                                            
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'BRONX' order by Timestamp desc ) as XCA_Query, "+     
                    "(select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'BRONX' order by Timestamp desc ),  "+                                                  
                    "(select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'BRONX' order by Timestamp desc ) as LErr "+                                                                                                         
        " from Heartbeat_Results "+                                                                                                                                     
        " union "+                                                                                                                                                                         
        " select top 1 'HEALTHLINKNY', "+                                                                                                        
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHLINKNY' order by Timestamp desc ) as XCA_Retrieve, "+                                                                                              
                    "(select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHLINKNY' order by Timestamp desc ) , "+                                                                                              
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHLINKNY' order by Timestamp desc ) as XCA_Query, "+     
                    "(select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHLINKNY' order by Timestamp desc ), "+                                                           
                    "(select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'HEALTHLINKNY' order by Timestamp desc ) as LErr "+                                                                                                         
        " from Heartbeat_Results"+                                                                                                                                      
        " union "+                                                                                                                                                                         
        " select top 1 'HIXNY',"+                                                                                                         
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HIXNY' order by Timestamp desc ) as XCA_Retrieve,  "+                                                                                             
                    "(select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HIXNY' order by Timestamp desc ) ,    "+                                                                                           
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HIXNY' order by Timestamp desc ) as XCA_Query, "+     
                    "(select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HIXNY' order by Timestamp desc ),  "+
                    "(select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'HIXNY' order by Timestamp desc ) as LErr "+                                                                                                         
        " from Heartbeat_Results"+                                                                                                                                      
        " union "+                                                                                                                                                                         
        " select top 1 'HEALTHELINK',"+                                                                                                         
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHELINK' order by Timestamp desc ) as XCA_Retrieve,  "+
                    "(select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHELINK' order by Timestamp desc ) ,  "+                                                                                             
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHELINK' order by Timestamp desc ) as XCA_Query, "+     
                    "(select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHELINK' order by Timestamp desc ),  "+
                    "(select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'HEALTHELINK' order by Timestamp desc ) as LErr "+                                                                                                         
        " from Heartbeat_Results  "+                                                                                                                                    
        " union "+                                                                                                                                                                         
        " select top 1 'NYCIG', "+                                                                                                      
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'NYCIG' order by Timestamp desc ) as XCA_Retrieve, "+                                                                                              
                    "(select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'NYCIG' order by Timestamp desc ) , "+                                                                                              
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'NYCIG' order by Timestamp desc ) as XCA_Query,"+     
                    "(select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'NYCIG' order by Timestamp desc ) , "+
                    "(select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'NYCIG' order by Timestamp desc ) as LErr "+                                                                                                         
        " from Heartbeat_Results"+                                                                                                                                      
        " union "+                                                                                                                                                                         
        " select top 1 'HEALTHECONN',"+                                                                                                         
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHECONN' order by Timestamp desc ) as XCA_Retrieve, "+                                                                                              
                    "(select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHECONN' order by Timestamp desc ) ,"+                                                                                               
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHECONN' order by Timestamp desc ) as XCA_Query, "+     
                    "(select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHECONN' order by Timestamp desc ), "+  
                    "(select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'HEALTHECONN' order by Timestamp desc ) as LErr "+                                                                                                         
        " from Heartbeat_Results "+                                                                                                                                     
        " union "+                                                                                                                                                                          
        "select top 1 'GRRHIO', "+                                                                                                        
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'GRRHIO' order by Timestamp desc ) as XCA_Retrieve,  "+                                                                                             
                    "(select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'GRRHIO' order by Timestamp desc ) ,"+                                                                                               
                    "(select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'GRRHIO' order by Timestamp desc ) as XCA_Query,"+     
                    "(select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'GRRHIO' order by Timestamp desc ), "+  
                    "(select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'GRRHIO' order by Timestamp desc ) as LErr "+                                                                                                         
        "from Heartbeat_Results "   
        try{
            new mssql.Request().query(qry).then(function(recordset) {
                //console.dir(recordset);
                if (callback){
                    callback(recordset);
                }
            }).catch(function(err) {
                //console.log(err);
                if (callback){
                    callback( []);
                }
            });   
        } catch(Ex) {
            callback(Ex);
        }
};
//---------------------------------------------------
exports.HBMlist = function(req, res){ // output raw json data
    _HBMlist((QElist)=> {
        res.send(QElist);
    });
}
//---------------------------------------------------
var _HBMlist = function(callback){
    getHBdashboard((result)=> {
        callback(result);
    });            
};
//----------------------------------------------------------------------------------
exports.HBMhtml = function(req, res){  // output to jqgrid
   setup_DashBoardColumns( (colSetup) => {   
        res.render('hbmdashboard',{page_title:"HeartBeat Monitor ",colsetup:colSetup});                        
   });        
}
//---------------------------------------------------
exports.list = function(req, res){
    var input = JSON.parse(JSON.stringify(req.params));
    var id = req.params.id;
    //console.log("SPRL Query Report List " + JSON.stringify(req.body));
    var qry = 'select top 1000  Count,QE_Name,QE_ID,Repository_Id,Document_Id,MPI_ID,Command,TimeStamp, Result, CONVERT(VARCHAR(1000),Failure_Description) as Failure_Description from Heartbeat_Results order by Timestamp desc ' ;
    try{
    new mssql.Request().query(qry).then(function(recordset) {
        //console.dir(recordset);
        res.send(recordset);
    }).catch(function(err) {
        console.log(err);
    }); 
    } catch(Ex) {
        res.send(Ex);
    }
};
//---------------------------------------------------
var sqlToJsDate = function(sqlDate){
    var sqlDateArr1 = sqlDate.split("-");
    var sYear = sqlDateArr1[0];
    var sMonth = (Number(sqlDateArr1[1]) - 1).toString();
    var sqlDateArr2 = sqlDateArr1[2].split(" ");
    var sDay = sqlDateArr2[0];
    var sqlDateArr3 = sqlDateArr2[1].split(":");
    var sHour = sqlDateArr3[0];
    var sMinute = sqlDateArr3[1];
    var sqlDateArr4 = sqlDateArr3[2].split(".");
    var sSecond = sqlDateArr4[0];
    var sMillisecond = sqlDateArr4[1];            
    return new Date(sYear,sMonth,sDay,sHour,sMinute,sSecond,sMillisecond);
};   
//------------------------------------------------------------        
var getHBlist = function(qe,date,callback){
   //  DATEDIFF(SECOND, CONVERT( DATETIME, '1970-01-01', 121 ), @ctimestamp)
    //var qry = "select convert(varchar,TimeStamp,121)  as date, SUBSTRING(CONVERT(VARCHAR(500),Failure_Description),4,12) as value,QE_Name,Command  from Heartbeat_Results   where result = 'Success'  and TimeStamp > '"+date+"'  " ;
    var limit = 50000;                                                             
    var qry = "select convert(varchar,TimeStamp,121)  as date, SUBSTRING(CONVERT(VARCHAR(500),Failure_Description),4,12) as value,QE_Name,Command,Result  from Heartbeat_Results   where TimeStamp > '"+date+"'  " ;                                                             
    if (! isEmpty(qe) ){
       qry += " and QE_name = '"+qe+"' " ; 
    }
    qry += " order by Timestamp asc ";           
   // console.log(qry);        
   try {
        new mssql.Request().query(qry).then(function(recordset) {
            //console.log(recordset);
            if (callback){
                alist = [];
                var command ;
                recordset.forEach( (element, index, theArray) =>{
                    if ( parseInt(element.value) == 'NaN') {
                        element.value = 0;                                                
                    }                
                    element.value = Math.min(limit,element.value);
                  
                    alist.push([element.date,element.value,element.Command,element.Result]);        
                });
                callback({"data":alist,"QE":qe,"command":command});
            } else {
                return recordset;
            }
        }).catch(function(err) {
            console.log(err);
            if (callback){
                callback({"data":[],"QE":'Error',"command":err});
            } else {
                return err;
            }
        });
   } catch(Ex) {
        if (callback){
                callback({"data":[],"QE":'Error',"command":Ex});
        } else {
            return Ex;
        }
   }
} 
//---------------------------------------------------
var qechart = exports.qechart = function(req, res) {  // output to graph
    var rhio = req.params.rhio;
    var date = req.params.date;
    if (isEmpty( rhio )){ 
        rhio=""; 
    };
    if (isEmpty(date)){
        var d = new Date();
        d.setDate(d.getDate()-2); 
        date = moment(d).format('YYYY-MM-DD');   
    };
    console.log("qechart-rhio = %s, date= %s",rhio,date);
    getHBlist(rhio,date,(list)=> {
        res.render('HBMqechart',{page_title:"Heartbeat",data:list,"rhio":rhio});
    }); 
};
//---------------------------------------------------
var chart = exports.chart = function(req, res){  // output to graph
    //console.log(JSON.stringify(req.params));
    var rhio = req.params.rhio;
    var date = req.params.date;
    if (isEmpty( rhio )){ 
        rhio=""; 
    };
    if (isEmpty(date)){
        var d = new Date();
        d.setDate(d.getDate()-2); 
        date = moment(d).format('YYYY-MM-DD');   
    };
    console.log("rhio = %s, date= %s",rhio,date);
    _qechartlist(rhio,date,(data) => {
        res.render('HBMchart',{page_title:"Heartbeat",data:data,"rhio":rhio});
    });
};
//---------------------------------------------------
var chart1 = exports.chart1 = function(req, res){  // output to graph
    //console.log(JSON.stringify(req.params));
    var rhio = req.params.rhio;
    var date = req.params.date;
    if (isEmpty( rhio )){ 
        rhio=""; 
    };
    if (isEmpty(date)){
        var d = new Date();
        d.setDate(d.getDate()-2); 
        date = moment(d).format('YYYY-MM-DD');   
    };
    console.log("rhio = %s, date= %s",rhio,date);
    _qechartlist(rhio,date,(data) => {
        res.render('HBMqechart1',{page_title:"Heartbeat",data:data,"rhio":rhio});
    });
};
//---------------------------------------------------
var charts = exports.charts = function(req, res){  // output to graph
    _qechartlist('','',(data) => {
        res.render('HBMcharts',{page_title:"QEMonitors",data:data});
    });
}
//---------------------------------------------------
var qechartlist = exports.qechartlist = function(req, res){ // output raw json data
    var rhio = req.params.rhio;
    var date = req.params.date;
    //console.log("rhio=%s, date =%s",rhio,date);
    if (isEmpty(date)) {        
        var d = new Date();
        d.setDate(d.getDate()-2); 
        date = moment(d).format('YYYY-MM-DD');        
    }  
    //console.log(date) ;
    _qechartlist(rhio,date,(list)=>{
        //console.log(list);
          res.send(list);
    });           
}
//---------------------------------------------------
var _qechartlist = function(qe,date,callback){ //
    var QElist = [];
    if (isEmpty(date)) {
        var d = new Date();
        d.setDate(d.getDate()-2); 
        date = moment(d).format('YYYY-MM-DD');   
    }
    //console.log("_qechartlist qe=%s",qe);
    if (isEmpty(qe)) {
            getHBlist(QEs[0],date,(list)=>{   
                QElist.push(list); 
                getHBlist(QEs[1],date,(list)=>{        
                    QElist.push(list);
                    getHBlist(QEs[2],date,(list)=>{        
                        QElist.push(list);
                        getHBlist(QEs[3],date,(list)=>{        
                            QElist.push(list);
                            getHBlist(QEs[4],date,(list)=>{        
                                QElist.push(list);
                                getHBlist(QEs[5],date,(list)=>{        
                                    QElist.push(list);
                                    getHBlist(QEs[6],date,(list)=>{        
                                        QElist.push(list);
                                        getHBlist(QEs[7],date,(list)=>{        
                                            QElist.push(list);
                                            callback(QElist);
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
    } else {
            getHBlist(qe,date,(list)=>{
                callback(list);
            });           
    };           
}
//---------------------------------------------------
exports.chartlist = function(req, res){ // output raw json data
    var qe = req.params.qe;
    _chartlist(qe,(QElist)=>{
        res.send(QElist);
    });
}
//---------------------------------------------------
var _chartlist = function(qe,callback){
    getHBlist(qe,(result)=>{
        callback(result);
    });            
};
//----------------------------------------------------------------------------------
exports.htmllist = function(req, res){  // output to jqgrid
   var tokenid = req.params.tokenid ;
   var type = req.params.type;
   if (isEmpty( type )){       
        type="JSON";
   }
   setup_columns( (colSetup) => {   
        res.render('hbm',{page_title:"HeartBeat Monitor ",colsetup:colSetup});                        
   });        
}
//----------------------------------------------------------------------------------
exports.htmlsprllist = function(req, res){  // output to jqgrid
   var mpiid = req.params.mpiid ;
   setup_MPIcolumns( (colSetup) => {   
        res.render('mpi',{mpiid:mpiid,page_title:"HeartBeat Monitor ",colsetup:colSetup});                        
   });        
}
//---------------------------------------------------
var setup_columns = function(callback) {
    //console.log("calling config_colums");
    dict.get("JSON","HBMCOLUMNS","JQGRID","",(err,data)=> {
        if ( isEmpty(data)) {
            var colsetup = { 
                url:'/HBM/XCAReport', 
                altRows: false, 
                deepempty: true,
                autowidth: true,
                ignoreCase: true,
                datatype: "json",
                mtype: "GET",       
                height:'auto',
                width:'auto',
                colModel:[
                    {label:'Result',name:'Result',index:'Result', editable:false ,width:50,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'TimeStamp',name:'TimeStamp',index:'TimeStamp', editable:false ,width:135,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'QE_Name',name:'QE_Name',index:'QE_Name', editable:false , width:75, searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']}},
                    {label:'QE_ID',name:'QE_ID',index:'QE_ID', editable:false ,width:300,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'Repository_Id',name:'Repository_Id',index:'Repository_Id', editable:false ,width:300, searchoptions:{sopt:['cn','bw','eq','bn','nc','ew','en']}},
                    {label:'MPI_ID',name:'MPI_ID',index:'MPI_ID', editable:true ,width:95,  searchoptions:{sopt:['cn','bw','eq','bn','nc','ew','en']}},
                    {label:'Document ID',name:'Document_Id', editable:false ,width:300,  searchoptions:{sopt:['cn','bw','eq','bn','nc','ew','en']}},
                    {label:'Command',name:'Command',index:'Command', editable:false ,width:75,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'Fail Count',name:'Count', editable:false ,width:35 },
                    {label:'Failure_Description',name:'Failure_Description',index:'Failure_Description', editable:false ,width:585,searchoptions:{sopt:['cn','bw','bn','eq','nc','ew','en']} },
                    {label:'', width:1,search:false}    
                ],
                loadonce:true,
                toppager:true,
                rowNum: 25,
                rowTotal:5000,
                viewrecords: true,
                gridview: true,
                autoencode: true,            
                caption: "Config"          
            }
            dict.set("JSON","HBMCOLUMNS","JQGRID","",colsetup);
            callback( colsetup);
        } else {
            //console.log("Using columns from dictionary"+data);
            callback(JSON.parse(data)); 
        }       
    });      
};
//---------------------------------------------------
var setup_DashBoardColumns = function(callback) {
       //dict.get("JSON","HBDASHBCOLUMNS","JQGRID","",function(data) {
       //if ( isEmpty(data)) {
            var colsetup = { 
                url:'/HBM/Report', 
                altRows: false, 
                deepempty: true,
                autowidth: false,
                ignoreCase: true,
                multiselect:false,
                multiboxonly:true,
                datatype: "json",
                mtype: "GET",       
                height:'auto',
                width:'auto',
                scrollOffset:0,
                colModel:[
                    {label:'RHIO',name:'RHIO', editable:false ,width:88 },
                    {label:'Retrieve',name:'XCA_Retrieve', editable:false ,width:50,
                        cellattr: function(rowId, val, rawObject) {
                            if (val == 'Failure') {
                                return " class='ui-state-error-text ui-state-error'";
                            }
                            if (val == 'Success') {
                                return " class='ui-state-highlight'";
                            }
                        }
                    },
                    {label:'Last Retrieve',name:'LRS', editable:false ,width:50 },
                    {label:'Query',name:'XCA_Query', editable:false ,width:50,
                        cellattr: function(rowId, val, rawObject) {
                            if (val == 'Failure') {
                                return " class='ui-state-error-text ui-state-error'";
                            }
                            if (val == 'Success') {
                                return " class='ui-state-highlight'";
                            }
                        }                    
                     },
                    {label:'Last Query',name:'LQS', editable:false ,width:50 } ,
                    {label:'Last Error',name:'LErr', editable:false ,width:125 } ,
                    {label:'-', width:1,search:false}              
                ],
                ondblClickRow: function (rowid, iRow,iCol) {
                    var rhio = $(this).jqGrid ('getCell', rowid, 'RHIO');
                    var url = "/HBM/Chart/"+rhio ;
                    var win = window.open(url , rhio +"HBM Chart","resizable=1,scrollbars=yes ,width=900,height=410");
                },
                loadonce:true,
                toppager:true,
                rowList: [],        // disable page size dropdown
                pgbuttons: false,     // disable page control like next, back button
                pgtext: null,         // disable pager text like 'Page 0 of 10'
                viewrecords: false ,   // disable current view record text like 'View 1-10 of 100' 
                gridview: false,
                autoencode: true,           
                caption: "HB Dasboard"          
            };
         // dict.set("JSON","HBDASHBCOLUMNS","JQGRID","",colsetup);
            callback( colsetup);
        //} else {
            //console.log("Using columns from dictionary"+data);
          //  callback(JSON.parse(data)); 
        //}       
    //});      
};
//---------------------------------------------------

let setup_MPIcolumns = function(callback) {
            var colsetup = { 
                url:'/SPRL/testmpi', 
                altRows: false, 
                deepempty: true,
                autowidth: true,
                ignoreCase: true,
                datatype: "json",
                mtype: "GET",       
                height:'auto',
                width:'auto',
                colModel:[
                    {label:'QE',name:'qe', editable:false ,width:150},
                    {label:'oid',name:'root', editable:false , width:250},
                    {label:'mpi',name:'ext', editable:false ,width:100 },
                    {label:'', width:1,search:false}    
                ],
                loadonce:true,
                toppager:true,
                rowNum: 25,
                rowTotal:5000,
                viewrecords: true,
                gridview: true,
                autoencode: true,            
                caption: "Config"          
            }
            callback( colsetup);  
};