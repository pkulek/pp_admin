
require('console-stamp')(console, 'yyyymmddHHmmss.l');
let config = require('../utils').getConfig().conf;
const CJSON = require("circular-json");
//let dict = require('../dic/dict');
let mssql = require('mssql');
let fs = require('fs')
let stopwatch = require("node-stopwatch").Stopwatch.create();
let moment = require('moment'); // for date time

const DEBUG = true ;

let QEs = ['BRONX','HIXNY','HEALTHECONN','HEALTHIX','HEALTHLINKNY','NYCIG','GRRHIO','HEALTHELINK'];

let utils =  require('../utils');
let isEmpty = utils.isEmpty ;

//---------------------------------------------------
let getHBdashboard = function(callback){
    let qry = 
`   select top 1 'HEALTHIX' as RHIO,            
            (select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHIX' order by Timestamp desc ) as XCA_Retrieve,
            (select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHIX' order by Timestamp desc ) as LRS,                                                                                               
            (select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHIX' order by Timestamp desc ) as XCA_Query , 
            (select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHIX' order by Timestamp desc ) as LQS , 
            (select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'HEALTHIX' order by Timestamp desc ) as LErr                                                                                                          
    from Heartbeat_Results                                                                                         
    union                                                                                                                                                                          
    select top 1 'BRONX',                                                                                                         
            (select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'BRONX' order by Timestamp desc ) as XCA_Retrieve,                                                                                               
            (select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'BRONX' order by Timestamp desc ) ,                                                                                               
            (select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'BRONX' order by Timestamp desc ) as XCA_Query, 
            (select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'BRONX' order by Timestamp desc ),                                                    
            (select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'BRONX' order by Timestamp desc ) as LErr                                                                                         
    from Heartbeat_Results                                                                                                                                      
    union                                                                                                                                                                          
    select top 1 'HEALTHLINKNY',                                                                                                         
            (select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHLINKNY' order by Timestamp desc ) as XCA_Retrieve,                                                                                               
            (select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHLINKNY' order by Timestamp desc ) ,                                                                                               
            (select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHLINKNY' order by Timestamp desc ) as XCA_Query, 
            (select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHLINKNY' order by Timestamp desc ),                                                            
            (select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'HEALTHLINKNY' order by Timestamp desc ) as LErr                                                                                                          
    from Heartbeat_Results                                                                                                                                      
    union                                                                                                                                                                          
    select top 1 'HIXNY',                                                                                                         
            (select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HIXNY' order by Timestamp desc ) as XCA_Retrieve,                                                                                               
            (select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HIXNY' order by Timestamp desc ) ,                                                                                               
            (select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HIXNY' order by Timestamp desc ) as XCA_Query,      
            (select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HIXNY' order by Timestamp desc ),  
            (select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'HIXNY' order by Timestamp desc ) as LErr                                                                                                          
    from Heartbeat_Results                                                                                                                                      
    union                                                                                                                                                                          
    select top 1 'HEALTHELINK',                                                                                                         
            (select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHELINK' order by Timestamp desc ) as XCA_Retrieve,  
            (select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHELINK' order by Timestamp desc ) ,                                                                                               
            (select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHELINK' order by Timestamp desc ) as XCA_Query,      
            (select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHELINK' order by Timestamp desc ),  
            (select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'HEALTHELINK' order by Timestamp desc ) as LErr                                                                                                          
    from Heartbeat_Results                                                                                                                                      
    union                                                                                                                                                                        
    select top 1 'NYCIG',                                                                                                       
            (select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'NYCIG' order by Timestamp desc ) as XCA_Retrieve,                                                                                               
            (select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'NYCIG' order by Timestamp desc ) ,                                                                                               
            (select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'NYCIG' order by Timestamp desc ) as XCA_Query,     
            (select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'NYCIG' order by Timestamp desc ) , 
            (select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'NYCIG' order by Timestamp desc ) as LErr                                                                                                          
    from Heartbeat_Results                                                                                                                                       
    union                                                                                                                                                                          
    select top 1 'HEALTHECONN',                                                                                                         
            (select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHECONN' order by Timestamp desc ) as XCA_Retrieve,                                                                                               
            (select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'HEALTHECONN' order by Timestamp desc ) ,                                                                                               
            (select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHECONN' order by Timestamp desc ) as XCA_Query,      
            (select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'HEALTHECONN' order by Timestamp desc ),   
            (select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'HEALTHECONN' order by Timestamp desc ) as LErr                                                                                                          
    from Heartbeat_Results                                                                                                                                      
    union                                                                                                                                                                           
    select top 1 'GRRHIO',                                                                                                         
            (select top 1 result from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'GRRHIO' order by Timestamp desc ) as XCA_Retrieve,                                                                                               
            (select top 1 DATEDIFF(ss,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Retrieve' and QE_name = 'GRRHIO' order by Timestamp desc ) ,                                                                                               
            (select top 1 result from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'GRRHIO' order by Timestamp desc ) as XCA_Query,     
            (select top 1 DATEDIFF(s,TimeStamp,GETDATE()) from Heartbeat_Results where command = 'XCA_Query' and QE_name = 'GRRHIO' order by Timestamp desc ),   
            (select top 1 TimeStamp from Heartbeat_Results where result = 'Failure' and QE_name = 'GRRHIO' order by Timestamp desc ) as LErr                                                                                                          
    from Heartbeat_Results 
`
            try{
                let request = new mssql.Request();
                request.query(qry,(err,result) => {
                    if(err) {
                        console.error(err)
                    }
                    if(result) {
                        callback(err,result.recordsets[0]);
                    } else {
                        callback(err,[{LErr:JSON.stringify(err)}]);                    
                    }
                });               
            } catch(Ex) {
                console.trace(Ex);
                callback(err,[{LErr:Ex}]);                    
            }        

};
//---------------------------------------------------
module.exports.HBMlist = function(req, res){ // output raw json data
    _HBMlist((QElist)=> {
        res.send(QElist);
    });
}
//---------------------------------------------------
let _HBMlist = function(callback){
    getHBdashboard((err,result)=> {
        callback(result);
    });            
};
//----------------------------------------------------------------------------------
module.exports.dashboard = function(req, res){  // output to jqgrid
    res.render('hbmdashboard',{page_title:"HeartBeat Monitor "});                        
}
//---------------------------------------------------
module.exports.list = function(req, res){
    let input = JSON.parse(JSON.stringify(req.params));
    let id = req.params.id;
    let qry = `select top 1000 setID,ResponseTime,Count,QE_Name,QE_ID,Repository_Id,Document_Id,MPI_ID,Command,TimeStamp, Result, CONVERT(VARCHAR(1000),Failure_Description) as Failure_Description from Heartbeat_Results order by TimeStamp desc ` ;
    try{
        let request = new mssql.Request();
        request.query(qry,(err,result) => {
            res.send(err||result.recordsets[0]);
        });
    } catch(Ex) {
        res.send(Ex);
    }
};
//---------------------------------------------------
let sqlToJsDate = function(sqlDate){
    let sqlDateArr1 = sqlDate.split("-");
    let sYear = sqlDateArr1[0];
    let sMonth = (Number(sqlDateArr1[1]) - 1).toString();
    let sqlDateArr2 = sqlDateArr1[2].split(" ");
    let sDay = sqlDateArr2[0];
    let sqlDateArr3 = sqlDateArr2[1].split(":");
    let sHour = sqlDateArr3[0];
    let sMinute = sqlDateArr3[1];
    let sqlDateArr4 = sqlDateArr3[2].split(".");
    let sSecond = sqlDateArr4[0];
    let sMillisecond = sqlDateArr4[1];            
    return new Date(sYear,sMonth,sDay,sHour,sMinute,sSecond,sMillisecond);
};   
//------------------------------------------------------------        
let getHBlist = function(qe,date,callback){
    let where = "";
    if(! isEmpty(date)) {         
        where = ` where TimeStamp >= '${date}' `                                                    
    }                                                             
    if (! isEmpty(qe) ){
        where += isEmpty(where) ? ` where QE_name = '${qe}' ` : ` and QE_name = '${qe}' `
    }
    let qry = `select top 1000 convert(varchar,TimeStamp,121)  as date, ResponseTime as value,QE_Name,Command,Result  from Heartbeat_Results ${where} order by Timestamp asc ` ;
    console.log(qry);           
    try {
        let request = new mssql.Request();
        request.query(qry,(err,result) => {
            if(err){
                console.error(err)
            }
            if ( result ) {
                if (callback){
                    alist = [];
                    result.recordsets.forEach( (obj) =>{
                        obj.forEach((element)=>{
                            if ( parseInt(element.value) == 'NaN') {
                                element.value = 0;                                                
                            }                
                            //console.log("ELEMENT=%j\n",element)
                            alist.push([element.date,element.value,element.Command,element.Result]);        
                        })
                    });
                    callback({"data":alist,"QE":qe});
                }
            }
        });
   } catch(Ex) {
        console.trace(Ex);
        if (callback){
                callback({"data":[],"QE":'Error',"command":Ex});
        } else {
            return Ex;
        }
   }
} 
//---------------------------------------------------
let qechart = module.exports.qechart = function(req, res) {  // output to graph
    let rhio = req.params.rhio;
    let date = req.params.date;
    if (isEmpty( rhio )){ 
        rhio=""; 
    };
    
    if (isEmpty(date)){
        let d = new Date();
        d.setDate(d.getDate()-2); 
        date = moment(d).format('YYYY-MM-DD');   
    };
    
    getHBlist(rhio,date,(list)=> {
        //console.log("qechart-rhio = %s, date= %s, %j",rhio,date,list);
        res.render('HBMqechart',{page_title:"Heartbeat",data:list,"rhio":rhio});
//        res.render('HBMqechart1',{page_title:"Heartbeat",data:list,"rhio":rhio});
    }); 
};
//---------------------------------------------------
let chart = module.exports.chart = function(req, res){  // output to graph
    //console.log(JSON.stringify(req.params));
    let rhio = req.params.rhio;
    let date = req.params.date;
    if (isEmpty( rhio )){ 
        rhio=""; 
    };
    
    if (isEmpty(date)){
        let d = new Date();
        d.setDate(d.getDate()-2); 
        date = moment(d).format('YYYY-MM-DD');   
    };
    
    _qechartlist(rhio,date,(data) => {
        //console.log("rhio = %s, date= %s",rhio,date);
        res.render('HBMchart',{page_title:"Heartbeat",data:data,"rhio":rhio});
    });
};
//---------------------------------------------------
let charts = module.exports.charts = function(req, res){  // output to graph
    _qechartlist('','',(data) => {
        res.render('HBMcharts',{page_title:"QEMonitors",data:data});
    });
}
//---------------------------------------------------
let chart1 = module.exports.chart1 = function(req, res){  // output to graph
    //console.log(JSON.stringify(req.params));
    let rhio = req.params.rhio;
    let date = req.params.date;
    if (isEmpty( rhio )){ 
        rhio=""; 
    };
    if (isEmpty(date)){
        let d = new Date();
        d.setDate(d.getDate()-2); 
        date = moment(d).format('YYYY-MM-DD');   
    };
    //console.log("rhio = %s, date= %s",rhio,date);
    _qechartlist(rhio,date,(data) => {
        //console.log("%j",data[0])
        //console.log("%j",data[1])
        res.render('HBMqechart1',{page_title:"Heartbeat",data:data,"rhio":rhio});
    });
};
//---------------------------------------------------
let qechartlist = module.exports.qechartlist = function(req, res){ // output raw json data
    let rhio = req.params.rhio;
    let date = req.params.date;
    //console.log("rhio=%s, date =%s",rhio,date);
    if (isEmpty(date)) {        
        let d = new Date();
        d.setDate(d.getDate()-2); 
        date = moment(d).format('YYYY-MM-DD');        
    }  
    //console.log(date) ;
    _qechartlist(rhio,date,(list)=>{
        //console.log(list);
          res.send(list);
    });           
}
//---------------------------------------------------
let _qechartlist = function(qe,date,callback){ //
    let QElist = [];
    
    if (isEmpty(date)) {
        let d = new Date();
        d.setDate(d.getDate()-2); 
        date = moment(d).format('YYYY-MM-DD');   
    }
    
    //console.log("_qechartlist qe=%s",qe);
    if (isEmpty(qe)) {
            getHBlist(QEs[0],date,(list)=>{   
                QElist.push(list); 
                getHBlist(QEs[1],date,(list)=>{        
                    QElist.push(list);
                    getHBlist(QEs[2],date,(list)=>{        
                        QElist.push(list);
                        getHBlist(QEs[3],date,(list)=>{        
                            QElist.push(list);
                            getHBlist(QEs[4],date,(list)=>{        
                                QElist.push(list);
                                getHBlist(QEs[5],date,(list)=>{        
                                    QElist.push(list);
                                    getHBlist(QEs[6],date,(list)=>{        
                                        QElist.push(list);
                                        getHBlist(QEs[7],date,(list)=>{        
                                            QElist.push(list);
                                            callback(QElist);
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
    } else {
        getHBlist(qe,date,(list)=>{
            callback(list);
        });           
    };           
}
//---------------------------------------------------
module.exports.chartlist = function(req, res){ // output raw json data
    let qe = req.params.qe;
    _chartlist(qe,(QElist)=>{
        res.send(QElist);
    });
}
//---------------------------------------------------
let _chartlist = function(qe,callback){
    getHBlist(qe,(result)=>{
        callback(result);
    });            
};
