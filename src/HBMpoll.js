
var dict = require('./dic/dict');
var email = require('./mail');
var CJSON = require('circular-json');
var users = require('./routes/users');
var sprl = require('./routes/sprl');

var intervalId = 0;
var counter = 0;
var tasklist = [];
const task_XCAQUERY = {"taskid":0,"name":"XCAQUERY","task": "XCAQuerytask();","duration":60000*4,"timestarted":0}; // every hour
const task_XCARETRIEVE = {"taskid":0,"name":"XCARETRIEVE","task": "XCARetrievetask();","duration":60000*10,"timestarted":0}; // every hour
//const tasks = {"taskid":0,"name":"HSREGISTER","task": "userstatustask('PP_REGISTRED');","duration":1000*60*60,"timestarted":0}; // every hour

//----------------------------------------------------------------------------------
function isEmpty(str) { // check if string is null or empty usually missing parameter
    return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
};
//----------------------------------------------------------------------------------
exports.tasklist = function(req, res){
    var out = [];
    tasklist.forEach( (element) => {
        out.push({"name":element.name,"task":element.task,"duration":element.duration,"timestarted":element.timestarted});    
    }, this);  
    res.send(out);
};
//-----------------------------------------------------------------------------
userstatustask = function(status){
    if (isEmpty(status) ) {
        status = "" ; // get all data
    }
    users.userlist(status,(rows,err)=>{
        if (rows.length > 0 ) {
            rows.forEach(function(part, index) {        
                if ( isEmpty(part.MPIID) || (part.MPIID == 'n/a') ){
                    var jsondata = JSON.stringify(part); 
                    console.log(part.verizon_id);
                    console.log(jsondata);
                    
                    //registerUser(part.verizon_id,JSON.stringify(part))
                    //sendemail("PP_AUTOREGISTERED",part);
                }
            });        
        }
    });
}
//-----------------------------------------------------------------------------
XCARetrievetask = function(){
    sprl.XCARetrieveAll();
}
//-----------------------------------------------------------------------------
XCAQuerytask = function(){
    sprl.XCAQuery();
}
//--------------------------------------------------------------------------------
sendemail = function(etype,jsonData) {
    if ( isEmpty(etype) ) {
        etype = "PP_AUTOREGISTERED" ;
    }
    var defaultMsg = 'Dear %s %s,'+
        '\nYour New York Patient Portal account has been activated. You can now log in and have a look at your medical data'+
        '\nYou now have access to a broad range of your medical records online. Please follow this link https://home.nypatientportal.org to log-in and take a look.'+
        '\nIf you are having issues with the NY Patient Portal, please do not hesitate to contact us.'+ 
        '\n\nThank you,'+
        '\nNYPP Support team'+ 
        '\n\n 1.888.633.6706'+
        '\nnyppsupport@nyehealth.org'
    dict.getMSG(etype,"MAIL",defaultMsg,'en-US',(data) => {
        var sprintf = require("sprintf-js").sprintf;
        var msg = sprintf(data,jsonData.given_name,jsonData.family_name)
        console.log("EMAIL to "+jsonData.email+"\n MSG="+msg);                  
        email.send({
            text:    msg ,
            from:   "nyppsupport@nyehealth.org", 
            to:     "nyppsupport@nyehealth.org",
            cc:      "",
            bcc:    "nyppsupport@nyehealth.org",
            subject: "Do no Reply: PP Account Activation"
        }, function(err, message)  { 
            console.log("err="+err);
            console.log("message="+JSON.stringify(message));
        });        
    });            
}            
//-----------------------------------------------------------------------------
registerUser = function(verizonid,rowData) {
    var Client = require('node-rest-client').Client;
    var defurl = "http://192.168.180.173:57772/csp/healthshare/hsppedge/api/NYeC.PP.Service.CID.ClinicalAuthentication/api" ;    
    dict.getURL("HSPPEDGE","",defurl, (data,error) =>{
        var url = data+"/userRegister/"+verizonid+"";
        var args = {
            data: rowData,
            headers: { "Content-Type": "application/json" }
        };
        var options_auth = { user: "HS_Services", password: "HS_Services" };
        var client = new Client(options_auth);
        var cReq = client.post(url, args, function (data, response) {
            console.log("RegisterUser data:"+response);
        });    
        cReq.on('requestTimeout', function (req) {
            console.log("Register User - request has expired");
            req.abort();
        });
        cReq.on('responseTimeout', function (res) {
            console.log("Register User - response has expired");
            req.abort();
        });   
    });        
}
//-----------------------------------------------------------------------------
exports.taskstart = function(req, res){
    console.log("taskstart BODY "+JSON.stringify(req.body));
    var taskname = req.params.taskname ;
    var task = req.params.task;
    var duration = req.params.duration;
    console.log(taskname);
    console.log(task);
    console.log(duration);
    if ( isEmpty(duration)) {
        duration = 1000*60*60 ;
    };
    if ( isEmpty(task)) {
        task = "userstatustask('PP_REGISTRED');";
    }
    if ( isEmpty(taskname)) {
        taskname = 'HSREGISTER';
    }    
    var o = startTask(taskname,duration,task)
    res.send(CJSON.stringify(o));
};
//-----------------------------------------------------------------------------
var startTask = exports.startTask = function(task){
    console.log('start '+task.name);
    var intervalId = setInterval(() => { 
         eval(task.task); 
    },task.duration );
    task.taskid = intervalId ;         
    task.timestarted = new Date();
    tasklist.push(task);
    console.log(tasklist);
    //return(o) ;
};
//-----------------------------------------------------------------------------
var stopTask = exports.stopTask = function(taskname){
    console.log('stop '+ taskname);
    tasklist.forEach( (element) => {
        if ( element.name == taskname ){
            clearInterval(element.taskid);
        }        
    }, this);
};



