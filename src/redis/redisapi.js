
/*
let REDIS_PORT = 6379 ;
let PROD_HOST = '192.168.180.141' ;
let STAGE_HOST = '192.168.160.141' ; 
let REDIS_OPTIONS =  {no_ready_check: true};
let  REDIS_HOST = eval(devEnv+'_HOST');
*/
let redis = require("redis");
let config =  require('../utils').getConfig().conf.redis;
let devEnv = "STAGE"
let auth = config.auth;

console.trace("Redis Config\n %j",config)
//       "redis":{"host":"192.168.180.141","auth":"manahealth","port":"6379","options":{"no_ready_check":"true"}},
try {
  var DB0 = redis.createClient(config.port, config.host, config.options);           
  DB0.auth(auth, function (err) {
    if (err) {
        console.log( err);
    }
  });

  var DB1 = redis.createClient(config.port, config.host, config.options);           
  DB1.auth(auth, function (err) {
    if (err) {
        console.log( err);
    }
  });
   
  var DB2 = redis.createClient(config.port, config.host, config.options);           
  DB2.auth(auth, function (err) {
    if (err) {
        console.log( err);
    }
  });
  
  var DB3 = redis.createClient(config.port, config.host, config.options);           
  DB3.auth(auth, function (err) {
    if (err) {
        console.log( err);
    }
  });
  DB0.select(0,function (){})
  DB1.select(1,function (){})
  DB2.select(2,function (){})
  DB3.select(3,function (){})
} catch(Ex){
    console.error(Ex);
}
module.exports = {
    DB0:DB0, DB1:DB1, DB2:DB2, DB3:DB3
};

