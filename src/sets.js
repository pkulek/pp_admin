// Set code for Node.js, which stores objects in arrays. All sets are
// mutable, and set update operations happen destructively. However,
// operations like set intersection and difference create a new set.

let SetPrototype = {
    // Does this set contain an element x? Returns true or false.
    has: function(x) {
	    return this._items.indexOf(x) >= 0;
    },

    // Does this set contain an element in array x? Returns true or false.
    contains: function(roles) {
		var ret = false;
		if (roles.constructor === Array ) {
			var arrayLength = roles.length;
			for (var i = 0; i < arrayLength; i++) {
				if (this.has(roles[i]) ){
					ret = true;
					break;
				} 
			}
		}
		return ret;
    },

    // Add an element x to this set, and return this set.
    add: function(x) {
	if (!this.has(x)) this._items.push(x);
	return this;
    },

    // Remove an element x from this set, if it is part of the set. If
    // it is not part of the set, do nothing. Returns this set.
    remove: function(x) {
	var pos = this._items.indexOf(x);
	if (pos >= 0) {
	    this._items.splice(pos, 1);
	}
	return this;
    },

    
    // Return a copy of the items in the set, as an array.
    array: function() {
	return this._items.concat();
    },

    // Return the size of the set.
    size: function() {
	return this._items.length;
    },

    // Return a shallow copy of the set.
    copy: function() {
	var result = new exports.Set();
	result._items = this._items.concat();
	return result;
    }

};

exports.Set = function(items) {
    // All items are stored in this list, in no particular order.
    this._items = [];

    // If initial items were given, add them to the set.
    if (typeof items !== "undefined")
	for (var i = 0; i < items.length; i++)
	    this.add(items[i]);
};

exports.Set.prototype = SetPrototype;


let StringSetPrototype = {
    // Does this set contain an element x? Returns true or false.
    has: function(x) {
	return this._items.hasOwnProperty(x);
    },

    // Add an element x to this set, and return this set.
    add: function(x) {
	if (!this.has(x)) {
	    this._items[x] = x;
	    this._size++;
	}
	return this;
    },

    // Remove an element x from this set, if it is part of the set. If
    // it is not part of the set, do nothing. Returns this set.
    remove: function(x) {
	if (this.has(x)) {
	    delete this._items[x];
	    this._size--;
	}
	return this;
    },

    
    // Return a copy of the items in the set, as an array.
    array: function() {
	var arr = [];
	for (var x in this._items)
	    arr.push(this._items[x]);
	return arr;
    },

    // Return the size of the set.
    size: function() {
	return this._size;
    },

    // Return a shallow copy of the set.
    copy: function() {
	var result = new exports.StringSet();
	for (var x in this._items) result.add(this._items[x]);
	return result;
    },
}


// A special type of set, where all keys have unique string
// representations. This works fine with a set containing only
// strings, or only numbers. However, if you want to have the number
// 42 and the string "42" in the same set, you should use the Set data
// type, not StringSet.
exports.StringSet = function(items) {
    // All items are stored in an object. 
    this._items = {};
    // We maintain a size variable for the cardinality of the set.
    this._size = 0;

    // If initial items were given, add them to the set.
    if (typeof items !== "undefined")
	for (var i = 0; i < items.length; i++)
	    this.add(items[i]);
};

exports.StringSet.prototype = StringSetPrototype;