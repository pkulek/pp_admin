
//var Service = require('node-windows').Service;

var express = require('express');
var bodyParser = require('body-parser');
var fileUpLoad = require('express-fileupload');
var http = require('http');
var path = require('path');
var fs = require('fs');
var io = require('socket.io').listen(http);

var sprl = require('./src/routes/sprl'); 
var pollUpdates = require('./src/pollUpdates');

var urlencodedParser = bodyParser.urlencoded({ extended: true })


// all environments
var tasks = express();

const task_XCAQUERY = {"taskid":0,"name":"XCAQUERY","task": "XCAQuerytask()","duration":60000*4,"timestarted":0}; // every hour
const task_XCARETRIEVE = {"taskid":0,"name":"XCARETRIEVE","task": "XCARetrievetask()","duration":60000*10,"timestarted":0}; // every hour

tasks.set('port', 4400);
tasks.set('views', path.join(__dirname, 'views'));
tasks.set('view engine', 'ejs');

tasks.use(bodyParser.urlencoded({extended:true}));
tasks.use(fileUpLoad());
tasks.use(express.static(path.join(__dirname, 'public')));



tasks.post('/task/start',pollUpdates.taskstart);
tasks.post('/task/start/:taskname',pollUpdates.taskstart);

tasks.get('/task/list',pollUpdates.tasklist);
tasks.get('/task/start/:taskname',pollUpdates.taskstart);
tasks.get('/task/start/:taskname/:task',pollUpdates.taskstart);
tasks.get('/task/start/:taskname/:task/:duration',pollUpdates.taskstart);



//pollUpdates.startTask("HSREGISTER");

setTimeout(function () {
  pollUpdates.startTask(task_XCAQUERY);
}, 1200);
setTimeout(function () {
    pollUpdates.startTask(task_XCARETRIEVE);
}, 2400);

http.createServer(tasks).listen(tasks.get('port'), function(){
     console.log('Patient Portal Task Scheduler on port ' + tasks.get('port'));
});

/*
// Create a new service object
var svc = new Service({
    name:'HBM',
    description: 'NYeC Heartbeat Monitor web service.',
    script: 'D:\\HBM\\Monitor\\TaskScheduler.js'
});
// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
    svc.start();
});

svc.install();

*/


