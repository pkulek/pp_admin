            function genPwd(){
                        var letters = ['a','b','c','d','e','f','g','h','i','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
                        var numbers = [0,1,2,3,4,5,6,7,8,9];
                        var nonlet = ['(','@','!','#','$','%','^',')'];
                        var randomstring = '';
                        for(var i=0;i<5;i++){
                            var rlet = Math.floor(Math.random()*letters.length);
                            randomstring += letters[rlet];
                        }
                        var rnum = Math.floor(Math.random()*nonlet.length);
                        randomstring += nonlet[rnum];            
                        for(var i=0;i<3;i++){
                            var rnum = Math.floor(Math.random()*numbers.length);
                            randomstring += numbers[rnum];
                        }
                        return randomstring ;
            }
            function toggleDiv(divId) {
                $("#"+divId).toggle();          
                // increase hieght old dialog
                $("#dialog-login").dialog("option", "maxHeight", 600);
            }
            function setAnswers(selector) {
                if ( isEmpty(selector) ) {
                    selector = 'securityq';
                }
                $.get({
                        url:"/login/secquestions?langid=en-US",
                        dataType: "json",
                        success: function (data) {
                            $.each(data,function(i,obj)  {
                            var div_data="<option value="+obj.id+">"+obj.question+"</option>";
                            $(div_data).appendTo('#'+selector); 
                            $(div_data).appendTo('#'+selector+'2'); 
                            });  
                        }
                    });
                        
            }
            
            $(function() {          
                    var dialog, form,  
                    emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
                    name = $( "#name" ),
                    email = $( "#email" ),
                    password = $( "#password" ),
                    allFields = $( [] ).add( name ).add( email ).add( password ).add( rname ).add( remail ).add( rpassword ).add( rfname ).add( rlname ),
                    tips = $( ".validateTips" );

                    function updateTips( t ) {
                            tips.text( t ).addClass( "ui-state-highlight" );
                            setTimeout(function() {
                                tips.removeClass( "ui-state-highlight", 1500 );
                            }, 500 );
                    }

                    $( "button:first" ).button({
                        icons: {
                            primary: "ui-icon-locked"
                        },
                        text: false
                        }).click( function( event ) {                  
                            $("#rpassword").val(genPwd());
                    })

                function checkLength( o, n, min, max ) {
                    if ( o.val().length > max || o.val().length < min ) {
                        o.addClass( "ui-state-error" );
                        updateTips( "Length of " + n + " must be between " +  min + " and " + max + "." );
                        return false;
                    } else {
                        return true;
                    }
                }    
                
                function checkMatch( n1,n2 ) {
                var v1 =  n1.valueOf() ;
                var v2 =  n2.valueOf() ;
                    if ( n1.val() === n2.val() ) {
                        return true;
                    } else {
                        n1.addClass( "ui-state-error" );
                        n2.addClass( "ui-state-error" );
                        updateTips( "Passwords do not match, please renter." );
                        return false;
                    }
                }             
                function checkRegexp( o, regexp, n ) {
                    if ( !( regexp.test( o.val() ) ) ) {
                    o.addClass( "ui-state-error" );
                    updateTips( n );
                    return false;
                    } else {
                    return true;
                    }
                }
                function registerUser() {
                    $("#userlogin").hide();
                    $("#changePassword").hide();
                    $("#forgotPassword").hide();
                    $("#registeruser").show();
                    $("#showRegisterUser").show();
                    $('.ui-dialog-buttonpane button:contains("Register User")').button().hide();
                    $('.ui-dialog-buttonpane button:contains("Submit")').button().hide();
                }
                function validateUser() {
                    var valid = true;    
                    allFields.removeClass( "ui-state-error" );    
                    if( $('#showRegisterUser').is(':visible') ) {
                    log("showRegisterUser"+valid)
                    if ( valid ) {          
                        var pwd = $('#rpassword').valueOf();
                        var userid = $('#rname').valueOf();
                        var fname = $('#rfname').valueOf();
                        var lname = $('#rlname').valueOf();
                        var email = $('#remail').valueOf();
                        var data = {};
                        data.pwd = pwd.val();
                        data.user_id =userid.val();
                        data.email = email.val();
                        data.fname = fname.val();
                        data.lname = lname.val();           
                        var url =  "/login/register/user?id="+data.user_id+"&pwd="+data.pwd+"&fname="+data.fname+"&lname="+data.lname+"&email="+data.email ;
                        log(url);
                        $.ajax({url: url, async:false,type:'POST' ,complete: function(result,status){
                                    if (token.validate){
                                            updateTips( "new User Registered" );
                                            //window.location = "/users/html?token="+token.tokenid ;
                                    } else {      
                                            updateTips( "New Registration Failed, please renter." );
                                    }
                                }
                            });                         
                        }                                   
                    } else {      
                        valid = true;
                        // valid = valid && checkLength( name, "name", 3, 16 );
                        // valid = valid && checkLength( email, "email", 6, 80 );
                        // valid = valid && checkLength( password, "password", 5, 16 );

                        //valid = valid && checkRegexp( name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
                        //valid = valid && checkRegexp( email, emailRegex, "eg. ui@jquery.com" );
                        //valid = valid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );
                        // check if password are the same for change pwd
                        if( $('#showChangePassword').is(':visible') ) {
                            //valid = valid && checkRegexp( newpassword, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );
                            //valid = valid && checkLength($('#newpassword'),'new password', 5, 16 );
                            valid = valid && checkMatch($('#newpassword'),$('#renterpassword')) ;
                            if (! valid) {
                                //updateTips( "Passwords do not match, please renter." );
                                return valid;
                            }
                        }
                        if ( valid ) {          
                            var p = $('#password').valueOf();
                            var pnew = $('#newpassword').valueOf();
                            var n = $('#name').valueOf();                            
                            $.ajax({url: "/login/verifyuser/?userid="+n.val()+"&password="+p.val(), async:false ,complete: function(result,status){
                                if (result.responseText == "Error" ) {                          
                                        updateTips( "Validation Failed, please renter." );
                                }
                                var token = JSON.parse(result.responseText) ;
                                if (token.valid){                                 
                                    if( $('#showChangePassword').is(':visible') ) {
                                            $.ajax({url: "/login/resetpwd/?userid="+n.val()+"&password="+p.val()+"&newpassword="+pnew.val(), async:false ,complete: function(result,status){
                                                if (result.responseText == "Error" ) {                          
                                                        updateTips( "Change password Failed, please renter." );
                                                } else {
                                                    $("#showChangePassword").hide();
                                                    updateTips( "Password chnged Succesfully, please renter with new password." );
                                                }
                                            }});                   
                                    } else {                                            
                                            window.location = "/users/html?tokenid="+token.tokenid  ;
                                            // do a get of the html and insert
                                            // $("#mainprog").html("Hello <b>world</b>!");
                                    }
                                } else {      
                                        updateTips( "Validation Failed, please renter." );
                                }
                            }});    
                        } else {
                            updateTips( "Validation Failed, please renter." );
                        }              
                    }
                    return valid;
                }
                dialog = $( "#dialog-login" ).dialog({
                    autoOpen: false,
                    height: 350,
                    width: 500,
                    modal: true,
                    show: {
                        effect: "blind",
                        duration: 500
                    },
                    hide: {
                        effect: "explode",
                        duration: 700
                    },
                    buttons: {
                        "Register User": registerUser,
                        "Submit": validateUser,
                        Cancel: function() {
                            dialog.dialog( "close" );
                        }
                    },                
                    close: function() {
                        form[ 0 ].reset();
                        allFields.removeClass( "ui-state-error" );
                        dialog.dialog('close');
                    }
                });    
