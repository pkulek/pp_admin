<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:context="http://www.springframework.org/schema/context"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.springframework.org/schema/beans
	http://www.springframework.org/schema/beans/spring-beans-3.1.xsd
	http://www.springframework.org/schema/context
	http://www.springframework.org/schema/context/spring-context-3.1.xsd">

	<!-- Main application controller -->
	<bean id="applicationController" class="controllers.Application"
		scope="prototype">
		<property name="googleAnalytics" value="${google.analytics}" />
		<property name="skinRepository" ref="skinRepository" />
		<property name="service" ref="challengeQuestionsService" />
		<property name="medlinePathBuilder" ref="medlinePathBuilder" />
		<property name="clientRepository" ref="clientRepository" />
		<property name="userSessionService" ref="userSessionService" />
		<property name="userContractService" ref="userContractService" />
		<property name="cookiePolicyService" ref="cookiePolicyService" />
		<property name="nyecId" value="${core.provider.nyec.id}" />
		<property name="userRepository" ref="userRepository" />
		<property name="xdomainString" value="${xdomain.html}" />
		<property name="xdomainAdminString" value="${xdomain.admin.html}" />
		<property name="manualLoginClients" value="${manual.login.clients}" />
	</bean>

	<!-- Demographics Controller -->
	<bean id="demographicsController" class="controllers.DemographicsController"
		scope="prototype">
		<property name="demographicsDAO" ref="demographicsDAO" />
		<property name="executionContext" ref="dbLookupContext" />
		<property name="userSessionService" ref="userSessionService" />
		<property name="loggingService" ref="loggingService" />
		<property name="demographicsService" ref="demographicsService" />
	</bean>

	<!-- Direct Account Controller -->
	<bean id="directAccountController" class="controllers.admin.direct.DirectAccountController"
		scope="prototype">
		<property name="directAccountService" ref="directAccountService" />
		<property name="userSessionService" ref="userSessionService" />
	</bean>

	<bean id="patientDirectAccountController"
		class="controllers.messaging.direct.PatientDirectAccountController"
		scope="prototype">
		<property name="directAccountService" ref="directAccountService" />
		<property name="userSessionService" ref="userSessionService" />
	</bean>

	<!-- Messaging Controller -->
	<bean id="messagingController" class="controllers.messaging.MessagingController"
		scope="prototype">
		<property name="messengerConduitService" ref="messengerConduitService" />
		<property name="localMessengerService" ref="localMessengerService" />
		<property name="userSessionService" ref="userSessionService" />
		<property name="auditLoggingService" ref="loggingService" />
		<property name="gson" ref="messagingControllerGson" />
		<property name="fileMaxSize" value="${messaging.maxfilesize}" />
		<property name="userAddressBookRepository" ref="userAddressBookRepository" />
	</bean>

	<!-- Audit Record Controller -->
	<bean id="auditRecordController" class="controllers.user.vdt.AuditRecordController"
		scope="prototype">
		<property name="vdtService" ref="patientVdtService" />
		<property name="userSessionService" ref="userSessionService" />
		<property name="gson" ref="standardGson" />
	</bean>

	<!-- Document Controller -->
	<bean id="documentController" class="controllers.document.DocumentController"
		scope="prototype">
		<property name="documentService" ref="documentService" />
		<property name="userSessionService" ref="userSessionService" />
		<property name="acceptedContentMimeTypes">
			<bean class="org.springframework.util.StringUtils"
				factory-method="commaDelimitedListToSet">
				<constructor-arg type="java.lang.String"
					value="${service.document.store.mimetypes}" />
			</bean>
		</property>
		<property name="loggingService" ref="loggingService" />
		<property name="gson" ref="basicGson" />
		<property name="userRepository" ref="userRepository" />
	</bean>

	<!-- Patient Controller -->
	<bean id="patientController" class="controllers.admin.PatientController"
		scope="prototype">
		<property name="encryptedHelper" ref="encryptedHelper" />
		<property name="userSessionService" ref="userSessionService" />
		<property name="cacheService" ref="cacheService" />
		<property name="loggingService" ref="loggingService" />
		<property name="gson" ref="standardGson" />
		<property name="userRepository" ref="userRepository" />
		<property name="clientRepository" ref="clientRepository" />
	</bean>

	<!-- Admin Management Controller -->
	<bean id="adminManagementController" class="controllers.admin.AdminManagementController"
		scope="prototype">
		<property name="userManagementService" ref="userManagementService" />
		<property name="userSessionService" ref="userSessionService" />
		<property name="gson" ref="standardGson" />
		<property name="loggingService" ref="loggingService" />
		<property name="userRepository" ref="userRepository" />
		<property name="clientRepository" ref="clientRepository" />
		<property name="clientOrganizationSetRepository" ref="clientOrganizationSetRepository" />
		<property name="portalLanguageRepository" ref="portalLanguageRepository" />
		<property name="securityRoleRepository" ref="securityRoleRepository" />
		<property name="userSecurityQuestionService" ref="userSecurityQuestionService" />
		<property name="demographicsService" ref="demographicsService" />
		<property name="emailNotificationService" ref="emailNotificationService" />
		<property name="directMessenger" ref="nyecDirectMessenger" />
		<property name="apiRepository" ref="apiRepository" />
		<property name="readyRegistrationService" ref="readyRegistrationService" />
		<property name="mpiVerificationURL" value="${patient.lookup.url}" />
		<property name="recordsConnectService" ref="recordsConnectService" />
	</bean>

	<bean id="clientController" class="controllers.client.ClientController"
		scope="prototype">
		<property name="userSessionService" ref="userSessionService" />
		<property name="loggingService" ref="loggingService" />
		<property name="userRepository" ref="userRepository" />
		<property name="gson" ref="standardGson" />
		<property name="clientRepository" ref="clientRepository" />
	</bean>

	<bean id="localNotificationController" class="controllers.notification.LocalNotificationController"
		scope="prototype">
		<property name="userSessionService" ref="userSessionService" />
		<property name="appNotificationService" ref="appNotificationService" />
		<property name="localNotificationRepository" ref="localNotificationRepository" />
		<property name="gson" ref="standardGson" />
	</bean>

	<bean id="skinController" class="controllers.client.SkinController"
		scope="prototype">
		<property name="userSessionService" ref="userSessionService" />
		<property name="documentMaxSizeInKB" value="${service.document.store.maxsize}" />
		<property name="loggingService" ref="loggingService" />
		<property name="gson" ref="basicGson" />
		<property name="skinRepository" ref="skinRepository" />
		<property name="userRepository" ref="userRepository" />
		<property name="clientRepository" ref="clientRepository" />
	</bean>

	<bean id="preferencesController" class="controllers.user.PreferencesController"
		scope="prototype">
		<property name="userSessionService" ref="userSessionService" />
		<property name="loggingService" ref="loggingService" />
		<property name="gson" ref="standardGson" />
		<property name="preferencesRepository" ref="preferencesRepository" />
		<property name="portalLanguageRepository" ref="portalLanguageRepository" />
	</bean>

	<bean id="userSettingsController" class="controllers.user.UserSettingsController"
		scope="prototype">
		<property name="userSessionService" ref="userSessionService" />
		<property name="gson" ref="standardGson" />
		<property name="userSettingsRepository" ref="userSettingsRepository" />
	</bean>

	<bean id="globalNotesController" class="controllers.note.Notes"
		scope="prototype">
		<property name="userSessionService" ref="userSessionService" />
		<property name="generalNoteMaxSize" value="${notes.general.maxsize}" />
		<property name="loggingService" ref="loggingService" />
		<property name="gson" ref="basicGson" />
		<property name="userRepository" ref="userRepository" />
		<property name="noteRepository" ref="noteRepository" />
	</bean>

	<bean id="sectionNotesController" class="controllers.note.SectionNotes"
		scope="prototype">
		<property name="userSessionService" ref="userSessionService" />
		<property name="sectionNoteMaxSize" value="${notes.section.maxsize}" />
		<property name="gson" ref="basicGson" />
		<property name="userRepository" ref="userRepository" />
		<property name="sectionNoteRepository" ref="sectionNoteRepository" />
	</bean>

	<bean id="tileNotesController" class="controllers.note.TileNotes"
		scope="prototype">
		<property name="userSessionService" ref="userSessionService" />
		<property name="tileNoteMaxSize" value="${notes.tile.maxsize}" />
		<property name="gson" ref="basicGson" />
		<property name="dashboardRepository" ref="dashboardRepository" />
		<property name="tileNoteRepository" ref="tileNoteRepository" />
		<property name="tileRepository" ref="tileRepository" />
	</bean>

	<bean id="dashboardsController" class="controllers.dashboard.DashboardsController"
		scope="prototype">
		<property name="userSessionService" ref="userSessionService" />
		<property name="loggingService" ref="loggingService" />
		<property name="dashboardRepository" ref="dashboardRepository" />
		<property name="gson" ref="basicGson" />
	</bean>

	<bean id="tilesController" class="controllers.dashboard.tile.TilesController"
		scope="prototype">
		<property name="userSessionService" ref="userSessionService" />
		<property name="gson" ref="basicGson" />
		<property name="tileRepository" ref="tileRepository" />
		<property name="dashboardRepository" ref="dashboardRepository" />
		<property name="userRepository" ref="userRepository" />
		<property name="recordService" ref="recordService" />
		<property name="recordSerializerGson" ref="recordSerializerGson" />
    </bean>
    
    <bean id="userEnteredDataController" class="controllers.user.UserEnteredDataController" scope="prototype">
    	<property name="userSessionService" ref="userSessionService" />
    	<property name="recordService" ref="recordService" />
    	<property name="loggingService" ref="loggingService" />
        <property name="userRepository" ref="userRepository" />
        <property name="gson" ref="recordSerializerGson" />
    </bean>
    
    <bean id="recordsController" class="controllers.Records" scope="prototype">
    	<property name="recordService" ref="recordService" />
    	<property name="userSessionService" ref="userSessionService" />
    	<property name="userEnteredDataController" ref="userEnteredDataController" />
        <property name="userRepository" ref="userRepository" />
        <property name="gson" ref="recordSerializerGson" />
		<property name="recordsConnectService" ref="recordsConnectService" />
    </bean>
    
    <bean id="downloadsController" class="controllers.Downloads" scope="prototype">
    	<property name="userSessionService" ref="userSessionService" />
    	<property name="pdfRecordDownloadService" ref="pdfRecordDownloadService" />
    	<property name="ccdRecordDownloadService" ref="ccdRecordDownloadService" />
    	<property name="loggingService" ref="loggingService" />
		<property name="cacheService" ref="cacheService" />
		<property name="nyecDocTempKey" value="${core.provider.nyec.key.temp}" />
		<property name="separatorSign" value="${service.user.session.property.accessTokenSeparatorSign}" />
    </bean>
    
    <bean id="userController" class="controllers.user.UserController" scope="prototype">
    	<property name="emailNotificationService" ref="emailNotificationService" />
    	<property name="userSessionService" ref="userSessionService" />
    	<property name="userManagementService" ref="userManagementService" />
    	<property name="cacheService" ref="cacheService" />
    	<property name="googleAnalytics" value="${google.analytics}" />
    	<property name="forgotPasswordTimeout" value="${forgotPassword.timeout}" />
    	<property name="resetPasswordTimeout" value="${resetPassword.timeout}" />
    	<property name="loggingService" ref="loggingService" />
    	<property name="gson" ref="standardGson" />
        <property name="skinRepository" ref="skinRepository" />
        <property name="userRepository" ref="userRepository" />
        <property name="userSecurityQuestionRepository" ref="userSecurityQuestionRepository" />
        <property name="executionContext" ref="notificationsContext" />
		<property name="userContractService" ref="userContractService" />
		<property name="flowsRepository" ref="flowsRepository" />
		<property name="flowUpdaterService" ref="flowUpdaterService" />
		<property name="userAPIRepository" ref="userAPIRepository" />
    </bean>
    
    <bean id="loginController" class="controllers.LogInController" scope="prototype">
    	<property name="userSessionService" ref="userSessionService" />
    	<property name="cacheService" ref="cacheService" />
    	<property name="cookiePolicyService" ref="cookiePolicyService" />
    	<property name="loggingService" ref="loggingService" />
    	<property name="gson" ref="standardGson" />
        <property name="userRepository" ref="userRepository" />
        <property name="clientRepository" ref="clientRepository" />
		<property name="concurrentSessions" value="${login.concurrentSession}" />
    </bean>
    
    <bean id="registerController" class="controllers.RegisterController" scope="prototype">
        <property name="userRepository" ref="userRepository" />
    </bean>

    <bean id="securityQuestionsController" class="controllers.user.UserSecurityQuestionController" scope="prototype">
    	<property name="userSessionService" ref="userSessionService" />
    	<property name="userSecurityQuestionService" ref="userSecurityQuestionService" />
    	<property name="loggingService" ref="loggingService" />
        <property name="userRepository" ref="userRepository" />
        <property name="userSecurityQuestionRepository" ref="userSecurityQuestionRepository" />
        <property name="securityQuestionRepository" ref="securityQuestionRepository" />
    </bean>
    
    <bean id="globalSecurityQuestionController" class="controllers.global.SecurityQuestionController" scope="prototype">
    	<property name="gson" ref="standardGson" />
        <property name="securityQuestionRepository" ref="securityQuestionRepository" />
    </bean>
    
    <bean id="featureController" class="controllers.admin.FeatureController" scope="prototype">
    	<property name="userSessionService" ref="userSessionService" />
    	<property name="loggingService" ref="loggingService" />
        <property name="userRepository" ref="userRepository" />
        <property name="clientRepository" ref="clientRepository" />
        <property name="gson" ref="basicGson" />
    </bean>
    
    <bean id="consentsController" class="controllers.consent.Consents" scope="prototype">
    	<property name="userSessionService" ref="userSessionService" />
        <property name="userRepository" ref="userRepository" />
        <property name="gson" ref="basicGson" />
    </bean>
    
    <bean id="usersStatsController" class="controllers.reports.users.UsersStatisticsController" scope="prototype">
    	<property name="userSessionService" ref="userSessionService" />
        <property name="clientRepository" ref="clientRepository" />
    	<property name="gson" ref="standardGson" />
    </bean>
    
	<bean id="loggingController" class="controllers.global.AuditLoggingController" scope="prototype">
    	<property name="loggingService" ref="loggingService" />
    </bean>

    <bean id="portalLanguageController" class="controllers.global.PortalLanguageController" scope="prototype">
        <property name="portalLanguageRepository" ref="portalLanguageRepository" />
    </bean>
    
    <bean id="MU2downloadController" class="controllers.reports.mu2.MU2DownloadController" scope="prototype">
    	<property name="vdtService" ref="vdtService" />
    	<property name="loggingService" ref="loggingService" />
        <property name="organizationRepository" ref="organizationRepository" />
		<property name="downloadReportCacheService" ref="downloadReportCacheService" />
		<property name="userSessionService" ref="userSessionService" />
    </bean>
    
    <bean id="MU2calculationController" class="controllers.reports.mu2.MU2CalculationController" scope="prototype">
    	<property name="vdtService" ref="vdtService" />
    	<property name="loggingService" ref="loggingService" />
    	<property name="gson" ref="standardGson" />
		<property name="pagingService" ref="pagingService" />
    </bean>

	<bean id="verizonSamlController" class="controllers.saml.VerizonSamlController" scope="prototype">
		<property name="cookiePolicyService" ref="cookiePolicyService" />
		<property name="registerRedirectUrl" value="${verizon.register.redirect}" />
		<property name="samlCreationService" ref="samlCreationService" />
		<property name="samlStorageHelper" ref="redisStorageHelper" />
		<property name="flowManager" ref="flowManager" />
	</bean>

	<bean id="userListController" class="controllers.admin.UserListController" scope="prototype">
		<property name="userSessionService" ref="userSessionService" />
		<property name="gson" ref="userReportGson" />
		<property name="userReporterService" ref="userReporterService" />
		<property name="pagingService" ref="pagingService" />
	</bean>

	<bean id="challengeQuestionsController" class="controllers.challengeQuestions.ChallengeQuestionsController" scope="prototype">
		<property name="challengeQuestionsService" ref="challengeQuestionsService" />
		<property name="userSessionService" ref="userSessionService"/>
		<property name="standardGson" ref="standardGson" />
		<property name="flowsRepository" ref="flowsRepository" />
	</bean>
</beans>
